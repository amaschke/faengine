//
//  ColorGradientTest.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/25/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//    The views and conclusions contained in the software and documentation are those
//    of the authors and should not be interpreted as representing official policies,
//    either expressed or implied, of the FreeBSD Project.

#include "ColorGradientTest.hpp"
#include "ColorGradient.hpp"

bool ColorGradientTest::test1()
{
    float data[] { 0.1f, 0.2f, 0.3f };
    
    printf("Index for 0.f: %i\n", ColorGradient::binary_range_search(data, 0.f, 0, 2));
    printf("Index for 0.15f: %i\n", ColorGradient::binary_range_search(data, 0.15f, 0, 2));
    printf("Index for 0.2f: %i\n", ColorGradient::binary_range_search(data, 0.2f, 0, 2));
    printf("Index for 0.25f: %i\n", ColorGradient::binary_range_search(data, 0.25f, 0, 2));
    printf("Index for 0.3f: %i\n", ColorGradient::binary_range_search(data, 0.3f, 0, 2));
    printf("Index for 0.4f: %i\n", ColorGradient::binary_range_search(data, 0.4f, 0, 2));
    printf("Index for 1.f: %i\n", ColorGradient::binary_range_search(data, 1.f, 0, 2));    
    printf("\n");
    
    return true;
}
