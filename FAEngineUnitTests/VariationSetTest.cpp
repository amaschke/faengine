//
//  VariationSetTest.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/9/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//    The views and conclusions contained in the software and documentation are those
//    of the authors and should not be interpreted as representing official policies,
//    either expressed or implied, of the FreeBSD Project.

#include <Magick++.h>
#include "VariationSetTest.hpp"
#include "VariationSet.hpp"
#include "Utilities.hpp"
#include "pugixml.hpp"
#include "FlameParse.hpp"
#include "FlameExpanded.hpp"
#include "DeviceProgram.hpp"

#ifdef _WIN32
#include "WindowsInterface.hpp"
#else
#include "CocoaInterface.h"
#endif

#include <fstream>
#include <iostream>
#include <string>

using namespace pugi;

std::string flamePathFromInputPath(const std::string &path)
{
    size_t len = path.length();
    if (len > 3) {
        if (path.substr(len - 3) == ".fa") {
            return path + "/fractal.flame";
        }
    }
    return path;
}

FlameExpandedVector readFromData(const std::string &string)
{
    xml_document doc;
    xml_parse_result result = doc.load_string(string.c_str(), parse_cdata);
    
    if (result.status != status_ok) {
        throw std::runtime_error(result.description());
    }
    xml_node root = doc.document_element();
    
    StringsSet set = FlameParse::preParseFlameEleForVariationsUsed(root);
    
    bool changed = false;
    VariationSetsVector variationSetsInDoc = VariationSet::preParseDocForVariationsUsed(doc, "foo", true, changed);

// DEBUG
//    std::cout << "All VarSets\n" << VariationSet::allVarsetsDescription(VariationSet::variationSets);
//    std::cout << "VarSets in Doc\n" << VariationSet::allVarsetsDescription(variationSetsInDoc);
    
    // older docs dont have a variation set - use Flam3 legacy
    SharedVariationSet firstVariationSet = variationSetsInDoc.size() > 0 ? variationSetsInDoc.front() : VariationSet::legacyVariationSet();
    
//    std::cout << firstVariationSet->description();
    
    FlameExpandedVector flames;
    std::string         palettePath;
    
    FlameParse::parseDoc(flames, doc, palettePath, firstVariationSet);
    FlameParse::uniqueFrameNames(flames);
    
	// DEBUG
	//for (size_t i = 0; i < flames.size(); i++) {
 //       std::cout << "Fractal #" << i << " =========================\n";
 //       SharedFlameExpanded fe = flames[i];
 //       std::cout << fe->textReport() << "\n\n";
 //   }
    return flames;
}

// when initializing from a varset file
FlameExpandedVector VariationSetTest::loadFromPath(const std::string & _path)
{
    std::string path = flamePathFromInputPath(_path);
    try {
        std::ifstream in(path.c_str());
        if (in.fail()) {
            std::string msg = string_format("Failed to open file: %s", path.c_str());
            throw std::runtime_error(msg);
        }
        std::string contents((std::istreambuf_iterator<char>(in)),
                             std::istreambuf_iterator<char>());
        
        return readFromData(contents);
    } catch (std::exception & e) {
        systemLog(e.what());
        return std::move(FlameExpandedVector());
    }
}

void VariationSetTest::setup()
{
    Magick::InitializeMagick(NULL);

    VariationSet::setupFAEngineApplicationSupport();
    DeviceProgram::setupFAEngineCaches();
    
    if (! VariationSet::cachedLibraryExists())
        VariationSet::cacheFactoryLibrary();
    
    if (! VariationSet::cachedMyLibraryExists())
        VariationSet::cacheMyLibrary();
    
    SharedVariationSet vs = VariationSet::loadVariationsLibrary();
    VariationSet::loadMyVariationsLibrary();
    VariationSet::updateMergedLibrary();
    
    VariationSet::makeLegacyVariationSet();
    VariationSet::makeStandard3DVariationSet();
    
    VariationSet::setupDefaultVariationSet();
}

bool VariationSetTest::test1()
{
    std::string path = pathForTestResource("TestData.bundle", "JuliaJulia.fa");
    
    try
    {
        loadFromPath(path);
    }
    
    catch(std::exception & e)
    {
        std::cerr << e.what() << std::endl;
        return false;
    }
    return true;
}

bool VariationSetTest::test2()
{
    std::string path = pathForTestResource("TestData.bundle", "electricsheep.243.16751.flam3");
    
    try
    {
        loadFromPath(path);
    }
    
    catch(std::exception & e)
    {
        std::cerr << e.what() << std::endl;
        return false;
    }
    return true;
}

