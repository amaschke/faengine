//
//  MyTest.h
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/9/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//    The views and conclusions contained in the software and documentation are those
//    of the authors and should not be interpreted as representing official policies,
//    either expressed or implied, of the FreeBSD Project.

#include <cxxtest/TestSuite.h>
#include "VariationSetTest.hpp"
#include "ColorGradientTest.hpp"
#include "RenderTest.hpp"
#include <Magick++.h>

const bool useCPU             = false;
const bool useGPU             = true;
const bool forceRebuild       = true;
const bool noForceRebuild     = false;
const bool ignoreQuarantine   = true;
const bool noIgnoreQuarantine = false;

class Fixture1 : public CxxTest::GlobalFixture
{
public:
    
    Fixture1() {
        std::set<uint> deviceNums;
        Magick::InitializeMagick(NULL);
        RenderTest::variationSetSetup();        

//        RenderTest::makeOpenClSingleton(useCPU, 0, noForceRebuild, ignoreQuarantine);  // test render on CPU

//        deviceNums.insert(0);    // first device
        deviceNums.insert(1);    // second device
        deviceNums.insert(2);    // third device
        RenderTest::makeOpenClSingleton(useGPU, deviceNums, noForceRebuild, ignoreQuarantine);  // GPU - second GPU
    }
};
static Fixture1 fixture1;


class MyTestSuite : public CxxTest::TestSuite
{
public:
    void testRenderFlam3( void )
    {
        RenderTest::test1(); 
//        RenderTest::test2();
//        RenderTest::test3();
//        RenderTest::test4();
//        RenderTest::test5();
//        RenderTest::test6();
//        RenderTest::test7();
//        RenderTest::test8();
//        RenderTest::test9(); // 3 frames
//        RenderTest::test10(); // 17 frames

//		RenderTest::test11();
//		RenderTest::test12();

        RenderTest::enqueueOpenClStop();
        TS_ASSERT(true);
    //}
    //void testColorGradient( void )
    //{
    //    ColorGradientTest::test1();
    //    TS_ASSERT(true);
    }
    
    void testParseFA( void )
    {
        VariationSetTest::test1();
        TS_ASSERT(true);
    }
    
    void testParseFlam3( void )
    {
        VariationSetTest::test2();
        TS_ASSERT(true);
    }
};
