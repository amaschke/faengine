/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <fstream>
#define _CXXTEST_HAVE_STD
#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XUnitPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    std::ofstream ofstr("TestResults.xml");
    CxxTest::XUnitPrinter tmp(ofstr);
    CxxTest::RealWorldDescription::_worldName = "cxxtest";
    status = CxxTest::Main<CxxTest::XUnitPrinter>( tmp, argc, argv );
    return status;
}
bool suite_MyTestSuite_init = false;
#include "MyTest.h"

static MyTestSuite suite_MyTestSuite;

static CxxTest::List Tests_MyTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_MyTestSuite( "C:/Users/sbrodhead/Source/Repos/faengine/FAEngineUnitTests/MyTest.h", 65, "MyTestSuite", suite_MyTestSuite, Tests_MyTestSuite );

static class TestDescription_suite_MyTestSuite_testRenderFlam3 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_MyTestSuite_testRenderFlam3() : CxxTest::RealTestDescription( Tests_MyTestSuite, suiteDescription_MyTestSuite, 68, "testRenderFlam3" ) {}
 void runTest() { suite_MyTestSuite.testRenderFlam3(); }
} testDescription_suite_MyTestSuite_testRenderFlam3;

static class TestDescription_suite_MyTestSuite_testParseFA : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_MyTestSuite_testParseFA() : CxxTest::RealTestDescription( Tests_MyTestSuite, suiteDescription_MyTestSuite, 93, "testParseFA" ) {}
 void runTest() { suite_MyTestSuite.testParseFA(); }
} testDescription_suite_MyTestSuite_testParseFA;

static class TestDescription_suite_MyTestSuite_testParseFlam3 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_MyTestSuite_testParseFlam3() : CxxTest::RealTestDescription( Tests_MyTestSuite, suiteDescription_MyTestSuite, 99, "testParseFlam3" ) {}
 void runTest() { suite_MyTestSuite.testParseFlam3(); }
} testDescription_suite_MyTestSuite_testParseFlam3;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";
