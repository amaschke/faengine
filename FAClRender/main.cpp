//
//  main.cpp
//  FAClRender
//
//  Created by Steven Brodhead on 5/25/16.
//    Copyright (c) 2016, Centcom Inc.
//    All rights reserved.
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//    The views and conclusions contained in the software and documentation are those
//    of the authors and should not be interpreted as representing official policies,
//    either expressed or implied, of the FreeBSD Project.

#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <fstream>
#include <chrono>
#include <Magick++.h>

#include <Exports.hpp>

#ifdef _WIN32
#include "WindowsInterface.hpp"
#else
#include <unistd.h>
#include "CocoaInterface.h"
#include <string.h>
#endif

#include "VariationSet.hpp"
#include "ClContext.hpp"
#include "ClDeviceKind.hpp"
#ifndef NO_CUDA
#include "CudaContext.hpp"
#include "CudaDeviceKind.hpp"
#endif
#include "DeviceUsage.hpp"
#include "FlameParse.hpp"
#include "FlameExpanded.hpp"
#include "RenderTile.hpp"
#include "RenderGeometry.hpp"
#include "Image.hpp"
#include "RenderListener.hpp"

#include "RenderHarness.hpp"


using std::string;
using namespace pugi;
using namespace std::chrono;
using uint = unsigned int;

static char buf[1024];
float reductionFactor = 0.8f; // amount memory is reduced each pass for GPU memory consumption test
volatile int progress = 0; // for progress updates


void usage()
{
    const char *usage =
    " usage: FAClRender -list                    Show available Device list for GPUs\n"
    "OR\n"
#ifndef NO_CUDA
    " usage: FAClRender -list -cuda              Show available CUDA Device list for GPUs\n"
    "OR\n"
#endif
    " usage: FAClRender -list -deviceType cpu    Show available Device list for CPU\n"
    "OR\n"
    " usage: FAClRender -list -deviceType gpu    Show available Device list for GPUs\n"
    "OR\n"
    "        FAClRender width height inputURL\n"
    "                       [-list]                Show available Device list - default NO\n"
    "                       [-deviceType cpu|gpu]  Device type                - default gpu\n"
    "                       [-deviceNum num]       Device number (from available device list) - default 0\n"
    "                       [-o outURL]            Output URL        - default is PNG file with same basename as inputURL\n"
    "                       [-q quality]           Render Quality    - default 50\n"
    "\n"
    "                       [-bpp bitsPerPixel]    BitsPerPixel      - default 8\n"
    "                       [-alpha]               Add Alpha Channel - default NO\n"
    "                       [-flip]                Flip image        - default NO\n"
    "                       [-epsilon epsilon]     Epsilon           - default 1.e-7f\n"
    "                       [-nde]                 No Density estimation phase    - default NO\n"
    "                       [-rebuild]             Force rebuild OpenCL kernels   - default NO\n"
    "                       [-space name]          Colorspace name                - default AdobeRGB\n"
    "                       [-silent]              Silent mode                    - default NO\n"
#ifndef NO_CUDA
    "                       [-cuda]                Cuda mode                      - default NO\n"
#endif
    ;
    printf("%s\n", usage);
}

class RenderTest : public RenderHarness {
    
    RenderTest(std::set<uint> _deviceNums)
    : RenderHarness(_deviceNums) {}
    
public:
	void setImage(SharedImage image, std::string & url, bool useAlpha, Size _size, duration<double> _duration) override;

	static RenderHarness *  makeOpenClSingleton(bool targetGPU, std::set<uint> _deviceNums, bool forceRebuild, bool ignoreQuarantine);
#ifndef NO_CUDA
        static RenderHarness *  makeCudaSingleton(std::set<uint> _deviceNums, bool forceRebuild, bool ignoreQuarantine);
#endif
};

std::string flamePathFromInputPath(const std::string &path)
{
    size_t len = path.length();
    if (len > 3) {
        if (path.substr(len - 3) == ".fa") {
            return path + "/fractal.flame";
        }
    }
    return ::expandTildeInPath(path.c_str());
}

string getOutURL(string & inputPath, uint frame)
{
    size_t pos = inputPath.rfind(".");
    if (pos != string::npos)
        return inputPath.substr(0, pos) + "-" + itoa(frame) + ".png";
    else
        return inputPath + "-" + itoa(frame) + ".png";
}

Magick::StorageType storageTypeFor(uint bitsPerPixel)
{
    Magick::StorageType storageType;
    if (bitsPerPixel == 8)
        storageType = Magick::CharPixel;
    else if (bitsPerPixel == 16)
        storageType = Magick::ShortPixel;
    else
        storageType = Magick::FloatPixel;
    return storageType;
}

uint bitsPerPixelFor(Magick::StorageType storageType)
{
    switch (storageType) {
        default:
        case Magick::CharPixel:
            return  8;
        case Magick::ShortPixel:
            return  16;
        case Magick::FloatPixel:
            return  32;
    }
}

// narrowing donw to the actual device to use is an ugly process
RenderHarness * narrowDeviceSelection(FlameExpandedVector & flames,
                                      cl_device_type devType,
                                      string & url,
                                      float width,
                                      float height,
                                      bool forceRebuild,
                                      uint & deviceNum,
                                      bool listDevices,
                                      bool deviceSpecified,
                                      int &status)
{
    // dont run on ATI 4XXX devices unless explictly asked to
    std::vector<string> vendors;
    std::vector<bool>   locals;
    std::set<uint> deviceNums;
    deviceNums.insert(deviceNum);
    
    // check if system has usable GPU devices and if GPU rendering was selected
    bool targetGPU = ClContext::hasGPU(vendors, true, locals, true) &&  devType == CL_DEVICE_TYPE_GPU;
    if (! targetGPU)
        deviceNum = 0;
    
    RenderHarness *harness = RenderTest::makeOpenClSingleton(targetGPU, deviceNums, false, true);  // GPU
    if (! harness->deviceContext) {
        systemLog("Initializing OpenCL failed");
        status = -5;
        return harness;
    }
    
    // check for enough GPU memory for the first flame
    SharedFlameExpanded & fe = flames[0];
    Flame *flame             = fe->getFlame();
    uint xformCount          = (uint)fe->countOfXforms();
    uint supersample         = flame->params.oversample;
    
    // for GPUs: 35 Mb minimum => 990,322 pixels squared or about 1000x990   == CPU uses virtual memory, so this check is not needed
    // Note: formula for amount of memory is: 1 Mb  +  (36 * area / 1048576) Mb
    if (targetGPU) {
        if (! harness->deviceContext->oclDeviceCheckForEnoughMemoryArea(ceilf(width*height), xformCount, supersample)) { // if not enough memory, swithc to CPU
            deviceNum = 0;
            targetGPU = false;
            harness->openClContextSetup(targetGPU, deviceNums, forceRebuild, true);
        }
    }
    
    // print the OpenCL render devices available
    if (listDevices) {
        for (SharedDeviceUsage & deviceUsage : harness->deviceContext->deviceUsages) {
            printf("%s\n", deviceUsage->name.c_str());
        }
    }
    
    // Print if the selected render device is quarantined
    SharedDeviceUsage &deviceUsage = harness->deviceContext->selectedDevices[harness->deviceContext->selectedDeviceNumForDeviceNum(deviceNum)];
    bool quarantined               = ClContext::checkQuarantineForDeviceID((cl_device_id)deviceUsage->deviceID);
    
    printf("Render Device:%s Quarantined:%s\n",
           deviceUsage->name.c_str(), quarantined ? "YES" : "NO");
    return harness;
}

#ifndef NO_CUDA
// narrowing donw to the actual device to use is an ugly process
RenderHarness * narrowCudaDeviceSelection(FlameExpandedVector & flames,
                                          string & url,
                                          float width,
                                          float height,
                                          bool forceRebuild,
                                          uint & deviceNum,
                                          bool listDevices,
                                          bool deviceSpecified,
                                          int &status)
{
    // dont run on ATI 4XXX devices unless explictly asked to
    std::vector<string> vendors;
    std::vector<bool>   locals;
    std::set<uint> deviceNums;
    deviceNums.insert(deviceNum);
    
    RenderHarness *harness = RenderTest::makeCudaSingleton(deviceNums, false, true);  // GPU
    if (! harness->deviceContext) {
        systemLog("Initializing CUDA failed");
        status = -5;
        return harness;
    }
    
    // check for enough GPU memory for the first flame
    SharedFlameExpanded & fe = flames[0];
    Flame *flame             = fe->getFlame();
    uint xformCount          = (uint)fe->countOfXforms();
    uint supersample         = flame->params.oversample;
    
    // for GPUs: 35 Mb minimum => 990,322 pixels squared or about 1000x990   == CPU uses virtual memory, so this check is not needed
    // Note: formula for amount of memory is: 1 Mb  +  (36 * area / 1048576) Mb
    if (! harness->deviceContext->oclDeviceCheckForEnoughMemoryArea(ceilf(width*height), xformCount, supersample)) { // if not enough memory, swithc to CPU
        deviceNum = 0;
        harness->cudaContextSetup(deviceNums, forceRebuild, true);
    }
    
    // print the CUDA render devices available
    if (listDevices) {
        for (SharedDeviceUsage & deviceUsage : harness->deviceContext->deviceUsages) {
            printf("%s\n", deviceUsage->name.c_str());
        }
    }
    
    // Print if the selected render device is quarantined
    SharedDeviceUsage &deviceUsage = harness->deviceContext->selectedDevices[harness->deviceContext->selectedDeviceNumForDeviceNum(deviceNum)];
    bool quarantined               = ClContext::checkQuarantineForDeviceID((cl_device_id)deviceUsage->deviceID);
    
    printf("Render Device:%s Quarantined:%s\n",
           deviceUsage->name.c_str(), quarantined ? "YES" : "NO");
    return harness;
}
#endif

void RenderTest::setImage(SharedImage _image, std::string & url, bool useAlpha, Size _size, duration<double> _duration)
{
    if (! _image) {
        static char buf[100];
        snprintf(buf, sizeof(buf), "Rendering flame recipe failed for: %s", url.c_str());
        systemLog(buf);
        return;
    }
    SharedImage image;
    if (! _image->transparent) {
        image = _image->RGBAtoRGB();
    }
    else {
        image = _image;
    }
    
    Magick::Image mimage(image->width, image->height, image->map, storageTypeFor(image->bitDepth), image->bitmap.get());
    if (image->flipped)
        mimage.flip();

    Magick::Blob blob(_image->profile->data(), _image->profile->size());
    mimage.iccColorProfile(blob);
	if (::fileExists(url))
#ifdef _WIN32
        _unlink(url.c_str());
#else
    unlink(url.c_str());
#endif
	mimage.write(url);
}

RenderHarness * RenderTest::makeOpenClSingleton(bool targetGPU, std::set<uint> _deviceNums, bool forceRebuild, bool ignoreQuarantine)
{
    if (! openClSingleton) {
        openClSingleton = new RenderTest(_deviceNums);
        openClSingleton->openClContextSetup(targetGPU, _deviceNums, forceRebuild, ignoreQuarantine);
    }
    return openClSingleton;
}

#ifndef NO_CUDA
RenderHarness * RenderTest::makeCudaSingleton(std::set<uint> _deviceNums, bool forceRebuild, bool ignoreQuarantine)
{
    if (! cudaSingleton) {
        cudaSingleton = new RenderTest(_deviceNums);
        cudaSingleton->cudaContextSetup(_deviceNums, forceRebuild, ignoreQuarantine);
    }
    return cudaSingleton;
}
#endif

int main(int argc, char *argv[])
{
    int quality      = 100;
    int bitsPerPixel = 8;
    uint deviceNum   = 0;
    float width      = 800.f;
    float height     = 600.f;
    bool hasAlpha    = false;
    bool flipped     = false;
    bool forceRebuild = false;
    bool densityEstimation = true;
    bool silent       = false;
    bool listDevices  = false;
    bool deviceSpecified = false;
    float epsilon     = 1.e-7f;
    bool  anyDevice   = true; // no what the user intent is - we dont run on ATI 4XXX devices unless asked to explicitly
    bool useCUDA      = false;
    
    cl_device_type devType = CL_DEVICE_TYPE_GPU;
    
    string colorspaceName = "AdobeRGB";
    
    string url;
    string outURL;
    if (argc == 1) {
        usage();
        return 0;
    }
    if ((strcmp(argv[1], "-h") == 0) || (strcmp(argv[1], "--help") == 0)) {
        usage();
        return 0;
    }
    if (strcmp(argv[1], "-list") == 0) {
        if (argc == 2)
            devType = CL_DEVICE_TYPE_GPU;
        else if (strcmp(argv[2], "-deviceType") == 0) {
            anyDevice = false;
            char *dt = argv[3];
            if ((strcmp(dt, "cpu") == 0) || (strcmp(dt, "CPU") == 0))
                devType = CL_DEVICE_TYPE_CPU;
            else if ((strcmp(dt, "gpu") == 0) || (strcmp(dt, "GPU") == 0))
                devType = CL_DEVICE_TYPE_GPU;
        }
#ifndef NO_CUDA
        else if (strcmp(argv[2], "-cuda") == 0) {
            CudaContext::printDeviceList();
            return 0;            
        }
#endif
        ClContext::printDeviceList(devType);
        return 0;
    }
    
    if (argc < 4) {
        usage();
        return -1;
    }
    if (argv[3][0] == '-') {
        usage();
        return -1;
    }

    url = string(argv[3]);
    
    sscanf(argv[1], "%f", &width);
    sscanf(argv[2], "%f", &height);
    width  = fabsf(width);
    height = fabs(height);
    
    for (int i = 4; i < argc; i++) {
        if (strcmp(argv[i], "-o") == 0) {
            outURL = string(argv[++i]);
        }
        if (strcmp(argv[i], "-q") == 0) {
            sscanf(argv[++i], "%u", &quality);
        }
        if (strcmp(argv[i], "-epsilon") == 0) {
            sscanf(argv[++i], "%e", &epsilon);
        }
        if (strcmp(argv[i], "-bpp") == 0) {
            sscanf(argv[++i], "%u", &bitsPerPixel);
        }
        if (strcmp(argv[i], "-alpha") == 0) {
            hasAlpha = true;
        }
        if (strcmp(argv[i], "-flip") == 0) {
            flipped = true;
        }
        if (strcmp(argv[i], "-nde") == 0) {
            densityEstimation = false;
        }
        if (strcmp(argv[i], "-rebuild") == 0) {
            forceRebuild = true;
        }
        if (strcmp(argv[i], "-list") == 0) {
            listDevices = true;
        }
        if (strcmp(argv[i], "-cuda") == 0) {
            useCUDA = true;
        }
        if (strcmp(argv[i], "-device") == 0) {
            sscanf(argv[++i], "%u", &deviceNum);
            deviceSpecified = true;
        }
        if (strcmp(argv[i], "-space") == 0) {
            colorspaceName = string(argv[++i]);
        }
        if (strcmp(argv[i], "-deviceType") == 0) {
            anyDevice = false;
            char *dt = argv[++i];
            if ((strcmp(dt, "cpu") == 0) || (strcmp(dt, "CPU") == 0))
                devType = CL_DEVICE_TYPE_CPU;
            else if ((strcmp(dt, "gpu") == 0) || (strcmp(dt, "GPU") == 0))
                devType = CL_DEVICE_TYPE_GPU;
        }
        if (strcmp(argv[i], "-silent") == 0) {
            silent = true;
        }
    }
    if (outURL.length() == 0) {
        size_t pos = url.rfind(".");
        if (pos != string::npos)
            outURL = url.substr(0, pos) + ".png";
        else
            outURL = url + ".png";

        outURL = std::string(::expandTildeInPath(outURL.c_str()));
    }
    Magick::InitializeMagick(NULL);
    RenderHarness::variationSetSetup();
    
    // the flame file might be inside a subdirectory (true for .fa files)
    std::string path           = flamePathFromInputPath(url);
    FlameExpandedVector flames = FlameParse::loadFromPath(path);
    if (flames.size() == 0) {
        snprintf(buf, sizeof(buf), "Reading/parsing flame recipe failed for: %s", url.c_str());
        systemLog(buf);
        return -2;
    }
    int status = 0;
    RenderHarness * harness = nullptr;
    if (! useCUDA)
        harness = narrowDeviceSelection(flames,
                                        devType,
                                        url,
                                        width,
                                        height,
                                        forceRebuild,
                                        deviceNum,
                                        listDevices,
                                        deviceSpecified,
                                        status);
#ifndef NO_CUDA
    else
        harness = narrowCudaDeviceSelection(flames,
                                            url,
                                            width,
                                            height,
                                            forceRebuild,
                                            deviceNum,
                                            listDevices,
                                            deviceSpecified,
                                            status);
#endif    
    if (status != 0)
        return status;
    
    // Now lets render
    if (harness->deviceCount() > 0) {
        
#ifdef NO_QUEUE
        for (uint i = 0; i < flames.size(); i++) {
            SharedFlameExpanded fe = flames[i];
            string _outURL = flames.size() > 1 ? getOutURL(outURL, i + 1) : outURL;
            printf("Rendering %s frame #%u to:   %s\n", fileNameFromPath(path).c_str(), i+1, _outURL.c_str());
            
            UniqueImage image = harness->flam4Render(harness->clContext,
                                          fe,
                                          deviceNum,
                                          url,
                                          _outURL,
                                          width,
                                          height,
                                          quality,
                                          bitsPerPixel,
                                          hasAlpha,
                                          colorspaceName.c_str(),
                                          flipped,
                                          epsilon,
                                          !silent);
        }
#else
        for (uint i = 0; i < flames.size(); i++) {
            SharedFlameExpanded fe = flames[i];
            string _outURL = flames.size() > 1 ? getOutURL(outURL, i + 1) : outURL;
            printf("Rendering %s frame #%u to:   %s\n", fileNameFromPath(path).c_str(), i+1, _outURL.c_str());
            
            status = harness->flam4QueuedRender(harness->deviceContext,
                                                fe,
                                                deviceNum,
                                                url,
                                                _outURL,
                                                width,
                                                height,
                                                quality,
                                                bitsPerPixel,
                                                hasAlpha,
                                                colorspaceName.c_str(),
                                                flipped,
                                                epsilon,
                                                !silent,
                                                i,
                                                (uint)(flames.size() - 1));
        }
        SharedClContext clContext = std::dynamic_pointer_cast<ClContext>(harness->deviceContext);
        
        if (clContext) {
            harness->enqueueOpenClStop(); // main thread will wait on queued renders completion here
        }
#ifndef NO_CUDA

        else {
            harness->enqueueCudaStop(); // main thread will wait on queued renders completion here
        }
#endif        
#endif
        
    }
    return status;
}
