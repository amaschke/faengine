//
//  main.cpp
//  FAClRender
//
//  Created by Steven Brodhead on 5/25/16.
//    Copyright (c) 2016, Centcom Inc.
//    All rights reserved.
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//    The views and conclusions contained in the software and documentation are those
//    of the authors and should not be interpreted as representing official policies,
//    either expressed or implied, of the FreeBSD Project.

#include <iostream>
#include <string>
#include <vector>
#include <memory>

#include <Magick++.h>
#include <fstream>

#include "VariationSet.hpp"
#include "ClContext.hpp"
#include "ClDeviceKind.hpp"
#include "DeviceUsage.hpp"
#include "FlameParse.hpp"
#include "FlameExpanded.hpp"
#include "RenderTile.hpp"
#include "RenderGeometry.hpp"
#include "CocoaInterface.h" // non-portable functions

#include "RenderHarness.hpp"

using std::string;
using namespace pugi;
using uint = unsigned int;

static char buf[1024];
float reductionFactor = 0.8f; // amount memory is reduced each pass for GPU memory consumption test
volatile int progress = 0; // for progress updates


void usage()
{
    const char *usage =
    " usage: FA4OpenClCmd -list                    Show available Device list for GPUs\n"
    "OR\n"
    " usage: FA4OpenClCmd -list -deviceType cpu    Show available Device list for CPU\n"
    "OR\n"
    " usage: FA4OpenClCmd -list -deviceType gpu    Show available Device list for GPUs\n"
    "OR\n"
    "        FA4OpenClCmd width height inputURL\n"
    "                       [-list]                Show available Device list - default NO\n"
    "                       [-deviceType cpu|gpu]  Device type                - default gpu\n"
    "                       [-deviceNum num]       Device number (from available device list) - default 0\n"
    "                       [-o outURL]            Output URL        - default is PNG file with same basename as inputURL\n"
    "                       [-q quality]           Render Quality    - default 50\n"
    "\n"
    "                       [-bpp bitsPerPixel]    BitsPerPixel      - default 8\n"
    "                       [-alpha]               Add Alpha Channel - default NO\n"
    "                       [-flip]                Flip image        - default NO\n"
    "                       [-epsilon epsilon]     Epsilon           - default 1.e-7f\n"
    "                       [-nde]                 No Density estimation phase    - default NO\n"
    "                       [-rebuild]             Force rebuild OpenCL kernels   - default NO\n"
    "                       [-space name]          Colorspace name                - default AdobeRGB\n"
    "                       [-silent]              Silent mode                    - default NO\n";
    printf("%s\n", usage);
}

std::string flamePathFromInputPath(const std::string &path)
{
    size_t len = path.length();
    if (len > 3) {
        if (path.substr(len - 3) == ".fa") {
            return path + "/fractal.flame";
        }
    }
    return ::expandTildeInPath(path.c_str());
}

string getOutURL(string & inputPath, uint frame)
{
    size_t pos = inputPath.rfind(".");
    if (pos != string::npos)
        return inputPath.substr(0, pos) + "-" + itoa(frame) + ".png";
    else
        return inputPath + "-" + itoa(frame) + ".png";
}

// narrowing the actual device to use is an ugly process
RenderHarness * narrowDeviceSelection(FlameExpandedVector & flames,
                                      unsigned &deviceCount,
                                      cl_device_type devType,
                                      string & url,
                                      float width,
                                      float height,
                                      bool forceRebuild,
                                      uint & deviceNum,
                                      bool listDevices,
                                      bool deviceSpecified,
                                      int &status)
{
    // dont run on ATI 4XXX devices unless explictly asked to
    std::vector<string> vendors;
    std::vector<bool>   locals;
    
    // check if system has usable GPU devices and if GPU rendering was selected
    bool targetGPU = ClContext::hasGPU(vendors, true, locals, true) &&  devType == CL_DEVICE_TYPE_GPU;
    if (! targetGPU)
        deviceNum = 0;
    
    RenderHarness *harness = RenderHarness::makeSingleton(targetGPU, deviceNum, false, true);  // GPU
    if (! harness->clContext) {
        systemLog("Initializing OpenCL failed");
        status = -5;
        return harness;
    }
    
    // check for enough GPU memory for the first flame
    SharedFlameExpanded & fe = flames[0];
    Flame *flame             = fe->getFlame();
    uint xformCount          = (uint)fe->countOfXforms();
    uint supersample         = flame->params.oversample;
    
    // for GPUs: 35 Mb minimum => 990,322 pixels squared or about 1000x990   == CPU uses virtual memory, so this check is not needed
    // Note: formula for amount of memory is: 1 Mb  +  (36 * area / 1048576) Mb
    if (targetGPU) {
        if (! harness->clContext->oclDeviceCheckForEnoughMemoryArea(ceilf(width*height), xformCount, supersample)) { // if not enough memory, swithc to CPU
            deviceNum = 0;
            targetGPU = false;
            harness->contextSetup(targetGPU, deviceNum, forceRebuild, true);
        }
    }
    
    // print the OpenCL render devices available
    if (listDevices) {
        for (SharedDeviceUsage & deviceUsage : harness->clContext->deviceUsages) {
            printf("%s\n", deviceUsage->name.c_str());
        }
    }
    
    // Print if the selected render device is quarantined
    SharedDeviceUsage &deviceUsage = harness->clContext->selectedDevices[harness->clContext->selectedDeviceNumForDeviceNum(deviceNum)];
    bool quarantined               = ClContext::checkQuarantineForDeviceID((cl_device_id)deviceUsage->deviceID);
    
    printf("Render Device:%s Quarantined:%s\n",
           deviceUsage->name.c_str(), quarantined ? "YES" : "NO");
    return harness;
}

int main(int argc, char *argv[])
{
    int quality      = 50;
    int bitsPerPixel = 8;
    uint deviceNum    = 0;
    float width      = 800.f;
    float height     = 600.f;
    bool hasAlpha    = false;
    bool flipped     = false;
    bool forceRebuild = false;
    bool densityEstimation = true;
    bool silent       = false;
    bool listDevices  = false;
    bool deviceSpecified = false;
    float epsilon     = 1.e-7f;
    bool  anyDevice   = true; // no what the user intent is - we dont run on ATI 4XXX devices unless asked to explicitly
    
    cl_device_type devType = CL_DEVICE_TYPE_GPU;
    
    string colorspaceName = "AdobeRGB";
    
    string url;
    string outURL;
    if (argc == 1) {
        usage();
        return 0;
    }
    if (strcmp(argv[1], "-list") == 0) {
        if (argc == 2)
            devType = CL_DEVICE_TYPE_GPU;
        else if (strcmp(argv[2], "-deviceType") == 0) {
            anyDevice = false;
            char *dt = argv[3];
            if ((strcmp(dt, "cpu") == 0) || (strcmp(dt, "CPU") == 0))
                devType = CL_DEVICE_TYPE_CPU;
            else if ((strcmp(dt, "gpu") == 0) || (strcmp(dt, "GPU") == 0))
                devType = CL_DEVICE_TYPE_GPU;
        }
        ClContext::printDeviceList(devType);
        return 0;
    }
    
    if (argc < 5) {
        return -1;
    }
    url = string(argv[3]);
    
    sscanf(argv[1], "%f", &width);
    sscanf(argv[2], "%f", &height);
    width  = fabsf(width);
    height = fabs(height);
    
    for (int i = 4; i < argc; i++) {
        if (strcmp(argv[i], "-o") == 0) {
            outURL = string(argv[++i]);
        }
        if (strcmp(argv[i], "-q") == 0) {
            sscanf(argv[++i], "%u", &quality);
        }
        if (strcmp(argv[i], "-epsilon") == 0) {
            sscanf(argv[++i], "%e", &epsilon);
        }
        if (strcmp(argv[i], "-bpp") == 0) {
            sscanf(argv[++i], "%u", &bitsPerPixel);
        }
        if (strcmp(argv[i], "-alpha") == 0) {
            hasAlpha = true;
        }
        if (strcmp(argv[i], "-flip") == 0) {
            flipped = true;
        }
        if (strcmp(argv[i], "-nde") == 0) {
            densityEstimation = false;
        }
        if (strcmp(argv[i], "-rebuild") == 0) {
            forceRebuild = true;
        }
        if (strcmp(argv[i], "-list") == 0) {
            listDevices = true;
        }
        if (strcmp(argv[i], "-device") == 0) {
            sscanf(argv[++i], "%u", &deviceNum);
            deviceSpecified = true;
        }
        if (strcmp(argv[i], "-space") == 0) {
            colorspaceName = string(argv[++i]);
        }
        if (strcmp(argv[i], "-deviceType") == 0) {
            anyDevice = false;
            char *dt = argv[++i];
            if ((strcmp(dt, "cpu") == 0) || (strcmp(dt, "CPU") == 0))
                devType = CL_DEVICE_TYPE_CPU;
            else if ((strcmp(dt, "gpu") == 0) || (strcmp(dt, "GPU") == 0))
                devType = CL_DEVICE_TYPE_GPU;
        }
        if (strcmp(argv[i], "-silent") == 0) {
            silent = true;
        }
    }
    if (outURL.length() == 0) {
        size_t pos = url.rfind(".");
        if (pos != string::npos)
            outURL = url.substr(0, pos) + ".png";
        else
            outURL = url + ".png";
        
        outURL = ::expandTildeInPath(outURL.c_str());
    }
    unsigned deviceCount = 0;
    
    Magick::InitializeMagick(NULL);
    RenderHarness::variationSetSetup();

    // the flame file might be inside a subdirectory (true for .fa files)
    std::string path           = flamePathFromInputPath(url);
    FlameExpandedVector flames = FlameParse::loadFromPath(path);
    if (flames.size() == 0) {
        snprintf(buf, sizeof(buf), "Reading/parsing flame recipe failed for: %s", url.c_str());
        systemLog(buf);
        return -2;
    }
    int status = 0;
    RenderHarness * harness = narrowDeviceSelection(flames,
                                                    deviceCount,
                                                    devType,
                                                    url,
                                                    width,
                                                    height,
                                                    forceRebuild,
                                                    deviceNum,
                                                    listDevices,
                                                    deviceSpecified,
                                                    status);
    if (status != 0)
        return status;
    
    // Now lets render
    if (deviceCount > 0) {
        for (uint i = 0; i < flames.size(); i++) {
            SharedFlameExpanded fe = flames[i];
            string _outURL = flames.size() > 1 ? getOutURL(outURL, i + 1) : outURL;
            printf("Rendering %s frame #%u to:   %s\n", fileNameFromPath(path).c_str(), i+1, _outURL.c_str());
            
            status = harness->flam4QueuedRender(harness->clContext,
                                 fe,
                                 deviceNum,
                                 url,
                                 _outURL,
                                 width,
                                 height,
                                 quality,
                                 bitsPerPixel,
                                 hasAlpha,
                                 colorspaceName.c_str(),
                                 flipped,
                                 epsilon,
                                 !silent);
        }
    }
    return status;
}
