## *Fractal Architect* Render Engine
***Open Source***, Portable Flame Fractal GPU Rendering Engine

---
**Why?**

Filling a deep need for extremely fast GPU rendering and high quality output to the Flame fractal community.
 
**Your favorite app can now support GPU rendering.**

**What is It?**

Packaged as a dynamic linked library, so it can be embedded in commercial or open source flame fractal editors like Apophysis, JWildfire, Chaotica.

**Portable**

Written using portable C++ 14. Portable rewrite of commercial Fractal Architect app's render engine.

*FA 5 is a commercial Apple Mac app.*

**Supported Platforms:**  Windows, Linux, MacOS

**Author:** Steven Brodhead    sbrodhead@mac.com

**Project Home:**    [Project Home](https://bitbucket.org/sbrodhead/faengine)

**Blog:**    [Blog](http://www.fractalarchitect.net/blog)

### License

Core Rendering Library:		licensed under GNU LGPL v 2.1.

Example apps' source code:   licensed under MIT license.

### News -- December 3, 2016

##### JWildfire
Jwildfire 3.0 uses this render engine for GPU accelerated rendering.
##### Windows 10

* Tested on Windows 10 using Nvidia, AMD, and Intel GPUs
* OpenCL/CUDA platform GPU rendering. Multiple GPU rendering.
* Great performance.
* Choice of OpenCL or CUDA (Nvidia GPUs) rendering mode.

#####Mac OS - Mavericks thru Sierra
* Same performance as FA 4 app.
* Dependent on Apple's OpenCL drivers, so Intel GPUs not supported on Mac OS El Capitan, but does work on Mac OS Sierra
* CUDA rendering on Nvidia GPUs.

### New Features in the Works

* Linux Support
* Support for new Jwildfire features

### Performance on Windows 10
 *---- Gaming PC ----*

    Nvidia GeForce GTX 980 Ti     $469 on 7/1/2016

    Intel i7-4790K @ 4.00 Ghz     $340 on 7/1/2016


electricsheep.244.01917.flam3    [1440X960] SS:2 Q:1000  Total:4.00 sec   DE:0.41 sec   **Mips:385.67**

    Jwildfire  3:10.3  or 190.3 sec         47.6 X faster with FA

electricsheep.245.07662.flam3    [1440X960] SS:2 Q:1000  Total:2.52 sec   DE:0.17 sec   **Mips:591.08**

    Jwildfire  1.44.5  or 104.5 sec         41.5 X faster with FA

 *---- Integrated Graphics PC ----*

    Intel i7-4790K @ 4.00 Ghz     $340 on 7/1/2016  --- Integrated GPU: Intel(R) HD Graphics 4600


electricsheep.244.01917.flam3 [1440X960] SS:2 Q:1000  Total:24.53 sec DE:1.64 sec **Mips:60.55**

    Jwildfire  3:10.3  or 190.3 sec         7.8 X faster with FA

electricsheep.245.07662.flam3 [1440X960] SS:2 Q:1000   Total:16.27 sec DE:1.59 sec **Mips:94.47**

    Jwildfire  1.44.5  or 104.5 sec         6.4 X faster with FA

 *---- High end Laptop ----*
    2015 MacBook Pro

    Intel i7-4870HQ @ 2.50 Ghz 
    AMD Radeon M9 M370X

electricsheep.244.01917.png [1440X960] SS:2 Q:1000  Total:18.08 sec DE:1.92 sec **Mips:85.79**

    Jwildfire  5:19  or 319. sec         17.6 X faster with FA

electricsheep.245.07662.png [1440X960] SS:1 Q:1000  Total:9.80 sec DE:0.30 sec **Mips:145.90**

    Jwildfire  2:51.5  or 171.5 sec      17.5 X faster with FA

 *---- High end Gaming Laptop ----*
    2016 Razer Blade  using CUDA

    Intel i7-6700HQ @ 2.60 Ghz 
    Nvidia GeForce GTX 1060

electricsheep.244.01917.png [1440X960] SS:4 Q:500  Total:3.54 sec DE:0.14 sec **Mips:191.70** 

    Jwildfire v3.0  65.69 sec         18.6 X faster with FA

electricsheep.245.07662.png [1440X960] SS:1 Q:500  Total:1.09 sec DE:0.01 sec **Mips:603.08**

    Jwildfire v3.0  35.94 sec         33.0 X faster with FA

## For Developers

##### 3rd Party Dependencies
* Library itself has no 3rd party library dependencies. It is completely standalone.
	* Exception: OpenSSL on Mac OS
* Unit Testing  framework: uses CXXTest Unit Test Framework
* Example app uses GraphicsMagick API to save images to file.

##### Build Instructions
See BuildingNotes.txt for complete instructions.

GraphicsMagick is a pain to build as its such a big API. 
The library itself does not use GraphicsMagick, it is only used by the example app.

So that means that you can use a different imaging API, like .Net, Cocoa, or Java.
There are only about 5 lines of code in the example app, that would need to be replaced.