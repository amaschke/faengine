//
//  DeviceKind.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/14/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "DeviceKind.hpp"
#include "DeviceProgram.hpp"
#include "VariationSet.hpp"
#include "Utilities.hpp"

#include <cstdlib>

DeviceKind::DeviceKind(const std::string & _kind, const std::string & _vendor, const SharedDeviceContext & _deviceContext)
:
    kind(_kind),
    vendor(_vendor),
    deviceContext(_deviceContext),
    deviceName(),
    rebuild(true),
    clDeviceInstances(),
    cudaDeviceInstances(),
    programs(),
    workGroupDims(nullptr)
{}

DeviceKind::~DeviceKind()
{
    if (workGroupDims)
        free(workGroupDims);
}

std::string DeviceKind::summary()
{
    std::string _deviceType = isCPU() ? "CPU:" : "GPU:";
    if (kind.find(vendor) != std::string::npos)
        vendor = "";
    return _deviceType + " " + vendor + (vendor.length() > 0 ? " " : "") + kind;
}

void * DeviceKind::deviceIDForInstanceIndex(size_t index)
{
    if (clDeviceInstances.size() == 0)
        return nullptr;
    
    return clDeviceInstances[index];
}

uint DeviceKind::maxDensityEstimationRadius()
{
    if (localMemSize == 49152)
        return 19;
    else if (localMemSize == 32768)
        return 14;
    else
        return 7;
}

#pragma mark Variation Set Support

DeviceProgram * DeviceKind::makeProgramFromSource(const SharedVariationSet &variationSet, bool forceRebuild, duration<double> & buildTime)
{
    std::shared_ptr<DeviceProgram> program = std::make_shared<DeviceProgram>(deviceContext.lock(), shared_from_this(), variationSet);
    programs[variationSet->uuid]           = program;
    buildTime                              = program->buildTime;
    return program.get();
}

DeviceProgram * DeviceKind::programForVariationSetUuid(const std::string &variationSetUuid, duration<double> & buildTime)
{
    if (programs.find(variationSetUuid) != programs.end())
        return programs[variationSetUuid].get();
    return makeProgramFromSource(VariationSet::variationSetForUuid(variationSetUuid), false, buildTime);
}

DeviceProgram * DeviceKind::programForVariationSet(const SharedVariationSet & variationSet, duration<double> & buildTime)
{
    return programForVariationSetUuid(variationSet->uuid, buildTime);
}

void DeviceKind::removeProgramForVariationSetUuid(const std::string &variationSetUuid)
{
    programs.erase(variationSetUuid);
}

DeviceProgram * DeviceKind::firstProgramForVariationSet(const SharedVariationSet &variationSet, duration<double> & buildTime)
{
    return programForVariationSet(variationSet, buildTime);
}

DeviceProgram * DeviceKind::firstProgram(duration<double> & buildTime)
{
    if (programs.size() == 0) {
        return programForVariationSet(deviceContext.lock().get()->defaultVariationSet, buildTime);
    }
    std::string variationSetUuid = allKeys(programs).front();
    return programForVariationSetUuid(variationSetUuid, buildTime);
}

std::string DeviceKind::uuid()
{
    duration<double> buildTime(0.);
    return firstProgram(buildTime)->uuid;
}

size_t DeviceKind::maxWorkGroupSizeForKernelsForVariationSet(const SharedVariationSet &variationSet)
{
    duration<double> buildTime(0.);
    return firstProgramForVariationSet(variationSet, buildTime)->maxWorkGroupSize;
}
