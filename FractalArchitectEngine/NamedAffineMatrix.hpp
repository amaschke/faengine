//
//  NamedAffineMatrix.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 4/30/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef NamedAffineMatrix_hpp
#define NamedAffineMatrix_hpp

#include <string>
#include "Exports.hpp"
#include "AffineMatrix.hpp"

class FAENGINE_API NamedAffineMatrix : public AffineMatrix {
public:
    
    NamedAffineMatrix() = default;
    NamedAffineMatrix(const NamedAffineMatrix &m) = default;
    NamedAffineMatrix(float _a, float _b, float _c, float _d, float _e, float _f);
    NamedAffineMatrix(const AffineMatrix &m, const std::string &_name, size_t _groupIndex);

    NamedAffineMatrix& operator=(const NamedAffineMatrix &m) = default;
    
    bool operator==(const NamedAffineMatrix &other) const;
    bool operator!=(const NamedAffineMatrix &other) const;
    
    // ==== Members ====
    
    std::string name;
    size_t groupIndex;
};

#endif /* NamedAffineMatrix_hpp */
