//
//  ClDeviceKind.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/17/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef ClDeviceKind_hpp
#define ClDeviceKind_hpp

#include "Exports.hpp"
#include "DeviceKind.hpp"

#ifdef __APPLE__
#include <OpenCL/OpenCL.h>
#elif defined(_WIN32)
#include <CL/opencl.h>
#else
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl.h>
#endif
#include <string>
#include <chrono>
#include "ClProgram.hpp"

class ClContext;
class VariationSet;

using StringsSet        = std::unordered_set<std::string>;
using SharedClProgram   = std::shared_ptr<ClProgram>;
using SharedClContext   = std::shared_ptr<ClContext>;
using WeakClContext     = std::weak_ptr<ClContext>;
using PlatformIDsVector = std::vector<cl_platform_id>;

using namespace std::chrono;

class FAENGINE_API ClDeviceKind : public DeviceKind {
    friend class ClProgram;
    friend class ClContext;
    friend class Flam4ClRuntime;
public:
    ClDeviceKind(std::string & _kind, std::string & _vendor, const SharedDeviceContext & _deviceContext, cl_context _context);
    
    virtual ~ClDeviceKind();
    
    bool hasLocalMemoryOnDevice() override { return localMemType == CL_LOCAL; }
    
    bool isCPU() override    { return (deviceType & CL_DEVICE_TYPE_CPU) != 0; }
    bool isGPU() override    { return (deviceType & CL_DEVICE_TYPE_GPU) != 0; }
    
    size_t numDevices() { return clDeviceInstances.size(); }

    uint determineWarpSizeForDevice(void * device);
    cl_device_id deviceIDForInstanceIndex(size_t index);
    
    std::string getDeviceVendor() override;
    std::string getDeviceName() override;
    std::string getDeviceOpenClVersion();
    std::string getDeviceExtensions();
    
    PointerVector & deviceIDs()  { return clDeviceInstances; }
    
    ClContext *clContext() { return (ClContext *)deviceContext.lock().get(); }
    
private:
    bool createContext() override;
    void setupSubDevicesFor(void * cpuDeviceID) override;
    
    virtual ClProgram * programForVariationSetUuid(const std::string & variationSetUuid, duration<double> & buildTime) override;
    virtual ClProgram * programForVariationSet(const SharedVariationSet & variationSet, duration<double> & buildTime)  override;
    void removeProgramForVariationSetUuid(std::string &variationSetUuid);
    virtual ClProgram * makeProgramFromSource(const SharedVariationSet & variationSet, bool forceRebuild, duration<double> & buildTime) override;
    
    void deviceKindProperties() override;
    
    Json::Value makeJson();
    Json::Value makeJsonForDeviceKindWithUuid(const std::string & uuid);
    static Json::Value & existingJsonForDeviceName(const std::string & name, Json::Value & kindsJson);
    static Json::Value & existingJsonForDeviceName(const std::string & name);
    
    Json::Value & existingJson();
    static bool JsonIsValid(Json::Value & Json);
    enum DeviceKindJsonStatus checkJsonStatus(Json::Value & kindsJson);
    const std::string processDeviceKind() override;
    
    // ============= Members ================
    
    static StringsSet devicesWithBrokenNonPointReplace;
    PlatformIDsVector platformInstances;
    
    cl_context      context;  // one context per device kind
    std::string     deviceOpenClVersion;
    
    // properties for this device Kind
    cl_device_type           deviceType;
    cl_device_local_mem_type localMemType;
};

#endif /* ClDeviceKind_hpp */
