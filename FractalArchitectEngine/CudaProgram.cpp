//
//  CudaProgram.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 7/7/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "CudaProgram.hpp"
#include "CudaContext.hpp"
#include "VariationSet.hpp"
#include "Utilities.hpp"
#include "CudaDeviceKind.hpp"

#ifdef __APPLE__
#include <crt_externs.h>
#endif

#include <fstream>

#ifdef _WIN32
#include <Windows.h>
#include "WindowsInterface.hpp"
#else
#include "CocoaInterface.h"
#endif

// for 2 warps per block
#define NUM_POINTS_WARPSIZE_32 64

using std::string;

static char buf[1024];

CudaProgram::CudaProgram(SharedCudaContext ctx, SharedDeviceKind _deviceKind, CUcontext _cucontext, bool forceRebuild, const SharedVariationSet &_variationSet)
: DeviceProgram(ctx, _deviceKind, _variationSet)
{
    cucontext    = _cucontext;
    forceRebuild = ctx->forceRebuild;
    
    std::vector<char>ptxContents = ptxContentsForUuid(uuid);
    std::string sourcePath       = variationSet->GPUKernelURL(useCUDA);
    
    SharedCudaDeviceKind cudaDeviceKind = std::dynamic_pointer_cast<CudaDeviceKind>(_deviceKind);
    
#ifdef _WIN32
    // templates are embedded in DLL
    if (VariationSet::outOfDateDerived(sourcePath, getThisDLLPath()))
        forceRebuild = true;
#endif
    bool outOfDate    = outOfDateBinary(pathForBinaryForUuid(uuid), sourcePath);
    
    if (ptxContents.size() == 0)
        forceRebuild = true;
    if (outOfDate)
        forceRebuild = true;
    
    // build from source
    if (forceRebuild) {
        uint warpSize    = cudaDeviceKind->warpSize;
        string sourceStr = sourceContents(useCUDA);
        const char *src  = sourceStr.c_str();
        
        nvrtcProgram program;
        nvrtcResult result = nvrtcCreateProgram(&program,
                                                src,
                                                fileNameFromPath(sourcePath).c_str(),
                                                0, NULL, NULL);
        if (result != NVRTC_SUCCESS) {
            throw std::runtime_error(("ERROR: nvrtcCreateProgram failed"));
        }
        
        CUdevice device = cudaDeviceKind->cudaDeviceForInstanceIndex(0);
        int compute_major, compute_minor;
        cuDeviceGetAttribute((int *)&compute_major, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR, device);
        cuDeviceGetAttribute((int *)&compute_minor, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR, device);
        
        StringsVector opts;
        
        snprintf(buf, sizeof(buf), "--gpu-architecture=compute_%u%u", compute_major, compute_minor);
        opts.emplace_back(buf);
        opts.emplace_back("--define-macro=KERNEL_RUNTIME");
        opts.emplace_back("--use_fast_math");
        opts.emplace_back("--generate-line-info");

        snprintf(buf, sizeof(buf), "--define-macro=WARP_SIZE=%u", warpSize);
        opts.emplace_back(buf);
        snprintf(buf, sizeof(buf), "--define-macro=NUM_POINTS=%u", NUM_POINTS_WARPSIZE_32/32*warpSize);
        opts.emplace_back(buf);
        snprintf(buf, sizeof(buf), "--define-macro=DENSITY_KERNAL_RADIUS=%u", cudaDeviceKind->maxDensityEstimationRadius());
        opts.emplace_back(buf);
        
        //            [opts addObject:@"-G"];  // embed debug info -- CAUTION: removes optimization - real slow!!
        
        if (! variationSet->is3DCapable) {
            opts.emplace_back("--define-macro=FOR_2D");
        }
        
#ifdef __APPLE__
        // getting this error: nvrtc: error: failed to load builtins - but in FA4 app ????
//        int argc = *_NSGetArgc();
//        char **argv = *_NSGetArgv();
//        char **progName = _NSGetProgname();
//        printf("PROGNAME: %s  %s\n\n", *progName, argv[0]);
        
        char ** environ = *_NSGetEnviron();
        while (*environ) {
//            printf("ENVIRON: %s\n", *environ);
            
            std::string nameValue = *environ;
            size_t pos = nameValue.find("=");
            if (pos != std::string::npos) {
                std::string name = nameValue.substr(0, pos);
                std::string value = nameValue.substr(pos + 1);
                setenv(name.c_str(), value.c_str(), 0);
            }
            environ++;
        }
        
        // DYLD_LIBRARY_PATH -- needs to be set for shared library 
        printf("DYLD_LIBRARY_PATH: %s\n", getenv("DYLD_LIBRARY_PATH"));
#endif
        
        std::vector<const char *>copts;
        
        for (size_t i = 0; i < opts.size(); i++) {
            const std::string & opt = opts[i];
            copts.push_back(opt.c_str());
        }
        // Build program from either source or binaries
        nvrtcResult compileResult = nvrtcCompileProgram(program,
                                                        (int)opts.size(),
                                                        copts.data());        
        if (compileResult != NVRTC_SUCCESS)
        {
            logErrStatusForProgramName(sourcePath, program);
            throw std::runtime_error(("ERROR: nvrtcCompileProgram failed"));
        }
        makeKernelsForDevice(program);
        // Destroy the program.
        nvrtcDestroyProgram(&program);
    }
    else {
        makeKernelsFromPTX(ptxContents);
    }
}

void CudaProgram::makeKernelsForDevice(nvrtcProgram program)
{
    // Obtain PTX from the program.
    size_t ptxSize;
    nvrtcResult result = nvrtcGetPTXSize(program, &ptxSize);
    char *ptx          = new char[ptxSize];
    result = nvrtcGetPTX(program, ptx);
    
    std::vector<char> ptxData(ptx, ptx + ptxSize);
    makeKernelsFromPTX(ptxData);
}

void CudaProgram::makeKernelsFromPTX(std::vector<char> &ptxData)
{
    writePtxForUuid(uuid, ptxData);
    
    _status = cuCtxPushCurrent_v2(cucontext);
    
    _status = cuModuleLoadDataEx(&_module, ptxData.data(), 0, 0, 0);
    
    time_point<high_resolution_clock> now = high_resolution_clock::now();
    _status = cuModuleGetFunction(&iteratePointsKernal, _module, "iteratePointsKernal");
    
    duration<double> _duration = duration_cast<duration<double>>(high_resolution_clock::now() - now);
    
    SharedCudaDeviceKind cudaDeviceKind = std::dynamic_pointer_cast<CudaDeviceKind>(deviceKind.lock());
    snprintf(buf, sizeof(buf), "%s IteratePoints compile time: %f secs", cudaDeviceKind->kind.c_str(), _duration.count());
    systemLog(buf);
    
    _status = cuModuleGetFunction(&setBufferKernal,              _module, "setBufferKernal");
    _status = cuModuleGetFunction(&reductionKernal,              _module, "reductionKernal");
    _status = cuModuleGetFunction(&postProcessStep1Kernal,       _module, "postProcessStep1Kernal");
    _status = cuModuleGetFunction(&postProcessStep2Kernal,       _module, "postProcessStep2Kernal");
    _status = cuModuleGetFunction(&colorCurveRGBChannelKernal,   _module, "colorCurveRGBChannelKernal");
    _status = cuModuleGetFunction(&colorCurveRGB3ChannelsKernal, _module, "colorCurveRGB3ChannelsKernal");
    _status = cuModuleGetFunction(&FlexibleDensityEstimationKernal, _module, "FlexibleDensityEstimationKernal");
    _status = cuModuleGetFunction(&RGBA128FtoRGBA32UKernal,      _module, "RGBA128FtoRGBA32UKernal");
    _status = cuModuleGetFunction(&RGBA128FtoBGRA32UKernal,      _module, "RGBA128FtoBGRA32UKernal");
    _status = cuModuleGetFunction(&RGBA128FtoRGBA64UKernal,      _module, "RGBA128FtoRGBA64UKernal");
    _status = cuModuleGetFunction(&RGBA128FtoRGBA128FKernal,     _module, "RGBA128FtoRGBA128FKernal");
    _status = cuModuleGetFunction(&MergeKernal,                  _module, "MergeKernal");
    _status = cuModuleGetFunction(&readChannelKernal,            _module, "readChannelKernel");
    _status = cuModuleGetFunction(&writeChannelKernal,           _module, "writeChannelKernel");
    _status = cuModuleGetFunction(&writeChannelStripedKernal,    _module, "writeChannelStripedKernel");
    _status = cuModuleGetFunction(&convolveRowKernal,            _module, "convolutionRowsKernel");
    _status = cuModuleGetFunction(&convolveColKernal,            _module, "convolutionColumnsKernel");
    
    _status = cuCtxSetCacheConfig(CU_FUNC_CACHE_PREFER_L1);
    _status = cuFuncSetCacheConfig(FlexibleDensityEstimationKernal, CU_FUNC_CACHE_PREFER_SHARED);
    
    for (int i = 0; i < cudaDeviceKind->numDevices(); i++) {
        size_t DK_mwgs = cudaDeviceKind->maxWorkGroupSize;
        int mwgs       = 0;
        
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, iteratePointsKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, setBufferKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, reductionKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, postProcessStep1Kernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, postProcessStep2Kernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, colorCurveRGB3ChannelsKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, colorCurveRGBChannelKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, FlexibleDensityEstimationKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, RGBA128FtoRGBA32UKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, RGBA128FtoBGRA32UKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, RGBA128FtoRGBA64UKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, RGBA128FtoRGBA128FKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, MergeKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, readChannelKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, writeChannelKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, writeChannelStripedKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, convolveRowKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        cuFuncGetAttribute(&mwgs, CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK, convolveColKernal);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        
        maxWorkGroupSize = DK_mwgs;
    }
    CUcontext acontext;
    _status = cuCtxPopCurrent_v2(&acontext);
}

void CudaProgram::logErrStatusForProgramName(const std::string &programName, nvrtcProgram program)
{
    // Obtain compilation log from the program.
    size_t logSize;
    nvrtcResult result = nvrtcGetProgramLogSize(program, &logSize);
    char *buildLog     = (char *)malloc(logSize);
    result             = nvrtcGetProgramLog(program, buildLog);
    
    SharedDeviceKind _deviceKind = deviceKind.lock();
    snprintf(buf, sizeof(buf), "Error building CUDA program:%s for device:%s", programName.c_str(), _deviceKind->kind.c_str());
    systemLog(buf);
    systemLog(buildLog);
    free(buildLog);
}

std::string CudaProgram::ptxPathForUuid(const std::string &_uuid)
{
    const std::string &cachePath        = DeviceProgram::cachePath();
    std::string binaryDirectory         = cachePath + "/" + _uuid + "/variationSets/" + variationSet->uuid;
    std::string path                    = binaryDirectory + "/" + basename + ".ptx";
    return  ::expandTildeInPath(path.c_str());
}

std::vector<char> CudaProgram::ptxContentsForUuid(const std::string & _uuid)
{
    std::string path = ptxPathForUuid(_uuid);
    
    std::ifstream stream(path, std::ios::in | std::ios::binary);
    if (stream.fail()) {
        return std::vector<char>();
    }
    
    std::vector<char> contents((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
    return contents;
}

void CudaProgram::writePtxForUuid(const std::string &_uuid, std::vector<char> &ptxData)
{
    std::string path = ptxPathForUuid(_uuid);
    
    if ((mkpath(path.c_str(), 0777) == -1)  && (errno != EEXIST)) {
        std::string msg = "Failed to open file: " + pathForBinaryForUuid(_uuid) + "  " + strerror(errno);
        throw std::runtime_error(msg);
    }
    std::string binaryPath = path + "/" + basename + ".cl.bin";
    
    std::ofstream stream(binaryPath, std::ios::out | std::ios::binary);
    if (stream.fail()) {
        std::string msg = string_format("Failed to open file: %s", binaryPath.c_str());
        throw std::runtime_error(msg);
    }
    stream.write(ptxData.data(), ptxData.size());
}

bool CudaProgram::outOfDateBinary(const std::string & binaryPath, const std::string &sourcePath)
{
    if (! fileExists(binaryPath))
        return true;
    
    struct stat sourceAttributes;
    struct stat binaryAttributes;
    
    stat(binaryPath.c_str(), &binaryAttributes);
    stat(sourcePath.c_str(), &sourceAttributes);
    
    time_t sourceModDate = sourceAttributes.st_mtime;
    time_t binaryModDate = binaryAttributes.st_mtime;
    
    return binaryModDate < sourceModDate;
}

CudaProgram::~CudaProgram()
{
    cuModuleUnload(_module);
}

CUresult CudaProgram::status()
{
    return _status;
}

void CudaProgram::setStatus(CUresult status)
{
    _status = status;
    
    if (_status != CUDA_SUCCESS) {
        snprintf(buf, sizeof(buf), "Bad CUDA status %d", _status);
    }
}

CUmodule CudaProgram::module()
{
    return _module;
}
