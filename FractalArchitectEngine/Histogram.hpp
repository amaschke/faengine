//
//  Histogram.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/31/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef Histogram_hpp
#define Histogram_hpp

#ifdef __STRICT_ANSI__
#undef __STRICT_ANSI__
#endif
#include <vector>
#include <string>

#include "common.hpp"
#include "ColorMap.hpp"
#include "Flame.hpp"
#include <math.h>

using namespace FA;

using uint        = unsigned int;

class ColorMap;

template <class T>
class Histogram {
public:
    Histogram(std::vector<T> & _histogramData, float _width, float _height, struct Flame *_flame, uint _batchesSoFar);
    
    std::string description();
    
    double getSumW() { return sumW; }
    T pointAtPointOffset(Point offset);
    float avgHitsPerBatch();
    
private:
    void calcHistogramStats();
    
    float k1();
    float k2();
    float actualQualityForBatches(uint batches);
    float actualQualityForBatchesSSadjusted(uint batches);
    float actualQuality();
    float actualQualitySSadjusted();
    float actualIterations();
    float percentRetained();
    
    T rgbaForPoint(T point);
    Color colorForPoint(T point);
    
    float luminanceForPoint2(T point);
    float luminanceForPoint(T point);
    
    float wAtPointOffset(Point offset);
    
    Color colorAtPointOffset(Point offset);
    float adobeLumaAtPointOffset(Point offset);
    
    static float actualIterationsForBatches(uint batches);
    
    // ===== Members ============
    std::vector<T>   histogramData;
    struct Flame *flame;
    int           width;
    int           height;
    uint          supersample;
    float         maxW;
    float         avgW;
    double         sumW;
    float         maxLuminance;
    float         logMaxL;
    float         logMaxW;
    unsigned      batchesSoFar;
    size_t        indexForMax;
};

template <class T>
Histogram<T>::Histogram(std::vector<T> & _histogramData, float _width, float _height, struct Flame *_flame, uint _batchesSoFar)
:
width(_width),
height(_height),
histogramData(_histogramData),
batchesSoFar(_batchesSoFar),
supersample(_flame->params.oversample),
flame(_flame)
{
    calcHistogramStats();
}

template <class T>
void Histogram<T>::calcHistogramStats()
{
    T *points    = (T *)histogramData.data();
    size_t length = histogramData.size();
    maxW         = 0.f;
    maxLuminance = 0.f;
    sumW         = 0.;
    indexForMax  = 0L;
    size_t altSumW = 0U;
    for (size_t i = 0; i < length; i++) {
        float w = points[i].w;
        sumW   += w;
        altSumW += w;
        if (w > maxW) {
            maxW = w;
            indexForMax = i;
        }
    }
    avgW    = sumW/(double)length;
    logMaxW = log10f(maxW);
}

template <class T>
float Histogram<T>::k1()
{
    return (flame->params.brightness*268.0f)/255.0f;
}

template <class T>
float Histogram<T>::avgHitsPerBatch()
{
    return avgW/batchesSoFar;
}

template <class T>
float Histogram<T>::actualQualityForBatches(uint batches)
{
    return 32.f * 1024.f * NUM_ITERATIONS * batches/(width * height);
}

template <class T>
float Histogram<T>::actualQualityForBatchesSSadjusted(uint batches)
{
    return 32.f * 1024.f * NUM_ITERATIONS * batches/(width * height) * (supersample * supersample);
}

template <class T>
float Histogram<T>::actualIterationsForBatches(uint batches)
{
    return 32.f * 1024.f * NUM_ITERATIONS * batches;
}

template <class T>
float Histogram<T>::actualQuality()
{
    return actualQualityForBatches(batchesSoFar);
}

template <class T>
float Histogram<T>::actualQualitySSadjusted()
{
    return actualQualityForBatchesSSadjusted(batchesSoFar);
}

template <class T>
float Histogram<T>::actualIterations()
{
    return Histogram<T>::actualIterationsForBatches(batchesSoFar);
}

template <class T>
float Histogram<T>::percentRetained()
{
    return sumW / actualIterations();
}

template <class T>
float Histogram<T>::k2()
{
    float area = fabs(flame->params.size[0] * flame->params.size[1]);
    return ((float)(width*height))/(area*((float)(NUM_ITERATIONS)) * flame->params.numBatches * 32.f * 1024.f);
}

template <class T>
T Histogram<T>::rgbaForPoint(T point)
{
    float a = (k1()* logf(1.0f + k2()*point.w));
    float ls = a/point.w;
    ls = isnan(ls) ? 1.f : ls;
    point.x = ls*point.x;
    point.y = ls*point.y;
    point.z = ls*point.z;
    
    float gammaThreshold = flame->params.gammaThreshold;
    float gamma          = flame->params.gamma;
    float alpha;
    if (a < gammaThreshold) {
        float fraction = a/gammaThreshold;
        alpha = (1.f - fraction) * a * (powf(gammaThreshold, gamma)/gammaThreshold) + fraction * powf(a, 1.0f/gamma-1.0f);
    }
    else
        alpha = powf(a, 1.0f/gamma-1.0f);
    ls = flame->params.vibrancy*alpha;
    
    float signX = point.x >= 0.f ? 1.f : -1.f;
    float signY = point.y >= 0.f ? 1.f : -1.f;
    float signZ = point.z >= 0.f ? 1.f : -1.f;
    point.x = ls*point.x+(1.0f-flame->params.vibrancy)*signX*powf(fabs(point.x),1.0f/flame->params.gamma);
    point.y = ls*point.y+(1.0f-flame->params.vibrancy)*signY*powf(fabs(point.y),1.0f/flame->params.gamma);
    point.z = ls*point.z+(1.0f-flame->params.vibrancy)*signZ*powf(fabs(point.z),1.0f/flame->params.gamma);
    
    if (a < gammaThreshold) {
        float fraction = a/gammaThreshold;
        alpha = (1.f - fraction) * a * (powf(gammaThreshold, gamma)/gammaThreshold) + fraction * powf(a, 1.0f/gamma);
    }
    else
        alpha = powf(a, 1.0f/flame->params.gamma);
    alpha = alpha < 1.f ? alpha : 1.f;
    rgba p(point.x, point.y, point.z, point.w);
    hsva hsv = rgba::RGBtoHSVHueAdjusted(p);
    if (hsv.v > 1.f)
        hsv.v = 1.f;
    
    p = hsva::HSVtoRGB(hsv);
    point = { p.r, p.g, p.b, p.a };
    
    if (isfinite(point.x)) {
        point.x = point.x + flame->params.background.r * (1.f-alpha);
        point.y = point.y + flame->params.background.g * (1.f-alpha);
        point.z = point.z + flame->params.background.b * (1.f-alpha);
        point.w = alpha;
    }
    return point;
}

template <class T>
Color Histogram<T>::colorForPoint(T point)
{
    T p = rgbaForPoint(point);
    return Color(p.x, p.y, p.z, 1.f);
}

template <class T>
float Histogram<T>::luminanceForPoint2(T point)
{
    extern int isfinite(float x);
    float a = (k1()* logf(1.0f+k2()*point.w));
    float l =  powf(a, 1.0f/flame->params.gamma-1.0f);
    if (! isfinite(l))
        l = 0.f;
    return l/maxLuminance;
}

template <class T>
float Histogram<T>::luminanceForPoint(T point)
{
    return log10f(point.w)/logMaxW;
}

template <class T>
T Histogram<T>::pointAtPointOffset(Point offset)
{
    T *points = histogramData.data();
    int index         = offset.y*width + offset.x; // handle flipped view
    return points[index];
}

template <class T>
float Histogram<T>::wAtPointOffset(Point offset)
{
    return pointAtPointOffset(offset).w;
}

template <class T>
Color Histogram<T>::colorAtPointOffset(Point offset)
{
    return colorForPoint(pointAtPointOffset(offset));
}

template <class T>
float Histogram<T>::adobeLumaAtPointOffset(Point offset)
{
    Color color = colorForPoint(pointAtPointOffset(offset));
    return 0.297361f * color.red + 0.627355f * color.green + 0.075285f * color.blue;
}

template <class T>
std::string Histogram<T>::description()
{
    static char buf[100];
    snprintf(buf, sizeof(buf), "Histogram maxHits=%zu sumHits=%zu averageHits=%f avg/Batch:%f quality:%f",
             (size_t)maxW, (size_t)sumW, avgW, avgW/batchesSoFar, actualQuality());
    return buf;
}

#endif /* Histogram_hpp */
