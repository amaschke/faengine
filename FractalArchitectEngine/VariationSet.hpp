//
//  VariationSet.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 4/30/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef VariationSet_hpp
#define VariationSet_hpp

#include <unordered_map>
#include <unordered_set>
#include <string>
#include <vector>
#include <istream>
#include <memory>

#include "Exports.hpp"
#include "pugixml.hpp"
#include "Enums.hpp"

#define MAX_VARIATIONS_IN_VARSET 128

//class AnimatableMetadata;
class TransformProperty;
class Variation;
class VariationGroup;
class VariationSet;
class VariationParameter;
class VarParInstance;

using uint = unsigned int;
using SharedVariation           = std::shared_ptr<Variation>;
using SharedVariationGroup      = std::shared_ptr<VariationGroup>;
using SharedVariationConst      = std::shared_ptr<Variation const>;
using SharedVariationSet        = std::shared_ptr<VariationSet>;
using SharedVarParInstance      = std::shared_ptr<VarParInstance>;
using SharedVariationsVector    = std::vector<SharedVariation>;
using SharedParameter           = std::shared_ptr<VariationParameter>;
using ParametersDict            = std::unordered_map<std::string, SharedParameter>;
using ParametersVector          = std::vector<SharedParameter>;
using StringsSet                = std::unordered_set<std::string>;
using StringsVector             = std::vector<std::string>;
using TransformPropertiesDict   = std::unordered_map<std::string, TransformProperty>;
using TransformPropertiesVector = std::vector<TransformProperty>;
using VariationsDict            = std::unordered_map<std::string, SharedVariation>;
using VariationSetsDict         = std::unordered_map<std::string, SharedVariationSet>;
using VariationSetsVector       = std::vector<SharedVariationSet>;
using UintsPair                 = std::pair<std::string, size_t>;
using UintsDict                 = std::unordered_map<std::string, size_t>;
using UintPairsDict             = std::unordered_map<std::string, UintsPair>;
using UintPairsVector           = std::vector<UintsPair>;
using XformIndexesDict          = std::unordered_map<size_t, SharedVariation>;

using VariationCompareFunc      = bool (*)(const SharedVariation &a, const SharedVariation &b);
using ParameterCompareFunc      = bool (*)(const SharedParameter &a, const SharedParameter &b);
using UintPairCompareFunc       = bool (*)(const UintsPair &a,       const UintsPair &b);


struct FAENGINE_API TransformProperty
{
    TransformProperty();
    TransformProperty(const std::string & _key);
    TransformProperty(const std::string & _key, size_t _xformIndex);
    TransformProperty(const char * _key,        size_t _xformIndex);
    TransformProperty(const TransformProperty & o);
    
    std::string key;		 // KVC key
    size_t      xformIndex;  // index into xformVars array for this variation
};

class FAENGINE_API VariationSet : public std::enable_shared_from_this<VariationSet>  {
    friend class VariationGroup;
    friend class ClProgram;
    friend class CudaProgram;
public:
    VariationSet() noexcept;
    VariationSet(const SharedVariationSet &v) noexcept;
    VariationSet(std::istream &is, bool _amLibrary);
    
    VariationSet(const SharedVariationSet  &library,
                 const StringsVector &variationNames,
                 const std::string   &preferredName,
                 const std::string   &_uuid);    
    VariationSet(const SharedVariationSet  &library,
                 const StringsVector &variationNames,
                 const std::string   &preferredName,
                 const std::string   &_uuid,
                 bool                 _amTemporary);
    VariationSet(const StringsVector &variationNames,
                 const std::string   &preferredName,
                 const std::string   &_uuid,
                 bool                 incomplete);
    VariationSet(const SharedVariationSet  &library,
                 const StringsVector &variationNames,
                 const std::string   &preferredName,
                 const std::string   &_uuid,
                 bool                 _incomplete,
                 bool                 _amTemporary);
    ~VariationSet();
    
    static SharedVariationSet makeVariationSet(pugi::xml_node & variationSetEle, bool installAsIs);
    
    void copyVariationsFrom(const SharedVariationSet  &library,
                            const StringsVector &variationNames,
                            StringsSet & variationNamesSet);

    
    std::string getUuid() const             { return uuid; }
    std::string getDefaultVariation() const { return defaultVariation; }
    
    VariationSet & operator=(const VariationSet &v) noexcept;
    
    void addVariation(Variation &variation);
    void addVariationParameter(VariationParameter &parameter);
    
    size_t numberOfVariations()            { return variationsByKey.size(); }
    
    void variationChangedName(SharedVariation variation, const std::string &oldName);
    void variationChangedKey(SharedVariation variation, const std::string &oldKey);
    void variationChangedXformIndex(SharedVariation variation, size_t xformIndex, size_t oldXformIndex);
    void parameterChangedKey(SharedParameter parameter, const std::string &oldKey);
    
    bool keyIsNotUnique(const std::string &variationKey) const;
    bool keyIsNotUnique(SharedVariationConst variation, const std::string &variationKey) const;
    bool nameIsNotUnique(SharedVariationConst variation, const std::string &variationName) const;
    
    static std::vector<SharedVariationSet> variationSetsUsing(const Variation &libraryVariation);
    static bool unknownVariation(const SharedVariation &variation);
    
    static const SharedVariationSet legacyVariationSet();
    static const SharedVariationSet & getFactoryLibrary();
    static const SharedVariationSet & getMyLibrary();
    static const SharedVariationSet & variationsLibraryPlus();
    static const SharedVariationSet & variationsLibrary();
    static const SharedVariationSet & myVariationsLibrary();
    
    static StringsVector factoryLibraryVariationKeys(VariationCompareFunc f);
    static StringsVector myLibraryVariationKeys(VariationCompareFunc f);
    
    static bool canCreateFromLibraryForVariationNames(StringsSet variationNames);
    
    static SharedVariationSet variationSetForUuid(const std::string &uuid);
    static SharedVariationSet firstVariationSetForName(const std::string &name);
    
    static SharedVariationSet variationSuperSetForUuid(const std::string & uuid);
    static void setSuperSetFor(const std::string &subSetUUID, const SharedVariationSet &variationSet);
    
    static void addVariationToMyLibrary(SharedVariation &variation);
    void duplicateVariation(const SharedVariation &variation);
    static SharedVariationSet searchForVariationSetForUuid(const char * uuid);
    static SharedVariationSet searchForVariationSetForUuid(const std::string & uuid);
    
    Variation *variationForXformIndex(size_t xformIndex);
    Variation *variationForKey(const std::string &key);
    SharedVariation sharedVariationForKey(const std::string &_key);
    
    SharedParameter parameterForKey(const std::string &key) const;
    
    SharedVariationSet newDuplicateVariationSet(SharedVariationSet &variationSet) const;
    
    static VariationSetsDict getVariationSets();
    static SharedVariationSet getCurrentVariationSet();
    static void setCurrentVariationSet(const SharedVariationSet &variationSet);
    static void setupDefaultVariationSet();
    
    static std::string uuidForLegacyVariationSet();
    static std::string uuidForStandard3DVariationSet();
    static std::string uuidForVariationsLibrary();
    static std::string uuidForMyVariationsLibrary();
    
    static void setuPreferredVariationSet();
    static SharedVariationSet makeLegacyVariationSet();
    static SharedVariationSet makeStandard3DVariationSet();
    static StringsVector flam3VariationsArray();
    static StringsVector standard3DVariationsArray();
    static SharedVariationSet makeVariationSetFromVariationNames(StringsSet &names,
                                                                 const std::string &variationSetName, bool incomplete);
    static SharedVarParInstance addFloatValueInstance(float value,
                                                      const std::string &key,
                                                      size_t _xformIndex,
                                                      SharedVariationGroup variationGroup);
    
    float floatValueForKey(const std::string &key, size_t instanceNum, SharedVariationGroup &variationGroup) const;
    static SharedVarParInstance setCurrentFloatValue(float value,
                                                     const std::string &key,
                                                     size_t instanceNum,
                                                     size_t _xformIndex,
                                                     SharedVariationGroup variationGroup);
    SharedVarParInstance setFloatValue(float value,
                                       const std::string &key,
                                       size_t instanceNum,
                                       size_t _xformIndex,
                                       SharedVariationGroup variationGroup);
    static void setInstance(SharedVarParInstance &instance, SharedVariationGroup variationGroup);
    
    static VariationSetsVector preloadVariationSets(pugi::xml_document & doc);
    
    bool hasAllVariationsNeededBySet(const StringsSet & namesSet) const;
    bool hasAllVariationsNeededBy(const StringsVector & namesArray) const;
    
    static SharedVariationSet findVariationSetForNeededNames(StringsSet &neededNames,
                                                             const std::string &variationSetName,
                                                             const VariationSetsVector & variationSetsInDoc);
    static bool walkEleForCustomVariationsUsed(pugi::xml_node & flameEle, SharedVariationSet &variationSet);
    static VariationSetsVector preParseDocForVariationsUsed(pugi::xml_document & doc, const std::string & path,
                                                            bool onlyParseFirstFlame, bool &changed);
    
private:
    void insertVariation(const Variation &variation);
    void loadFromStream(std::istream & in);
    bool readFromData(const std::string &string);
    void saveKernelsAmUpgradedAllPlatforms(bool upgraded);
    
    void copyVariation(SharedVariation &variation, const SharedVariationSet &variationSet);
    void addCopy(SharedVariation copy, Variation &variation);
    void finishCopyFromOtherVariationSet(bool installAsIs);

    static void mergeForeignVariationSetIntoMyLibrary(SharedVariationSet &variationSet);
    
    void setupTransformProperties();
    
    size_t varsUsed()     const;
    size_t paramsUsed()   const;
    size_t otherUsed()    const;
    size_t slotsUsed()    const;
    bool valid()          const;
    size_t sourceLength();
    bool validXformKey(const std::string &key);
    size_t xformIndexForKey(const std::string &key);
    
    static std::unordered_set<std::string> prerequisiteVariationsFor3D();
    static StringsVector preMatrix3dSupport();
    static StringsVector matrix3dSupport();
    static StringsVector postMatrix3dSupport();
    static StringsVector preMatrix2dSupport();
    static StringsVector matrix2dSupport();
    static StringsVector postMatrix2dSupport();
    static std::string   uniqueUnknownName();
    
    ParametersVector        makeSortedParamVector(SharedVariation & variation, ParameterCompareFunc f);
    SharedVariationsVector  makeSortedVariations(VariationCompareFunc f);
    UintPairsVector         makeUintPairsVector(UintPairsDict & dict, UintPairCompareFunc f);
    
    static bool pairCompare(const UintsPair &a, const UintsPair &b);
    
    static std::vector<std::string> parseXmlElementForVariationNames(pugi::xml_node & variationSetEle, std::string & name, std::string & uuid);
    bool parseXmlElement(pugi::xml_node variationSetEle);
    bool canProcessXmlElement(pugi::xml_node & variationEle, uint variationIndex, uint parameterIndex);
    SharedVariation variationForXmlElement(pugi::xml_node &variationEle, uint variationIndex, uint & parameterIndex);
    
    // ==== Kernel Management Related ========
    static bool outOfDateDerived(const std::string & derivedURL, const std::string &factoryURL);
    static bool kernelOutOfDate(const std::string & kernelPath, const std::string & templateBasename, const std::string & extension);
    static time_t modificationDateOfFile(const std::string & path);
    static void setFileModificationDate(time_t modDate, const std::string & path);
    
    void saveKernelsAmUpgraded(bool upgraded, enum GpuPlatformUsed gpuPlatformInUse);
    void removeKernels();
    
    static std::string get2DSnippet(const std::string & snippet);
    static std::string get3DSnippet(const std::string & snippet);
    std::string createVarParStructs(enum GpuPlatformUsed gpuPlatform);
    static std::string switchCasesForVariations(SharedVariationsVector & variations, bool for3D, enum GpuPlatformUsed gpuPlatform);
    static std::string indentVariationSource(const std::string & string);
    std::string createKernelForCPU(bool forCPU, const std::string & extension, enum GpuPlatformUsed gpuPlatform);
    
    std::string CPUKernelURL();
    std::string GPUKernelURL(enum GpuPlatformUsed gpuPlatform);
    
    static std::string kernelTemplateFromBasename(const std::string & basename, const std::string & extension);
    static std::string cpuKernelTemplate(const std::string & extension);
    static std::string gpuKernelTemplate(const std::string & extension);
    
    bool upgradeCachedVariationSet();

    void xmlElement(pugi::xml_node & parent);
    void makeXmlDocument(pugi::xml_document & xmlDoc);
    
    static void saveLibraryXML();
    static void saveMyLibraryXML();
    void saveVariationSetXML();
    
    void writeXmlToPath(const std::string & absolutePath);
    void writeXmlToPath(std::ostream & stream);
    std::string xmlFilePath();
    
    void writetoKernelPath(const std::string & kernelURL,
                           bool forCPU,
                           const std::string & extension,
                           enum GpuPlatformUsed gpuPlatform);
    
    static std::string factoryVariationSetBaseName();
    
public:
    static void cacheFactoryVariationSets();
    static void cacheFactoryLibrary();
    static std::string pathForResource(const std::string & basename, const std::string & extension);
    
    static std::string variationSetsDirectoryURL();
    static std::string cachedLibraryURL();
    static std::string cachedMyLibraryURL();
    static std::string cachedLegacyVariationSetURL();
    static std::string initialMyLibraryPath();
    static std::string myLibraryURL();

    static bool cachedLibraryExists();
    static bool cachedMyLibraryExists();
    static bool cachedLegacyVariationSetExists();
    static void cacheMyLibrary();
    
    static const char * factoryLibraryBaseName();
    
    static void setupFAEngineApplicationSupport();
    static SharedVariationSet loadVariationSetFromURL(const std::string & variationSetURL, bool amLibrary);
    static SharedVariationSet loadVariationsLibrary();
    static SharedVariationSet loadMyVariationsLibrary();
    static SharedVariationSet loadLegacyVariationSet();
    
    static void loadSavedVariationSets();
    static void regenerateAllKernels();
    static void removeAllSavedKernels();
    
    static void updateMergedLibrary();
    
    std::string kernelURL(bool isGPU, enum GpuPlatformUsed gpuPlatformInUse);
    
#pragma mark Descriptions
    
    std::string describeVariations();
    std::string describeParameters();
    std::string description();
    
    static std::string allVarsetsDescription();
    static std::string allVarsetsDescription(VariationSetsDict & dict);
    static std::string allVarsetsDescription(VariationSetsVector & vector);
    
    StringsVector variationDescriptions(); // easier to see in debugger
    StringsVector parameterDescriptions();
    
private:
    
#pragma mark Members
    // ============== Members ======================
    VariationsDict          variationsByKey;
    VariationsDict          variationsByName;
    ParametersDict          variationParametersByKey;
    ParametersDict          variationParametersByAlias;
    XformIndexesDict        variationsByXformIndex;
    TransformPropertiesDict transformPropertiesByKey;
    StringsVector           transformProperties;
    StringsSet              namesInUse;
    
public:
    std::string          uuid;
    std::string          defaultVariation;
    std::string          name;
    std::string          version;
//    AnimatableMetadata  *animatableMetadata;
    bool                 amLibrary;
    bool                 inUse;
    bool                 is3DCapable;
    bool                 amTemporary; // used to test compiles with - dont keep
    std::string          variationStructName;
    
private:
    static SharedVariationSet factoryLibrary;
    static SharedVariationSet myLibrary;
    static SharedVariationSet factoryPlusMyLibrary;
    
public:
    static SharedVariationSet currentVariationSet;
    static VariationSetsDict variationSets;
    static VariationSetsDict variationSuperSets;
   
};

#endif /* VariationSet_hpp */
