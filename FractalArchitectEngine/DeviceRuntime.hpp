//
//  DeviceRuntime.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/15/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef DeviceRuntime_hpp
#define DeviceRuntime_hpp

#include <vector>
#include <memory>
#include <mutex>
#include <chrono>
#include "DefinesOCL.h"
#include "Exports.hpp"
#include "common.hpp"

using namespace FA;

class DeviceContext;
class DeviceRuntime;
class ElectricSheepStuff;
class FlameExpanded;
class RenderState;
class VariationSet;

using uint                = unsigned int;
using uchar               = unsigned char;
using SharedDeviceContext = std::shared_ptr<DeviceContext>;
using SharedRuntime       = std::shared_ptr<DeviceRuntime>;
using SharedVariationSet  = std::shared_ptr<VariationSet>;
using StringsVector       = std::vector<std::string>;
using SharedRenderState   = std::shared_ptr<RenderState>;
using BoolsVector         = std::vector<bool>;
using SharedElc           = std::shared_ptr<ElectricSheepStuff>;
using SharedFE            = std::shared_ptr<FlameExpanded>;

using namespace std::chrono;

const unsigned warpsPerBlock       = 2;
const unsigned BLOCKSX_WARPSIZE_32 = 16;   // for WARPSIZE = 32
const unsigned CLEAR_FREQUENCY     = 200; // iterations per pixel when we add the render target to accum buffer, then clear the render target buffer
const uint     FUSE_ITERATIONS     = 30;

const uint numPointsWarp16  =  NUM_POINTS_WARPSIZE_32/32 * 16;
const uint numPointsWarp32  =  NUM_POINTS_WARPSIZE_32/32 * 32;
const uint numPointsWarp64  =  NUM_POINTS_WARPSIZE_32/32 * 64;
const uint numPointsWarp128 =  NUM_POINTS_WARPSIZE_32/32 * 128;


class DeviceRuntime : public std::enable_shared_from_this<DeviceRuntime> {
    friend class RenderTile;
protected:
    struct Flame** g_pFlame;
    char *         hostAccumBuffer;
    uint           numDevices;
    uint numBatches;
    
    uint xDim;
    uint yDim;
    uint resampledXdim;
    uint resampledYdim;
    
    uint *BLOCKDIM;
    uint *BLOCKSX;
    uint *BLOCKSY;
    uint *NUMPOINTS;
    uint *renderedBatches;
    std::mutex clearMutex;

public:
    
    DeviceRuntime(uint numBatches, uint _numDevices);
    virtual ~DeviceRuntime();
    
    bool checkRenderedBatchCounts();
    
//    virtual uint fuseIterations() = 0;
    
    static uint ITERATIONS();

    size_t maxDeviceCountFor(uint _numBatches, SharedDeviceContext & deviceContext);
    
    virtual void clearRenderTargetForSelectedDeviceNum(uint selectedDeviceNum,
                                                       uint maxWorkGroupSize,
                                                       const SharedVariationSet & variationSet) = 0;
    
    virtual Point finishFrame(SharedRenderState &renderState,
                              void *image,
                              float *backgroundImage,
                              size_t renderedBatches,
                              const SharedVariationSet & variationSet,
                              SharedElc &elc,
                              SharedFE & flameExpanded,
                              duration<double> & deDuration) = 0;
    
    virtual size_t numSelectedDevices() = 0;
    
    virtual void addToAccumBufferForSelectedDeviceNum(uint selectedDeviceNum) = 0;
    
    virtual void makeVarUsageListsForSelectedDeviceNum(uint selectedDeviceNum,
                                                       Flame &flm,
                                                       const SharedVariationSet & variationSet) = 0;
    
    virtual void fuseIterations(uint _fuseIterations, uint selectedDeviceNum) = 0;
    
    virtual void renderBatchForSelectedDeviceNum(uint selectedDeviceNum,
                                                 SharedElc &elc,
                                                 float epsilon,
                                                 uint maxWorkGroupSize,
                                                 const SharedVariationSet & variationSet,
                                                 uint batchNum) = 0;
    
    virtual void startCuda(SharedRenderState & renderState, uint selectedDeviceNum) = 0;
    
    virtual void runFuseForSelectedDeviceNum(uint selectedDeviceNum,
                                             float epsilon,
                                             uint maxWorkGroupSize,
                                             const SharedVariationSet & variationSet) = 0;
    
    virtual void startFrameForSelectedDeviceNum(uint selectedDeviceNum,
                                                uint maxWorkGroupSize,
                                                const SharedVariationSet & variationSet,
                                                duration<double> & buildTime) = 0;
    
    virtual void grabFrameFromSelectedDeviceNum(uint selectedDeviceNum) = 0;
    
    virtual void synchronizeQueueForSelectedDeviceNum(uint selectedDeviceNum) = 0;
    
    virtual void mergeFramesToSelectedDeviceNum(uint selectedDeviceNum,
                                                uint fromSelectedDeviceNum,
                                                uint maxWorkGroupSize,
                                                const SharedVariationSet & variationSet,
                                                SharedRenderState & renderState) = 0;
    
    virtual void stopCudaForSelectedDeviceNum(uint selectedDeviceNum) = 0;
    
    static size_t totalIterationsForBatches(size_t batches);
    static size_t totalIterationsForBatches(size_t batches, size_t blocksY);
    
    // convert quality to batches
    static size_t batchesPerTileForDesiredAdaptive(double desiredQuality,
                                                   double width,
                                                   double height,
                                                   size_t tiles,
                                                   size_t & blocksY,
                                                   uint perPointFuseIterations);
    
    static size_t batchesTotalForDesired(double desiredQuality,
                                         size_t bladeCount,
                                         double width,
                                         double height,
                                         size_t tiles,
                                         size_t & blocksY,
                                         uint perPointFuseIterations);
    
    static double actualQualityWith(size_t batchesPerTile,
                                    double width,
                                    double height,
                                    size_t tiles,
                                    uint perPointFuseIterations);
    
    static double actualQuality(double desiredQuality,
                                size_t bladeCount,
                                double width,
                                double height,
                                size_t tiles,
                                uint perPointFuseIterations);
    
    static double actualQuality(double desiredQuality,
                                size_t bladeCount,
                                double width,
                                double height,
                                size_t tiles,
                                size_t blocksY,
                                uint perPointFuseIterations);
    
    static size_t adaptiveBlockYDim(double desiredQuality,
                                    double width,
                                    double height,
                                    size_t tiles,
                                    uint perPointFuseIterations);
    
    void copyFlame(struct Flame * flame, uint selectedDeviceNum);
    
    std::string flameReport(uint selectedDeviceNum);
};

#endif /* DeviceRuntime_hpp */
