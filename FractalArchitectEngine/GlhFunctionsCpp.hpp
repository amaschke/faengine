//
//  GlhFunctionsCpp.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/22/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//

#ifndef GlhFunctionsCpp_h
#define GlhFunctionsCpp_h

extern "C" void glhLookAtf2( float *matrix, float *eyePosition3D,
                            float *center3D, float *upVector3D );
extern "C" void glhLookAtLHf2( float *matrix, float *eyePosition3D,
                              float *center3D, float *upVector3D );
extern "C" void glhTranslatef2(float *matrix, float x, float y, float z);
extern "C" void glhPerspectiveInfiniteFarPlanef2(float *matrix, float fovyInDegrees, float aspectRatio, float znear);
extern "C" void glhOrthofInfiniteFarPlane(float *matrix, float left, float right, float bottom, float top);
extern "C" void glhOrthof2(float *matrix, float left, float right, float bottom, float top, float znear, float zfar);
extern "C" void MultiplyMatrices4by4OpenGL_FLOAT(float *result, float *matrix1, float *matrix2);
extern "C" int glhProjectf(float objx, float objy, float objz,
                           float *modelview, float *projection,
                           int *viewport,
                           float *windowCoordinate);
extern "C" int glhUnProjectf(float winx, float winy, float winz,
                             float *modelview, float *projection,
                             int *viewport,
                             float *objectCoordinate);
extern "C" void glhRotateAboutZf2(float *matrix, float angleInRadians);
extern "C" void glhTransposeMatrixf2(float *result, float *m);


extern "C" void glhQuaternionIdentityf(float *quat);
extern "C" int glhQuaternionIsIdentityf(float *quat);
extern "C" int glhQuaternionIsPuref(float *quat);
extern "C" void glhQuaternionConjugatef(float *quat);
extern "C" void glhQuaternionMagnitudef(float *magnitude, float *quat);
extern "C" void glhQuaternionMagnitudeSquaredf(float *magnitude, float *quat);
extern "C" void glhQuaternionDotProductf(float *result, float *quat1, float *quat2);
extern "C" void glhQuaternionExpf(float *outquat, float *quat);
extern "C" void glhQuaternionLnf(float *outquat, float *quat);
extern "C" void glhQuaternionNormalizef(float *quat);
extern "C" void glhQuaternionMultiplyf(float *outquat, float *quat1, float *quat2);
extern "C" void glhQuaternionQuatToRotationMatrixf(float *matrix, float *quat);
extern "C" void glhQuaternionQuatToRotationMatrixTf(float *matrix, float *quat);
extern "C" void glhQuaternionRotationMatrixToQuatf(float *quat, float *matrix);
extern "C" void glhQuaternionQuatToAxisAndAnglef(float *axis, float *angleInRadians, float *quat);
extern "C" void glhQuaternionAxisAndAngleToQuatf(float *quat, float *axis, float *angleInRadians);
extern "C" void glhQuaternionEulerAnglesToQuatf(float *quat, float roll, float pitch, float yaw);
extern "C" void glhQuaternionSLERPf(float *outquat, float *quat1, float *quat2, float t);
extern "C" void glhQuaternionSQUADSplinef(float *outquat, float *quat1, float *quat2, float *quat3, float *quat4, float t, int amFirst, int amLast);
extern "C" int glhExtractLookAtFromMatrixf2(float *matrix, float *eyePosition3D, float *center3D, float *upVector3D);
extern "C" void NormalizeVectorFLOAT_2(float *pvector);
extern "C" float DistanceSquareBetweenTwoPoints_FLOAT(float *ppoint1, float *ppoint2);

#endif /* GlhFunctionsCpp_h */
