//
//  DeviceProgram.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/14/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "DeviceProgram.hpp"
#include "DeviceKind.hpp"
#include "Utilities.hpp"
#include "VariationSet.hpp"

#ifdef _WIN32
#include "WindowsInterface.hpp"
#else
#include "CocoaInterface.h"
#endif

#include <fstream>
#include <string.h>

DeviceProgram::DeviceProgram(SharedDeviceContext ctx, SharedDeviceKind _deviceKind, const SharedVariationSet & _variationSet)
:
    variationSet(_variationSet),
    context(ctx),
    deviceKind(_deviceKind),
    uuid(),
    bitcodeFilename(),
    buildTime(0)
{
    
    uuid     = _deviceKind->processDeviceKind();
#ifdef _WIN32
	basename = "Flam4_Kernal";
#else
	basename = _deviceKind->isCPU() ? "Flam4_KernalCPU" : "Flam4_Kernal";
#endif
}

std::string DeviceProgram::cachePath()
{
    return  ::cachePath();
}

std::string DeviceProgram::bundleIdentifier()
{
    return "com.centcom.FractalArchitectEngine";
}

void DeviceProgram::setupFAEngineCaches()
{
    // create VariationSets directory in the FractalArchitect's Application Support directory structure
    const char * expandedVarSetsPath = ::expandTildeInPath(DeviceProgram::cachePath().c_str());
    
    if (! fileExists(expandedVarSetsPath))
		mkpath(expandedVarSetsPath, 0777);
}

Json::Value DeviceProgram::readDevicesJson()
{
    Json::Reader reader;
    Json::Value  json;
    
    const std::string &cachePath  = DeviceProgram::cachePath();
    std::string jsonPath          = cachePath + "/" + "devices.json";
    const char * expandedJsonPath = ::expandTildeInPath(jsonPath.c_str());
    
    std::ifstream ifstr(expandedJsonPath);
    if (ifstr.fail()) {
        return json;
    }
    std::string contents((std::istreambuf_iterator<char>(ifstr)),
                         std::istreambuf_iterator<char>());
    
    if ( !reader.parse(contents, json) )
        throw std::runtime_error("Failed to parse Json");
    return json;
}

void DeviceProgram::writeDevicesJson(Json::Value & json)
{
    const std::string &cachePath        = DeviceProgram::cachePath();
    std::string jsonPath                = cachePath + "/" + "devices.json";
    
    jsonPath = ::expandTildeInPath(jsonPath.c_str());

    std::ofstream ofstr(jsonPath);
    if (ofstr.fail()) {
        std::string msg = string_format("Failed to open file: %s", jsonPath.c_str());
        throw std::runtime_error(msg);
    }
   
    Json::StyledWriter writer;
    std::string output = writer.write(json);
    
    ofstr << output;
}

std::string DeviceProgram::pathForBinariesFolderForUuid(const std::string & _uuid)
{
    const std::string &cachePath        = DeviceProgram::cachePath();
    std::string path         = cachePath + "/" + _uuid + "/variationSets/" + variationSet->uuid;
    return  ::expandTildeInPath(path.c_str());
}

std::string DeviceProgram::pathForBinaryForUuid(const std::string & _uuid)
{
    const std::string &cachePath        = DeviceProgram::cachePath();
    std::string binaryDirectory         = cachePath + "/" + _uuid + "/variationSets/" + variationSet->uuid;
    std::string path                    = binaryDirectory + "/" + basename + ".cl.bin";
    return  ::expandTildeInPath(path.c_str());
}

std::vector<char> DeviceProgram::binaryContentsForUuid(const std::string & _uuid)
{
    std::string path = pathForBinaryForUuid(_uuid);
    
    std::ifstream stream(path, std::ios::in | std::ios::binary);
    if (stream.fail()) {
        return std::vector<char>();
    }
    
    std::vector<char> contents((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
    return contents;
}

void DeviceProgram::writeBinaryForUuid(const std::string &_uuid, const char * data, size_t length)
{
    std::string path = pathForBinariesFolderForUuid(_uuid);
    
    if ((mkpath(path.c_str(), 0777) == -1)  && (errno != EEXIST)) {
        std::string msg = "Failed to open file: " + pathForBinaryForUuid(_uuid) + "  " + strerror(errno);
        throw std::runtime_error(msg);
    }
    std::string binaryPath = path + "/" + basename + ".cl.bin";
    
    std::ofstream stream(binaryPath, std::ios::out | std::ios::binary);
    if (stream.fail()) {
        std::string msg = string_format("Failed to open file: %s", binaryPath.c_str());
        throw std::runtime_error(msg);
    }
    stream.write(data, length);
}

std::string DeviceProgram::sourceContents(enum GpuPlatformUsed gpuPlatformInUse)
{
    const std::string &sourceURL = variationSet->kernelURL(deviceKind.lock()->isGPU(), gpuPlatformInUse);
    
    std::ifstream ifstr(sourceURL);
    if (ifstr.fail()) {
        std::string msg = string_format("Failed to open file: %s", sourceURL.c_str());
        throw std::runtime_error(msg);
    }
    std::string contents((std::istreambuf_iterator<char>(ifstr)),
                         std::istreambuf_iterator<char>());
    return contents;
}
