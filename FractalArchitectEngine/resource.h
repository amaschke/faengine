//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Resource.rc
//
#define IDR_LIBRARY_XML1                101
#define IDR_LIBRARY_XML                 101
#define IDR_ADOBERGB1                   105
#define IDR_ADOBERGB                    105
#define IDR_SRGB                        110
#define IDR_PROPHOTO                    111
#define IDR_WIDEGAMUT                   112
#define IDR_CL_KERNEL_CPU               113
#define IDR_CL_KERNEL_GPU               114
#define IDR_MY_LIBRARY_XML              115
#define IDR_PALETTES                    116
#define IDR_CUDA_TEMPLATE1              118

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        119
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
