//
//  SpatialFilter.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/13/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef SpatialFilter_hpp
#define SpatialFilter_hpp

#include <string>
#include <unordered_map>

#include "Exports.hpp"
#include "ElectricSheepStuff.hpp"

struct SpatialFilter;

using FilterFunction  = float (*)(float t);
using UniqueFilter    = std::unique_ptr<SpatialFilter>;
using UniqueFilterMap = std::unordered_map<int, UniqueFilter>;


struct FAENGINE_API SpatialFilter  {
    std::string    name;
    FilterFunction func;
    float          support;
    
    static UniqueFilterMap filters;
    
    SpatialFilter(std::string & _name, FilterFunction _func, float _support);
    SpatialFilter(const char * _name, FilterFunction _func, float _support);
    
    static void initialize();
    
    static std::string filterNameForFilterShapeEnum(enum FilterShape filterShape);
    static uint filterWidthFor(struct ElectricSheepStuff & elc,  uint maxFilterWidth);
    
    static float maxFilterStrengthFor(enum FilterShape filterShape, uint supersample, uint maxFilterWidth);
    static float maxFilterStrengthFor(struct ElectricSheepStuff & elc, uint maxFilterWidth);

    static float *createFilter(struct ElectricSheepStuff &elc, uint *pFilterWidth,  uint maxFilterWidth);
};

#endif /* SpatialFilter_hpp */
