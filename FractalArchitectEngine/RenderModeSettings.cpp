//
//  RenderModeSettings.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/7/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "RenderModeSettings.hpp"

enum RenderMode RenderModeSettings::renderModeFromString(const std::string &renderModeStr)
{
    if (renderModeStr == "compositeOverColor")
        return renderModeCompositeOverColor;
    else if (renderModeStr  =="compositeOverImage")
        return renderModeCompositeOverImage;
    else if (renderModeStr ==  "transparent")
        return renderModeTransparent;
    else
        return renderModeNormal;
}



#ifdef __ORIGINAL__

//
//  RenderModeSettings.m
//  FractalArchitect
//
//  Created by Steven Brodhead on 1/8/16.
//
//

#include "RenderModeSettings.h"
#include "Enums.h"
#include "FlameMetaData.h"
#include "NSString+PrettyFloat.h"

@implementation RenderModeSettings
@synthesize renderMode, backgroundImage, compositeColor, imagePath, estimatorRadius, gamma, alphaGamma;

- (id)init
{
    self = [super init];
    if (self) {
        renderMode      = renderModeNormal;
        compositeColor  = [[NSColor whiteColor] retain];
        backgroundImage = nil;
        imagePath       = nil;
        estimatorRadius = 7;
        gamma           = 4.f;
        alphaGamma      = 3.f;
    }
    return self;
}

- (id)initFromFlameMeta:(struct FlameMetaData *)meta
{
    self = [super init];
    if (self) {
        renderMode = meta->renderMode;
        [self setCompositeColor:meta->compositeColor];
        [self setBackgroundImage:meta->backgroundImage];
        [self setImagePath:meta->imagePath];
        estimatorRadius = 7;
        gamma           = 4.f;
        alphaGamma      = 3.f;
    }
    return self;
}

+ (RenderModeSettings *)settings
{
    return [[[RenderModeSettings alloc] init] autorelease];
}

+ (RenderModeSettings *)settingsFromFlameMeta:(struct FlameMetaData *)meta
{
    return [[[RenderModeSettings alloc] initFromFlameMeta:meta] autorelease];
}

- (void)dealloc
{
    [backgroundImage release];
    [imagePath release];
    [compositeColor release];
    
    [super dealloc];
}

- (NSDictionary *)propertyList
{
    NSString *compositeColorStr = nil;
    if (compositeColor) {
        if ([[compositeColor colorSpaceName] isEqualToString:@"NSCalibratedWhiteColorSpace"]) {
            float white = [compositeColor whiteComponent];
            compositeColorStr = [NSString stringWithFormat:@"%f %f %f", white, white, white];
        }
        else {
            compositeColorStr = [NSString stringWithFormat:@"%f %f %f",
                                 [compositeColor redComponent], [compositeColor greenComponent], [compositeColor blueComponent]];
        }
    }
    
    // if no background image, dont save imagePath this as reopening the file will add an unwanted image
    NSString *imagePathStr = nil;
    if (backgroundImage && imagePath) {
        imagePathStr = [imagePath lastPathComponent]; // we want just the file name for image inside the file package
    }
    NSString *renderModeStr = [RenderModeSettings renderModeDescription:renderMode];
    NSDictionary *plist =
    [NSDictionary dictionaryWithObjectsAndKeys:
     compositeColorStr ? compositeColorStr : @"",            @"compositeColor",
     imagePathStr ? imagePathStr : @"",                      @"imagePath",
     renderModeStr,                                          @"renderMode",
     [NSNumber numberWithUnsignedInt:estimatorRadius],       @"estimatorRadius",
     [NSNumber numberWithFloat:gamma],                       @"gamma",
     [NSNumber numberWithFloat:alphaGamma],                  @"alphaGamma",
     nil];
    return plist;
}

+ (NSDictionary *)propertyListForXmlElement:(NSXMLElement *)ele
{
    NSString *compositeColor  = [[ele attributeForName:@"compositeColor"] stringValue];
    NSString *imagePath       = [[ele attributeForName:@"imagePath"] stringValue];
    NSString *renderMode      = [[ele attributeForName:@"renderMode"] stringValue];
    NSString *estimatorRadius = [[ele attributeForName:@"estimatorRadius"] stringValue];
    NSString *gamma           = [[ele attributeForName:@"gamma"] stringValue];
    NSString *alphaGamma      = [[ele attributeForName:@"alphaGamma"] stringValue];
    
    NSDictionary *plist =
    [NSDictionary dictionaryWithObjectsAndKeys:
     compositeColor ? compositeColor : @"",              @"compositeColor",
     imagePath ? imagePath : @"",                        @"imagePath",
     renderMode ? renderMode : @"normal",                @"renderMode",
     [NSNumber numberWithUnsignedInt:[estimatorRadius intValue]], @"estimatorRadius",
     gamma ? gamma : @"4.",                              @"gamma",
     alphaGamma ? alphaGamma : @"3.",                    @"alphaGamma",
     nil];
    return plist;
}

- (NSXMLElement *)xmlElement
{
    NSXMLElement *ele = [NSXMLNode elementWithName:@"renderModeSettings"];
    
    NSString *compositeColorStr = @"";
    if (compositeColor) {
        if ([[compositeColor colorSpaceName] isEqualToString:@"NSCalibratedWhiteColorSpace"]) {
            float white = [compositeColor whiteComponent];
            compositeColorStr = [NSString stringWithFormat:@"%f %f %f", white, white, white];
        }
        else {
            compositeColorStr = [NSString stringWithFormat:@"%f %f %f",
                                 [compositeColor redComponent], [compositeColor greenComponent], [compositeColor blueComponent]];
        }
    }
    
    // if no background image, dont save imagePath this as reopening the file will add an unwanted image
    NSString *imagePathStr = @"";
    if (backgroundImage && imagePath) {
        imagePathStr = [imagePath lastPathComponent]; // we want just the file name for image inside the file package
    }
    NSString *renderModeStr = [RenderModeSettings renderModeDescription:renderMode];
    NSString *gammaStr      = [NSString stringWithFormat:@"%@", [NSString prettyFloat:gamma]];
    NSString *alphaGammaStr = [NSString stringWithFormat:@"%@", [NSString prettyFloat:alphaGamma]];
    
    [ele addAttribute:[NSXMLNode attributeWithName:@"compositeColor" stringValue:compositeColorStr]];
    [ele addAttribute:[NSXMLNode attributeWithName:@"imagePath" stringValue:imagePathStr]];
    [ele addAttribute:[NSXMLNode attributeWithName:@"renderMode" stringValue:renderModeStr]];
    [ele addAttribute:[NSXMLNode attributeWithName:@"estimatorRadius"
                                       stringValue:[NSString stringWithFormat:@"%u", estimatorRadius]]];
    [ele addAttribute:[NSXMLNode attributeWithName:@"gamma" stringValue:gammaStr]];
    [ele addAttribute:[NSXMLNode attributeWithName:@"alphaGamma" stringValue:alphaGammaStr]];
    return ele;
}

// split a string using its embedded whites space - drops out any zero length strings
+ (NSMutableArray *)splitOnSpaces:(NSString *)str
{
    NSArray *arr         = [str componentsSeparatedByString:@" "];
    NSMutableArray *copy = [[NSMutableArray alloc] initWithCapacity: [arr count]];
    int i = 0;
    for (NSString * str in arr) {
        if ([str length] > 0) {
            [copy addObject:[arr objectAtIndex:i++]];
        }
    }
    return [copy autorelease];
}

- (id)initWithPropertyList:(NSDictionary *)plist
document:(NSDocument *)document
{
    self = [super init];
    if (self) {
        NSString *_compositeColor       = [plist objectForKey:@"compositeColor"];
        if ([_compositeColor isEqualToString:@""])
            _compositeColor = nil;
            NSString *_imagePath            = [plist objectForKey:@"imagePath"];
            if ([_imagePath isEqualToString:@""])
                _imagePath = nil;
                NSString *_renderMode           = [plist objectForKey:@"renderMode"];
                if (!_renderMode || [_renderMode isEqualToString:@""])
                    _renderMode = @"normal";
                    NSString *_gamma                = [plist objectForKey:@"gamma"];
                    NSString *_alphaGamma           = [plist objectForKey:@"alphaGamma"];
                    
                    gamma      = [_gamma floatValue];
                    alphaGamma = [_alphaGamma floatValue];
                    
                    renderMode = [RenderModeSettings renderModeFromString:_renderMode];
                    
                    if (_compositeColor) {
                        NSMutableArray *compositeColorArray = [RenderModeSettings splitOnSpaces:_compositeColor];
                        
                        float r =  [[compositeColorArray objectAtIndex:0] floatValue];
                        float g =  [[compositeColorArray objectAtIndex:1] floatValue];
                        float b =  [[compositeColorArray objectAtIndex:2] floatValue];
                        
                        [self setCompositeColor:[[NSColor colorWithCalibratedRed:r green:g blue:b alpha:1.f] retain]];
                    }
                    else
                        [self setCompositeColor:nil];
        
        [self setImagePath:_imagePath];
        
        if (imagePath) {
            NSURL *imageURL = nil;
            if ([[imagePath pathComponents] count] == 1)
                imageURL = [[document fileURL] URLByAppendingPathComponent:imagePath];
                else
                    imageURL = [NSURL fileURLWithPath:imagePath];
                    [self setBackgroundImage:[[[NSImage alloc] initWithContentsOfURL:imageURL] autorelease]];
        }
        else
            backgroundImage = nil;
            }
    return self;
}

+ (NSString *)renderModeDescription:(enum RenderMode)renderMode
{
    switch (renderMode) {
        default:
        case renderModeNormal:
            return @"normal";
            break;
        case renderModeCompositeOverColor:
            return @"compositeOverColor";
            break;
        case renderModeCompositeOverImage:
            return @"compositeOverImage";
            break;
        case renderModeTransparent:
            return @"transparent";
            break;
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ %@", [super description], [RenderModeSettings renderModeDescription:renderMode]];
}

@end

#endif