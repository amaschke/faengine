//
//  DeviceContext.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/14/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef DeviceContext_hpp
#define DeviceContext_hpp

#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>
#include <vector>
#include "Exports.hpp"

class DeviceContext;
class DeviceKind;
class DeviceUsage;
class VariationSet;

enum DeviceType {
    DeviceTypeCPU = 2,
    DeviceTypeGPU = 4,
};

using uint                = unsigned int;
using BoolsVector         = std::vector<bool>;
using CountsVector        = std::vector<uint>;
using StringsVector       = std::vector<std::string>;
using SharedVariationSet  = std::shared_ptr<VariationSet>;
using SharedDeviceContext = std::shared_ptr<DeviceContext>;
using WeakDeviceContext   = std::weak_ptr<DeviceContext>;
using SharedDeviceKind    = std::shared_ptr<DeviceKind>;
using SharedDeviceUsage   = std::shared_ptr<DeviceUsage>;
using DeviceKindDict      = std::unordered_map<std::string, SharedDeviceKind>;
using DeviceUsageVector   = std::vector<SharedDeviceUsage>;
using DevicePredicate     = bool (*)(const SharedDeviceUsage &);


class FAENGINE_API DeviceContext : public std::enable_shared_from_this<DeviceContext> {
    friend class ClDeviceKind;
    friend class Flam4ClRuntime;
protected:
    void *              *deviceIDs;
    void *              *subDeviceIDs;
    uint                 subDeviceCount;
    bool                *available;
    bool                *quarantined;
    bool                 canUseGPU;
    StringsVector        deviceNames;
    StringsVector        deviceVendor;
    DeviceKindDict       deviceKinds;
    std::mutex           mutex;
    
public:
    DeviceUsageVector    selectedDevices;
    DevicePredicate      predicate;
    uint                 numDevices;
    DeviceUsageVector    deviceUsages;
    SharedVariationSet   defaultVariationSet;
    bool                 singleDeviceAllowed;
    std::string          platformVersion;
    bool                 canPartitionCPU;
    uint                 cpuDeviceIndex;
    CountsVector         computeUnitCounts;
    uint                 computeUnitCountIndex;
	bool                 forceRebuild; // if true, the OpenCl programs will be built from source, ignoring pre-built binaries
    
protected:
    static std::string gpuVendor;
    static uint cpuFuseIterations;
    static uint gpuFuseIterations;
    static float finalXformFuseMultiplier;
    
public:
    DeviceContext(bool forceRebuild, SharedVariationSet defaultVariationSet, bool _singleDeviceAllowed, bool _canUseGPU, bool ignoreQuarantine);
    virtual ~DeviceContext();
    
    virtual bool        createDeviceKinds() = 0;
    virtual std::string selectedDevicesDescription() = 0;
    
    static uint getCpuFuseIterations();
    static uint getGpuFuseIterations();
    static void setCpuFuseIterations(uint _cpuFuseIterations);
    static void setGpuFuseIterations(uint _gpuFuseIterations);
    
    static float getFinalXformFuseMultiplier();
    static void  setFinalXformFuseMultiplier(float _finalXformFuseMultiplier);
    
    bool         getCanUseGPU();
    virtual bool hasDiscreteGPU() = 0;
    
    SharedDeviceKind deviceKindForDevice(size_t i);
    std::string      uuidForDevice(size_t i);
    std::string      vendorForDevice(size_t i);
    std::string      nameForDevice(size_t i);

    SharedDeviceUsage deviceUsageForDeviceID(void *deviceID);
    size_t deviceNumForDeviceID(void * deviceID);
    
    size_t indexOfDeviceUsage(const SharedDeviceUsage & deviceUsage);

    size_t selectedDeviceCount();
    size_t selectedDeviceNumForDeviceID(void *deviceID);
    size_t selectedDeviceNumForDeviceNum(uint deviceNum);
    
    void rearrangeSelectedDevices();
    size_t deviceNumForSelectedDeviceNum(uint selectedDeviceNum);
    
    std::string      uuidForSelectedDevice(size_t selectedDeviceNum);
    SharedDeviceKind deviceKindForSelectedDevice(size_t selectedDeviceNum);
    uint             warpSizeForSelectedDevice(size_t selectedDeviceNum);
    uint             fuseIterationsForSelectedDevice(size_t selectedDeviceNum);
    size_t           maxMemAllocSizeForSelectedDevice(size_t selectedDeviceNum);
    
    uint   warpSizeForDevice(size_t i);
    size_t globalMemSizeForDevice(size_t i);
    size_t maxMemAllocSizeForDevice(size_t i);
    
    size_t maxWorkGroupSizeForDevice(size_t i);
    size_t maxWorkGroupSizeForKernelsForDevice(size_t i, const SharedVariationSet &variationSet);
    size_t maxWorkGroupSizeForKernelsForSelectedDevice(size_t selectedDeviceNum, const SharedVariationSet &variationSet);
    
    std::string getGpuVendor();
    size_t firstUsableDeviceNum();
    
    virtual bool oclDeviceAllocCheckForDeviceNum
        (size_t deviceNum,
         float & area,
         size_t & amountAlloced,
         uint warpSize,
         uint xformCount,
         uint supersample) = 0;
    virtual bool oclDeviceCheckForEnoughMemoryArea(size_t area,
                                                   uint xformCount,
                                                   uint supersample) = 0;
    bool checkOclGPUMemAvailable(size_t & memoryAvailable,
                                         float & area,
                                         float niceFactor,
                                         uint xformCount,
                                         uint supersample);
    virtual bool checkOclGPUMemAvailable(size_t & memoryAvailable,
                                         float & area,
                                         float niceFactor,
                                         uint warpSize,
                                         uint xformCount,
                                         uint supersample,
                                         uint deviceNum) = 0;

    
    void refreshProgramForVariationSet(const SharedVariationSet &variationSet, bool rebuild);
    bool makeTestProgramForVariationSet(const SharedVariationSet & variationSet);
    
    bool selectedDevicesHasGPU();
    void *firstCPUDeviceID();
    uint maxFuseIterationsFromSelectedDevices(bool hasFinalXform);
    uint maxEstimatorRadiusForSelectedDevices();
    
    virtual void refreshMemoryStats() = 0;
    void unStageUsage();
    void setCanUseGPU(bool _canUseGPU);
    virtual void tempLicenseForDeviceID1(void *deviceID1,  void *deviceID2, uint deviceCount);
};

#endif /* DeviceContext_hpp */
