//
//  RenderState.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/24/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef RenderState_hpp
#define RenderState_hpp

#include "Exports.hpp"
#include "RenderEnums.hpp"
#include <memory>
#include <chrono>

class DeviceContext;
class DeviceRuntime;
class RenderInfo;
class ImageRepTileArray;
class FlameTileArray;

using uint                = unsigned int;
using SharedDeviceContext = std::shared_ptr<DeviceContext>;
using SharedRuntime       = std::shared_ptr<DeviceRuntime>;
using SharedRenderInfo    = std::shared_ptr<RenderInfo>;
using SharedRepTileArray  = std::shared_ptr<ImageRepTileArray>;
using SharedFlmTileArray  = std::shared_ptr<FlameTileArray>;

using namespace std::chrono;


struct FAENGINE_API RenderState {
    RenderState(duration<double> &cummRenderTime, float &fusedFraction, float  &retainedFraction);
    RenderState()  = delete;
    ~RenderState() = default;
    
    // ========= Members ==============================
    
    SharedDeviceContext deviceContext;
    SharedRenderInfo    renderInfo;
    SharedRepTileArray  imageArray;
    SharedFlmTileArray  tiles;
    SharedRuntime       runtime;
//    void               *priorState;
    uint                tileIndex;
    uint                maxWorkGroupSize;
    uint                selectedDeviceNum;       // device specific
    duration<double>   &cummRenderTime;          // device specific
    float              &fusedFraction;           // device specific
    float              &retainedFraction;        // device specific
    float               priorFraction;           // device specific
    enum PriorUse       usePriorFuseFraction;
    enum ProceedType    proceed;
    
    //    NSObject<ProgressListenerProtocol> *progressListener;
};

#endif /* RenderState_hpp */
