//
//  ColorNode.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/8/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef ColorNode_hpp
#define ColorNode_hpp

#include "Exports.hpp"
#include "common.hpp"
#include <string>

// color map node
struct FAENGINE_API ColorNode {
    Color  color;		// this color nodes colof value - assumes RGB colorspace
    float  location;		// offset of color node between 0.0 and 1.0
    
    ColorNode(const Color &_color, float _location);
    
    bool operator==(const ColorNode &c);
    bool operator!=(const ColorNode &c)  { return ! operator==(c); }

    std::string description();
    std::string lineDescriptionForColorNodeCount(size_t colorNodeCount);
    
    float red();
    float green();
    float blue();
    
};

#endif /* ColorNode_hpp */
