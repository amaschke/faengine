//
//  DeviceKind.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/14/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef DeviceKind_hpp
#define DeviceKind_hpp

#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <chrono>
#include "Exports.hpp"
#include "DeviceContext.hpp"

enum DeviceKindJsonStatus {
    jsonOK       = 0,
    noJsonFile   = 1,
    noJsonEntry  = 2,
    binaryInvalid = 3,
};

class Context;
class DeviceKind;
class DeviceProgram;
class VariationSet;

using uint                = unsigned int;
using SharedDeviceKind    = std::shared_ptr<DeviceKind>;
using SharedDeviceProgram = std::shared_ptr<DeviceProgram>;
using WeakDeviceKind      = std::weak_ptr<DeviceKind>;
using StringsSet          = std::unordered_set<std::string>;
using PointerVector       = std::vector<void *>;
using IntVector           = std::vector<int>;
using ProgramsDict        = std::unordered_map<std::string, SharedDeviceProgram>;
using SharedVariationSet  = std::shared_ptr<VariationSet>;

using namespace std::chrono;

struct FAENGINE_API DeviceKind  : public std::enable_shared_from_this<DeviceKind> {
    std::string         kind;
    std::string         vendor;
    std::string         deviceName;
    PointerVector       clDeviceInstances;
    IntVector           cudaDeviceInstances;
    bool                rebuild;
    ProgramsDict        programs;
    WeakDeviceContext   deviceContext;
    
    // properties for this device Kind
    uint                maxWorkgroupDims;
    size_t             *workGroupDims;
    size_t              maxWorkGroupSize;
    bool                deviceImageSupport;
    size_t              maxMemAllocSize;
    size_t              globalMemSize;
    bool                unifiedMemory;
    size_t              maxConstantBufSize;
    size_t              localMemSize;
    uint                maxComputeUnits;
    uint                warpSize;
    
    DeviceKind(const std::string & _kind, const std::string & _vendor, const SharedDeviceContext & _deviceContext);
    virtual ~DeviceKind();
    
    std::string summary();
    void * deviceIDForInstanceIndex(size_t index);
    uint maxDensityEstimationRadius();
    
    virtual bool createContext() = 0;
    virtual void deviceKindProperties() = 0;
    virtual void setupSubDevicesFor(void * cpuDeviceID) = 0;
    
    virtual std::string getDeviceVendor() = 0;
    virtual std::string getDeviceName() = 0;
    
    virtual bool isCPU() = 0;
    virtual bool isGPU() = 0;
    virtual bool hasLocalMemoryOnDevice() = 0;
    virtual const std::string processDeviceKind() = 0;
    
    size_t numClDevices()   { return clDeviceInstances.size(); }
    size_t numCudaDevices() { return cudaDeviceInstances.size(); }
        
    std::string uuid();
    
#pragma mark Variation Set Support
    
    virtual DeviceProgram * makeProgramFromSource      (const SharedVariationSet &variationSet, bool forceRebuild, duration<double> & buildTime);
    virtual DeviceProgram * programForVariationSetUuid (const std::string &variationSetUuid, duration<double> & buildTime);
    virtual DeviceProgram * programForVariationSet     (const SharedVariationSet & variationSet, duration<double> & buildTime);
    DeviceProgram *  firstProgramForVariationSet(const SharedVariationSet &variationSet, duration<double> & buildTime);
    DeviceProgram *  firstProgram(duration<double> & buildTime);
    virtual void removeProgramForVariationSetUuid(const std::string &variationSetUuid);
    
    size_t maxWorkGroupSizeForKernelsForVariationSet(const SharedVariationSet &variationSet);
};


#endif /* DeviceKind_hpp */
