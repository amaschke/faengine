//
//  VariationParameter.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 4/30/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "VariationParameter.hpp"
#include "Variation.hpp"
#include "VariationSet.hpp"
#include "Utilities.hpp"
#include "FlameExpanded.hpp"
#include "VariationChain.hpp"
#include "VariationGroup.hpp"
#include "VarParInstance.hpp"
#include "pugixml.hpp"

using namespace pugi;

std::string string_format(const std::string&, const std::string &);
std::string string_format(const std::string&, const std::string &, uint);
std::string string_format(const std::string&, const std::string &, const std::string &);

VariationParameter::VariationParameter(const std::string &_name,
                                       const std::string &_key,
                                       SharedVariation   _variation,
                                       float _defaultValue)
:
    variation(_variation),
    name(_name),
    alias(nullptr),
    key(_key),
    defaultValue(_defaultValue),
    minRandom(-2.f),
    maxRandom(2.f),
    discrete(false),
    nonZero(false),
    xformIndex(0)
{}

VariationParameter::VariationParameter(const VariationParameter &p)
:
    variation(p.variation),
    name(p.name),
    alias(p.alias ? new std::string(*p.alias) : nullptr),
    key(p.key),
    defaultValue(p.defaultValue),
    minRandom(p.minRandom),
    maxRandom(p.maxRandom),
    discrete(p.discrete),
    nonZero(p.nonZero),
    xformIndex(p.xformIndex)
{}


VariationParameter & VariationParameter::operator=(const VariationParameter &p)
{
    std::enable_shared_from_this<VariationParameter>::operator=(p);

    variation = p.variation;
    name      = p.name;
    alias     = p.alias ? new std::string(*p.alias) : nullptr;
    key       = p.key;
    defaultValue = p.defaultValue;
    minRandom = p.minRandom;
    maxRandom = p.maxRandom;
    discrete  = p.discrete;
    nonZero   = p.nonZero;
    xformIndex = p.xformIndex;
    
    return *this;
}

VariationParameter::~VariationParameter()
{
    delete alias;
}

bool VariationParameter::namePairCompare(const PairParameter &_a, const PairParameter &_b)
{
    const SharedParameter &a = _a.second;
    const SharedParameter &b = _b.second;
    return nameCompare(a, b);
}

bool VariationParameter::xformIndexPairCompare(const PairParameter &_a, const PairParameter &_b)
{
    const SharedParameter &a = _a.second;
    const SharedParameter &b = _b.second;
    return xformIndexCompare(a, b);
}

bool VariationParameter::nameCompare(const SharedParameter &a, const SharedParameter &b)
{
    return icompare(a->name, b->name);
}

bool VariationParameter::xformIndexCompare(const SharedParameter &a, const SharedParameter &b)
{
    return a->xformIndex < b->xformIndex;
}

#pragma mark Properties

void VariationParameter::setKey(const std::string &parameterKey)
{
    const std::string oldKey = key;
    key = parameterKey;
    
    if (oldKey.length() > 0) {
        SharedParameter holdit = shared_from_this(); // dont want Parameter to be deleted by this
        variation.lock()->variationSet.lock()->parameterChangedKey(shared_from_this(), oldKey);
    }
}

void VariationParameter::setName(const std::string &_name)
{
    const std::string base    = _name;
    std::string parameterName = _name;
    
    // enforce new unique variation name
    uint i = 1;
    const std::string oldName = name;
    while (variation.lock()->parameters.find(parameterName) != variation.lock()->parameters.end()) {
        name = parameterName = string_format("%s_%u", base.c_str(), i++);
    }
    if (oldName.length() > 0) {
        SharedParameter holdit = shared_from_this(); // dont want this deelted by this shuffling
        variation.lock()->parameterChangedName(shared_from_this(), oldName);
    }

    std::string newKey = string_format("%s_x%s", variation.lock()->key.c_str(), name.c_str());
    setKey(newKey);
}

bool VariationParameter::operator==(const VariationParameter &other)
{
    return key == other.key;
}
#pragma mark Description

template std::string string_format<const char *, const char *, const char *, int>
                                (const char *, const char *, const char *, const char *, int);

std::string VariationParameter::description()
{
    return string_format("%s %s %s xformIndex:%u", "VariationParameter", key.c_str(), name.c_str(), xformIndex);
}

#pragma mark Parameters Used by Flame

ParametersSet VariationParameter::variationParametersForFlameExpanded(const SharedFlameExpanded &fe)
{
    ParametersSet set;

    SharedVariationSet variationSet = VariationSet::variationSetForUuid(fe->getVariationSetUuid());
    Flame *flame                    = fe->getFlame();
    
    // walk the fractal and see what variations it uses
    for (int iXform = 0; iXform < flame->params.numTrans; iXform++) {
        SharedVariationChain & chain = flame->xformVarChains[iXform];
        for (int i = 0; i < chain->size(); i++) {
            SharedVariationGroup & group = chain->operator[](i);
            for (const std::string & key : group->allKeys()) {
                if (VarParInstance::instanceNumOfKey(key) == std::string::npos)
                    continue;
                SharedVariation variation = variationSet->sharedVariationForKey(key);
                if (variation) {
                    SharedVarParInstance & varpar = group->operator[](key);
                    if (varpar->floatValue == 0.f)
                        continue;
                    
                    float value = varpar->floatValue;
                    if (value != 0.f) {
                        for (const std::string & _key : keysSortedByValue(variation->parameters, VariationParameter::namePairCompare)) {
                            SharedParameter & parameter = variation->parameters.operator[](_key);
                            set.insert(parameter);
                        }
                    }
                }
            }
        }
    }
    return set;
}

void VariationParameter::xmlElement(pugi::xml_node & parent)
{
    SharedVariation _variation = variation.lock();
    
    //    <parameter variation="blob" name="high" key="blob_high" xformIndex="109" minRandom="5.f" maxRandom="3.f"  default="1.f" />
    
    xml_node xmlElement = parent.append_child("parameter");
    
    xmlElement.append_attribute("variation")  = _variation->key.c_str();
    xmlElement.append_attribute("name")       = name.c_str();
    xmlElement.append_attribute("key")        = key.c_str();
    xmlElement.append_attribute("xformIndex") = xformIndex;
    xmlElement.append_attribute("minRandom")  = prettyFloatF(minRandom).c_str();
    xmlElement.append_attribute("maxRandom")  = prettyFloatF(maxRandom).c_str();
    xmlElement.append_attribute("default")    = prettyFloatF(defaultValue).c_str();
    if (discrete)
        xmlElement.append_attribute("discrete") = discrete ? "YES" : "NO";
    if (nonZero)
        xmlElement.append_attribute("nonzero")  = nonZero ? "YES" : "NO";
    if (alias)
        xmlElement.append_attribute("alias")    = alias;
}

