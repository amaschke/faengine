//
//  RenderEnums.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/24/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef RenderEnums_h
#define RenderEnums_h

enum PriorUse {
    noPrior = 0,  // dont use prior fused/retained info
    usePrior,     // use prior fused/retained info
    preCalibrate, // do a calibration render first, use its info - for tiled render
    nonAdaptive,  // dont use adaptive gamma correction - video or tiled render w
};

enum ProceedType {
    proceedOK = 0,
    proceedCancel,
    proceedSuspend,
};

#endif /* RenderEnums_h */
