//
//  VariationParameter.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 4/30/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef VariationParameter_hpp
#define VariationParameter_hpp

#include <string>
#include <memory>
#include <unordered_set>

#include "Exports.hpp"
#include "pugixml.hpp"

class Variation;
class VariationSet;
class VariationParameter;
class FlameExpanded;

using SharedParameter      = std::shared_ptr<VariationParameter>;
using SharedVariation      = std::shared_ptr<Variation>;
using SharedVariationConst = std::shared_ptr<Variation const>;
using PairParameter        = std::pair<const std::string, SharedParameter>;
using ParametersSet        = std::unordered_set<SharedParameter>;
using SharedFlameExpanded  = std::shared_ptr<FlameExpanded>;



class FAENGINE_API VariationParameter : public std::enable_shared_from_this<VariationParameter> {
    friend class Flam4ClRuntime;
    friend class Flam4CudaRuntime;
    friend class Variation;
    friend class VariationGroup;
    friend class VariationSet;
public:
    
    VariationParameter(const std::string &_name,
                       const std::string &_key,
                       SharedVariation   _variation,
                       float _defaultValue);
    VariationParameter(const VariationParameter &p);
    
    ~VariationParameter();
    
    VariationParameter & operator=(const VariationParameter &other);
    
    bool operator==(const VariationParameter &other);
    
    // Getters & Setters
    std::string getKey() const                      { return key; }
    void setKey(const std::string &_key);
    void setKeyAsIs(const std::string &_key)        { key=_key; }
    
    std::string getName()  const                    { return name; }
    void setName(const std::string &parameterName);
    void setNameAsIs(const std::string &_name)      { name=_name; }
    
    size_t getXformIndex()                          { return xformIndex; }
    void setXformIndex(size_t _xformIndex)          { xformIndex = _xformIndex; }
    
    static bool namePairCompare(const PairParameter &_a, const PairParameter &_b);
    static bool xformIndexPairCompare(const PairParameter &_a, const PairParameter &_b);
    static bool nameCompare(const SharedParameter &a, const SharedParameter &b);
    static bool xformIndexCompare(const SharedParameter &a, const SharedParameter &b);
    
    static ParametersSet variationParametersForFlameExpanded(const SharedFlameExpanded &fe);
    
    std::string description();
    void xmlElement(pugi::xml_node & parent);
    
private:
    
    std::string  name;		// name for display
    std::string *alias;     // name used in input file - for compatibility with other renderers
    std::string  key;		// KVC key
    std::weak_ptr<Variation>   variation;	// Variation owner
    float        defaultValue;
    float        minRandom;   // minimum random value
    float        maxRandom;   // minimum random value
    size_t       xformIndex;  // index into xformVars array for this paramater
    bool         discrete;    // true if only discrete values are allowed for this parameter
    bool         nonZero;     // some discrete parameters cannot be zero (to prevent integer divide by zero)

};

#endif /* VariationParameter_hpp */





