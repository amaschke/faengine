//
//  VarParInstance.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/2/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef VarParInstance_hpp
#define VarParInstance_hpp

#include <string>
#include <memory>
#include "Exports.hpp"

class VariationGroup;
class VarParInstance;

using uint                 = unsigned int;
using SharedPvg            = std::shared_ptr<VariationGroup>;
using WeakPvg              = std::weak_ptr<VariationGroup>;
using SharedVarParInstance = std::shared_ptr<VarParInstance>;
using PairInstance         = std::pair<const std::string, SharedVarParInstance>;


struct FAENGINE_API VarParInstance {
public:
    VarParInstance(const std::string &_varparKey,
                   size_t _instanceNum,
                   const SharedPvg &_group,
                   size_t _xformIndex);
    VarParInstance(const std::string &_varparKey,
                   size_t _instanceNum,
                   const SharedPvg &_group,
                   size_t _xformIndex,
                   size_t _targetGroupIndex);
    VarParInstance(const std::string &_varparKey,
                   size_t _instanceNum,
                   const SharedPvg &_group,
                   size_t _xformIndex,
                   size_t _targetGroupIndex,
                   float floatValue);
    VarParInstance(VarParInstance &other);
    VarParInstance(const VarParInstance &other);
    
    bool operator==(const VarParInstance &);
    
    SharedPvg getGroup() const;
    void setGroup(const SharedPvg &_group);
    
    size_t groupIndex() const;
    size_t getInstanceNum() const;
    void setInstanceNum(size_t _instanceNum);
    
    std::string instancedKey();
    std::string instancedKey2();
    static std::string makeInstancedKey(const std::string &baseKey,    size_t instanceNum);
    static std::string makeInstancedGUIKey(const std::string &baseKey, size_t instanceNum);
    static std::string makeXmlAttributeKey(const std::string &baseKey, size_t instanceNum);
    static std::string uninstancedKey(const std::string &key);
    static std::string variationMatrixPrefixForKey(const std::string &key);
    static std::string uninstancedAttributeKey(const std::string &key);
    static size_t      instanceNumOfKey(const std::string &key);
    static size_t      instanceNumOfAttributeKey(const std::string &key);
    static std::string instanceNumSubstringOfKey(const std::string &key);
    static std::string convertFromAttributeKey(std::string key);
    static std::string convertToAttributeKey(std::string key);
    static bool        amInstancedAttributeKey(const std::string &key);
    static bool        amInstancedKey(const std::string &key);
    
    static bool compare(const SharedVarParInstance &a, const SharedVarParInstance &b);
    static bool xformIndexCompare(const SharedVarParInstance &a, const SharedVarParInstance &b);
    static bool xformIndexPairCompare(const PairInstance &_a, const PairInstance &_b);
    
    std::string description();
    
    // ========== Members ======================
    std::string     varparKey;
    size_t          instanceNum;
    float           floatValue;
    WeakPvg         group;
    size_t          xformIndex; // needed to order the kernel's input varpar list
    //  - determines the order of evaluation across different variation types
    size_t          targetGroupIndex; // when group is nil, this is the routing information for where to put this interest
    
private:
    
// Note: design decision was made to not force intValue rounding here as the "discrete" parameter metadata was not trustworthy
// Each variation that needs to force rounding of a parameter valueshould do it itself and not depend on the input parser
// too many fractals exist that have fractional values for things like julian power

};

#endif /* VarParInstance_hpp */
