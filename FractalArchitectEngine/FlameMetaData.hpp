//
//  FlameMetaData.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/5/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//

#ifndef FlameMetaData_h
#define FlameMetaData_h

#include <string>
#include <vector>


// Copyright 2008 Steven Brodhead
 
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "Enums.hpp"
#include <string>
#include <memory>
#include <vector>

#include "Exports.hpp"
#include "common.hpp"
#include "Utilities.hpp"
#include "Image.hpp"

using SharedData         = std::shared_ptr<std::vector<char>>;
using SharedPointVector  = std::shared_ptr<std::vector<FA::Point>>;

struct FAENGINE_API FlameMetaData : public std::enable_shared_from_this<FlameMetaData>
{
    std::string       name;
    std::string       url;
    std::string       nick;
    std::string       notes;
    std::string       editXML;
    std::string       uuid;
    std::string       variationSetUuid;
    SharedPointVector rgbCurve;
    SharedPointVector redCurve;
    SharedPointVector greenCurve;
    SharedPointVector blueCurve;
    
    enum RenderMode   renderMode;
    Color             compositeColor;
    std::string       imagePath;
    FA::Image     backgroundImage;
    
    bool      applyToBackground;
    float     qualityAdjust;
    
    FlameMetaData(const std::string  &_uuid, const std::string  &_variationSetUuid);
    FlameMetaData(FlameMetaData &other);
    
    void setUuid(const std::string  &_uuid);
    void setVariationSetUuid(const std::string  &_variationSetUuid);
};


#endif /* FlameMetaData_h */
