//
//  Variation.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 4/30/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include <string.h>
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <list>
#include "Variation.hpp"
#include "VariationParameter.hpp"
#include "VariationSet.hpp"
#include "Utilities.hpp"
#include "pugixml.hpp"

#ifdef _WIN32
#include "WindowsInterface.hpp"
#else
#include "CocoaInterface.h"
#endif

#ifdef BOOST_REGEX
#include <boost/regex.hpp>
#define REGEX boost
#else
#include <regex>
#define REGEX std
#endif

using namespace pugi;

template std::string string_format<const char *>(const char *, const char *);
template std::string string_format<const char *, unsigned int>(const char *, const char *, unsigned int);
template std::string string_format<const char *, const char *>(const char *, const char *, const char *);


Variation::Variation(SharedVariationSet _variationSet,
                     const std::string &_name, const std::string &_key, float value)
: std::enable_shared_from_this<Variation>(),
    variationSet(_variationSet),
    name(_name),
    key(_key),
    source(),
    functionsSource(),
    functionsPrototype(),
    license(),
    author(),
    copyright(),
    md5sum(),
    md5sum2(),
    parameters(),
    defaultValue(value),
    selected(false),
    xformIndex(7),
    dimension(dim2D),
    usesDirectColor(false),
    usage(normalUsage),
    status(useable),
    usageCount(0)
{}

Variation::Variation(const Variation &v)
: std::enable_shared_from_this<Variation>(),
    variationSet(v.variationSet),
    name(v.name),
    key(v.key),
    source(v.source),
    functionsSource(v.functionsSource),
    functionsPrototype(v.functionsPrototype),
    license(v.license),
    author(v.author),
    copyright(v.copyright),
    md5sum(v.md5sum),
    md5sum2(v.md5sum2),
    parameters(),
    defaultValue(v.defaultValue),
    selected(v.selected),
    xformIndex(v.xformIndex),
    dimension(v.dimension),
    usesDirectColor(v.usesDirectColor),
    usage(v.usage),
    status(v.status),
    usageCount(v.usageCount)
{}

Variation& Variation::operator=(const Variation &v)
{
    std::enable_shared_from_this<Variation>::operator=(v);
    variationSet = v.variationSet;
    name         = v.name;
    key          = v.key;
    source       = v.source;
    functionsSource = v.functionsSource;
    functionsPrototype = v.functionsPrototype;
    license      = v.license;
    author       = v.author;
    copyright    = v.copyright;
    md5sum       = v.md5sum;
    md5sum2      = v.md5sum2;
    defaultValue = v.defaultValue;
    selected     = v.selected;
    xformIndex   = v.xformIndex;
    dimension    = v.dimension;
    usesDirectColor = v.usesDirectColor;
    usage        = v.usage;
    status       = v.status;
    usageCount   = v.usageCount;

    parameters   = ParametersDictionary();
    
    if (v.parameters.size() > 0) {
        for ( auto iter : v.parameters ) {
            const std::string &_name         =iter.first;
            SharedParameter &fromParameter=iter.second;
            SharedParameter parameterCopy(fromParameter);
            parameterCopy->variation  = shared_from_this();
            parameters[_name] = parameterCopy;
        }
    }
    return *this;
}

 SharedVariation Variation::makeSharedCopy(const Variation &v)
{
    SharedVariation variation = std::shared_ptr<Variation>(new Variation(v));
    if (v.parameters.size() > 0) {
        for ( auto iter : v.parameters ) {
            const std::string &_name         =iter.first;
            SharedParameter &fromParameter=iter.second;
            SharedParameter parameterCopy(fromParameter);
            parameterCopy->variation  = variation->shared_from_this();
            variation->parameters[_name] = parameterCopy;
        }
    }
    return variation;
}

#pragma mark Description

const char * Variation::is3DString()
{
    return dimension == dim3D ? "3D" : "";
}

const char * Variation::directColorString()
{
    return usesDirectColor ? "DC" : "";
}

template std::string string_format<const char *, int, const char *, const char *>
                    (const char *, const char *, int, const char *, const char *);

std::string Variation::description()
{
    return string_format("%s %s xformIndex:%u %s %s",
                         "Variation", name.c_str(), xformIndex, is3DString(), directColorString());
}


#pragma mark Getters & Setters

void Variation::setKey(const std::string &_key)
{
    const std::string base   = _key;
    std::string variationKey = _key;
    
    // enforce new unique variation key
    uint i = 1;
    while (keyIsNotUnique(variationKey))
        variationKey = string_format("%s_x%u", base.c_str(), i++);
    
    const std::string oldKey = key;
    key = variationKey;
    if (oldKey.length() > 0)
        changedKey(oldKey);
}

void Variation::setName(const std::string &_variationName)
{
    std::string variationName = _variationName;
    std::string base    = variationName;
    std::string oldName = name;

    // enforce new unique variation name
    uint i = 1;
    while (nameIsNotUnique(variationName))
        variationName = string_format("%s_x%u", base.c_str(), i++);

    name = variationName;
    if (oldName != name) {
        SharedVariation holdit = shared_from_this();
        variationSet.lock()->variationChangedName(shared_from_this(), oldName);
    }
}

void Variation::setXformIndex(size_t _xformIndex)
{
    size_t oldXformIndex = xformIndex;
    xformIndex           = _xformIndex;
    if (oldXformIndex != xformIndex) {
        SharedVariation holdit = shared_from_this(); // dont want Variation to be deleted by this
        variationSet.lock()->variationChangedXformIndex(Variation::shared_from_this(), xformIndex, oldXformIndex);
    }
}

// Sets of variations use just key for membership check - chose and weights can be different
bool Variation::operator==(const Variation &v) const
{
    return key == v.key;
}

bool Variation::operator!=(const Variation &v) const
{
    return key != v.key;
}

float Variation::weight() const
{
    return defaultValue;
}

bool Variation::namePairCompare(const PairVariation &_a, const PairVariation &_b)
{
    const SharedVariation &a = _a.second;
    const SharedVariation &b = _b.second;
    return nameCompare(a, b);
}

bool Variation::xformIndexPairCompare(const PairVariation &_a, const PairVariation &_b)
{
    const SharedVariation &a = _a.second;
    const SharedVariation &b = _b.second;
    return xformIndexCompare(a, b);
}

bool Variation::prePostXformIndexPairCompare(const PairVariation &_a, const PairVariation &_b)
{
    const SharedVariation &a = _a.second;
    const SharedVariation &b = _b.second;
    return prePostXformIndexCompare(a, b);
}

bool Variation::nameCompare(const SharedVariation &a, const SharedVariation &b)
{
    return a->name.compare(b->name);
}

bool Variation::xformIndexCompare(const SharedVariation &a, const SharedVariation &b)
{
    return a->xformIndex < b->xformIndex;
}

bool Variation::prePostXformIndexCompare(const SharedVariation &a, const SharedVariation &b)
{
    if (a->name.length() >= 10 && a->name.substr(0, 10) == "pre_matrix") {
        if (b->name.length() >= 10 && b->name.substr(0, 10) == "pre_matrix")
            return xformIndexCompare(a,b);
        else
            return true;
    }
    else if (b->name.length() >= 10 && b->name.substr(0, 10) == "pre_matrix") {
         if (a->name.length() >= 10 && a->name.substr(0, 10) == "pre_matrix")
            return xformIndexCompare(a,b);
        else
            return false;
    }
    else if (a->name.length() >= 11 && a->name.substr(0, 11) == "post_matrix") {
         if (b->name.length() >= 11 && b->name.substr(0, 11) == "post_matrix")
            return xformIndexCompare(a,b);
        else
            return false;
    }
    else if (b->name.length() >= 11 && b->name.substr(0, 11) == "post_matrix") {
         if (a->name.length() >= 11 && a->name.substr(0, 11) == "post_matrix")
            return xformIndexCompare(a,b);
        else
            return true;
    }
    else if (a->name.length() >= 4 && a->name.substr(0, 4) == "pre_") {
        if (b->name.length() >= 4 && b->name.substr(0, 4) == "pre_")
            return xformIndexCompare(a,b);
        else
            return true;
    }
    else if (b->name.length() >= 4 && b->name.substr(0, 4) == "pre_") {
         if (a->name.length() >= 4 && a->name.substr(0, 4) == "pre_")
            return xformIndexCompare(a,b);
        else
            return false;
    }
    else if (a->name.length() >= 5 && a->name.substr(0, 5) == "post_") {
         if (b->name.length() >= 5 && b->name.substr(0, 5) == "post_")
            return xformIndexCompare(a,b);
        else
            return false;
    }
    else if (b->name.length() >= 5 && b->name.substr(0, 5) == "post_") {
         if (a->name.length() >= 5 && a->name.substr(0, 5) == "post_")
            return xformIndexCompare(a,b);
        else
            return true;
    }
    else {
        return xformIndexCompare(a, b);
    }
}

size_t Variation::parameterCount() const
{
    return parameters.size();
}

bool Variation::hasParameters() const
{
    return parameters.size() > 0;
}

const std::string & Variation::getSource() const
{
    return source;
}

void Variation::setSource(const std::string &_source)
{
    if (source == _source)
        return;
    md5sum = ::md5DigestString(source);
}

const std::string & Variation::getFunctionsSource() const
{
    return functionsSource;
}

void Variation::setFunctionsSource(const std::string &_functionsSource)
{
    if (functionsSource == _functionsSource)
        return;
    md5sum2 = md5DigestString(functionsSource);
}

std::string Variation::compressWhiteSpace(const std::string string)
{
    REGEX::regex regex("\\s+");
    
    return std::string(REGEX::regex_replace (string, regex,std::string(" ")));
}

std::string Variation::compressedWhiteSpaceSource() const
{
    return Variation::compressWhiteSpace(source);
}

std::string Variation::compressedWhiteSpaceFunctionsSource() const
{
    return Variation::compressWhiteSpace(functionsSource);
}

bool Variation::isCompressedEqualTo(const SharedVariation &v) const
{
    if (functionsSource.length() == 0 && v->functionsSource.length() > 0)
        return false;
    else if (functionsSource.length() > 0 && v->functionsSource.length() == 0)
        return false;
    else if (functionsSource.length() > 0 && v->functionsSource.length() > 0) {
        if (functionsSource != v->functionsSource) {
            if (compressedWhiteSpaceFunctionsSource() != v->compressedWhiteSpaceFunctionsSource())
                return false;
        }
    }
    if (source.length() == 0 && v->source.length() > 0)
        return false;
    else if (source.length() > 0 && v->source.length() == 0)
        return false;
    else if (source.length() > 0 && v->source.length() > 0) {
        if (source == v->source)
            return true;
        if (source != v->source) {
            if (compressedWhiteSpaceSource() != v->compressedWhiteSpaceSource())
                return false;
        }
    }
    return true;
}

std::string Variation::replaceXformWithVarpar(const std::string &string)
{
    if (string.length() == 0)
        return std::string();
    
    std::string s(string);
    std::string compressed(string);
    
    std::unordered_set<std::string> reserved
    { "a", "b", "c", "d", "e", "f", "pa", "pb", "pc", "pd", "pe", "pf",
        "color", "symmetry", "weight", "opacity", "var_color", "rotates"
    };

    REGEX::regex e("\\bxform->(\\w+)\\b");
    REGEX::smatch sm;
        
    std::list<REGEX::smatch> matches;
    
    // push onto front of list so we can retrieve matches in reverse order
    while (REGEX::regex_search (s,sm,e)) {
        matches.push_front(sm);
        s = sm.suffix().str();
    }
    
    // replace in reverse order
    for (REGEX::smatch  m : matches) {
        std::string identifier = string.substr(m.position(1), m.length(1));
        if (reserved.count(identifier) > 0) {
            compressed.replace(m.position(0UL), 5, "varpar");
        }
    }
    
    REGEX::regex regex2("\\bactivePoint\\[index\\]\\.");
    return std::string(REGEX::regex_replace (compressed, regex2,std::string("__p")));
}

size_t Variation::replace(const std::string &pattern, const std::string &replacement, const std::string &string)
{
    std::string s(string);
    REGEX::regex regex(pattern);
    REGEX::smatch sm;
    
    auto begin = REGEX::sregex_iterator(s.begin(), s.end(), regex);
    auto end   = REGEX::sregex_iterator();
    
    size_t matchCount = std::distance(begin, end);
    
    REGEX::regex_replace (s, regex, replacement);
    return matchCount;
}


std::string Variation::cudaCompatibleSincos(const std::string &string)
{
    // case 1:    float s = sincos(ang, &c);   <=== check for this case first
    // case 2 :   s = sincos(ang, &c);         <=== then you can check for this case
    
    // $1 is the sine variable name
    // $2 is the stuff after sincos( excluding the & char before cosine variable name
    // $3 is the cosine variable name
    
    std::string patternCase1     = "\\bfloat[ ]+(\\w+)[ ]*=[ ]*sincos\\(([^%]+)&(\\w+)\\)";
    std::string replacementCase1 = "float $1;\nsincos($2&$1, &$3)";
    
    std::string patternCase2      = "\\b(\\w+)[ ]*=[ ]*sincos\\(([^%]+)&(\\w+)\\)";
    std::string replacementCase2  = "sincos($2&$1, &$3)";
    
    std::string compressed(string);

    size_t matchCount = replace(patternCase1, replacementCase1, compressed);
    
    if (matchCount == 0)
        replace(patternCase2, replacementCase2, compressed);
    
    return compressed;
}


std::string Variation::convertToCudaCompatibleSincos(const std::string &lines)
{
    std::string s;
    std::istringstream iss(lines);
    std::string line;
    while (std::getline(iss, line, '\n'))
    {
        s += convertToCUDAfunctions(cudaCompatibleSincos(line));
    }
    return s;
}

std::string Variation::convertToCUDAfunctions(const std::string  &string)
{
    std::string source(string);
    
    std::vector<std::string> patterns{
        "\\b(fmax)\\b",
        "\\b(fmin)\\b",
        "\\b(floor)\\b",
        "\\b(pow)\\b",
        "\\b(powr)\\b",
        "\\b(cos)\\b",
        "\\b(min)\\b",
        "\\b(ceil)\\b",
        "\\b(exp)\\b",
        "\\b(sin)\\b",
        "\\b(cosh)\\b",
        "\\b(sinh)\\b",
        "\\b(sincos)\\b",
        "\\b(sqrt)\\b",
        "\\b(atan2)\\b",
        "\\b(acos)\\b",
        "\\b(asin)\\b",
        "\\b(atan)\\b",
        "\\b(isfinite)\\b",
        "\\b(log)\\b",
        "\\b(fabs)\\b",
        "\\b(select)\\b",
        "\\b(erf)\\b",
        "\\b(fmod)\\b",
        "\\b(rint)\\b",
        "\\b(round)\\b",
        "\\b(trunc)\\b",
        "\\b(rsqrt)\\b",
        "\\b(acosh)\\b",
        "\\b(copysign)\\b"};
    
    for (std::string pattern : patterns) {
        REGEX::regex regex(pattern);
        REGEX::regex_replace (source, regex, std::string("$1f"));
    }
    
    
    std::vector<std::string> patterns2 {
                          "\\bnative_(cos)\\b",
                          "\\bnative_(sin)\\b",
                          "\\bnative_(tan)\\b",
                          "\\bnative_(sqrt)\\b",
                          "\\bnative_(exp)\\b",
                          "\\bnative_(powr)\\b",
                          "\\bnative_(pow)\\b",
                          "\\bnative_(log)\\b",
                          "\\bnative_(rsqrt)\\b",
                          "\\bnative_(log10)\\b"};
    
    for (std::string pattern : patterns2) {
        REGEX::regex regex(pattern);
        REGEX::regex_replace (source, regex, std::string("$1f"));
    }
    
    
    std::vector<std::string> patterns3 {
                          "\\bnative_(sqrt)\\b",
                          "\\bnative_(powr)\\b",
                          "\\bnative_(log)\\b",
                          "\\bnative_(rsqrt)\\b"};
    
    for (std::string pattern : patterns2) {
        REGEX::regex regex(pattern);
        REGEX::regex_replace (source, regex, std::string("$1f"));
    }

    replace("\\bpowrf\\b", "powf", source);
    replace("\\b__powrf\\b", "__powf", source);
    
#ifdef __linux__
    replace("\\bsinfh\\b", "sinhf", source);
#endif
    return source;
}

void Variation::parameterChangedName(const SharedParameter &parameter, const std::string &oldName)
{
    parameters[parameter->name] = parameter;
    parameters.erase(oldName);
}

// if it is my own key, it is still unique
bool Variation::keyIsNotUnique(const std::string &variationKey) const
{
    return variationSet.lock()->keyIsNotUnique(shared_from_this(), variationKey);
}

// if it is my own name, it is still unique
bool Variation::nameIsNotUnique(const std::string &variationName) const
{
    return variationSet.lock()->nameIsNotUnique(shared_from_this(), variationName);
}


void Variation::changedKey(const std::string &oldKey)
{
    SharedVariation holdit = shared_from_this(); // dont want Variation to be deleted by this
    variationSet.lock()->variationChangedKey(shared_from_this(), oldKey);

    std::string targetString  = string_format("varpar->%s", oldKey.c_str());
    std::string replaceString = string_format("varpar->%s", key.c_str());
    
    std::string mutableSource = source;
    
    REGEX::regex_replace(mutableSource, REGEX::regex(targetString), replaceString);
    source = mutableSource;
    
    ParametersVector sortedParameters;
    for( auto it = parameters.begin(); it != parameters.end(); ++it ) {
        sortedParameters.push_back( it->second );
    }
    std::sort(sortedParameters.begin(), sortedParameters.end(), VariationParameter::xformIndexCompare);
    
    // we have to also adjust the parameter keys for the new unique variation key
    for (SharedParameter &sp : sortedParameters) {
        std::string parameterName = sp->name;

        // deep copy the parameters - replace the old parameter with its copy
        SharedParameter &parameter = parameters[parameterName];
        std::string oldParamKey = parameter->key;
        std::string newParamKey = string_format("%s_x%s", key.c_str(), parameter->name.c_str());
        parameter->setKeyAsIs(newParamKey);
        
        SharedParameter holdit = parameter;
        parameters[newParamKey] = parameter;
        parameters.erase(oldParamKey);
        
        SharedParameter holdit2 = parameter->shared_from_this(); // dont want Parameter to be deleted by this
        variationSet.lock()->parameterChangedKey(parameter, oldParamKey);
    }
}

void Variation::localXformIndexParamOrdering()
{
    ParametersVector sortedParameters;
    for( auto it = parameters.begin(); it != parameters.end(); ++it ) {
        sortedParameters.push_back( it->second );
    }
    std::sort(sortedParameters.begin(), sortedParameters.end(), VariationParameter::xformIndexCompare);
    
    // renumber the parameter xformIndexes to just represent the ordering of the parameters for just this variation    
    uint i = 0;
    for (SharedParameter &sp : sortedParameters) {
        SharedParameter &parameter = parameters[sp->name];
        parameter->xformIndex = i++;
    }
}


std::string Variation::autoVersion(std::string string, UniquenessFunc f, const StringsSet &names, bool bumpVersion)
{
    // check for version ending
    REGEX::regex regex("_v([0-9]+)$");
    REGEX::smatch versionMatch;
    REGEX::regex_match(string, versionMatch, regex);
    
    if (versionMatch.empty()) {
        std::string versionString = string.substr(versionMatch.position(1), versionMatch.length(1));
        std::string base          = string.substr(0, versionMatch.position(1));
        int version               = std::stoi(versionString);
        
        // enforce new unique variation key by changeing version ending
        int i = version;
        
        while ((this->*f)(string) || names.find(string) != names.end()) {
            string = string_format("%s_v%u", base.c_str(), i++);
        }
        return string;
    }
    
    // Autoversion pattern
    regex = REGEX::regex("_av([0-9]+)$");
    REGEX::smatch autoVersionMatch;
    REGEX::regex_match(string, autoVersionMatch, regex);
    
    if (autoVersionMatch.empty()) {
        std::string versionString = string.substr(autoVersionMatch.position(1), autoVersionMatch.length(1));
        std::string base          = string.substr(0, autoVersionMatch.position(1));
        int version               = std::stoi(versionString);
        
        // enforce new unique variation key by changeing version ending
        int i = version;
        
        while ((this->*f)(string) || names.find(string) != names.end()) {
            string = string_format("%s_av%u", base.c_str(), i++);
        }
        return string;
    }

    std::string base = string;
    // enforce new unique variation key by changeing autoversion ending
    uint i = 1;
    while ((this->*f)(string) || names.find(string) != names.end()) {
        string = string_format(bumpVersion ? "%s_v%u" : "%s_av%u", base.c_str(), i++);
    }
    return string;
}


std::string Variation::autoVersionKeyKeepDifferentFrom(const StringsSet &names, bool bumpVersion)
{
    return autoVersion(key, &Variation::keyIsNotUnique, names, bumpVersion);
}

std::string Variation::autoVersionNameKeepDifferentFrom(const StringsSet &names, bool bumpVersion)
{
    return autoVersion(name, &Variation::nameIsNotUnique, names, bumpVersion);
}


void Variation::autoVersionDifferentFrom(const StringsSet &names)
{
    const std::string &autoKey  = autoVersionKeyKeepDifferentFrom(names, false);
    const std::string &autoName = autoVersionNameKeepDifferentFrom(names, false);
    setKey(autoKey);
    setName(autoName);
}

StringsSet Variation::predefinedNames()
{
    return std::unordered_set<std::string>{
        "weight",
        "coefs",
        "color",
        "symmetry",
        "color_speed",
        "animate",
        "opacity",
        "var_color",
        "rotates",
        "chaos",
        "links",
        "var",
        "vaf1",
        "post",
        "antialias_amount",
        "antialias_radius",
        "name"
    };
}

#pragma mark XML Serialization

void Variation::xmlElement(xml_node & parent)
{
    // <variation name="pre_blur" key="pre_blur" default="0.f" >
    
    xml_node xmlElement = parent.append_child("variation");
    
    const char *dimString;
    switch (dimension) {
        case dim2D:
            dimString = "2d";
            break;
        case dim3D:
            dimString = "3d";
            break;
        case both2D_3D:
            dimString = "both";
            break;
    }
    xmlElement.append_attribute("name")        = name.c_str();
    xmlElement.append_attribute("key")         = key.c_str();
    xmlElement.append_attribute("default")     = prettyFloat(defaultValue).c_str();
    xmlElement.append_attribute("dimension")   = dimString;
    xmlElement.append_attribute("directColor") = usesDirectColor ? "yes" : "no";

    if (author.length() > 0) {
        xmlElement.append_attribute("author")  = author.c_str();
    }
    if (copyright.length() > 0) {
        xmlElement.append_attribute("copyright") = copyright.c_str();
    }
    if (status == inDevelopment) {
        xmlElement.append_attribute("inDevelopment")  = "yes";
    }
    else if (status == broken) {
        xmlElement.append_attribute("broken")  = "yes";
    }
    
    if (parameters.size() > 0) {
        for (std::string & parameterKey : keysSortedByValue(parameters, VariationParameter::xformIndexPairCompare)) {
            SharedParameter & parameter = parameters[parameterKey];
            parameter->xmlElement(xmlElement);
        }
    }
    xml_node ele = xmlElement.append_child("source");
    ele.append_attribute("license") = license.c_str();
    ele.append_child(node_cdata).text().set(source.c_str());
    
    if (functionsPrototype.length() > 0) {
        xml_node ele = xmlElement.append_child("prototypes");
        ele.append_attribute("license") = license.c_str();
        ele.append_child(node_cdata).text().set(functionsPrototype.c_str());
    }
    if (functionsSource.length() > 0 && functionsPrototype.length() > 0) {
        xml_node ele = xmlElement.append_child("functions");
        ele.append_attribute("license") = license.c_str();
        ele.append_child(node_cdata).text().set(functionsPrototype.c_str());
    }
}

