//
//  VarParInstance.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/2/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "VarParInstance.hpp"
#include "VariationGroup.hpp"
#include "VariationChain.hpp"
#include "Utilities.hpp"

template std::string string_format<const char *, unsigned long>(const char *, const char *, unsigned long);
template std::string string_format<const char *, unsigned long, unsigned long>(const char *, const char *, unsigned long, unsigned long);

VarParInstance::VarParInstance(const std::string &_varparKey,
                               size_t _instanceNum,
                               const SharedPvg &_group,
                               size_t _xformIndex)
:
    varparKey(_varparKey),
    instanceNum(_instanceNum),
    group(_group),
    xformIndex(_xformIndex),
    floatValue(0.f),
    targetGroupIndex(std::string::npos)
{
}

VarParInstance::VarParInstance(const std::string &_varparKey,
                               size_t _instanceNum,
                               const SharedPvg &_group,
                               size_t _xformIndex,
                               size_t _targetGroupIndex)
: VarParInstance(_varparKey, _instanceNum, _group, _xformIndex)
{
    targetGroupIndex = std::string::npos;
}

VarParInstance::VarParInstance(const std::string &_varparKey,
                               size_t _instanceNum,
                               const SharedPvg &_group,
                               size_t _xformIndex,
                               size_t _targetGroupIndex,
                               float _floatValue)
: VarParInstance(_varparKey, _instanceNum, _group, _xformIndex, _targetGroupIndex)
{
    floatValue = _floatValue;
}

VarParInstance::VarParInstance(VarParInstance &v)
:
    varparKey(v.varparKey),
    instanceNum(v.instanceNum),
    group(v.group),
    xformIndex(v.xformIndex),
    floatValue(v.floatValue),
    targetGroupIndex(v.targetGroupIndex)
{}

VarParInstance::VarParInstance(const VarParInstance &v)
:
varparKey(v.varparKey),
instanceNum(v.instanceNum),
group(v.group),
xformIndex(v.xformIndex),
floatValue(v.floatValue),
targetGroupIndex(v.targetGroupIndex)
{}

SharedPvg VarParInstance::getGroup() const
{
    if (group.expired())
        return nullptr;
    return group.lock();
}

void VarParInstance::setGroup(const SharedPvg &_sgroup)
{
    group = _sgroup;
    if (_sgroup)
        targetGroupIndex = _sgroup->groupIndex();
}

std::string VarParInstance::makeInstancedKey(const std::string &baseKey, size_t instanceNum)
{
    if (instanceNum == std::string::npos)
        return baseKey;
    return string_format("%s#%lu", baseKey.c_str(), (unsigned long)instanceNum);
}

std::string VarParInstance::makeInstancedGUIKey(const std::string &baseKey, size_t instanceNum)
{
    if (instanceNum == std::string::npos)
        return baseKey;
    return string_format("%s #%lu", baseKey.c_str(), (unsigned long)instanceNum);
}

std::string VarParInstance::makeXmlAttributeKey(const std::string &baseKey, size_t instanceNum)
{
    if (instanceNum == std::string::npos)
        return baseKey;
    return string_format("%s-%lu", baseKey.c_str(), (unsigned long)instanceNum);
}

std::string VarParInstance::instancedKey()
{
    return makeInstancedKey(varparKey, instanceNum);
}

std::string VarParInstance::instancedKey2()
{
    if (instanceNum == std::string::npos)
        return varparKey;
    return string_format("%s#%u@%u", varparKey.c_str(), instanceNum, groupIndex());
}

size_t VarParInstance::groupIndex() const
{
    std::shared_ptr<VariationGroup> sgroup = group.lock();
    // copying the varpar leaves the group = nil, so this is how we effectively copy the "groupIndex"
    if (! sgroup || sgroup->chain.expired())
        return targetGroupIndex;
    return sgroup->chain.lock()->indexOfGroup(group.lock(), targetGroupIndex);
}

size_t VarParInstance::getInstanceNum() const
{
    return instanceNum;
}

void VarParInstance::setInstanceNum(size_t _instanceNum)
{
    instanceNum = _instanceNum;
}

bool VarParInstance::operator==(const VarParInstance &other)
{
    if (varparKey != other.varparKey)
        return false;
    if (instanceNum != other.instanceNum)
        return false;
    if (groupIndex() != other.groupIndex())
        return false;
    if (floatValue != other.floatValue)
        return false;
    return true;
}

std::string VarParInstance::uninstancedKey(const std::string &key)
{
    size_t pos = key.rfind("_variationMatrix");
    if (pos != std::string::npos) {
        std::string prefix = key.substr(0, pos);
        prefix             = uninstancedKey(prefix);
        return prefix + "_variationMatrix";
    }
    pos = key.find_first_of('#');
    if (pos != std::string::npos) {
        return key.substr(0, pos);
    }
    return key;
}

std::string VarParInstance::variationMatrixPrefixForKey(const std::string &key)
{
    size_t pos = key.rfind("_variationMatrix");
    if (pos != std::string::npos) {
        return key.substr(0, pos);
    }
    return key;
}

std::string VarParInstance::uninstancedAttributeKey(const std::string &key)
{
    size_t pos = key.rfind("_variationMatrix");
    if (pos != std::string::npos) {
        std::string prefix = key.substr(0, pos);
        prefix             = uninstancedAttributeKey(prefix);
        return prefix + "_variationMatrix";
    }
    pos = key.find_first_of('-');
    if (pos != std::string::npos) {
        return key.substr(0, pos);
    }
    return key;
}

size_t VarParInstance::instanceNumOfKey(const std::string &key)
{
    size_t pos = key.rfind("_variationMatrix");
    if (pos != std::string::npos) {
        std::string prefix = key.substr(0, pos);
        return instanceNumOfKey(prefix);
    }
    pos = key.find_first_of('#');
    if (pos != std::string::npos) {
        return std::stoi(key.substr(pos + 1));
    }
    return std::string::npos;
}

std::string VarParInstance::instanceNumSubstringOfKey(const std::string &key)
{
    size_t pos = key.rfind("_variationMatrix");
    if (pos != std::string::npos) {
        std::string prefix = key.substr(0, pos);
        return instanceNumSubstringOfKey(prefix);
    }
    pos = key.find_first_of('#');
    if (pos != std::string::npos) {
        return key.substr(pos + 1);
    }
    return "";
}

bool VarParInstance::amInstancedAttributeKey(const std::string &key)
{
    size_t pos = key.find_first_of('-');
    return pos != std::string::npos;
}

std::string VarParInstance::convertFromAttributeKey(std::string key)
{
    size_t pos = key.find_first_of('-');
    if (pos != std::string::npos)
        key.replace(pos, 1, "#");
    return key;
}

std::string VarParInstance::convertToAttributeKey(std::string key)
{
    size_t pos = key.find_first_of('#');
    if (pos != std::string::npos)
        key.replace(pos, 1, "-");
    return key;
}

bool VarParInstance::amInstancedKey(const std::string &key)
{
    size_t pos = key.find_first_of('#');
    return pos != std::string::npos;
}

size_t VarParInstance::instanceNumOfAttributeKey(const std::string &key)
{
    size_t pos = key.rfind("_variationMatrix");
    if (pos != std::string::npos) {
        std::string prefix = key.substr(0, pos);
        return instanceNumOfAttributeKey(prefix);
    }
    pos = key.find_first_of('-');
    if (pos != std::string::npos) {
        return std::stoi(key.substr(pos + 1));
    }
    return 1;  // in no instance component of attribute name (older flame files), this is the first instance
}

bool VarParInstance::compare(const SharedVarParInstance &a, const SharedVarParInstance &b)
{
    if (icompare(a->varparKey, b->varparKey))
        return true;
    if (iequal(a->varparKey, b->varparKey)) {
        if (a->instanceNum < b->instanceNum)
            return true;
        if (a->instanceNum == b->instanceNum)
            return a->groupIndex() < b->groupIndex();
    }
    return false;
}

bool VarParInstance::xformIndexPairCompare(const PairInstance &_a, const PairInstance &_b)
{
    const SharedVarParInstance &a = _a.second;
    const SharedVarParInstance &b = _b.second;
    return xformIndexCompare(a, b);
}

bool VarParInstance::xformIndexCompare(const SharedVarParInstance &a, const SharedVarParInstance &b)
{
    if (a->xformIndex < b->xformIndex) {
        return true;
    }
    if (a->xformIndex == b->xformIndex && a->instanceNum < b->instanceNum)
        return true;
    return false;
}

template std::string string_format<const char *, uint, uint, float>
                            (const char *, const char *, uint, uint, float);
template std::string string_format<const char *, const char *, unsigned long, const char *, uint, float>
                            (const char *, const char *, const char *, unsigned long, const char *, uint, float);

std::string VarParInstance::description()
{
    if (group.expired()) {
        return string_format("%s %s targetGroupIndex:%lu  xformIndex:%u value:%f",
                             "VarParInstance", instancedKey().c_str(), targetGroupIndex, xformIndex, floatValue);
    }
    else {
        std::string groupName = group.lock()->groupName();
        return string_format("%s %s group:#%lu %s xformIndex:%u value:%f",
                             "VarParInstance",
                             instancedKey().c_str(),
                             (unsigned long)group.lock()->groupIndex(),
                             groupName.c_str(),
                             xformIndex, floatValue);
    }
}

