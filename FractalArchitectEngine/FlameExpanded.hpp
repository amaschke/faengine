//
//  FlameExpanded.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/6/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef FlameExpanded_hpp
#define FlameExpanded_hpp

//
//  FlameExpanded.h
//  FractalArchitect
//
//  Created by StevenBrodhead on 3/7/11.
//  Copyright 2011-2012 Centcom Inc. All rights reserved.
//

#include <memory>
#include <string>

#include "Exports.hpp"
#include "Flame.hpp"
#include "ElectricSheepStuff.hpp"
#include "FlameMetaData.hpp"
#include "Utilities.hpp"
#include "Image.hpp"

class ColorMap;
class FlameExpanded;
class VariationGroup;
class VariationSet;
class XaosForXformsVector;

using SharedVariationSet  = std::shared_ptr<VariationSet>;
using SharedFlame         = std::shared_ptr<Flame>;
using SharedElc           = std::shared_ptr<ElectricSheepStuff>;
using SharedFlameMetaData = std::shared_ptr<FlameMetaData>;
using SharedData          = std::shared_ptr<std::vector<char>>;
using UniqueXaosMatrix    = std::unique_ptr<XaosForXformsVector>;

using namespace FA;


class FAENGINE_API FlameExpanded : public std::enable_shared_from_this<FlameExpanded>
{
    friend class FlameParse;
    friend class Flam4ClRuntime;
public:
    FlameExpanded(); // only to be used by friends
    FlameExpanded(SharedFlame && _flameData,
                  SharedElc && _elcData,
                  SharedFlameMetaData && _meta,
                  const std::string &_uuid,
                  const std::string &_variationSetUuid);
    FlameExpanded(const FlameExpanded &other);
    
    bool operator==(const FlameExpanded &f);
    bool operator!=(const FlameExpanded &f) { return ! operator==(f); }
    
    static SharedElc createDefaultElectricSheepStuff(int transformCount);
    
    Flame               * getFlame() const            { return flame.get(); }
    ElectricSheepStuff  * getElc()   const            { return elc.get(); }
    FlameMetaData       * getMeta();
    XaosForXformsVector * xaosMatrix()                { return getElc()->xaosMatrix.get(); }
    const std::string  & getVariationSetUuid() const  { return variationSetUuid; }
    size_t countOfXforms() const;
    bool nonBlankChains() const;
    
    void splitTransformAtIndex(uint splitIndex);
    void increaseXformCountTo(unsigned xformCount);
    
    void setupMeta();
    void setMeta(FlameMetaData *_meta);
    void setMeta(SharedFlameMetaData &&_meta);
    
    const std::string &getUuid() const { return uuid; }
    void setUuid(const std::string &_uuid);
    
    void setVariationSetUuid(const std::string &_variationSetUuid);
    
    const SharedPointVector & getRgbCurve() const   { return rgbCurve; }
    const SharedPointVector & getRedCurve() const   { return redCurve; }
    const SharedPointVector & getGreenCurve() const { return greenCurve; }
    const SharedPointVector & getBlueCurve() const  { return blueCurve; }
    
    void setRgbCurve(SharedPointVector   _rgbCurve);
    void setRedCurve(SharedPointVector   _redCurve);
    void setGreenCurve(SharedPointVector _greenCurve);
    void setBlueCurve(SharedPointVector  _blueCurve);
    
    bool getApplyToBackground() const { return applyToBackground; }
    float getQualityAdjust() const    { return qualityAdjust; }

    void setApplyToBackground(bool _applyToBackground);
    void setQualityAdjust(float _qualityAdjust);
    
    std::string textReport();
    static std::string flameReport(Flame * flame);
    static std::string xformReport(struct xForm *xform, SharedVariationChain chain, size_t which, bool isFinalXform);
private:
    std::string headerTextReport();
    std::string curvesTextReport();
    static std::string curvesTextReportForCurve(SharedPointVector &curve, size_t cpCount, const std::string & which);
    static std::string curvesTextReportForFlameExpanded();
    static std::string xformTextReport(struct xForm *xform, SharedVariationChain chain, size_t which, bool isFinalXform, const SharedVariationSet variationSet);
    static std::string chaosTextReportForMatrix(XaosForXformsVector *matrix);

public:
    
    SharedFlame         flame;
    SharedElc           elc;
    SharedFlameMetaData meta;
    std::string         uuid;
    std::string         variationSetUuid;
    SharedPointVector   rgbCurve;
    SharedPointVector   redCurve;
    SharedPointVector   greenCurve;
    SharedPointVector   blueCurve;
    bool                applyToBackground;
    float               qualityAdjust;
    
    FA::Image           backgroundImage;
    enum RenderMode     renderMode;
    Color               compositeColor;
    std::string         imagePath;
};

#endif /* FlameExpanded_hpp */
