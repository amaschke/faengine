//
//  Enums.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/5/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef Enums_h
#define Enums_h

enum RenderingIntent {
    RenderingIntentDefault,
    RenderingIntentAbsoluteColorimetric,
    RenderingIntentRelativeColorimetric,
    RenderingIntentPerceptual,
    RenderingIntentSaturation
};


enum GpuPlatformUsed {
    useOpenCL = 0,
    useMetal  = 1,
    useCUDA   = 2,
};

enum PaletteMode {
    modeStep   = 0,
    modeLinear = 1, // the old default
    modeSmooth = 2  //
};


// used for the colorspace selection
enum InitialColorSpace {
    GenericRGB = 0,
    sRGB = 1,
    AdobeRGB = 2,
    WideGamutRGB = 3,
    ProPhotoRGB = 4
};

enum EpsilonValueMode {
    emulateFlam3     = 0,
    emulateFlam4CUDA = 1,
};

enum OpenCLDevice {
    CPU = 0,
    GPU = 1,
};

enum RenderMode {
    renderModeNormal = 0,
    renderModeTransparent,
    renderModeCompositeOverColor,
    renderModeCompositeOverImage,
};


#endif /* Enums_h */
