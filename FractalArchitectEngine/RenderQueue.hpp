//
//  RenderQueue.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 6/5/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef RenderQueue_hpp
#define RenderQueue_hpp

#include <memory>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

#include "Exports.hpp"
#include "RenderParams.hpp"
#include "Image.hpp"
#include <chrono>
#include "common.hpp"

using namespace FA;

class RenderParams;
class RenderResult;

using SharedImage  = std::shared_ptr<FA::Image>;
using SharedParams = std::shared_ptr<RenderParams>;

using namespace std::chrono;

class FAENGINE_API RenderQueue {
    
public:
    RenderQueue();
   ~RenderQueue();
    
    void enqueue(SharedParams request);
    void enqueueStop(SharedDeviceContext deviceContext);
    
private:
    static void f(RenderQueue *);
    static void f2(RenderQueue *);
    void handleRequest(SharedParams request);
    
    static void calcRenderTime(SharedParams & request,
                               std::string filename,
                               duration<double> _duration,
                               duration<double> deDuration,
                               bool hasFinalXfom,
                               float fusedFraction,
                               float retainedFraction);
    
    // ====== Members ==============
    std::queue<SharedParams> queue;
    std::mutex mutex;
    std::condition_variable condition;
    std::thread worker;
	bool stop;
};

#endif /* RenderQueue_hpp */
