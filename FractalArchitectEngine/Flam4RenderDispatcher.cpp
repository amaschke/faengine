//
//  Flam4RenderDispatcher.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 7/31/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//

#include <string>


#include "Flam4RenderDispatcher.hpp"
#include "RenderState.hpp"
#include "RenderInfo.hpp"
#include "FlameExpanded.hpp"
#include "Flame.hpp"
#include "RenderTile.hpp"


Flam4RenderDispatcher::Flam4RenderDispatcher(SharedRenderState _renderState, size_t _bladeIndex, size_t _subSample, time_point<high_resolution_clock> _now)
:
    renderState(_renderState),
    bladeIndex(_bladeIndex),
    subSample(_subSample),
    renderedBatches(0),
    proceed(proceedOK),
    mutex(),
	now(_now)
{
    checkSetup();
    setRenderedBatches(RenderTile::renderedBatchesSoFar(renderState, subSample, bladeIndex));
}

size_t Flam4RenderDispatcher::getBladeIndex()
{
    std::unique_lock<std::mutex> lock(mutex);
    return bladeIndex;
}

void Flam4RenderDispatcher::setBladeIndex(size_t _bladeIndex)
{
    std::unique_lock<std::mutex> lock(mutex);
    bladeIndex = _bladeIndex;
}

size_t Flam4RenderDispatcher::getSubSample()
{
    std::unique_lock<std::mutex> lock(mutex);
    return subSample;
}

void Flam4RenderDispatcher::setSubSample(size_t _subSample)
{
    std::unique_lock<std::mutex> lock(mutex);
    subSample = _subSample;
}

size_t Flam4RenderDispatcher::getRenderedBatches()
{
    std::unique_lock<std::mutex> lock(mutex);
    return renderedBatches;
}

void Flam4RenderDispatcher::setRenderedBatches(size_t _renderedBatches)
{
    std::unique_lock<std::mutex> lock(mutex);
    renderedBatches = _renderedBatches;
}

enum ProceedType Flam4RenderDispatcher::getProceed()
{
    std::unique_lock<std::mutex> lock(mutex);
    return proceed;
}

void Flam4RenderDispatcher::setProceed(enum ProceedType _proceed)
{
    std::unique_lock<std::mutex> lock(mutex);
    proceed = _proceed;
}


size_t Flam4RenderDispatcher::checkSetup()
{
    SharedBlades blades = renderState->renderInfo->blades;
    if (bladeIndex < blades->size())  {
        SharedFE fe     = blades->operator[](bladeIndex);
        size_t numBatches = fe->flame->params.numBatches;
        
        if (subSample >= numBatches) {
            ++bladeIndex;
            subSample = 0;
        }
    }
    
    size_t batchCount = 0;
    for (SharedFE fe : *blades) {
        size_t numBatches = fe->flame->params.numBatches;
        batchCount += numBatches;
    }
    return batchCount;
}

struct Allocation Flam4RenderDispatcher::allocateBatch()   // allocation holds bladeIndex, subSample
{
    
    // if render canceled or suspended
    if (proceed != proceedOK)
        return Allocation { std::string::npos, 0};
        
    std::unique_lock<std::mutex> lock(mutex);
    SharedBlades blades = renderState->renderInfo->blades;
    if (bladeIndex >= blades->size())
        return Allocation { std::string::npos, 0};
    
    Allocation range { bladeIndex, subSample };
    SharedFE fe     = blades->operator[](bladeIndex);
    size_t numBatches = fe->flame->params.numBatches;
    
    if (++subSample >= numBatches) {
        ++bladeIndex;
        subSample = 0;
    }
    return range;
}

void Flam4RenderDispatcher::batchCompleted()
{
    std::unique_lock<std::mutex> lock(mutex);
    renderedBatches++;
}

bool Flam4RenderDispatcher::lastSubSampleForSubSample(size_t _subSample, size_t _bladeIndex)
{
    SharedBlades blades = renderState->renderInfo->blades;
    SharedFE fe         = blades->operator[](_bladeIndex);
    size_t numBatches     = fe->flame->params.numBatches;
    
    return _subSample + 1 == numBatches;
}

size_t Flam4RenderDispatcher::batchesToGo()
{
    size_t _bladeIndex = bladeIndex;
    size_t _subSample  = subSample;
    size_t count       = 0;
    
    SharedBlades blades = renderState->renderInfo->blades;
    while (_bladeIndex < blades->size())  {
        SharedFE fe         = blades->operator[](_bladeIndex);
        size_t numBatches     = fe->flame->params.numBatches;
        
        while (_subSample++ < numBatches) {
            count++;
        }
        ++_bladeIndex;
        _subSample = 0;
    }
    return count;
}
