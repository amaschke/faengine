//
//  rand.h
//  FractalArchitect
//
//  Created by Steven Brodhead on 8/4/11.
//  Copyright 2011-2012 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef rand_h
#define rand_h

float randValue();
float randInRange(float min, float max);
int randIntInRange(int min, int max);
float gaussDistribution(float mean, float sigma);
double randDoubleValue();
float exponentialDistribution(float max, float lambda);
float cumulExponentialDistribution(float x, float lambda);

#endif
