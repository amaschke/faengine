//
//  Flam4ClRuntime.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/21/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef Flam4ClRuntime_hpp
#define Flam4ClRuntime_hpp

#ifdef __APPLE__
#include <OpenCL/OpenCL.h>
#elif defined(_WIN32)
#include <CL/opencl.h>
#else
#include <CL/cl.h>
#endif
#include <vector>
#include <chrono>

#include "Exports.hpp"
#include "DefinesOCL.h"
#include "DeviceRuntime.hpp"
#include "ClContext.hpp"
//#include "Histogram.hpp"

class ClContext;
class Flam4ClRuntime;
class Flame;
class RenderState;
class VariationSet;

using SharedClContext     = std::shared_ptr<ClContext>;
using WeakClContext       = std::weak_ptr<ClContext>;
using SharedPointVector   = std::shared_ptr<std::vector<Point>>;
using SharedFlameExpanded = std::shared_ptr<FlameExpanded>;
using FlamesVector        = std::vector<SharedFlameExpanded>;
using SharedClRuntime     = std::shared_ptr<Flam4ClRuntime>;
using ColorVector         = std::vector<cl_float4>;

using namespace std::chrono;

class FAENGINE_API Flam4ClRuntime : public DeviceRuntime {
public:
    Flam4ClRuntime(SharedClContext _clContext, uint _numBatches);
    
    virtual ~Flam4ClRuntime();
    
    size_t numSelectedDevices() override { return clContext->selectedDeviceCount(); }    

    
    static bool oclMemCheck(ClContext *clContext,
                            uint    deviceNumber,
                            size_t  area,
                            uint    numColors,
                            size_t & amountAlloced,
                            int     bitDepth,
                            int     warpSize,
                            bool    deviceImageSupport,
                            uint    xformCount,
                            uint    supersample);

    void optBlockYforQuality(int quality, uint selectedDeviceNum);
    
    void createCUDAbuffers(ClContext *_clContext,
                           uint selectedDeviceNum,
                           uint bitDepth,
                           uint warpSize,
                           uint desiredQuality,
                           bool saveRenderState,
                           size_t maxXformCount,
                           size_t maxPointPoolCount,
                           size_t xformCount);
    void startCuda(SharedRenderState & renderState, uint selectedDeviceNum) override;
    
    void runFuseForSelectedDeviceNum(uint selectedDeviceNum,
                                     float epsilon,
                                     uint maxWorkGroupSize,
                                     const SharedVariationSet & variationSet) override;
    void startFrameForSelectedDeviceNum(uint selectedDeviceNum,
                                        uint maxWorkGroupSize,
                                        const SharedVariationSet & variationSet,
                                        duration<double> & buildTime) override;
    void clearRenderTargetForSelectedDeviceNum(unsigned selectedDeviceNum,
                                               uint maxWorkGroupSize,
                                               const SharedVariationSet & variationSet) override;
    void renderBatchForSelectedDeviceNum(unsigned selectedDeviceNum,
                                         SharedElc & elc,
                                         float epsilon,
                                         uint maxWorkGroupSize,
                                         const SharedVariationSet & variationSet,
                                         uint batchNum) override;
    void grabFrameFromSelectedDeviceNum(uint selectedDeviceNum) override;
    void synchronizeQueueForSelectedDeviceNum(uint selectedDeviceNum) override;
    
    void mergeFramesToSelectedDeviceNum(uint selectedDeviceNum,
                                        uint fromSelectedDeviceNum,
                                        uint maxWorkGroupSize,
                                        const SharedVariationSet & variationSet,
                                        SharedRenderState & renderState) override;
    Point finishFrame(SharedRenderState &renderState,
                      void *image,
                      float *backgroundImage,
                      size_t renderedBatches,
                      const SharedVariationSet & variationSet,
                      SharedElc &elc,
                      SharedFE & flameExpanded,
                      duration<double> & deDuration) override;
    void stopCudaForSelectedDeviceNum(uint selectedDeviceNum) override;
    
    void fuseIterations(uint _fuseIterations, uint selectedDeviceNum) override;
    
    size_t batchesPerTileForActualAdaptive(double actualQuality,
                                           double width,
                                           double height,
                                           uint tiles,
                                           uint selectedDeviceNum);
//    uint fuseIterations() override { return isGPU ? DeviceContext::gpuFuseIterations : DeviceContext::cpuFuseIterations; }

private:
    size_t accumBufferSize() { return xDim*yDim * sizeof(cl_float4); }

    void initGlobalBuffers();
    
    void logVarlist(Flame & flame,
                    uint selectedDeviceNum,
                    unsigned int *xformVarUsageIndexes,
                    struct VariationListNode *varList,
                    float *varparInstances,
                    uint total,
                    uint totalVarPar,
                    const SharedVariationSet & variationSet);
    
    void makeVarUsageListsForSelectedDeviceNum(uint selectedDeviceNum,
                                               Flame & flame,
                                               const SharedVariationSet & variationSet) override;
    void saveRenderTarget(cl_command_queue queue, uint selectedDeviceNum);
    
    uint referenceCountfor(cl_mem memobj);
    void deleteCUDAbuffers(cl_command_queue queue, uint selectedDeviceNum);
    void retainCUDAbuffers(cl_command_queue queue, uint selectedDeviceNum);
    void unpinBuffers(cl_command_queue queue, uint selectedDeviceNum);
    
    void addToAccumBufferForSelectedDeviceNum(uint selectedDeviceNum) override;
    
    void readPointIterations(cl_command_queue queue, uint selectedDeviceNum, uint perXformPointCount, uint xformCount);
    
    void rgbCurveAdjust(SharedClContext         & _clContext,
                        const SharedPointVector & curveData,
                        uint                      selectedDeviceNum,
                        cl_kernel                 colorCurveRGB3ChannelsKernal,
                        size_t                  * globalWorkSize,
                        size_t                  * localWorkSize);
    void curveAdjust(SharedClContext         & _clContext,
                     const SharedPointVector & curveData,
                     uint                      channel,
                     uint                      selectedDeviceNum,
                     cl_kernel                 colorCurveRGBChannelKernal,
                     size_t                  * globalWorkSize,
                     size_t                  * localWorkSize);
    size_t reduceMarkCountsForSelectedDeviceNum(uint selectedDeviceNum,
                                                uint maxWorkGroupSize,
                                                cl_kernel reductionKernal,
                                                cl_command_queue queue);
    size_t reduceMarkCountsForAllDevices(uint maxWorkGroupSize, const SharedVariationSet & variationSet);
    size_t reducePixelCountsForSelectedDeviceNum(uint selectedDeviceNum,
                                                 uint maxWorkGroupSize,
                                                 cl_kernel reductionKernal,
                                                 cl_command_queue queue);
    size_t reducePixelCountsForAllDevices(uint maxWorkGroupSize, const SharedVariationSet & variationSet);
    
    void resample(SharedClContext &_clContext,
                  cl_kernel readChannelKernal,
                  cl_kernel writeChannelKernal,
                  cl_kernel writeChannelStripedKernal,
                  cl_kernel convolveRowKernal,
                  cl_kernel convolveColKernal,
                  uint selectedDeviceNum,
                  uint maxWorkGroupSize,
                  SharedElc & elc);
    static size_t maxXformCount(FlamesVector & blades);
    size_t maxPointPoolCount(FlamesVector & blades);
    
    float logHistogramStatsFromData(ColorVector &data,
                                    const char * prefix,
                                    uint batches,
                                    size_t maxTheoretical,
                                    uint selectedDeviceNum);
    float logHistogramStatsFromQueue(cl_command_queue queue,
                                     uint selectedDeviceNum,
                                     const char * prefix,
                                     uint batches);
    
    // ========== Members ============
    SharedClContext clContext;
    
    cl_mem*     d_g_Palette;
    cl_mem*     d_g_PaletteTexture;
    cl_mem*     d_g_Flame;
    cl_mem*     d_g_Camera;
    cl_mem*     d_g_Xforms;
    cl_mem*     d_g_varUsages;
    cl_mem*     d_g_varUsageIndexes;
    cl_mem*     varpars;
    cl_mem*     d_g_accumBuffer;
    cl_mem*     d_g_resultStaging;
    cl_mem*     d_g_startingXform;
    cl_mem*     d_g_markCounts;
    cl_mem*     d_g_pixelCounts;
    cl_mem*     d_g_reduction;
    cl_mem*     d_g_pointList;
    cl_mem*     d_g_pointIterations;
    cl_mem*     d_g_randSeeds;
    cl_mem*     d_g_randK;
    cl_mem*     d_g_renderTarget;
    cl_mem*     d_g_switchMatrix;
    cl_mem      pin_mergeStaging;
    cl_mem*     permutations;
    cl_mem*     gradients;
    cl_mem*     shuffle;
    cl_mem*     iterationCount;
    
    // buffers whose size is warpsize dependent
    // d_g_randSeeds,  d_g_randK,  d_g_reduction
    
    // GCD multi-host threads support
    cl_float4*  mergeStagingBuffer;
    bool        isMergePinned;
    
    uint *_fuseIterations;
    char **buf; // thread specific snprintf buffers
    static constexpr size_t bufsize = 1024;
};

#endif /* Flam4ClRuntime_hpp */
