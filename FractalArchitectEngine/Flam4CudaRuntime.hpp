//
//  Flam4CudaRuntime.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 7/8/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef Flam4CudaRuntime_hpp
#define Flam4CudaRuntime_hpp

#include <cuda.h>
#include <cuda_runtime.h>
#include <vector_types.h>

#include "Exports.hpp"
#include "DefinesOCL.h"
#include "DeviceRuntime.hpp"
#include "CudaContext.hpp"
#include "Histogram.hpp"

class CudaContext;
class Flam4CudaRuntime;
class Flame;
class RenderState;
class VariationSet;

using SharedCudaContext   = std::shared_ptr<CudaContext>;
using WeakCudaContext     = std::weak_ptr<CudaContext>;
using SharedPointVector   = std::shared_ptr<std::vector<Point>>;
using SharedFlameExpanded = std::shared_ptr<FlameExpanded>;
using FlamesVector        = std::vector<SharedFlameExpanded>;
using SharedCudaRuntime   = std::shared_ptr<Flam4CudaRuntime>;

class FAENGINE_API Flam4CudaRuntime : public DeviceRuntime {
public:
    Flam4CudaRuntime(SharedCudaContext _cudaContext, uint _numBatches);
    
    virtual ~Flam4CudaRuntime();
    
    size_t numSelectedDevices() override { return cudaContext->selectedDeviceCount(); }
    
    
    static bool memCheck(uint    deviceNumber,
                         size_t  area,
                         uint    numColors,
                         size_t & amountAlloced,
                         int     bitDepth,
                         int     warpSize,
                         bool    deviceImageSupport,
                         uint    xformCount,
                         uint    supersample);
    
    void optBlockYforQuality(int quality, uint selectedDeviceNum);
    
    void createCUDAbuffers(CudaContext *_cudaContext,
                           uint selectedDeviceNum,
                           uint bitDepth,
                           uint warpSize,
                           uint desiredQuality,
                           bool saveRenderState,
                           size_t maxXformCount,
                           size_t maxPointPoolCount,
                           size_t xformCount);
    void setupFlameConstants(Flame* flm,
                             CUstream queue,
                             uint selectedDeviceNum,
                             const SharedVariationSet & variationSet);
    void setupPalette(Flame*flm,
                      CUstream queue,
                      uint selectedDeviceNum,
                      const SharedVariationSet & variationSet);
    void startCuda(SharedRenderState & renderState, uint selectedDeviceNum) override;
    
    void runFuseForSelectedDeviceNum(uint selectedDeviceNum,
                                     float epsilon,
                                     uint maxWorkGroupSize,
                                     const SharedVariationSet & variationSet) override;
    void startFrameForSelectedDeviceNum(uint selectedDeviceNum,
                                        uint maxWorkGroupSize,
                                        const SharedVariationSet & variationSet,
                                        duration<double> & buildTime) override;
    void clearRenderTargetForSelectedDeviceNum(unsigned selectedDeviceNum,
                                               uint maxWorkGroupSize,
                                               const SharedVariationSet & variationSet) override;
    void renderBatchForSelectedDeviceNum(unsigned selectedDeviceNum,
                                         SharedElc & elc,
                                         float epsilon,
                                         uint maxWorkGroupSize,
                                         const SharedVariationSet & variationSet,
                                         uint batchNum) override;
    void grabFrameFromSelectedDeviceNum(uint selectedDeviceNum) override;
    void synchronizeQueueForSelectedDeviceNum(uint selectedDeviceNum) override;
    
    void mergeFramesToSelectedDeviceNum(uint selectedDeviceNum,
                                        uint fromSelectedDeviceNum,
                                        uint maxWorkGroupSize,
                                        const SharedVariationSet & variationSet,
                                        SharedRenderState & renderState) override;
    Point finishFrame(SharedRenderState &renderState,
                      void *image,
                      float *backgroundImage,
                      size_t renderedBatches,
                      const SharedVariationSet & variationSet,
                      SharedElc &elc,
                      SharedFE & flameExpanded,
                      duration<double> & deDuration) override;
    void stopCudaForSelectedDeviceNum(uint selectedDeviceNum) override;
    
    void fuseIterations(uint _fuseIterations, uint selectedDeviceNum) override;
    
    size_t batchesPerTileForActualAdaptive(double actualQuality,
                                           double width,
                                           double height,
                                           uint tiles,
                                           uint selectedDeviceNum);
    //    uint fuseIterations() override { return isGPU ? DeviceContext::gpuFuseIterations : DeviceContext::cpuFuseIterations; }
    
    CUresult getStatus();
    void setStatus(CUresult _status);
    
private:
    size_t accumBufferSize() { return xDim*yDim * 4 *sizeof(float); }

    void initGlobalBuffers();
    
    void logVarlist(Flame & flame,
                    uint selectedDeviceNum,
                    unsigned int *xformVarUsageIndexes,
                    struct VariationListNode *varList,
                    float *varparInstances,
                    uint total,
                    uint totalVarPar,
                    const SharedVariationSet & variationSet);
    void logBufferInfoForBufferName(const std::string & name, uint selectedDeviceNum, CUdeviceptr buffer);
    void logAllBuffersForDeviceNum(uint selectedDeviceNum);
    
    void makeVarUsageListsForSelectedDeviceNum(uint selectedDeviceNum,
                                               Flame & flame,
                                               const SharedVariationSet & variationSet) override;
    void saveRenderTarget(CUstream queue, int selectedDeviceNum);
//    uint referenceCountfor(cl_mem memobj);
    void deleteCUDAbuffers(CUstream queue, uint selectedDeviceNum);
    void retainCUDAbuffers(CUstream queue, uint selectedDeviceNum);
    void unpinBuffers(CUstream queue, uint selectedDeviceNum);
    
    void addToAccumBufferForSelectedDeviceNum(uint selectedDeviceNum) override;
    
    void readPointIterations(CUstream queue, uint selectedDeviceNum, uint perXformPointCount, uint xformCount);
    
    void rgbCurveAdjust(const SharedPointVector & curveData,
                        uint                      selectedDeviceNum,
                        CUfunction                colorCurveRGB3ChannelsKernal,
                        uint                    * globalWorkSize,
                        uint                    * localWorkSize);
    void curveAdjust(const SharedPointVector & curveData,
                     uint                      channel,
                     uint                      selectedDeviceNum,
                     CUfunction                colorCurveRGBChannelKernal,
                     uint                    * globalWorkSize,
                     uint                    * localWorkSize);
    size_t reduceMarkCountsForSelectedDeviceNum(uint selectedDeviceNum,
                                                uint maxWorkGroupSize,
                                                CUfunction reductionKernal,
                                                CUstream queue);
    size_t reduceMarkCountsForAllDevices(uint maxWorkGroupSize, CUfunction reductionKernal);
    size_t reducePixelCountsForSelectedDeviceNum(uint selectedDeviceNum,
                                                 uint maxWorkGroupSize,
                                                 CUfunction reductionKernal,
                                                 CUstream queue);
    size_t reducePixelCountsForAllDevices(uint maxWorkGroupSize, CUfunction reductionKernal);
    
    void resample(CUfunction readChannelKernal,
                  CUfunction writeChannelKernal,
                  CUfunction writeChannelStripedKernal,
                  CUfunction convolveRowKernal,
                  CUfunction convolveColKernal,
                  uint selectedDeviceNum,
                  uint maxWorkGroupSize,
                  SharedElc & elc);
    static size_t maxXformCount(FlamesVector & blades);
    size_t maxPointPoolCount(FlamesVector & blades);
    
    float logHistogramStatsFromData(std::vector<float4>  data,
                                    const char * prefix,
                                    uint batches,
                                    size_t maxTheoretical,
                                    uint selectedDeviceNum);
    float logHistogramStatsFromQueue(CUstream queue,
                                     uint selectedDeviceNum,
                                     const char * prefix,
                                     uint batches);
    float logAccumHistogramStatsFromQueue(CUstream queue,
                                          uint selectedDeviceNum,
                                          const char * prefix);
    void logHostAccumHistogramStatsFromSelectedDeviceNum(int selectedDeviceNum, const std::string & prefix);
    void logPostProcessConstants(uint selectedDeviceNum);
    
    size_t logMarkCountsWithTotalIterCount(size_t totalIterationCount, CUstream queue, uint selectedDeviceNum);
    size_t logPixelCountsWithTotalIterCount(size_t totalIterationCount, CUstream queue, uint selectedDeviceNum);
    size_t sumMarkCountsWithQueue(CUstream queue, uint selectedDeviceNum);
    size_t sumHistogramCountsWithQueue(CUstream queue, uint selectedDeviceNum);

    static bool deviceAllocCheck(int deviceNum,
                                 float & area,
                                 size_t & amountAlloced,
                                 uint xformCount,
                                 uint supersample);
    static bool deviceAllocCheck(int deviceNum,
                                 float & xOptDim,
                                 float & yOptDim,
                                 size_t & amountAlloced,
                                 uint xformCount,
                                 uint supersample);
    static bool allDevicesAllocCheck(unsigned numDevices,
                                     float & area,
                                     size_t & amountAlloced,
                                     float niceFactor,
                                     uint xformCount,
                                     uint supersample);
    static bool allDevicesAllocCheck(unsigned numDevices,
                                     float & xOptDim,
                                     float & yOptDim,
                                     size_t & amountAlloced,
                                     float niceFactor,
                                     uint xformCount,
                                     uint supersample);
    static std::string getAllocSummary(bool success,
                                       float xOptDim,
                                       float yOptDim,
                                       size_t amountAlloced);
    static std::string getAllocSummary(bool success,
                                       float area,
                                       size_t amountAlloced);
    static std::string deviceAllocSummary(unsigned numDevices,
                                          float & xOptDim,
                                          float & yOptDim,
                                          size_t & amountAlloced,
                                          float niceFactor,
                                          uint xformCount,
                                          uint supersample);
    static bool checkGPUMemAvailable(size_t & memoryAvailable,
                                     float & area,
                                     float niceFactor,
                                     uint xformCount,
                                     uint supersample);

    
    // ========== Members ============
    CudaContext *cudaContext;
    
    CUdeviceptr*     d_g_Palette;
    CUarray*         d_g_PaletteTexture;
    CUdeviceptr*     d_g_Camera;
    CUdeviceptr*     d_g_varUsages;
    CUdeviceptr*     d_g_varUsageIndexes;
    CUdeviceptr*     varpars;
    CUdeviceptr*     d_g_accumBuffer;
    CUdeviceptr*     d_g_resultStaging;
    CUdeviceptr*     d_g_startingXform;
    CUdeviceptr*     d_g_markCounts;
    CUdeviceptr*     d_g_pixelCounts;
    CUdeviceptr*     d_g_reduction;
    CUdeviceptr*     d_g_pointList;
    CUdeviceptr*     d_g_pointIterations;
    CUdeviceptr*     d_g_randSeeds;
    CUdeviceptr*     d_g_randK;
    CUdeviceptr*     d_g_renderTarget;
    CUdeviceptr*     d_g_switchMatrix;
    CUdeviceptr      pin_mergeStaging;
    CUdeviceptr*     permutations;
    CUdeviceptr*     gradients;
    CUdeviceptr*     shuffle;
    CUdeviceptr*     iterationCount;
    
    size_t totalUsageSize;
    size_t totalVarparSize;
    
    CUcontext *contexts;
    CUstream *streams;
    
    CUresult status; // shuts up the unused variable warnings

    // buffers whose size is warpsize dependent
    // d_g_randSeeds,  d_g_randK,  d_g_reduction
    
    // GCD multi-host threads support
    float*  mergeStagingBuffer;
    bool        isMergePinned;
    
    uint *_fuseIterations;
    
    char **buf; // thread specific snprintf buffers
    static constexpr size_t bufsize = 1024;
};



#endif /* Flam4CudaRuntime_hpp */
