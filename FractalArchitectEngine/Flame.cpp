//
//  Flame.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 4/29/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include <iostream>
#include <vector>
#include <string>
#include <string.h>

#include "Flame.hpp"

#include "VariationGroup.hpp"
#include "VariationChain.hpp"

Flame::Flame()
: xformVarChains(), finalVarChains(), pageAligned(false), alignment(0)
{
    this->params.numTrans  = 0;
    this->params.numFinal  = 0;
    trans                  = nullptr;
    finals                 = nullptr;
    this->numColors        = 0;
    colorIndex             = nullptr;
    colorLocations         = nullptr;
    switchMatrix           = nullptr; // for rendering use - init later
    params.useXaos         = 0;
    params.oversample      = 1;
}

Flame::Flame(int numTrans,int paletteSize, int numFinal)
: xformVarChains(), finalVarChains(), pageAligned(false), alignment(0)
{
    memset(&this->params, 0, sizeof(struct FlameParams)); // set all parameters to zero
    this->params.numTrans  = numTrans;
    this->params.numFinal  = numFinal;
    trans                  = new xForm[numTrans];
    finals                 = new xForm[numFinal];
    this->numColors        = paletteSize;
    colorIndex             = new rgba[paletteSize];
    colorLocations         = new float[paletteSize];
    switchMatrix           = nullptr; // for rendering use - init later
    params.useXaos         = 0;
    params.oversample      = 1;
    
    for (int i = 0; i < numTrans; i++)
        xformVarChains.push_back(VariationChain::makeVariationChain());
    for (int i = 0; i < numFinal; i++)
        finalVarChains.push_back(VariationChain::makeVariationChain());
}

Flame::Flame(int numTrans,int paletteSize, int numFinal, int _alignment)
: xformVarChains(), finalVarChains(), pageAligned(true), alignment(_alignment)
{
    memset(&this->params, 0, sizeof(struct FlameParams)); // set all parameters to zero
    this->params.numTrans  = numTrans;
    this->params.numFinal  = numFinal;
    
	ALIGNED_MALLOC(xForm, trans, alignment, sizeof(xForm)*numTrans);
	memset(trans, '\0', sizeof(xForm)*numTrans);

    if (numFinal > 0) {
		ALIGNED_MALLOC(xForm, finals, alignment, sizeof(xForm)*numFinal);
		memset(finals, '\0', sizeof(xForm)*numFinal);
    }
    else
        finals = nullptr;
    this->numColors = paletteSize;
    
    paletteSize = (paletteSize + alignment - 1) ^ (alignment - 1); // pad out paletteSize
    
	ALIGNED_MALLOC(rgba, colorIndex, alignment, sizeof(rgba)*paletteSize);
	memset(colorIndex, '\0', sizeof(rgba)*paletteSize);
	ALIGNED_MALLOC(float, colorLocations, alignment, sizeof(float)*paletteSize);
	memset(colorLocations, '\0', sizeof(float)*paletteSize);
    
    switchMatrix    = nullptr; // for rendering use - init later
    
    params.useXaos         = 0;
    params.oversample      = 1;
    for (int i = 0; i < numTrans; i++)
        xformVarChains.push_back(VariationChain::makeVariationChain());
    for (int i = 0; i < numFinal; i++)
        finalVarChains.push_back(VariationChain::makeVariationChain());
}

Flame::Flame(const Flame &other)
: xformVarChains(), finalVarChains(), pageAligned(false), alignment(0)
{
    params = other.params;
    switchMatrix = nullptr; // for rendering use - init later
    
    trans          = new xForm[params.numTrans];
    finals         = new xForm[params.numFinal];
    numColors      = other.numColors;
    colorIndex     = new rgba[numColors];
    colorLocations = new float[numColors];
    
    for (int n = 0; n < params.numTrans; n++)
    {
        trans[n] = other.trans[n];
        xformVarChains.push_back(other.xformVarChains[n]);
    }
    for (int n = 0; n < params.numFinal; n++)
    {
        finals[n] = other.finals[n];
        finalVarChains.push_back(other.finalVarChains[n]);
    }
    
    for (int n = 0; n < numColors; n++)
    {
        colorIndex[n] = other.colorIndex[n];
        colorLocations[n] = other.colorLocations[n];
    }
}

Flame::Flame(const Flame &other, int _alignment)
: xformVarChains(), finalVarChains(), pageAligned(true), alignment(_alignment)
{
    params = other.params;
    switchMatrix = nullptr; // for rendering use - init later
    
	ALIGNED_MALLOC(xForm, trans, alignment, sizeof(xForm)*params.numTrans);
	memset(trans, '\0', sizeof(xForm)*params.numTrans);
	if (params.numFinal > 0) {
		ALIGNED_MALLOC(xForm, finals, alignment, sizeof(xForm)*params.numFinal);
		memset(finals, '\0', sizeof(xForm)*params.numFinal);
	}
	else
		finals = nullptr;
    
    numColors = other.numColors;
    int paletteSize = (numColors + alignment - 1) ^ (alignment - 1); // pad out paletteSize
    
	ALIGNED_MALLOC(rgba, colorIndex, alignment, sizeof(rgba)*paletteSize);
	memset(colorIndex, '\0', sizeof(rgba)*paletteSize);
	ALIGNED_MALLOC(float, colorLocations, alignment, sizeof(float)*paletteSize);
	memset(colorLocations, '\0', sizeof(float)*paletteSize);
    
    params = other.params;
    switchMatrix = nullptr; // for rendering use - init later
    
    for (int n = 0; n < params.numTrans; n++)
    {
        trans[n] = other.trans[n];
        xformVarChains.push_back(VariationChain::makeCopyOf(*other.xformVarChains[n]));
    }
    for (int n = 0; n < params.numFinal; n++)
    {
        finals[n] = other.finals[n];
        finalVarChains.push_back(VariationChain::makeCopyOf(*other.finalVarChains[n]));
    }
    
    for (int n = 0; n < numColors; n++)
    {
        colorIndex[n] = other.colorIndex[n];
        colorLocations[n] = other.colorLocations[n];
    }
}

void Flame::Clone(Flame** target)
{
    if (!*target)
    {
        *target = new Flame(this->params.numTrans, this->numColors, this->params.numFinal);
    }
    if (((*target)->params.numTrans != this->params.numTrans) ||
        ((*target)->params.numFinal != this->params.numFinal) ||
        ((*target)->numColors != this->numColors))
    {
        delete *target;
        *target = new Flame(this->params.numTrans, this->numColors, this->params.numFinal);
    }
    
    for (int n = 0; n < this->params.numTrans; n++)
    {
        (*target)->trans[n] = this->trans[n];
        (*target)->xformVarChains[n] = VariationChain::makeCopyOf(*xformVarChains[n]);
    }
    for (int n = 0; n < this->params.numFinal; n++)
    {
        (*target)->finals[n] = this->finals[n];
        (*target)->finalVarChains[n] = VariationChain::makeCopyOf(*finalVarChains[n]);
    }
    
    for (int n = 0; n < this->numColors; n++)
    {
        (*target)->colorIndex[n] = this->colorIndex[n];
        (*target)->colorLocations[n] = this->colorLocations[n];
    }
    (*target)->params = this->params;
    (*target)->switchMatrix = nullptr;
    
    (*target)->params.useXaos = this->params.useXaos;
    (*target)->params.oversample = this->params.oversample;
}

// want color pallet to be aligned and padded to 16 bytes - for SSE
// also add transform lite structure if needed
void Flame::CloneAligned(Flame** target, int alignment)
{
    if (!*target)
    {
        *target = new Flame(this->params.numTrans, this->numColors, this->params.numFinal, alignment);
    }
    if (((*target)->params.numTrans != this->params.numTrans) ||
        ((*target)->params.numFinal != this->params.numFinal) ||
        ((*target)->numColors != this->numColors))
    {
        delete *target;
        *target = new Flame(this->params.numTrans, this->numColors, this->params.numFinal, alignment);
    }
    for (int n = 0; n < this->params.numTrans; n++)
    {
        (*target)->trans[n] = this->trans[n];
        (*target)->xformVarChains[n] = VariationChain::makeCopyOf(*xformVarChains[n]);
    }
    for (int n = 0; n < this->params.numFinal; n++)
    {
        (*target)->finals[n] = this->finals[n];
        (*target)->finalVarChains[n] = VariationChain::makeCopyOf(*finalVarChains[n]);
    }
    
    for (int n = 0; n < this->numColors; n++)
    {
        (*target)->colorIndex[n] = this->colorIndex[n];
        (*target)->colorLocations[n] = this->colorLocations[n];
    }
    (*target)->params = this->params;
    (*target)->switchMatrix = nullptr;
    
    (*target)->params.useXaos = this->params.useXaos;
    (*target)->params.oversample = this->params.oversample;
}

static void normalizeWeights(float *weights, int count)
{
    // normalize the weights and put them into intervals on [0,1]
    float sum = 0.0f;
    for (int n = 0; n < count; n++)
    {
        sum += weights[n];
    }
    float sum2 = 0.0f;
    for (int n = 0; n < count; n++)
    {
        sum2       += weights[n]/sum;
        weights[n]  = sum2;
    }
    weights[count-1] = 1.f; // remove possibility of round off error
}

void Flame::prepareSwitchMatrix (float *brick)
{
    if (params.numTrans == 0 || brick == nullptr)
        return;
    if (switchMatrix)
        delete switchMatrix;
    switchMatrix = new float[params.numTrans * params.numTrans];
    
    params.useXaos = 0;
    for (int i = 0; i < params.numTrans; i++) {
        for (int j = 0; j < params.numTrans; j++) {
            float conditional = brick[i * params.numTrans + j];
            if (conditional != 1.f)
                params.useXaos = 1;
            switchMatrix[i * params.numTrans + j] = trans[j].weight * conditional;
        }
        normalizeWeights(&switchMatrix[i * params.numTrans],  params.numTrans);
    }
    delete brick;
}

void Flame::deleteChildren()
{
	if (pageAligned) {
		ALIGNED_FREE(trans);
		ALIGNED_FREE(finals);
		ALIGNED_FREE(colorIndex);
		ALIGNED_FREE(colorLocations);
		delete[] switchMatrix;
	}
	else {
		delete[] trans;
		delete[] finals;
		delete[] colorIndex;
		delete[] colorLocations;
		delete[] switchMatrix;
	}

    trans          = nullptr;
    finals         = nullptr;
    colorIndex     = nullptr;
    colorLocations = nullptr;
    switchMatrix   = nullptr;
}

Flame::~Flame()
{
	deleteChildren();
}

#pragma mark Flame Equivalence

bool Flame::equivalentTransforms(struct xForm * xform1, struct xForm *xform2, VariationChain &chain1, VariationChain &chain2)
{
    // this needs to be changed to determine equivalent VariationChain's too
    bool same =  memcmp(xform1, xform2, sizeof(struct xForm)) == 0;
    return same;
}

bool Flame::equivalentPalettes(struct rgba *palette1, struct rgba *palette2, unsigned numColors)
{
    return memcmp(palette1, palette2, numColors * sizeof(struct rgba)) == 0;
}

bool Flame::equivalentPaletteLocations(float *palette1, float *palette2, unsigned numColors)
{
    return memcmp(palette1, palette2, numColors * sizeof(float)) == 0;
}

bool Flame::equivalentColors(struct rgba *color1, struct rgba *color2)
{
    return memcmp(color1, color2, sizeof(struct rgba)) == 0;
}

bool Flame::equivalentFlameParams(struct FlameParams *params1, struct FlameParams *params2)
{
    // ignores quality, numBatches, supersampleWidth, frame, oversample, highlightpower
    if (params1->numTrans != params2->numTrans)
        return false;
    if (params1->numFinal != params2->numFinal)
        return false;
    if (!equivalentPalettes(&params1->background, &params2->background, 1))
        return false;
    if (params1->center[0] != params2->center[0])
        return false;
    if (params1->center[1] != params2->center[1])
        return false;
    if (params1->size[0] != params2->size[0])
        return false;
    if (params1->size[1] != params2->size[1])
        return false;
    if (params1->scale != params2->scale)
        return false;
    if (params1->zoom != params2->zoom)
        return false;
    if (params1->cam_dof != params2->cam_dof)
        return false;
    if (params1->clipToNDC != params2->clipToNDC)
        return false;
    if (params1->cam_yaw != params2->cam_yaw)
        return false;
    if (params1->cam_pitch != params2->cam_pitch)
        return false;
    if (params1->cam_zpos != params2->cam_zpos)
        return false;
    if (params1->cam_perspective != params2->cam_perspective)
        return false;
    if (params1->cam_x != params2->cam_x)
        return false;
    if (params1->cam_y != params2->cam_y)
        return false;
    if (params1->cam_z != params2->cam_z)
        return false;
    if (params1->cam_fov != params2->cam_fov)
        return false;
    if (params1->cam_near != params2->cam_near)
        return false;
    if (params1->cam_orthowide != params2->cam_orthowide)
        return false;
    if (params1->hue != params2->hue)
        return false;
    if (params1->rotation != params2->rotation)
        return false;
    if (params1->symmetryKind != params2->symmetryKind)
        return false;
    if (params1->brightness != params2->brightness)
        return false;
    if (params1->gamma != params2->gamma)
        return false;
    if (params1->gammaThreshold != params2->gammaThreshold)
        return false;
    if (params1->estimatorRadius != params2->estimatorRadius)
        return false;
    if (params1->alphaGamma != params2->alphaGamma)
        return false;
    if (params1->vibrancy != params2->vibrancy)
        return false;
    if (params1->useXaos != params2->useXaos)
        return false;
    if (params1->hue != params2->hue)
        return false;
    if (params1->hue != params2->hue)
        return false;
    return true;
}

bool Flame::equivalentFlames(struct Flame *flame1, struct Flame *flame2)
{
    if (!equivalentFlameParams(&flame1->params, &flame2->params))
        return false;
    
    if (flame1->numColors != flame2->numColors)
        return false;
    if (!equivalentPalettes(flame1->colorIndex, flame2->colorIndex, flame1->numColors))
        return false;
    if (!equivalentPaletteLocations(flame1->colorLocations, flame2->colorLocations, flame1->numColors))
        return false;
    
    for (unsigned i = 0; i < flame1->params.numTrans; i++) {
        if (!equivalentTransforms(&flame1->finals[i], &flame2->finals[i], *flame1->xformVarChains[i], *flame2->xformVarChains[i]))
            return false;
    }
    for (unsigned i = 0; i < flame1->params.numFinal; i++) {
        if (!equivalentTransforms(&flame1->trans[i], &flame2->trans[i], *flame1->finalVarChains[i], *flame2->finalVarChains[i]))
            return false;
    }
    return true;
}

unsigned * Flame::makeShuffledIndex()
{
    unsigned *array = (unsigned *)calloc(params.numTrans, sizeof(unsigned));
    array[0] = 0;
    for (unsigned i = 1; i < params.numTrans; i ++) {
        unsigned j = rand() % (i + 1);
        if (j != i)
            array[i] = array[j];
        array[j] = i;
    }
    return array;
}

// increase
void Flame::increaseXformCountTo(unsigned xformCount)
{
    if (xformCount <= params.numTrans)
        return;
    unsigned extra = xformCount - params.numTrans;
    
    for (unsigned j = 0; j < extra; j++) {
        float maxWeight = trans[0].weight;
        unsigned index  = 0;
        for (unsigned i = 1; i < params.numTrans; i ++)
        {
            if (trans[i].weight > maxWeight) {
                maxWeight = trans[i].weight;
                index     = i;
            }
        }
        splitTransformAt(index);
    }
}

void Flame::splitTransformAt(unsigned index)
{
    xForm *old = trans;
    trans      = new xForm[params.numTrans + 1];
    
    memcpy(trans, old, params.numTrans*sizeof(xForm));
    memcpy(&trans[params.numTrans], &old[index], sizeof(xForm));
    
    xformVarChains.push_back(VariationChain::makeCopyOf(*xformVarChains[index]));
    
    float weight = 0.5f * old[index].weight;
    trans[index].weight    = weight;
    trans[params.numTrans].weight = weight;
    params.numTrans += 1;
    delete [] old;
}
