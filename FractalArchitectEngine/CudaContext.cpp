//
//  CudaContext.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 7/6/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include <cuda.h>
#include <cuda_runtime_api.h>
#include <string>

#include "CudaContext.hpp"
#include "DeviceUsage.hpp"
#include "CudaDeviceKind.hpp"
#include "Flame.hpp"
#include "Flam4CudaRuntime.hpp"

using std::string;

static char buf[1024];

CudaContext::CudaContext(bool forceRebuild,
                         SharedVariationSet variationSet,
                         bool _singleDeviceAllowed,
                         bool _canUseGPU,
                         bool ignoreQuarantine)
: DeviceContext(forceRebuild, variationSet, _singleDeviceAllowed, _canUseGPU, ignoreQuarantine)
{
    CUresult result = CUDA_SUCCESS;
    result = cuInit(0);
    result = cuDeviceGetCount((int *)&numDevices);
    
    if (numDevices == 0)
        throw std::runtime_error("No CUDA devices on this Mac");
    
    devices  = (CUdevice *)malloc(sizeof(CUdevice)   * numDevices);
    contexts = (CUcontext *)malloc(sizeof(CUcontext) * numDevices);
    streams  = (CUstream *)malloc(sizeof(CUstream)   * numDevices);
    
    for (int i = 0; i < numDevices; i++) {
        result = cuDeviceGet(&devices[i], i);
        result = cuCtxCreate_v2(&contexts[i], 0, devices[i]);
        result = cuStreamCreate(&streams[i], CU_STREAM_DEFAULT);
        CUcontext acontext;
        result = cuCtxPopCurrent_v2(&acontext);
    }
    
    // Iterate through the list of platforms until we find one that supports
    // a CPU device, otherwise fail with an error.
    available   = (bool *)malloc(sizeof(bool) * numDevices);
    quarantined = (bool *)malloc(sizeof(bool) * numDevices);
    
    for (int i = 0; i < numDevices; i++) {
        deviceVendor.push_back(std::string("Nvidia"));
    }
    
    static char name[200];
    for (size_t i = 0; i < numDevices; i++) {
        CUdevice device = devices[i];
        result = cuDeviceGetName(name, sizeof(name), device);
        deviceNames.push_back(std::string(name));
        available[i]   = true;
        quarantined[i] = false;
    }
}

void CudaContext::phase2Constructor() 
{
    if (! createDeviceKinds()) {
        throw std::runtime_error(("ERROR: Creating Device Kinds failed"));
    }
    
    bool haveUsableDevice = false;
    for (int i = 0; i < numDevices; i++) {
        if (available[i] && (! quarantined[i])) {
            haveUsableDevice = true;
            break;
        }
    }
    
    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage deviceUsage = deviceUsages[i];
        deviceUsage->available    = available[i];
        deviceUsage->quarantined  = quarantined[i];
        deviceUsage->stagedUsed   = available[i] && ! quarantined[i];
        deviceUsage->used         = available[i] && ! quarantined[i];
    }
//    [self selectDevicesFromPrefs]; // if no prefs, select all
    rearrangeSelectedDevices();
    
    // enforce initial selection when single device limit applies
    if (singleDeviceAllowed) {
        CUdevice selectedDevice = 0;
        for (uint selectedDeviceNum = 0; selectedDeviceNum < selectedDeviceCount(); selectedDeviceNum++)
        {
            SharedDeviceUsage & deviceUsage = selectedDevices[selectedDeviceNum];
            if (deviceUsage->stagedUsed) {
                selectedDevice = deviceUsage->cudaDevice;
                break;
            }
        }
        for (int i = 0; i < numDevices; i++) {
            SharedDeviceUsage & deviceUsage = deviceUsages[i];
            if (deviceUsage->cudaDevice != selectedDevice)
                deviceUsage->stagedUsed = false;
        }
    }
    rearrangeSelectedDevices();
}

void CudaContext::printDeviceList()
{
    int numDevices = 0;
    CUresult result = CUDA_SUCCESS;
    result = cuInit(0);
    result = cuDeviceGetCount((int *)&numDevices);
    
    if (numDevices == 0) {
        printf("No CUDA devices\n");
        return;
    }
    
    static char name[200];
    for (size_t i = 0; i < numDevices; i++) {
        CUdevice device;
        result = cuDeviceGet(&device, (int)i);
        result = cuDeviceGetName(name, sizeof(name), device);
        printf("%s\n", name);
    }
}


SharedCudaContext CudaContext::makeCudaContext(bool forceRebuild,
                                               SharedVariationSet & _defaultVariationSet,
                                               bool _singleDeviceAllowed,
                                               bool _canUseGPU,
                                               bool _ignoreQuarantine)
{
    SharedCudaContext cudaContext = std::shared_ptr<CudaContext>(new CudaContext(forceRebuild,
                                                                                 _defaultVariationSet,
                                                                                 _singleDeviceAllowed,
                                                                                 _canUseGPU,
                                                                                 _ignoreQuarantine));
    cudaContext->phase2Constructor();
    return cudaContext;
}

// with CUDA, each device has its own deviceKind (so each device has its own programs)
bool CudaContext::createDeviceKinds()
{
    for (size_t i = 0; i < deviceNames.size(); i++) {
        string & kind               = deviceNames[i];
        string vendor               = deviceVendor[i];
        SharedDeviceKind deviceKind = std::make_shared<CudaDeviceKind>(kind, vendor, shared_from_this(), contexts[i]);
        deviceKinds[kind]           = deviceKind;
        
        SharedCudaDeviceKind cudaDeviceKind = std::dynamic_pointer_cast<CudaDeviceKind>(deviceKind);
        cudaDeviceKind->cudaDeviceInstances.push_back(devices[i]);
        deviceKind->deviceKindProperties();
        
        string deviceType = deviceKind->isCPU() ? "CPU:" : "GPU:";
        if (deviceKind->isCPU())
            cpuDeviceIndex = (uint)i;
        string deviceStatus   = (! available[i] || quarantined[i]) ? "Quarantined" : "";
        string & name         = deviceNames[i];
        if (name.find(vendor) != string::npos)
            vendor = "";
        
        snprintf(buf, sizeof(buf), "%s%s%s%s %s",
                 deviceType.c_str(),
                 vendor.c_str(),
                 vendor.length() > 0 ? " " : "",
                 deviceNames[i].c_str(),
                 deviceStatus.c_str());
        string description = buf;
        
        SharedDeviceUsage deviceUsage = std::make_shared<DeviceUsage>(devices[i], nullptr, name, description, true);
        
        deviceUsage->isGPU          = deviceKind->isGPU();
        deviceUsage->deviceKind     = deviceKind;
        deviceUsage->enforceLicense(canUseGPU);
        
        deviceUsages.push_back(deviceUsage);
        deviceKind->warpSize = cudaDeviceKind->determineWarpSizeForCudaDevice(devices[i]);
        deviceUsage->warpSize = deviceKind->warpSize;
    }
    refreshMemoryStats();
    return true;
}

unsigned CudaContext::deviceCount()
{
    int deviceCount = 0;
    //    cudaError_t status =
    cudaGetDeviceCount(&deviceCount);
    return deviceCount;
}

bool CudaContext::checkDriverVersion()
{
    int driverVersion, runtimeVersion;
    cudaDriverGetVersion(&driverVersion);
    cudaRuntimeGetVersion(&runtimeVersion);
    
    return driverVersion >= runtimeVersion;
}

// this is a simple test for OpenCL devices that have defective implementations
// The GeForce GT 650M OpenCL device driver locks up the Mac - its defective
bool CudaContext::checkQuarantineForCudaDevice(CUdevice device)
{
    return false;
}

bool CudaContext::checkAvailabilityForCudaDevice(CUdevice device)
{
    return true;
}

size_t CudaContext::deviceNumForCudaDevice(CUdevice device)
{
    for (uint i = 0; i < numDevices; i++) {
        if (devices[i] == device)
            return i;
    }
    return std::string::npos;
}

size_t CudaContext::selectedDeviceNumForCudaDevice(CUdevice device)
{
    for (size_t selectedDeviceNum = 0; selectedDeviceNum < selectedDevices.size(); selectedDeviceNum++) {
        SharedDeviceUsage deviceUsage = selectedDevices[selectedDeviceNum];
        if (deviceUsage->cudaDevice == device)
            return selectedDeviceNum;
    }
    return 0;
}

size_t CudaContext::selectedDeviceNumForDeviceNum(uint deviceNum)
{
    SharedDeviceUsage & deviceUsage = deviceUsages[deviceNum];
    return selectedDeviceNumForCudaDevice(deviceUsage->cudaDevice);
}

SharedDeviceUsage CudaContext::deviceUsageForCudaDevice(CUdevice device)
{
    for (uint i = 0; i < numDevices; i++) {
        SharedDeviceUsage deviceUsage = deviceUsages[i];
        if (deviceUsage->cudaDevice == device)
            return deviceUsage;
    }
    return std::shared_ptr<DeviceUsage>();
}

size_t CudaContext::deviceUsageIndexForCudaDevice(CUdevice device)
{
    for (size_t i = 0; i < numDevices; i++) {
        SharedDeviceUsage deviceUsage = deviceUsages[i];
        if (deviceUsage->cudaDevice == device)
            return i;
    }
    return std::string::npos;
}

bool CudaContext::checkAvailabilityOfOneCudaDevice(CUdevice *ids, unsigned count)
{
    for (unsigned i = 0; i < count; i++) {
        if (checkAvailabilityForCudaDevice(ids[i]) == true) {
            return true;
        }
    }
    return false;
}

bool CudaContext::checkNonQuarantineOfCudaDevice(CUdevice *ids, unsigned count)
{
    for (unsigned i = 0; i < count; i++) {
        if (checkQuarantineForCudaDevice(ids[i]) == false) {
            return true;
        }
    }
    return false;
}

bool CudaContext::needsStartupConfig()
{
    if (numDevices == 0)
        return false;
//    if (! DeviceContext::wasLastShutdownOK())
//        return true;
    return false;
}

bool CudaContext::hasGPU(StringsVector &vendors, bool checkLocalMemoryOnDevice, BoolsVector & localMemoryArray, bool ignoreQuarantine)
{
    return CudaContext::deviceCount() > 0;
}

bool CudaContext::hasDiscreteGPU()
{
    return CudaContext::deviceCount() > 0;
}

bool CudaContext::hasADiscreteGPU()
{
    return CudaContext::deviceCount() > 0;
}

void CudaContext::tempLicenseForDeviceID1(void *deviceID1)
{
    std::unordered_set<SharedDeviceUsage> usedSet;
    
    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        if (deviceUsage->used)
            usedSet.insert(deviceUsage);
    }
    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        deviceUsage->used       = false;
        deviceUsage->stagedUsed =false;
    }
    
    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        if (deviceUsage->cudaDevice == (size_t)deviceID1) {
            deviceUsage->used        = true;
            deviceUsage->tempLicense = true;
        }
    }
    // one shot override
    predicate = DeviceUsage::predicateForTempLicenseAvailableAndUsedIgnoreQuarantine;
    rearrangeSelectedDevices();
    
    // restage the previously used devices - they will be restored on the next image render
    for (SharedDeviceUsage deviceUsage : usedSet) {
        deviceUsage->stagedUsed = true;
    }
}

void CudaContext::tempLicenseForDeviceID1(void *deviceID1, void *deviceID2, uint deviceCount)
{
    if (deviceCount == 0 || deviceCount > 2)
        return;
    
    std::unordered_set<SharedDeviceUsage> usedSet;
    
    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        if (deviceUsage->used)
            usedSet.insert(deviceUsage);
    }
    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        deviceUsage->used       = false;
        deviceUsage->stagedUsed =false;
    }
    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        if (deviceCount == 1) {
            if (deviceUsage->cudaDevice == (size_t)deviceID1) {
                deviceUsage->used        = true;
                deviceUsage->tempLicense = true;
            }
        }
        else {
            if (deviceUsage->cudaDevice == (size_t)deviceID1 ||
                deviceUsage->cudaDevice == (size_t)deviceID2) {
                deviceUsage->used        = true;
                deviceUsage->tempLicense = true;
            }
        }
    }
    // one shot override
    predicate = DeviceUsage::predicateForTempLicenseAvailableAndUsedIgnoreQuarantine;
    rearrangeSelectedDevices();
    
    // restage the previously used devices - they will be restored on the next image render
    for (SharedDeviceUsage deviceUsage : usedSet) {
        deviceUsage->stagedUsed = true;
    }
}

std::string CudaContext::selectedDevicesPreferenceKey()
{
    return "SelectedCudaDevices";
}

std::string CudaContext::selectedDevicesDescription()
{
    snprintf(buf, sizeof(buf), "%zu Cuda Devices:\n", selectedDevices.size());
    std::string s = buf;
    
    for (SharedDeviceUsage & deviceUsage : selectedDevices) {
        s.append("\t").append(deviceUsage->description()).append("\n");
    }
    return s;
}

CUdevice CudaContext::deviceForDeviceIndex(size_t index)
{
    if (index < numDevices)
        return devices[index];
    return (CUdevice)0;
}

SharedDeviceKind CudaContext::deviceKindForDevice(uint i)
{
    if (i >= numDevices)
        return std::shared_ptr<CudaDeviceKind>();
    SharedDeviceUsage &deviceUsage = deviceUsages[i];
    return deviceUsage->deviceKind;
}

uint CudaContext::warpSizeForDevice(uint i)
{
    if (i >= numDevices)
        return 1;
    SharedDeviceKind deviceKind = deviceKindForDevice(i);
    return deviceKind->warpSize;
}

size_t CudaContext::globalMemSizeForDevice(uint i)
{
    if (i >= numDevices)
        return 1;
    SharedDeviceKind deviceKind = deviceKindForDevice(i);
    return deviceKind->globalMemSize;
}

size_t CudaContext::maxMemAllocSizeForDevice(uint i)
{
    if (i >= numDevices)
        return 1;
    SharedDeviceKind deviceKind = deviceKindForDevice(i);
    return deviceKind->maxMemAllocSize;
}

size_t CudaContext::maxWorkGroupSizeForDevice(uint i)
{
    if (i >= numDevices)
        return 1;
    SharedDeviceKind deviceKind = deviceKindForDevice(i);
    return deviceKind->maxWorkGroupSize;
}

size_t CudaContext::maxWorkGroupSizeForKernelsForDevice(uint i, SharedVariationSet variationSet)
{
    if (i >= numDevices)
        return 1;
    SharedDeviceKind deviceKind = deviceKindForDevice(i);
    size_t mwgs = deviceKind->maxWorkGroupSizeForKernelsForVariationSet(variationSet);
    // on Mountain Lion Preview 3, for CPU, this returns 1024 - but should be 1
    if (deviceKind->isCPU())
        mwgs = 1;
        return mwgs;
}

size_t CudaContext::indexForDevice(CUdevice device)
{
    for (size_t i = 0; i < numDevices; i++) {
        if (devices[i] == device)
            return i;
    }
    return -1; // will cause an exception
}

CUdevice CudaContext::deviceForSelectedDevice(uint selectedDeviceNum)
{
    SharedDeviceUsage deviceUsage = selectedDevices[selectedDeviceNum];
    return deviceUsage->cudaDevice;
}

CUcontext CudaContext::cucontextForSelectedDevice(uint selectedDeviceNum)
{
    size_t deviceNum = deviceNumForSelectedDeviceNum(selectedDeviceNum);
    return contexts[deviceNum];
}

CUstream CudaContext::streamForSelectedDevice(uint selectedDeviceNum)
{
    size_t deviceNum = deviceNumForSelectedDeviceNum(selectedDeviceNum);
    return streams[deviceNum];
}

bool CudaContext::oclDeviceCheckForEnoughMemoryArea(size_t area, uint xformCount, uint supersample)
{
    int numColors = 256;
    size_t amountAlloced;
    for (size_t selectedDeviceNum = 0; selectedDeviceNum < selectedDevices.size(); selectedDeviceNum++) {
        SharedDeviceUsage deviceUsage = selectedDevices[selectedDeviceNum];
        SharedDeviceKind deviceKind   = deviceKindForSelectedDevice(selectedDeviceNum);
        uint warpSize                 = deviceUsage->warpSize;
        uint deviceNum                = (uint)indexOfDeviceUsage(deviceUsage);

        bool success = Flam4CudaRuntime::memCheck(deviceNum,
                                                  area,
                                                  numColors,
                                                  amountAlloced,
                                                  8,
                                                  warpSize,
                                                  true,
                                                  xformCount,
                                                  supersample);
        if (! success)
            return false;
    }
    return true;
}

bool CudaContext::oclDeviceAllocCheckForDeviceNum(size_t deviceNum, float & area, size_t & amountAlloced, uint warpSize, uint xformCount, uint supersample)
{
    SharedDeviceKind deviceKind = deviceKindForDevice((uint)deviceNum);
    // variables used to find the largest area we can support
    int numColors     = 256;
    bool success      = false;
    while (!success) {
        if (!(success = Flam4CudaRuntime::memCheck((uint)deviceNum,
                                                   area,
                                                   numColors,
                                                   amountAlloced,
                                                   8,
                                                   warpSize,
                                                   true,
                                                   xformCount,
                                                   supersample))) {
            area = floorf(area * 0.8f);
            if (area <= 1.0f) // the smallest possible tile size!
                return false;
        }
    }
    return success;
}

bool CudaContext::checkOclGPUMemAvailable(size_t & memoryAvailable,
                                          float & area,
                                          float niceFactor,
                                          uint warpSize,
                                          uint xformCount,
                                          uint supersample,
                                          uint deviceNum)
{
    if (numDevices == 0)
        return false;
    
    size_t amountAlloced = UINT_MAX;
    float    areaGlobal         = (float)UINT_MAX;
    bool     success            = true;
    
    size_t maxMemAllocSize = maxMemAllocSizeForDevice(deviceNum);
    float deviceMaxArea    = floorf(maxMemAllocSize/16.f); // biggest chunk is 16 * area
    
    // start with available memory that can be allocated, then reduce it to 1.2X the area if it greater than that
    areaGlobal = deviceMaxArea > area ? area : deviceMaxArea;
    
    success = oclDeviceAllocCheckForDeviceNum(deviceNum, areaGlobal, amountAlloced, warpSize, xformCount, supersample);
    areaGlobal = floorf(niceFactor * areaGlobal);
    
    memoryAvailable = amountAlloced;
    area            = areaGlobal;
    return success;
}

void CudaContext::refreshMemoryStats()
{
    std::unique_lock<std::mutex> lock(mutex);
    
    for (int i = 0; i < deviceNames.size(); i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        SharedDeviceKind deviceKind    = deviceUsage->deviceKind;
        
        size_t memoryAvailable;
        float area = deviceKind->maxMemAllocSize/16.f;
        checkOclGPUMemAvailable(memoryAvailable,
                                area,
                                1.f,
                                deviceKind->warpSize,
                                MAX_XFORMS,
                                1,
                                i);
        size_t freeMem = 0, totalSize = 0;
        cuMemGetInfo_v2(&freeMem, &totalSize);
        size_t maxMemory = freeMem;
        
        deviceUsage->maxMemory       = maxMemory;
        deviceUsage->availableMemory = memoryAvailable;
        deviceUsage->empty           = memoryAvailable < 36700160.f; // 35 Mb floor
    }
    rearrangeSelectedDevices();
}

#ifdef __ORIGINAL__

- (NSDictionary *)plistForDevice:(CUdevice)device
{
    int index = -1;
    
    for (int i = 0; i < numDevices; i++) {
        if (devices[i] == device) {
            index = i;
            break;
        }
    }
    if (index == -1)
        return nil;
    DeviceUsage *deviceUsage = [deviceUsages objectAtIndex:index];
    DeviceKind *deviceKind = [deviceUsage deviceKind];
    return [deviceKind existingPlist];
}

#endif