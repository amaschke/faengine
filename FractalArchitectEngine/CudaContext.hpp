//
//  CudaContext.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 7/6/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef CudaContext_hpp
#define CudaContext_hpp

#include "Exports.hpp"
#include "DeviceContext.hpp"
#include <cuda.h>

#include <memory>

class CudaContext;
class VariationSet;

using SharedVariationSet   = std::shared_ptr<VariationSet>;
using SharedCudaContext    = std::shared_ptr<CudaContext>;


class FAENGINE_API CudaContext : public DeviceContext {
    CUdevice              *devices;
    CUcontext             *contexts;
    CUstream              *streams;
    
public:
    CudaContext(bool forceRebuild,
                SharedVariationSet variationSet,
                bool _singleDeviceAllowed,
                bool _canUseGPU,
                bool ignoreQuarantine);
    
    void phase2Constructor();
    
    virtual bool createDeviceKinds() override;
    
    static SharedCudaContext makeCudaContext(bool forceRebuild,
                                             SharedVariationSet & _defaultVariationSet,
                                             bool _singleDeviceAllowed,
                                             bool _canUseGPU,
                                             bool _ignoreQuarantine);
    
    static void printDeviceList();
    
    static unsigned deviceCount();
    static bool checkDriverVersion();
    static bool checkQuarantineForCudaDevice(CUdevice device);
    static bool checkAvailabilityForCudaDevice(CUdevice device);
    static bool checkAvailabilityOfOneCudaDevice(CUdevice *ids, unsigned count);
    static bool checkNonQuarantineOfCudaDevice(CUdevice *ids, unsigned count);
    
    size_t deviceNumForCudaDevice(CUdevice device);
    size_t selectedDeviceNumForCudaDevice(CUdevice device);
    size_t selectedDeviceNumForDeviceNum(uint deviceNum);
    
    SharedDeviceUsage deviceUsageForCudaDevice(CUdevice device);
    size_t deviceUsageIndexForCudaDevice(CUdevice device);
    virtual void tempLicenseForDeviceID1(void *deviceID1, void *deviceID2, uint deviceCount) override;
    
    virtual void refreshMemoryStats() override;
    bool needsStartupConfig();
    bool hasGPU(StringsVector &vendors, bool checkLocalMemoryOnDevice, BoolsVector & localMemoryArray, bool ignoreQuarantine);
    virtual bool hasDiscreteGPU() override;
    static bool hasADiscreteGPU();
    
    void tempLicenseForDeviceID1(void *deviceID1);
    
    std::string selectedDevicesPreferenceKey();
    virtual std::string selectedDevicesDescription() override;
    
    CUdevice deviceForDeviceIndex(size_t index);
    SharedDeviceKind deviceKindForDevice(uint i);
    uint warpSizeForDevice(uint i);
    size_t globalMemSizeForDevice(uint i);
    size_t maxMemAllocSizeForDevice(uint i);
    
    size_t maxWorkGroupSizeForDevice(uint i);
    size_t maxWorkGroupSizeForKernelsForDevice(uint i, SharedVariationSet variationSet);
    size_t indexForDevice(CUdevice device);
    
    CUdevice deviceForSelectedDevice(uint selectedDeviceNum);
    CUcontext cucontextForSelectedDevice(uint selectedDeviceNum);
    CUstream streamForSelectedDevice(uint selectedDeviceNum);
    
    bool oclDeviceCheckForEnoughMemoryArea(size_t area,
                                           uint xformCount,
                                           uint supersample) override;
    bool oclDeviceAllocCheckForDeviceNum(size_t deviceNum,
                                         float & area,
                                         size_t & amountAlloced,
                                         uint warpSize,
                                         uint xformCount,
                                         uint supersample) override;
    
    bool checkOclGPUMemAvailable(size_t & memoryAvailable,
                                 float & area,
                                 float niceFactor,
                                 uint warpSize,
                                 uint xformCount,
                                 uint supersample,
                                 uint deviceNum) override;
};

#endif /* CudaContext_hpp */
