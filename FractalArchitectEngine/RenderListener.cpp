//
//  RenderListener.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 6/6/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "RenderListener.hpp"
#include "Utilities.hpp"
#include "DeviceRuntime.hpp"

RenderResult::RenderResult(SharedImage _image,
                           float _fusedFraction,
                           float _retainedFraction,
                           duration<double> _renderTime,
                           enum InitialColorSpace colorSpace)
: image(std::move(_image)), fusedFraction(_fusedFraction), retainedFraction(_retainedFraction), renderTime(_renderTime), eColorSpace(colorSpace)
{}

void RenderListener::logRenderStats(std::string filename,
                                    duration<double> _duration,
                                    duration<double> deDuration,
                                    float width,
                                    float height,
                                    float actualQuality,
                                    size_t numBatches,
                                    std::string tilesLabel,
                                    float fusedFraction,
                                    float retainedFraction,
                                    uint supersample)
{
    char buf[200];
    float sampleLevel = log2f(actualQuality * retainedFraction);
    
    
    
    size_t totalIterations = DeviceRuntime::totalIterationsForBatches(numBatches);
    
    duration<double> delta = duration_cast<duration<double>>(_duration - deDuration);
    double mips            = (double)totalIterations/delta.count()/1000000.;
    
    snprintf(buf, sizeof(buf), "%s [%.0fX%.0f]%s SS:%u Total:%0.2f sec DE:%0.2f sec Mips:%0.2f Quality:%.0f Batches:%zu fusedFraction:%.2f%% retainedFraction:%.2f%% Sampling Level:%.2f",
             filename.c_str(),
             width, height, tilesLabel.c_str(), supersample, _duration.count(), deDuration.count(), mips, actualQuality, numBatches,
             100.f*fusedFraction, 100.f*retainedFraction, sampleLevel);
    systemLog(buf);
}

