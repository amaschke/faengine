//
//  Utilities.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/2/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef Utilities_hpp
#define Utilities_hpp

#include <string>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <memory>
#include <sys/stat.h>

#include "Exports.hpp"

template<typename ... Args>
std::string string_format(const char * format, Args ... args)
{
    size_t size = snprintf( nullptr, 0, format, args ... ) + 1; // Extra space for '\0'

    std::unique_ptr<char[]> buf( new char[ size ] );
    snprintf( buf.get(), size, format, args ... );
    return std::string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
}

template <class K, class T>
std::vector<K> allKeys(const std::unordered_map<K, T> & dict)
{
    std::vector<K> keys;
    for (auto & kvp : dict)
    {
        keys.emplace_back(kvp.first);
    }
    return keys;
}

template <class K, class T>
std::vector<T> allValues(std::unordered_map<K, T> dict)
{
    std::vector<T> values;
    for (auto & kvp : dict)
    {
        values.emplace_back(kvp.second);
    }
    return values;
}

template <class K, class T, class CompareFunc>
std::vector<K> keysSortedByValue(const std::unordered_map<K, T> & dict, CompareFunc f)
{
    std::vector<std::pair<K, T>> pairs(dict.begin(), dict.end());  // -- value copies
//    std::vector<std::reference_wrapper<std::pair<K, T>>> pairs; // pair references
//    std::copy(dict.begin(), dict.end(), pairs.begin());
    std::sort(pairs.begin(), pairs.end(), f);
    std::vector<K> keys;
    for (std::pair<K, T> & kvp : pairs)
    {
        keys.emplace_back(kvp.first);
    }
    return keys;
}

template <class K, class T>
std::vector<T> sortedValues(const std::unordered_map<K, T> & dict)
{
    std::vector<T> values = allValues(dict);
    std::sort(values.begin(), values.end());
    return values;
}

// return ndw UUID
FAENGINE_API std::string stringWithUUID();

// case insensitive string compare function
FAENGINE_API bool icompare(std::string const& a, std::string const& b);

// case insensitive string equality
FAENGINE_API bool iequal(std::string const& a, std::string const& b);

// system loggin
FAENGINE_API void systemLog(const char *msg);
FAENGINE_API void systemLog(const std::string &msg);

// get filename from path
FAENGINE_API std::string fileNameFromPath(const std::string &path);
FAENGINE_API const char * directoryNameFromPath(const std::string &path);

// does this file exist?
FAENGINE_API bool fileExists(const std::string& spath);
FAENGINE_API bool fileExists(const char *path);

// trim the specified character from both ends of the string
FAENGINE_API std::string string_trim(const std::string &s, char _char);

// make a pretty string from float values for reporting purposes
FAENGINE_API std::string prettyFloat(float value);

// make a pretty string from float values for reporting purposes - keeps the float .f format
FAENGINE_API std::string prettyFloatF(float value);

// recursive mkdir
#ifndef _WIN32
int mkpath(const char *s, mode_t mode);
#else
FAENGINE_API int mkpath(const char *s, int mode);
#endif

// convert integer i to string
FAENGINE_API const char * itoa(int i);

#ifndef _WIN32
// get hardware tick counter value
time_t GetTickCount(void);
#endif

#endif /* Utilities_hpp */
