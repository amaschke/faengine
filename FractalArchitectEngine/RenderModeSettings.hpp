//
//  RenderModeSettings.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/7/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef RenderModeSettings_hpp
#define RenderModeSettings_hpp

#include <string>
#include "Exports.hpp"
#include "Enums.hpp"
#include "common.hpp"
#include "Image.hpp"

using uint = unsigned int;

class FAENGINE_API RenderModeSettings {
public:
    static enum RenderMode renderModeFromString(const std::string &renderModeStr);
    
    
    enum RenderMode  renderMode;
    FA::Image       *backgroundImage;
    Color           *compositeColor;
    std::string     *imagePath;
    uint             estimatorRadius;
    float            gamma;
    float            alphaGamma;
};

#endif /* RenderModeSettings_hpp */
