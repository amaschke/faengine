//
//  Flame.hpp
//  FractalArchitectEngine
//
//  Copyright 2008 Steven Brodhead
//
//  Created by Steven Brodhead on 4/29/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef Flame_hpp
#define Flame_hpp

#include <vector>
#include <memory>
#include "common.hpp"
#include "Exports.hpp"

#define MAX_XFORMS 58 //We're limited to 64KB constant memory for compute capacity 1.0.
//All xForms must fit in this.
#define VAR_COUNT             97
#define PARM_COUNT           100
#define OTHER_STUFF_COUNT     18
#define XFORM_VARS_MIN_COUNT 213
#define FIRST_VAR_OFFSET      18

#ifdef _WIN32
#define ALIGNED_MALLOC(TYPE, target, alignment, size)   target = (TYPE *)_aligned_malloc(size, alignment)
#define ALIGNED_FREE(target) _aligned_free(target)
#else
#define ALIGNED_MALLOC(TYPE, target, alignment, size)   posix_memalign((void **)&target, alignment, size)
#define ALIGNED_FREE(target) free(target)
#endif


class VariationChain;

using uint = unsigned int;
using SharedVariationChain = std::shared_ptr<VariationChain>;

struct FAENGINE_API alignas(8) xForm
{
    float a;
    float b;
    float c;
    float d;
    float e;
    float f;
    float pa;
    float pb;
    float pc;
    float pd;
    float pe;
    float pf;
    float color;
    float symmetry;
    float weight;
    float opacity;
    float var_color;
    int rotates;
};

struct FAENGINE_API alignas(16) FlameParams
{
    struct rgba background;
    float center[2];				//{x,y}
    float size[2];					//size/(scale*zoom)
    float scale;
    float zoom;
    float cam_yaw;
    float cam_pitch;
    float cam_perspective;
    int   clipToNDC;
    float cam_dof;
    float cam_zpos;
    float cam_x;
    float cam_y;
    float cam_z;
    float cam_fov;
    float cam_near;
    float cam_orthowide;
    float hue;
    float numBatches;
    float quality;
    float desiredQuality;
    float rotation;
    float symmetryKind;
    float brightness;
    float gamma;
    float gammaThreshold;
    float alphaGamma;
    float vibrancy;
    unsigned int   numTrans;
    unsigned int   numFinal;
    float supersampleWidth;
    int   frame;
    int   useXaos;
    int   oversample;
    float   highlightPower;
    int    estimatorRadius;			// default 7
    float  estimatorCurve;			// default 0.4
};

struct FAENGINE_API alignas(16) Flame : public std::enable_shared_from_this<Flame>
{
    struct FlameParams params;
    int                numColors;
	bool               pageAligned;
	uint               alignment;
    struct xForm      *trans;
    struct xForm      *finals;
    struct rgba       *colorIndex;
    float             *colorLocations;
    float             *switchMatrix;
    
    std::vector<SharedVariationChain> xformVarChains;
    std::vector<SharedVariationChain> finalVarChains;
    
#ifdef __cplusplus
    Flame();
    Flame(int numTrans,int paletteSize, int numFinal);
    Flame(int numTrans,int paletteSize, int numFinal, int alignment);
    Flame(const Flame &other);
    Flame(const Flame &other, int alignment);
    
    void Clone(Flame** target);
    void CloneAligned(Flame** target, int alignment);
    void deleteChildren();
    void prepareSwitchMatrix (float *brick);
    ~Flame();
    
    static bool equivalentTransforms(struct xForm * xform1, struct xForm *xform2,
                                     VariationChain &chain1, VariationChain &chain2);
    static bool equivalentPalettes(struct rgba *palette1, struct rgba *palette2, unsigned numColors);
    static bool equivalentPaletteLocations(float *palette1, float *palette2, unsigned numColors);
    static bool equivalentColors(struct rgba *color1, struct rgba *color2);
    static bool equivalentFlames(struct Flame *flame1, struct Flame *flame2);
    static bool equivalentFlameParams(struct FlameParams *params1, struct FlameParams *params2);
    unsigned * makeShuffledIndex();
    void randomTranformShuffle();
    void splitTransformAt(unsigned i);
    void increaseXformCountTo(unsigned xformCount);
#endif
};


#endif /* Flame_hpp */
