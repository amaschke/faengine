#pragma once

#ifdef _WIN32
//#define FAENGINE_API
#ifdef FAENGINE_EXPORTS
#define FAENGINE_API __declspec(dllexport)
#else
#define FAENGINE_API __declspec(dllimport)
#endif
#else

#define FAENGINE_API

#endif
