//
//  ImageRepTileArray.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/16/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "ImageRepTileArray.hpp"
#include "FlameTile.hpp"

#include <exception>
#include <string.h>

#ifdef __linux
namespace std {
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
}
#endif

using namespace FA;

ImageRepTileArray::ImageRepTileArray(size_t _rows, size_t _cols, bool _flipped)
:
    rows(_rows),
    cols(_cols),
    flipped(_flipped),
    reps(),
    renderTimes(),
    batchesRendered(),
    overlap(FlameTile::overlap())
{
    for (size_t i = 0; i <  _rows * _cols; i++) {
        SharedImage image = std::make_shared<FA::Image>();
        reps.push_back(image);
    }
    renderTimes.resize(_rows * _cols, duration<double>(0.));
    batchesRendered.resize(_rows * _cols, 0);
}

void ImageRepTileArray::setTileAt(size_t row,
                                  size_t col,
                                  SharedImage & _image,
                                  duration<double> time,
                                  uint numBatches)
{
    reps[row * cols + col]            = _image;
    renderTimes[row * cols + col]     = time;
    batchesRendered[row * cols + col] = numBatches;
}

// blend the vertical overlapped seam between tiles - we process from the bottom tile upwards flipping rows as we go
void ImageRepTileArray::blendVerticalOverlapRowTop(uint i, uint j, uchar *topTarget)
{
	SharedImage & bottomTile = tileAt(i, j);
	SharedImage & topTile    = tileAt(i - 1, j);
    
    uint samplesPerPixel     = bottomTile->samplesPerPixel();
    uint tileWidth           = bottomTile->bytesPerRow();
    uint tilePixelsWide      = bottomTile->width;
    uint overlapBytesTop     = tileWidth * overlapTop(i);
    uint topOverlapBytesBtm  = tileWidth * overlapBottom(i - 1);
    uint overlappedPixels    = overlapBottom(i - 1) + overlapTop(i);
    uchar *topBytes          = topTile->bitmap.get();
    uchar *bottomBytes       = bottomTile->bitmap.get() + bottomTile->height*tileWidth - overlapBytesTop - topOverlapBytesBtm;
    uchar *target            = topTarget - topOverlapBytesBtm;
    float ramp               = 1.f/((float)overlappedPixels + 1);
    
    for (unsigned n = 1; n <= overlappedPixels; n ++) {
        for (unsigned m = 0; m < tilePixelsWide; m++) {
            target[0] = (float)n * ramp * topBytes[0] + (float)(overlappedPixels - n + 1) * ramp * bottomBytes[0];
            target[1] = (float)n * ramp * topBytes[1] + (float)(overlappedPixels - n + 1) * ramp * bottomBytes[1];
            target[2] = (float)n * ramp * topBytes[2] + (float)(overlappedPixels - n + 1) * ramp * bottomBytes[2];
            if (samplesPerPixel == 4) // do we have an alpha channel?
                target[3] = (float)n * ramp * topBytes[3] + (float)(overlappedPixels - n + 1) * ramp * bottomBytes[3];
                
            target      += samplesPerPixel;
            bottomBytes += samplesPerPixel;
            topBytes    += samplesPerPixel;
        }
    }
}

// blend the vertical overlapped seam between tiles - we process from the bottom tile upwards flipping rows as we go
void ImageRepTileArray::blendVerticalOverlapRowBottom(uint i, uint j, uchar *bottomTarget)
{
	SharedImage & bottomTile = tileAt(i, j);
	SharedImage & topTile    = tileAt(i - 1, j);
    
    uint samplesPerPixel     = topTile->samplesPerPixel();
    uint tileWidth           = topTile->bytesPerRow();
    uint tilePixelsWide      = topTile->width;
    
    // calculate top and bottom overlap in bytes - not pixels
    uint topOverlapBytesBtm  = tileWidth * overlapBottom(i - 1);
    uint botOverlapBytesTop  = tileWidth * overlapTop(i);
    uint overlappedPixels    = overlapBottom(i - 1) + overlapTop(i);
    uchar *topBytes          = topTile->bitmap.get() + topTile->height*tileWidth - topOverlapBytesBtm - botOverlapBytesTop;
    uchar *bottomBytes       = bottomTile->bitmap.get();
    uchar *target            = bottomTarget - topOverlapBytesBtm;
    float ramp               = 1.f/((float)overlappedPixels + 1);
    
    for (unsigned n = 1; n <= overlappedPixels; n ++) {
        for (unsigned m = 0; m < tilePixelsWide; m++) {
            target[0] = (float)n * ramp * bottomBytes[0] + (float)(overlappedPixels - n + 1) * ramp * topBytes[0];
            target[1] = (float)n * ramp * bottomBytes[1] + (float)(overlappedPixels - n + 1) * ramp * topBytes[1];
            target[2] = (float)n * ramp * bottomBytes[2] + (float)(overlappedPixels - n + 1) * ramp * topBytes[2];
            if (samplesPerPixel == 4) // do we have an alpha channel?
                target[3] = (float)n * ramp * bottomBytes[3] + (float)(overlappedPixels - n + 1) * ramp * topBytes[3];
                
                // for debug visualization only
                //target[0] = 0.5f * target[0] + 0.5f;  // overlay a red on top of overlapped zone so we can see it !!
                //target[1] = 0.5f * target[1];
                //target[2] = 0.5f * target[2];
                
                target      += samplesPerPixel;
                bottomBytes += samplesPerPixel;
                topBytes    += samplesPerPixel;
                }
    }
}

// blend the horizontal overlapped seam between tiles
void ImageRepTileArray::blendHorizontalOverlapRow(uint i, uint j, uint k, uchar *rightPixels, uchar *rightTarget)
{
	SharedImage & rightTile    = tileAt(i, j);
	SharedImage & leftTile     = tileAt(i, j - 1);
    uint samplesPerPixel       = rightTile->samplesPerPixel();
    uint overlapBytesLeft      = overlapLeft(j) * rightTile->bitsPerPixel()/8; // overlap for a single strip - not whole tile
    uint leftTileWidth         = leftTile->bytesPerRow();
    uint leftOverlapBytesRight = overlapRight(j - 1) * leftTile->bitsPerPixel()/8;
    uint overlappedPixels      = overlapRight(j - 1) + overlapLeft(j);
    uchar *leftBytes           = leftTile->bitmap.get() + k * leftTileWidth +
                                                leftTileWidth - leftOverlapBytesRight - overlapBytesLeft;
    uchar *rightBytes          = rightPixels - overlapBytesLeft;
    uchar *target              = rightTarget - leftOverlapBytesRight;
    float ramp                 = 1.f/((float)overlappedPixels + 1);
    
    for (unsigned n = 1; n <= overlappedPixels; n ++) {
        target[0] = (float)n * ramp * rightBytes[0] + (float)(overlappedPixels - n + 1) * ramp * leftBytes[0];
        target[1] = (float)n * ramp * rightBytes[1] + (float)(overlappedPixels - n + 1) * ramp * leftBytes[1];
        target[2] = (float)n * ramp * rightBytes[2] + (float)(overlappedPixels - n + 1) * ramp * leftBytes[2];
        if (samplesPerPixel == 4) // do we have an alpha channel?
            target[3] = (float)n * ramp * rightBytes[3] + (float)(overlappedPixels - n + 1) * ramp * leftBytes[3];
            
            target     += samplesPerPixel;
            rightBytes += samplesPerPixel;
            leftBytes  += samplesPerPixel;
            }
}

// create a large image by combining all of the tiles
SharedImage ImageRepTileArray::combinedImageRep(const UniqueBlob & profileData)
{
    for (size_t i = 0; i < reps.size(); i++) {
        if (! reps[i]) {
            throw std::runtime_error("MissingImagesException - Not all tile images have been processed");
        }
    }
    if (rows == 1 && cols == 1)
        return tileAt(0, 0);
    
	SharedImage & rep = tileAt(0, 0);
    
    uint combinedBytesPerRow  = 0;
    uint combinedPixelsWide   = 0;
    uint combinedPixelsHigh   = 0;
    for (uint j = 0; j < cols; j++) {
        combinedBytesPerRow += tileAt(0, j)->bytesPerRow() - (overlapLeft(j) + overlapRight(j)) * rep->bitsPerPixel()/8;
        combinedPixelsWide  += tileAt(0, j)->width         - (overlapLeft(j) + overlapRight(j));
    }
    for (uint i = 0; i < rows; i++) {
        combinedPixelsHigh += tileAt(i, 0)->height - (overlapTop(i) + overlapBottom(i));
    }
    
    Image combined(combinedPixelsWide,
                   combinedPixelsHigh,
                   rep->map,
                   rep->bitDepth, 
					rep->transparent,
                   new uchar[combinedBytesPerRow * combinedPixelsHigh]);
    combined.flipped = rep->flipped;
    combined.profile = std::make_unique<Blob>(*profileData.get());
    
    
    // combine them row by row
    uchar *pixels  = combined.bitmap.get();
    
    if (cols == 1) {
        // simpler case
        if (flipped) {
            for (int i = (int)rows - 1; i >= 0; i--) {  // for each row of pixels in reverse order - flips the image
				SharedImage & tile    = tileAt(i, 0);
                uint tileWidth        = tile->bytesPerRow();
                uint tileHeight       = tile->height;
                // calculate top and bottom overlap in bytes - not pixels
                uint overlapBytesTop  = tile->width * overlapTop(i)    * tile->bitsPerPixel()/8;
                uint overlapBytesBtm  = tile->width * overlapBottom(i) * tile->bitsPerPixel()/8;
                
                uint stride           = tileWidth * tileHeight - overlapBytesTop - overlapBytesBtm;	// pointer advance amount for this tile
                uchar *tilePixels     = tile->bitmap.get() + overlapBytesBtm;		// we dont blit the leading overlap
                
                memcpy((void *)pixels, tilePixels, stride);
                
                // blend our bottom overlap with our top neighbor's top overlap - remember we are flipping pixel rows
                if (i < rows - 1) { // top row has no bottom neighbor
                    blendVerticalOverlapRowTop(i + 1, 0, pixels);
                }
                pixels += stride;
            }
        }
        else {
            for (uint i = 0; i < rows; i++) {
				SharedImage & tile    = tileAt(i, 0);
                uint tileWidth        = tile->bytesPerRow();
                uint tileHeight       = tile->height;
                // calculate top and bottom overlap in bytes - not pixels
                uint overlapBytesTop  = tile->width * overlapTop(i)    * tile->bitsPerPixel()/8;
                uint overlapBytesBtm  = tile->width * overlapBottom(i) * tile->bitsPerPixel()/8;
                
                uint stride           = tileWidth * tileHeight - overlapBytesTop - overlapBytesBtm;	// pointer advance amount for this tile
                uchar *tilePixels     = tile->bitmap.get() + overlapBytesTop;
                
                memcpy((void *)pixels, tilePixels, stride);
                
                // blend our top overlap with our top neighbor's bottom overlap
                if (i > 0) { // top row has no top neighbor
                    blendVerticalOverlapRowBottom(i, 0, pixels);
                }
                pixels += stride;
            }
        }
    }
    else {
        // interleave the pixels
        for (int i = (int)rows - 1; i >= 0; i--) {  // for each row of pixels in reverse order - flips the image
			SharedImage & col0Tile = tileAt(i, 0);
            
            uint tilePixelsHigh  = col0Tile->height - overlapBottom(i); // we dont copy the bottom overlap
            uchar * pixelsForRow = pixels;
            for (uint k = overlapBottom(i); k < tilePixelsHigh; k++) { // for each row of pixels in this row of tiles
                for (uint j = 0; j < cols; j++) {     // for each column of tiles
					SharedImage & tile     = tileAt(i, j);
                    uint tileWidth         = tile->bytesPerRow();
                    uint overlapBytesLeft  = overlapLeft(j)  * tile->bitsPerPixel()/8; // overlap for a single strip - not whole tile
                    uint overlapBytesRight = overlapRight(j) * tile->bitsPerPixel()/8;
                    
                    uint stride            = tileWidth - overlapBytesLeft - overlapBytesRight;	// pointer advance amount for this strip
                    uchar *tilePixels      = tile->bitmap.get() + k * tileWidth + overlapBytesLeft; // k is already adjusted for top overlap
                    memcpy((void *)pixels, tilePixels, stride);
                    
                    // blend our left overlap with our left neighbor's right overlap
                    if (j > 0) { // first column has no left neighbor
                        blendHorizontalOverlapRow(i, j, k, tilePixels, pixels);
                    }
                    pixels += stride;
                }
            }
            
            // blend our bottom overlap with our top neighbor's top overlap - remember we are flipping pixel rows
            if (i < rows - 1) { // top column has no bottom neighbor
                blendVerticalOverlapRowTop(i + 1, 0, pixelsForRow);
                
            }
        }
    }
    return std::make_unique<Image>(combined);
}


#ifdef __ORIGINAL__

- (NSString *)description
{
    NSMutableString * s = [NSMutableString stringWithFormat:@"%s\n", [super description]];
    for (size_t i = 0; i < rows; i++) {
        for (size_t j = 0; j < cols; j++) {
            [s appendFormat:@"\trow:%lu col:%lu batchesRendered:%u\n", i, j, [self batchesRenderedAtIndex:i*j]];
        }
    }
    return s;
}

#endif
