//
//  GlhFunctions.c
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/22/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//

//This Software(*), glhlib created by Vrej Melkonian, is open source under the LGPL license.
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of
//this Software and associated documentation files (the "Software"), to deal in the
//Software without restriction, including without limitation the rights to use,
//copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//the Software, and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR BE LIABLE
//FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
//TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
//THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <math.h>
#include "GlhFunctions.h"

#define TRUE 1
#define FALSE 0

#define MAT(m,r,c) (m)[(c)*4+(r)]
#define SWAP_ROWS_FLOAT(a, b) { float *_tmp = a; (a)=(b); (b)=_tmp; }

void NormalizeVectorFLOAT_2(float *pvector)
{
    float normalizingConstant;
    normalizingConstant=1.0/sqrtf(pvector[0]*pvector[0]+pvector[1]*pvector[1]+pvector[2]*pvector[2]);
    pvector[0]*=normalizingConstant;
    pvector[1]*=normalizingConstant;
    pvector[2]*=normalizingConstant;
}

float DistanceSquareBetweenTwoPoints_FLOAT(float *ppoint1, float *ppoint2)
{
    float pvector[3];
    
    pvector[0]=ppoint1[0]-ppoint2[0];
    pvector[1]=ppoint1[1]-ppoint2[1];
    pvector[2]=ppoint1[2]-ppoint2[2];
    
    return pvector[0]*pvector[0]+pvector[1]*pvector[1]+pvector[2]*pvector[2];
}


int glhInvertMatrixf2(float *m, float *out)
{
    float wtmp[4][8];
    float m0, m1, m2, m3, s;
    float *r0, *r1, *r2, *r3;
    
    r0 = wtmp[0], r1 = wtmp[1], r2 = wtmp[2], r3 = wtmp[3];
    
    r0[0] = MAT(m, 0, 0), r0[1] = MAT(m, 0, 1),
    r0[2] = MAT(m, 0, 2), r0[3] = MAT(m, 0, 3),
    r0[4] = 1.0, r0[5] = r0[6] = r0[7] = 0.0,
    r1[0] = MAT(m, 1, 0), r1[1] = MAT(m, 1, 1),
    r1[2] = MAT(m, 1, 2), r1[3] = MAT(m, 1, 3),
    r1[5] = 1.0, r1[4] = r1[6] = r1[7] = 0.0,
    r2[0] = MAT(m, 2, 0), r2[1] = MAT(m, 2, 1),
    r2[2] = MAT(m, 2, 2), r2[3] = MAT(m, 2, 3),
    r2[6] = 1.0, r2[4] = r2[5] = r2[7] = 0.0,
    r3[0] = MAT(m, 3, 0), r3[1] = MAT(m, 3, 1),
    r3[2] = MAT(m, 3, 2), r3[3] = MAT(m, 3, 3),
    r3[7] = 1.0, r3[4] = r3[5] = r3[6] = 0.0;
    
    /* choose pivot - or die */
    if (fabsf(r3[0]) > fabsf(r2[0]))
        SWAP_ROWS_FLOAT(r3, r2);
    if (fabsf(r2[0]) > fabsf(r1[0]))
        SWAP_ROWS_FLOAT(r2, r1);
    if (fabsf(r1[0]) > fabsf(r0[0]))
        SWAP_ROWS_FLOAT(r1, r0);
    if (0.0 == r0[0])
        return 0;
    
    /* eliminate first variable     */
    m1 = r1[0] / r0[0];
    m2 = r2[0] / r0[0];
    m3 = r3[0] / r0[0];
    s = r0[1];
    r1[1] -= m1 * s;
    r2[1] -= m2 * s;
    r3[1] -= m3 * s;
    s = r0[2];
    r1[2] -= m1 * s;
    r2[2] -= m2 * s;
    r3[2] -= m3 * s;
    s = r0[3];
    r1[3] -= m1 * s;
    r2[3] -= m2 * s;
    r3[3] -= m3 * s;
    s = r0[4];
    if (s != 0.0) {
        r1[4] -= m1 * s;
        r2[4] -= m2 * s;
        r3[4] -= m3 * s;
    }
    s = r0[5];
    if (s != 0.0) {
        r1[5] -= m1 * s;
        r2[5] -= m2 * s;
        r3[5] -= m3 * s;
    }
    s = r0[6];
    if (s != 0.0) {
        r1[6] -= m1 * s;
        r2[6] -= m2 * s;
        r3[6] -= m3 * s;
    }
    s = r0[7];
    if (s != 0.0) {
        r1[7] -= m1 * s;
        r2[7] -= m2 * s;
        r3[7] -= m3 * s;
    }
    
    /* choose pivot - or die */
    if (fabsf(r3[1]) > fabsf(r2[1]))
        SWAP_ROWS_FLOAT(r3, r2);
    if (fabsf(r2[1]) > fabsf(r1[1]))
        SWAP_ROWS_FLOAT(r2, r1);
    if (0.0 == r1[1])
        return 0;
    
    /* eliminate second variable */
    m2 = r2[1] / r1[1];
    m3 = r3[1] / r1[1];
    r2[2] -= m2 * r1[2];
    r3[2] -= m3 * r1[2];
    r2[3] -= m2 * r1[3];
    r3[3] -= m3 * r1[3];
    s = r1[4];
    if (0.0 != s) {
        r2[4] -= m2 * s;
        r3[4] -= m3 * s;
    }
    s = r1[5];
    if (0.0 != s) {
        r2[5] -= m2 * s;
        r3[5] -= m3 * s;
    }
    s = r1[6];
    if (0.0 != s) {
        r2[6] -= m2 * s;
        r3[6] -= m3 * s;
    }
    s = r1[7];
    if (0.0 != s) {
        r2[7] -= m2 * s;
        r3[7] -= m3 * s;
    }
    
    /* choose pivot - or die */
    if (fabsf(r3[2]) > fabsf(r2[2]))
        SWAP_ROWS_FLOAT(r3, r2);
    if (0.0 == r2[2])
        return 0;
    
    /* eliminate third variable */
    m3 = r3[2] / r2[2];
    r3[3] -= m3 * r2[3], r3[4] -= m3 * r2[4],
    r3[5] -= m3 * r2[5], r3[6] -= m3 * r2[6], r3[7] -= m3 * r2[7];
    
    /* last check */
    if (0.0 == r3[3])
        return 0;
    
    s = 1.0 / r3[3];		/* now back substitute row 3 */
    r3[4] *= s;
    r3[5] *= s;
    r3[6] *= s;
    r3[7] *= s;
    
    m2 = r2[3];			/* now back substitute row 2 */
    s = 1.0 / r2[2];
    r2[4] = s * (r2[4] - r3[4] * m2), r2[5] = s * (r2[5] - r3[5] * m2),
    r2[6] = s * (r2[6] - r3[6] * m2), r2[7] = s * (r2[7] - r3[7] * m2);
    m1 = r1[3];
    r1[4] -= r3[4] * m1, r1[5] -= r3[5] * m1,
    r1[6] -= r3[6] * m1, r1[7] -= r3[7] * m1;
    m0 = r0[3];
    r0[4] -= r3[4] * m0, r0[5] -= r3[5] * m0,
    r0[6] -= r3[6] * m0, r0[7] -= r3[7] * m0;
    
    m1 = r1[2];			/* now back substitute row 1 */
    s = 1.0 / r1[1];
    r1[4] = s * (r1[4] - r2[4] * m1), r1[5] = s * (r1[5] - r2[5] * m1),
    r1[6] = s * (r1[6] - r2[6] * m1), r1[7] = s * (r1[7] - r2[7] * m1);
    m0 = r0[2];
    r0[4] -= r2[4] * m0, r0[5] -= r2[5] * m0,
    r0[6] -= r2[6] * m0, r0[7] -= r2[7] * m0;
    
    m0 = r0[1];			/* now back substitute row 0 */
    s = 1.0 / r0[0];
    r0[4] = s * (r0[4] - r1[4] * m0), r0[5] = s * (r0[5] - r1[5] * m0),
    r0[6] = s * (r0[6] - r1[6] * m0), r0[7] = s * (r0[7] - r1[7] * m0);
    
    MAT(out, 0, 0) = r0[4];
    MAT(out, 0, 1) = r0[5], MAT(out, 0, 2) = r0[6];
    MAT(out, 0, 3) = r0[7], MAT(out, 1, 0) = r1[4];
    MAT(out, 1, 1) = r1[5], MAT(out, 1, 2) = r1[6];
    MAT(out, 1, 3) = r1[7], MAT(out, 2, 0) = r2[4];
    MAT(out, 2, 1) = r2[5], MAT(out, 2, 2) = r2[6];
    MAT(out, 2, 3) = r2[7], MAT(out, 3, 0) = r3[4];
    MAT(out, 3, 1) = r3[5], MAT(out, 3, 2) = r3[6];
    MAT(out, 3, 3) = r3[7];
    
    return 1;
}


//glOrtho(0.0, Width, 0.0, Height, 1.0, -1.0);        <--- use this
//Set modelview to identity
//modelview is 16 elements, projection is 16, viewport is 4
//windowcoordinate is 3.
//This is only correct when glDepthRange(0.0, 1.0)
//Returns 1 if OK else 0.
int glhProjectf(float objx, float objy, float objz,
                float *modelview, float *projection,
                int *viewport,
                float *windowCoordinate)
{
    //Transformation vectors
    float fTempo[8];
    //Modelview transform
    fTempo[0]=modelview[0]*objx+
    modelview[4]*objy+
    modelview[8]*objz+
    modelview[12];		//w is always 1
    fTempo[1]=modelview[1]*objx+
    modelview[5]*objy+
    modelview[9]*objz+
    modelview[13];
    fTempo[2]=modelview[2]*objx+
    modelview[6]*objy+
    modelview[10]*objz+
    modelview[14];
    fTempo[3]=modelview[3]*objx+
    modelview[7]*objy+
    modelview[11]*objz+
    modelview[15];
    //Projection transform, the final row of projection matrix is always [0 0 -1 0]
    //so we optimize for that.
    fTempo[4]=projection[0]*fTempo[0]+
    projection[4]*fTempo[1]+
    projection[8]*fTempo[2]+
    projection[12]*fTempo[3];
    fTempo[5]=projection[1]*fTempo[0]+
    projection[5]*fTempo[1]+
    projection[9]*fTempo[2]+
    projection[13]*fTempo[3];
    fTempo[6]=projection[2]*fTempo[0]+
    projection[6]*fTempo[1]+
    projection[10]*fTempo[2]+
    projection[14]*fTempo[3];
    fTempo[7]=-fTempo[2];
    
    //The result normalizes between -1 and 1
    if(fTempo[7]==0.0)	//The w value
        return 0;
    
    fTempo[7]=1.0/fTempo[7];
    
    //Perspective division
    fTempo[4]*=fTempo[7];
    fTempo[5]*=fTempo[7];
    fTempo[6]*=fTempo[7];
    
    //Window coordinates
    //Map x, y to range 0-1
    windowCoordinate[0]=(fTempo[4]*0.5+0.5)*viewport[2]+viewport[0];
    windowCoordinate[1]=(fTempo[5]*0.5+0.5)*viewport[3]+viewport[1];
    
    //This is only correct when glDepthRange(0.0, 1.0)
    windowCoordinate[2]=(1.0+fTempo[6])*0.5;	//Between 0 and 1
    return 1;
}

int glhUnProjectf(float winx, float winy, float winz,
                  float *modelview, float *projection,
                  int *viewport,
                  float *objectCoordinate)
{
    //Transformation matrices
    float m[16], A[16];
    float in[4], out[4];
    
    //Calculation for inverting a matrix, compute projection x modelview
    //and store in A[16]
    MultiplyMatrices4by4OpenGL_FLOAT(A, projection, modelview);
    //Now compute the inverse of matrix A
    if(glhInvertMatrixf2(A, m)==0)
        return 0;
    
    //Transformation of normalized coordinates between -1 and 1
    in[0]=(winx-(float)viewport[0])/(float)viewport[2]*2.0-1.0;
    in[1]=(winy-(float)viewport[1])/(float)viewport[3]*2.0-1.0;
    in[2]=2.0*winz-1.0;
    in[3]=1.0;
    
    //Objects coordinates
    MultiplyMatrixByVector4by4OpenGL_FLOAT(out, m, in);
    if(out[3]==0.0)
        return 0;
    out[3]=1.0/out[3];
    objectCoordinate[0]=out[0]*out[3];
    objectCoordinate[1]=out[1]*out[3];
    objectCoordinate[2]=out[2]*out[3];
    return 1;
}

void glhFrustumf2(float *matrix, float left, float right, float bottom, float top, float znear, float zfar)
{
    float temp, temp2, temp3, temp4;
    temp=2.0*znear;
    temp2=right-left;
    temp3=top-bottom;
    temp4=zfar-znear;
    matrix[0]=temp/temp2;
    matrix[1]=0.0;
    matrix[2]=0.0;
    matrix[3]=0.0;
    matrix[4]=0.0;
    matrix[5]=temp/temp3;
    matrix[6]=0.0;
    matrix[7]=0.0;
    matrix[8]=(right+left)/temp2;
    matrix[9]=(top+bottom)/temp3;
    matrix[10]=(-zfar-znear)/temp4;
    matrix[11]=-1.0;
    matrix[12]=0.0;
    matrix[13]=0.0;
    matrix[14]=(-temp*zfar)/temp4;
    matrix[15]=0.0;
}

void glhFrustumInfiniteFarPlanef2(float *matrix, float left, float right, float bottom, float top, float znear)
{
    float temp, temp2, temp3;
    temp=2.0*znear;
    temp2=right-left;
    temp3=top-bottom;
    matrix[0]=temp/temp2;
    matrix[1]=0.0;
    matrix[2]=0.0;
    matrix[3]=0.0;
    matrix[4]=0.0;
    matrix[5]=temp/temp3;
    matrix[6]=0.0;
    matrix[7]=0.0;
    matrix[8]=(right+left)/temp2;
    matrix[9]=(top+bottom)/temp3;
    matrix[10]=-1.0;
    matrix[11]=-1.0;
    matrix[12]=0.0;
    matrix[13]=0.0;
    matrix[14]=-2.0*znear;
    matrix[15]=0.0;
}

void glhPerspectiveInfiniteFarPlanef2(float *matrix, float fovyInDegrees, float aspectRatio, float znear)
{
    float ymax, xmax;
    ymax=znear*tanf(fovyInDegrees*M_PI/360.0);
    //ymin=-ymax;
    //xmin=-ymax*aspectRatio;
    xmax=ymax*aspectRatio;
    glhFrustumInfiniteFarPlanef2(matrix, -xmax, xmax, -ymax, ymax, znear);
}

void glhPerspectivef2(float *matrix, float fovyInDegrees, float aspectRatio, float znear, float zfar)
{
    float ymax, xmax;
    ymax=znear*tanf(fovyInDegrees*M_PI/360.0);
    //ymin=-ymax;
    //xmin=-ymax*aspectRatio;
    xmax=ymax*aspectRatio;
    glhFrustumf2(matrix, -xmax, xmax, -ymax, ymax, znear, zfar);
}

void glhTranslatef2(float *matrix, float x, float y, float z)
{
    matrix[12]=matrix[0]*x+matrix[4]*y+matrix[8]*z+matrix[12];
    matrix[13]=matrix[1]*x+matrix[5]*y+matrix[9]*z+matrix[13];
    matrix[14]=matrix[2]*x+matrix[6]*y+matrix[10]*z+matrix[14];
    matrix[15]=matrix[3]*x+matrix[7]*y+matrix[11]*z+matrix[15];
}

// right handed lookat
void glhLookAtf2( float *matrix, float *eyePosition3D,
                 float *center3D, float *upVector3D )
{
    float forward[3], side[3], up[3];
    //------------------
    forward[0] = center3D[0] - eyePosition3D[0];
    forward[1] = center3D[1] - eyePosition3D[1];
    forward[2] = center3D[2] - eyePosition3D[2];
    
    float invDist = 1.f/sqrt(forward[0]*forward[0] + forward[1]*forward[1] + forward[2]*forward[2]);
    forward[0] *= invDist;
    forward[1] *= invDist;
    forward[2] *= invDist;
    //------------------
    //Side = forward x up
    side[0] = forward[1] * upVector3D[2] - forward[2] * upVector3D[1];
    side[1] = forward[2] * upVector3D[0] - forward[0] * upVector3D[2];
    side[2] = forward[0] * upVector3D[1] - forward[1] * upVector3D[0];
    invDist = 1.f/sqrt(side[0]*side[0] + side[1]*side[1] + side[2]*side[2]);
    side[0] *= invDist;
    side[1] *= invDist;
    side[2] *= invDist;
    
    //------------------
    //Recompute up as: up = side x forward
    up[0] = side[1] * forward[2] - side[2] * forward[1];
    up[1] = side[2] * forward[0] - side[0] * forward[2];
    up[2] = side[0] * forward[1] - side[1] * forward[0];
    
    //------------------
    matrix[0] = side[0];
    matrix[4] = side[1];
    matrix[8] = side[2];
    matrix[12] = 0.0;
    //------------------
    matrix[1] = up[0];
    matrix[5] = up[1];
    matrix[9] = up[2];
    matrix[13] = 0.0;
    //------------------
    matrix[2] = -forward[0];
    matrix[6] = -forward[1];
    matrix[10] = -forward[2];
    matrix[14] = 0.0;
    //------------------
    matrix[3] = matrix[7] = matrix[11] = 0.0;
    matrix[15] = 1.0;
    //------------------
    
    glhTranslatef2(matrix, -eyePosition3D[0], -eyePosition3D[1], -eyePosition3D[2]);
}

// left handed variant
void glhLookAtLHf2( float *matrix, float *eyePosition3D,
                   float *center3D, float *upVector3D )
{
    float forward[3], side[3], up[3];
    //------------------
    forward[0] = center3D[0] - eyePosition3D[0];
    forward[1] = center3D[1] - eyePosition3D[1];
    forward[2] = center3D[2] - eyePosition3D[2];
    
    float invDist = 1.f/sqrt(forward[0]*forward[0] + forward[1]*forward[1] + forward[2]*forward[2]);
    forward[0] *= invDist;
    forward[1] *= invDist;
    forward[2] *= invDist;
    //------------------
    //Side = forward x up - right handed   ==> negate components for lefthanded
    side[0] = forward[1] * upVector3D[2] - forward[2] * upVector3D[1];
    side[1] = forward[2] * upVector3D[0] - forward[0] * upVector3D[2];
    side[2] = forward[0] * upVector3D[1] - forward[1] * upVector3D[0];
    side[0] = -side[0];
    side[1] = -side[1];
    side[2] = -side[2];
    
    invDist = 1.f/sqrt(side[0]*side[0] + side[1]*side[1] + side[2]*side[2]);
    side[0] *= invDist;
    side[1] *= invDist;
    side[2] *= invDist;
    
    //------------------
    //Recompute up as: up = side x forward (right handed) ==> negate components for lefthanded
    up[0] = side[1] * forward[2] - side[2] * forward[1];
    up[1] = side[2] * forward[0] - side[0] * forward[2];
    up[2] = side[0] * forward[1] - side[1] * forward[0];
    up[0] = -up[0];
    up[1] = -up[1];
    up[2] = -up[2];
    
    //------------------
    matrix[0] = side[0];
    matrix[4] = side[1];
    matrix[8] = side[2];
    matrix[12] = 0.0;
    //------------------
    matrix[1] = up[0];
    matrix[5] = up[1];
    matrix[9] = up[2];
    matrix[13] = 0.0;
    //------------------
    matrix[2] = -forward[0];
    matrix[6] = -forward[1];
    matrix[10] = -forward[2];
    matrix[14] = 0.0;
    //------------------
    matrix[3] = matrix[7] = matrix[11] = 0.0;
    matrix[15] = 1.0;
    //------------------
    
    glhTranslatef2(matrix, -eyePosition3D[0], -eyePosition3D[1], -eyePosition3D[2]);
}

void glhOrthof2(float *matrix, float left, float right, float bottom, float top, float znear, float zfar)
{
    float temp2, temp3, temp4;
    temp2=right-left;
    temp3=top-bottom;
    temp4=zfar-znear;
    matrix[0]=2.0/temp2;
    matrix[1]=0.0;
    matrix[2]=0.0;
    matrix[3]=0.0;
    matrix[4]=0.0;
    matrix[5]=2.0/temp3;
    matrix[6]=0.0;
    matrix[7]=0.0;
    matrix[8]=0.0;
    matrix[9]=0.0;
    matrix[10]=-2.0/temp4;
    matrix[11]=0.0;
    matrix[12]=(-right-left)/temp2;
    matrix[13]=(-top-bottom)/temp3;
    matrix[14]=(-zfar-znear)/temp4;
    matrix[15]=1.0;
}

void glhOrthofInfiniteFarPlane(float *matrix, float left, float right, float bottom, float top)
{
    float temp2, temp3;
    temp2=right-left;
    temp3=top-bottom;
    matrix[0]=2.0/temp2;
    matrix[1]=0.0;
    matrix[2]=0.0;
    matrix[3]=0.0;
    matrix[4]=0.0;
    matrix[5]=2.0/temp3;
    matrix[6]=0.0;
    matrix[7]=0.0;
    matrix[8]=0.0;
    matrix[9]=0.0;
    matrix[10]=-0.;
    matrix[11]=0.0;
    matrix[12]=(-right-left)/temp2;
    matrix[13]=(-top-bottom)/temp3;
    matrix[14]=-1.;
    matrix[15]=1.0;
}

void MultiplyMatrixByVector4by4OpenGL_FLOAT(float *resultvector, const float *matrix, const float *pvector)
{
    resultvector[0]=matrix[0]*pvector[0]+matrix[4]*pvector[1]+matrix[8]*pvector[2]+matrix[12]*pvector[3];
    resultvector[1]=matrix[1]*pvector[0]+matrix[5]*pvector[1]+matrix[9]*pvector[2]+matrix[13]*pvector[3];
    resultvector[2]=matrix[2]*pvector[0]+matrix[6]*pvector[1]+matrix[10]*pvector[2]+matrix[14]*pvector[3];
    resultvector[3]=matrix[3]*pvector[0]+matrix[7]*pvector[1]+matrix[11]*pvector[2]+matrix[15]*pvector[3];
}

//PURPOSE:	For square matrices. This is column major for OpenGL
void MultiplyMatrices4by4OpenGL_FLOAT(float *result, float *matrix1, float *matrix2)
{
    result[0]=matrix1[0]*matrix2[0]+
    matrix1[4]*matrix2[1]+
    matrix1[8]*matrix2[2]+
    matrix1[12]*matrix2[3];
    result[4]=matrix1[0]*matrix2[4]+
    matrix1[4]*matrix2[5]+
    matrix1[8]*matrix2[6]+
    matrix1[12]*matrix2[7];
    result[8]=matrix1[0]*matrix2[8]+
    matrix1[4]*matrix2[9]+
    matrix1[8]*matrix2[10]+
    matrix1[12]*matrix2[11];
    result[12]=matrix1[0]*matrix2[12]+
    matrix1[4]*matrix2[13]+
    matrix1[8]*matrix2[14]+
    matrix1[12]*matrix2[15];
    
    result[1]=matrix1[1]*matrix2[0]+
    matrix1[5]*matrix2[1]+
    matrix1[9]*matrix2[2]+
    matrix1[13]*matrix2[3];
    result[5]=matrix1[1]*matrix2[4]+
    matrix1[5]*matrix2[5]+
    matrix1[9]*matrix2[6]+
    matrix1[13]*matrix2[7];
    result[9]=matrix1[1]*matrix2[8]+
    matrix1[5]*matrix2[9]+
    matrix1[9]*matrix2[10]+
    matrix1[13]*matrix2[11];
    result[13]=matrix1[1]*matrix2[12]+
    matrix1[5]*matrix2[13]+
    matrix1[9]*matrix2[14]+
    matrix1[13]*matrix2[15];
    
    result[2]=matrix1[2]*matrix2[0]+
    matrix1[6]*matrix2[1]+
    matrix1[10]*matrix2[2]+
    matrix1[14]*matrix2[3];
    result[6]=matrix1[2]*matrix2[4]+
    matrix1[6]*matrix2[5]+
    matrix1[10]*matrix2[6]+
    matrix1[14]*matrix2[7];
    result[10]=matrix1[2]*matrix2[8]+
    matrix1[6]*matrix2[9]+
    matrix1[10]*matrix2[10]+
    matrix1[14]*matrix2[11];
    result[14]=matrix1[2]*matrix2[12]+
    matrix1[6]*matrix2[13]+
    matrix1[10]*matrix2[14]+
    matrix1[14]*matrix2[15];
    
    result[3]=matrix1[3]*matrix2[0]+
    matrix1[7]*matrix2[1]+
    matrix1[11]*matrix2[2]+
    matrix1[15]*matrix2[3];
    result[7]=matrix1[3]*matrix2[4]+
    matrix1[7]*matrix2[5]+
    matrix1[11]*matrix2[6]+
    matrix1[15]*matrix2[7];
    result[11]=matrix1[3]*matrix2[8]+
    matrix1[7]*matrix2[9]+
    matrix1[11]*matrix2[10]+
    matrix1[15]*matrix2[11];
    result[15]=matrix1[3]*matrix2[12]+
    matrix1[7]*matrix2[13]+
    matrix1[11]*matrix2[14]+
    matrix1[15]*matrix2[15];
}

void glhRotateAboutZf2(float *matrix, float angleInRadians)
{
    //See page 191 of Elementary Linear Algebra by Howard Anton
    float m[16], rotate[16];
    
    //Make a copy
    m[0]=matrix[0];
    m[1]=matrix[1];
    m[2]=matrix[2];
    m[3]=matrix[3];
    m[4]=matrix[4];
    m[5]=matrix[5];
    m[6]=matrix[6];
    m[7]=matrix[7];
    m[8]=matrix[8];
    m[9]=matrix[9];
    m[10]=matrix[10];
    m[11]=matrix[11];
    //m[12]=matrix[12];
    //m[13]=matrix[13];
    //m[14]=matrix[14];
    //m[15]=matrix[15];
    
    rotate[ 0]=cosf(angleInRadians);
    rotate[ 1]=sinf(angleInRadians);
    //rotate[ 2]=0.0;	//Not needed
    //rotate[ 3]=0.0;	//Not needed
    
    rotate[ 4]=-rotate[1];
    rotate[ 5]=rotate[0];
    //rotate[ 6]=0.0;	//Not needed
    //rotate[ 7]=0.0;	//Not needed
    
    //rotate[ 8]=0.0;	//Not needed
    //rotate[ 9]=0.0;	//Not needed
    //rotate[10]=1.0;	//Not needed
    //rotate[11]=0.0;		//Not needed
    //The last column of rotate[] is {0 0 0 1}
    
    
    matrix[0]=m[0]*rotate[0]+
    m[4]*rotate[1];
    
    matrix[4]=m[0]*rotate[4]+
    m[4]*rotate[5];
    
    //matrix[8]=m[8];	//No change
    
    //matrix[12]=matrix[12];
    
    matrix[1]=m[1]*rotate[0]+
    m[5]*rotate[1];
    
    matrix[5]=m[1]*rotate[4]+
    m[5]*rotate[5];
    
    //matrix[9]=m[9];	//No change
    
    //matrix[13]=matrix[13];
    
    matrix[2]=m[2]*rotate[0]+
    m[6]*rotate[1];
    
    matrix[6]=m[2]*rotate[4]+
    m[6]*rotate[5];
    
    //matrix[10]=m[10];		//No change
    
    //matrix[14]=matrix[14];
    
    matrix[3]=m[3]*rotate[0]+
    m[7]*rotate[1];
    
    matrix[7]=m[3]*rotate[4]+
    m[7]*rotate[5];
    
    //matrix[11]=m[11];		//No change
    
    //matrix[15]=matrix[15];
}

//PURPOSE:	Set quat to {0, 0, 0, 1}, xyzw (NOT wxyz)
void glhQuaternionIdentityf(float *quat)
{
    //Any quat multiplied by identity quat will be unchanged.
    //Also, {0, 0, 0, 0} is addition identity quat
    quat[0]=quat[1]=quat[2]=0.0;		//xyz is a vector
    quat[3]=1.0;
}

//PURPOSE:	If quat is {0, 0, 0, 1}, returns 1 (TRUE), else 0 (FALSE)
int glhQuaternionIsIdentityf(float *quat)
{
    if((quat[0]==0.0)&&(quat[1]==0.0)&&(quat[2]==0.0)&&(quat[3]==1.0))
        return 1;
    else
        return 0;
}

//PURPOSE:	If quat[3] == 1.0, returns 1 (TRUE), else 0 (FALSE)
int glhQuaternionIsPuref(float *quat)
{
    if(quat[3]==1.0)
        return 1;
    else
        return 0;
}

//PURPOSE:	Calculate the conjugate of a quat.
//			xyz is simply negated. quat is xyzw
void glhQuaternionConjugatef(float *quat)
{
    quat[0]=-quat[0];
    quat[1]=-quat[1];
    quat[2]=-quat[2];
}

//PURPOSE:	Calculate the magnitude of a quat.
void glhQuaternionMagnitudef(float *magnitude, float *quat)
{
    magnitude[0]=sqrtf(quat[0]*quat[0]+quat[1]*quat[1]+quat[2]*quat[2]+quat[3]*quat[3]);
}

//PURPOSE:	Calculate the magnitude squared of a quat.
void glhQuaternionMagnitudeSquaredf(float *magnitude, float *quat)
{
    magnitude[0]=quat[0]*quat[0]+quat[1]*quat[1]+quat[2]*quat[2]+quat[3]*quat[3];
}

//PURPOSE:	Compute dot product between 2 quats.
void glhQuaternionDotProductf(float *result, float *quat1, float *quat2)
{
    result[0]=quat1[0]*quat2[0]+quat1[1]*quat2[1]+quat1[2]*quat2[2]+quat1[3]*quat2[3];
}

//PURPOSE:	Calculates the exponential of a quat.
//			quat is expected to be a pure quaternion. See glhQuaternionIsPuref.
void glhQuaternionExpf(float *outquat, float *quat)
{
    float dist_2 = quat[0]*quat[0]+quat[1]*quat[1]+quat[2]*quat[2]+quat[3]*quat[3];
    if (dist_2 == 0.f) {
        outquat[0]=0.f;
        outquat[1]=0.f;
        outquat[2]=0.f;
        outquat[3]=1.f;
        return;
    }
    
    //Normalize
    float n=1.0/sqrtf(dist_2);
    quat[0]*=n;
    quat[1]*=n;
    quat[2]*=n;
    quat[3]*=n;
    
    //Convert a quaternion to a rotation axis and angle
    float sin_a_2;
    float cos_a=quat[3];
    float angle=acosf(cos_a)*2.0;
    float sin_a=sqrtf(1.0-cos_a*cos_a);
    if(fabsf(sin_a)<0.0005)
        sin_a=sin_a_2=1.0;
    else
        sin_a_2=1.0/sin_a;
    
    float axis[3];
    axis[0]=quat[0]*sin_a_2;
    axis[1]=quat[1]*sin_a_2;
    axis[2]=quat[2]*sin_a_2;
    
    //From D3D docs :
    //Given a pure quaternion defined by:
    //q = (0, theta * v);
    //
    //This method calculates the exponential result.
    //exp(Q) = (cos(theta), sin(theta) * v)
    float theta=sinf(angle*0.5);
    outquat[3]=cosf(theta);
    theta=sinf(theta);
    outquat[0]=theta*axis[0];
    outquat[1]=theta*axis[1];
    outquat[2]=theta*axis[2];
    
    
}

//PURPOSE:	Calculate the natural logarithm of a quat.
//			quat should be unit quat.
void glhQuaternionLnf(float *outquat, float *quat)
{
    //From D3D docs:
    //A unit quaternion, is defined by:
    //Q == (cos(theta), sin(theta) * v) where |v| = 1
    //The natural logarithm of Q is, ln(Q) = (0, theta * v)
    glhQuaternionNormalizef(quat);
    
    float theta=acosf(quat[3]);
    
    if (theta == 0.f) {
        outquat[0] = outquat[1] = outquat[2] = outquat[3] = 0.f;
    }
    else {
        float sin_theta_2=1.0/sinf(theta);
        outquat[0]=quat[0]*sin_theta_2*theta;
        outquat[1]=quat[1]*sin_theta_2*theta;
        outquat[2]=quat[2]*sin_theta_2*theta;
        outquat[3]=0.0;
    }
}

//PURPOSE:	Normalize quat. Some glhQuaternion***** require normalized quats for efficiency.
void glhQuaternionNormalizef(float *quat)
{
    float n=1.0/sqrtf(quat[0]*quat[0]+quat[1]*quat[1]+quat[2]*quat[2]+quat[3]*quat[3]);
    quat[0]*=n;
    quat[1]*=n;
    quat[2]*=n;
    quat[3]*=n;
}

//PURPOSE:	Normalize quat. Some glhQuaternion***** require normalized quats for efficiency.
void glhQuaternionInvertf(float *outquat, float *quat)
{
    outquat[0]=-quat[0];
    outquat[1]=-quat[1];
    outquat[2]=-quat[2];
    outquat[3]= quat[3];
    
    float n=1.0/sqrtf(outquat[0]*outquat[0]+outquat[1]*outquat[1]+outquat[2]*outquat[2]+outquat[3]*outquat[3]);
    outquat[0]*=n;
    outquat[1]*=n;
    outquat[2]*=n;
    outquat[3]*=n;
}

//PURPOSE:	outquat = quat1 * quat2
//			If quat1 and quat2 are normalized, outquat will be a unit quat.
void glhQuaternionMultiplyf(float *outquat, float *quat1, float *quat2)
{
    //x = w1*x2 + w2*x1 + y1*z2 - z1*y2
    outquat[0]=quat1[3]*quat2[0]+quat2[3]*quat1[0]+quat1[1]*quat2[2]-quat1[2]*quat2[1];
    //y = w1*y2 + w2*y1 + z1*x2 - x1*z2
    outquat[1]=quat1[3]*quat2[1]+quat2[3]*quat1[1]+quat1[2]*quat2[0]-quat1[0]*quat2[2];
    //z = w1*z2 + w2*z1 + x1*y2 - y1*x2
    outquat[2]=quat1[3]*quat2[2]+quat2[3]*quat1[2]+quat1[0]*quat2[1]-quat1[1]*quat2[0];
    //w = w1*w2 - v1*v2
    outquat[3]=quat1[3]*quat2[3]-quat1[0]*quat2[0]-quat1[1]*quat2[1]-quat1[2]*quat2[2];
}

//PURPOSE:	Build a 4x4 column major rotation matrix from a quat.
//			quat should be normalized. See glhQuaternionNormalizef.
void glhQuaternionQuatToRotationMatrixf(float *matrix, float *quat)
{
    //    |       2     2                                         |
    //    | 1 - 2Y  - 2Z    2XY - 2ZW      2XZ + 2YW          0   |
    //    |                                                       |
    //    |                       2     2                         |
    //M = | 2XY + 2ZW       1 - 2X  - 2Z   2YZ - 2XW          0   |
    //    |                                                       |
    //    |                                      2     2          |
    //    | 2XZ - 2YW       2YZ + 2XW      1 - 2X  - 2Y       0   |
    //    |                                                       |
    //    |                                                       |
    //    |   0                0              0               1   |
    
    float twoXSquared=2.0*quat[0]*quat[0];
    float twoYSquared=2.0*quat[1]*quat[1];
    float twoZSquared=2.0*quat[2]*quat[2];
    
    float twoXY=2.0*quat[0]*quat[1];
    float twoXW=2.0*quat[0]*quat[3];
    float twoYW=2.0*quat[1]*quat[3];
    float twoZW=2.0*quat[2]*quat[3];
    float twoXZ=2.0*quat[0]*quat[2];
    float twoYZ=2.0*quat[1]*quat[2];
    
    
    matrix[0]=1.0-twoYSquared-twoZSquared;
    matrix[1]=twoXY+twoZW;
    matrix[2]=twoXZ-twoYW;
    matrix[3]=0.0;
    
    matrix[4]=twoXY-twoZW;
    matrix[5]=1.0-twoXSquared-twoZSquared;
    matrix[6]=twoYZ+twoXW;
    matrix[7]=0.0;
    
    matrix[8]=twoXZ+twoYW;
    matrix[9]=twoYZ-twoXW;
    matrix[10]=1.0-twoXSquared-twoYSquared;
    matrix[11]=0.0;
    
    matrix[12]=matrix[13]=matrix[14]=0.0;
    matrix[15]=1.0;
}

//PURPOSE:	Build a 4x4 column major rotation matrix from a quat.
//			quat should be normalized. See glhQuaternionNormalizef.
void glhQuaternionQuatToRotationMatrixTf(float *matrix, float *quat)
{
    //    |       2     2                                         |
    //    | 1 - 2Y  - 2Z    2XY - 2ZW      2XZ + 2YW          0   |
    //    |                                                       |
    //    |                       2     2                         |
    //M = | 2XY + 2ZW       1 - 2X  - 2Z   2YZ - 2XW          0   |
    //    |                                                       |
    //    |                                      2     2          |
    //    | 2XZ - 2YW       2YZ + 2XW      1 - 2X  - 2Y       0   |
    //    |                                                       |
    //    |                                                       |
    //    |   0                0              0               1   |
    
    float twoXSquared=2.0*quat[0]*quat[0];
    float twoYSquared=2.0*quat[1]*quat[1];
    float twoZSquared=2.0*quat[2]*quat[2];
    
    float twoXY=2.0*quat[0]*quat[1];
    float twoXW=2.0*quat[0]*quat[3];
    float twoYW=2.0*quat[1]*quat[3];
    float twoZW=2.0*quat[2]*quat[3];
    float twoXZ=2.0*quat[0]*quat[2];
    float twoYZ=2.0*quat[1]*quat[2];
    
    
    matrix[0]=1.0-twoYSquared-twoZSquared;
    matrix[1]=twoXY-twoZW;
    matrix[2]=twoXZ+twoYW;
    matrix[3]=0.0;
    
    matrix[4]=twoXY+twoZW;
    matrix[5]=1.0-twoXSquared-twoZSquared;
    matrix[6]=twoYZ-twoXW;
    matrix[7]=0.0;
    
    matrix[8]=twoXZ-twoYW;
    matrix[9]=twoYZ+twoXW;
    matrix[10]=1.0-twoXSquared-twoYSquared;
    matrix[11]=0.0;
    
    matrix[12]=matrix[13]=matrix[14]=0.0;
    matrix[15]=1.0;
}


//PURPOSE:	Build a quat from a 4x4 column major rotation matrix.
//			quat will be automatically normalized.
void glhQuaternionRotationMatrixToQuatf(float *quat, float *matrix)
{
    //This comes from matrixfaq.htm
    //It uses a row major format and matrix is linear (matrix[16])
    //I had to change the indices to be GL compatible
    float trace, S;
    
    /*//Calculate trace of matrix
     trace=matrix[0]+matrix[5]+matrix[10]+1.0;
     
     //If the trace of the matrix is greater than zero, then
     //perform an "instant" calculation.
     if(trace>0.0)
     {
     S=0.5/sqrtf(trace);
     quat[0]=(matrix[6]-matrix[9])*S;
     quat[1]=(matrix[8]-matrix[2])*S;
     quat[2]=(matrix[1]-matrix[4])*S;
     quat[3]=0.25/S;
     }
     else //if(trace<=0.0)
     {
     //Identify which major diagonal element has the greatest value.
     //The major diagonal of a matrix is the set of elements where the
     //row number is equal to the column number.
     //We only care about column 0, 1 and 2
     if(matrix[0]>matrix[5])
     {
     if(matrix[0]>matrix[10])
     {
     //matrix[0] is largest
     //Column 0
     S=1.0/sqrtf(1.0+matrix[0]-matrix[5]-matrix[10])*2.0;
     
     quat[0]=0.5/S;
     quat[1]=(matrix[1]+matrix[4])*S;
     quat[2]=(matrix[8]+matrix[2])*S;
     quat[3]=(matrix[6]+matrix[9])*S;
     }
     else
     {
     //matrix[10] is largest
     //Column 2
     S=1.0/sqrtf(1.0+matrix[10]-matrix[0]-matrix[5])*2.0;
     
     quat[0]=(matrix[8]+matrix[2])*S;
     quat[1]=(matrix[6]+matrix[9])*S;
     quat[2]=0.5/S;
     quat[3]=(matrix[1]+matrix[4])*S;
     }
     }
     else if(matrix[5]>matrix[10])
     {
     //matrix[5] is largest
     //Column 1
     S=1.0/sqrtf(1.0+matrix[5]-matrix[0]-matrix[10])*2.0;
     
     quat[0]=(matrix[1]+matrix[4])*S;
     quat[1]=0.5/S;
     quat[2]=(matrix[6]+matrix[9])*S;
     quat[3]=(matrix[8]+matrix[2])*S;
     }
     else
     {
     //matrix[10] is largest
     //Column 2
     S=1.0/sqrtf(1.0+matrix[10]-matrix[0]-matrix[5])*2.0;
     
     quat[0]=(matrix[8]+matrix[2])*S;
     quat[1]=(matrix[6]+matrix[9])*S;
     quat[2]=0.5/S;
     quat[3]=(matrix[1]+matrix[4])*S;
     }
     }*/
    
    //This is from http://www.gamasutra.com/features/19980703/quaternions_01.htm
    //which appears to give result consistent with D3DX library and
    //also I can confirm by using the inverse operation --> glhQuaternionQuatToRotationMatrixf
    
    //Calculate trace of matrix
    trace=matrix[0]+matrix[5]+matrix[10];
    
    //If the trace of the matrix is greater than zero, then
    //perform an "instant" calculation.
    if(trace>0.0)
    {
        S=0.5/sqrtf(trace+1.0);
        quat[0]=(matrix[6]-matrix[9])*S;
        quat[1]=(matrix[8]-matrix[2])*S;
        quat[2]=(matrix[1]-matrix[4])*S;
        quat[3]=0.25/S;
    }
    else //if(trace<=0.0)
    {
        //Identify which major diagonal element has the greatest value.
        //The major diagonal of a matrix is the set of elements where the
        //row number is equal to the column number.
        //We only care about column 0, 1 and 2
        
        // I'm gone recode this part for efficiency
        /*int i=0, j, k;
         if(matrix[5]>matrix[0]) i=1;
         if(matrix[10]>matrix[i]) i=2;
         
         int nxt[]={1, 2, 0};
         
         j=nxt[i];
         k=nxt[j];
         
         S=sqrtf(1.0+matrix[i*4+i]-matrix[j*4+j]-matrix[k*4+k]);
         
         float q[4];
         q[i]=S*0.5;
         
         if(S!=0.0) S=0.5/S;
         
         q[3]=(matrix[j*4+k]-matrix[k*4+j])*S;
         q[j]=(matrix[i*4+j]+matrix[j*4+i])*S;
         q[k]=(matrix[i*4+k]+matrix[k*4+i])*S;
         
         quat[0]=q[0];
         quat[1]=q[1];
         quat[2]=q[2];
         quat[3]=q[3];*/
        
        if(matrix[0]>matrix[5])
        {
            if(matrix[0]>matrix[10])
            {
                S=sqrtf(1.0+matrix[0]-matrix[5]-matrix[10]);
                
                quat[0]=S*0.5;
                if(S!=0.0) S=0.5/S;
                quat[1]=(matrix[1]+matrix[4])*S;
                quat[2]=(matrix[2]+matrix[8])*S;
                quat[3]=(matrix[6]-matrix[9])*S;
            }
            else
            {
                S=sqrtf(1.0+matrix[10]-matrix[0]-matrix[5]);
                
                quat[2]=S*0.5;
                if(S!=0.0) S=0.5/S;
                quat[0]=(matrix[8]+matrix[2])*S;
                quat[1]=(matrix[9]+matrix[6])*S;
                quat[3]=(matrix[1]-matrix[4])*S;
            }
        }
        else if(matrix[5]>matrix[10])
        {
            S=sqrtf(1.0+matrix[5]-matrix[10]-matrix[0]);
            
            quat[1]=S*0.5;
            if(S!=0.0) S=0.5/S;
            quat[3]=(matrix[8]-matrix[2])*S;
            quat[2]=(matrix[6]+matrix[9])*S;
            quat[0]=(matrix[4]+matrix[1])*S;
        }
        else
        {
            S=sqrtf(1.0+matrix[10]-matrix[0]-matrix[5]);
            
            quat[2]=S*0.5;
            if(S!=0.0) S=0.5/S;
            quat[0]=(matrix[8]+matrix[2])*S;
            quat[1]=(matrix[9]+matrix[6])*S;
            quat[3]=(matrix[1]-matrix[4])*S;
        }
    }
}


//PURPOSE:	Convert quat to a axis of rotation and an angle in radians.
//			axis will receive xyz.
//			Quat should be normalized.
void glhQuaternionQuatToAxisAndAnglef(float *axis, float *angleInRadians, float *quat)
{
    float scale=1.0/sqrtf(quat[0]*quat[0]+quat[1]*quat[1]+quat[2]*quat[2]);
    axis[0]=quat[0]*scale;
    axis[1]=quat[1]*scale;
    axis[2]=quat[2]*scale;
    
    angleInRadians[0]=2.0*acosf(quat[3]);
}

//PURPOSE:	Convert axis of rotation and an angle in radians to quat.
//			axis must be xyz. Should be unit vector.
void glhQuaternionAxisAndAngleToQuatf(float *quat, float *axis, float *angleInRadians)
{
    float thetaDivTwo=angleInRadians[0]*0.5;
    float sinThetaDivTwo=angleInRadians[0]*0.5;
    
    quat[0]=axis[0]*sinThetaDivTwo;
    quat[1]=axis[1]*sinThetaDivTwo;
    quat[2]=axis[2]*sinThetaDivTwo;
    quat[3]=cosf(thetaDivTwo);
}

//PURPOSE:	Convert Euler angles into a quat. Angles are in radians.
void glhQuaternionEulerAnglesToQuatf(float *quat, float roll, float pitch, float yaw)
{
    float cr, cp, cy, sr, sp, sy, cpcy, spsy;
    float roll_half, pitch_half, yaw_half;
    
    //Calculate trig identities
    roll_half=roll*0.5;
    yaw_half=yaw*0.5;
    pitch_half=pitch*0.5;
    
    cr=cosf(roll_half);
    cp=cosf(pitch_half);
    cy=cosf(yaw*0.5);
    
    sr=sinf(roll_half);
    sp=sinf(pitch_half);
    sy=sinf(yaw_half);
    
    cpcy=cp*cy;
    spsy=sp*sy;
    
    quat[3]=cr*cpcy + sr*spsy;
    quat[0]=sr*cpcy - cr*spsy;
    quat[1]=cr*sp*cy + sr*cp*sy;
    quat[2]=cr*cp*sy - sr*sp*cy;
}

//PURPOSE:	Interpolates between two quaternions, using spherical linear interpolation.
//			SLERP = Spherical Linear intERPolation
//			Interpolate 2 quats along smallest arc between them.
//			t is the interpolation factor where t = 0.0 gives outquat = quat1
//			and t = 1.0 gives outquat = quat2
//			t should be [0, 1]
//			You should make sure quat1 and quat2 are
void glhQuaternionSLERPf(float *outquat, float *quat1, float *quat2, float t)
{
    float omega, cosom, sinom, scale0, scale1;
    float cosom2;
    
    //Calc cosine. quat1 DOT quat2
    cosom=quat1[0]*quat2[0]+quat1[1]*quat2[1]+quat1[2]*quat2[2]+quat1[3]*quat2[3];
    
    //Adjust signs (if necessary)
    if(cosom<0.0f)
    {
        cosom2=-cosom;
    }
    else
    {
        cosom2=cosom;
    }
    
    //Calculate coefficients
    if((1.0f-cosom2)>0.00005f)
    {
        //Standard case (slerp)
        omega=acosf(cosom2);
        sinom=sinf(omega);
        scale0=sinf((1.0f-t)*omega)/sinom;
        scale1=sinf(t*omega)/sinom;
    }
    else
    {
        //"from" and "to" quaternions are very close
        //... so we can do a linear interpolation
        scale0=1.0f-t;
        scale1=t;
    }
    
    //Calculate final values
    if(cosom<0.0)
    {
        outquat[0]=scale0*quat1[0]-scale1*quat2[0];
        outquat[1]=scale0*quat1[1]-scale1*quat2[1];
        outquat[2]=scale0*quat1[2]-scale1*quat2[2];
        outquat[3]=scale0*quat1[3]-scale1*quat2[3];
    }
    else
    {
        outquat[0]=scale0*quat1[0]+scale1*quat2[0];
        outquat[1]=scale0*quat1[1]+scale1*quat2[1];
        outquat[2]=scale0*quat1[2]+scale1*quat2[2];
        outquat[3]=scale0*quat1[3]+scale1*quat2[3];
    }
}

//PURPOSE:	Interpolates between two quaternions, using spherical cubic interpolation.
void glhQuaternionSQUADf(float *outquat, float *quat1, float *quat2, float *quat3, float *quat4, float t)
{
    float outQuat1[4], outQuat2[4];
    
    glhQuaternionSLERPf(outQuat1, quat1, quat4, t);
    glhQuaternionSLERPf(outQuat2, quat2, quat3, t);
    
    float t2 = 2.f * t * (1.f - t);
    glhQuaternionSLERPf(outquat, outQuat1, outQuat2, t2);
}

void glhQuaternionSplineIntermefiatef(float *outquat, float *quat1, float *quat2, float *quat3)
{
    float outQuat1[4], outQuat2[4], outQuat3[4], outQuat4[4];
    float quat2inv[4];
    
    glhQuaternionInvertf(quat2inv, quat2);
    
    glhQuaternionMultiplyf(outQuat1, quat2inv, quat1);
    glhQuaternionMultiplyf(outQuat2, quat2inv, quat3);
    glhQuaternionLnf(outQuat3, outQuat1);
    glhQuaternionLnf(outQuat4, outQuat2);
    
    outQuat1[0] = -0.25f * (outQuat3[0] + outQuat4[0]);
    outQuat1[1] = -0.25f * (outQuat3[1] + outQuat4[1]);
    outQuat1[2] = -0.25f * (outQuat3[2] + outQuat4[2]);
    outQuat1[3] = -0.25f * (outQuat3[3] + outQuat4[3]);
    
    // input to Expf is not normalized except for 1 special case
    glhQuaternionExpf(outQuat2, outQuat1);
    glhQuaternionNormalizef(outQuat2);
    glhQuaternionMultiplyf(outquat, quat2, outQuat2);
    glhQuaternionNormalizef(outquat);
}

//PURPOSE:	Interpolates between two quaternions, using spherical cubic interpolation. between quat2 and quat3
// if amFirst, the
void glhQuaternionSQUADSplinef(float *outquat, float *quat1, float *quat2, float *quat3, float *quat4, float t, int amFirst, int amLast)
{
    float outQuat1[4], outQuat2[4], a1Quat[4], a2Quat[4];
    
    // special case for first segment in spline segment chain
    if (amFirst) {
        a1Quat[0] = quat2[0];
        a1Quat[1] = quat2[1];
        a1Quat[2] = quat2[2];
        a1Quat[3] = quat2[3];
    }
    else {
        glhQuaternionSplineIntermefiatef(a1Quat, quat1, quat2, quat3);
    }
    // special case for last segment in spline segment chain
    if (amLast) {
        a2Quat[0] = quat3[0];
        a2Quat[1] = quat3[1];
        a2Quat[2] = quat3[2];
        a2Quat[3] = quat3[3];
    }
    else {
        glhQuaternionSplineIntermefiatef(a2Quat, quat2, quat3, quat4);
    }
    
    glhQuaternionSLERPf(outQuat1, quat2, quat3, t);
    glhQuaternionSLERPf(outQuat2, a1Quat, a2Quat, t);
    glhQuaternionNormalizef(outQuat1);
    glhQuaternionNormalizef(outQuat2);
    
    float t2 = 2.f * t * (1.f - t);
    glhQuaternionSLERPf(outquat, outQuat1, outQuat2, t2);
    glhQuaternionNormalizef(outquat);
}

//PURPOSE:	After calling
//			float matrix[16], eyePosition3D[3], center3D[3], upVector3D[3];
//			glhLoadIdentityf2(matrix);
//			glhLookAtf2(matrix, eyePosition3D, center3D, upVector3D);
//
//			you might want to retreive eyePosition3D, center3D and upVector3D directly from the matrix.
//			This function will attempt to do so.
//			**** WARNING : center3D will not be the original value you input because this information is lossed when
//			               you call glhLookAtf2
//			               center3D will be eyePosition3D + normalize(lookAtVector)
//
//			Hopefully, the matrix contains valid data else extracting will be a problem.
//			Even if you call glhRotatef2, glhTranslatef2 and other functions, this function will be able to extract
//			the new glhLookAt2 variables.
//
//PARAMETERS:
//			[in] matrix : Column major 4x4 matrix
//			[out] eyePosition3D : Position of the "eye" aka camera position
//			[out] center3D : The point at which we are looking at
//			[out] upVector3D : A normalized up vector
//
//RETURN:
//			1 for success (Most likely)
//			-1 for error. (Most likely the matrix contains some unexpected entries in the upper left 3x3 matrix.)
int glhExtractLookAtFromMatrixf2(float *matrix, float *eyePosition3D, float *center3D, float *upVector3D)
{
    float rightVector[3], newUpVector[3], lookAt[3];
    float ftemp;
    float factors[4], newRightVector[3];
    float newRightVector2[3];
    float newMatrix_12_13_14[3], newMatrix_12_13_14_2[3];
    
    int is_eyePosition3D_Solved=0;
    
    int done[3];
    
    rightVector[0]=matrix[0];
    rightVector[1]=matrix[4];
    rightVector[2]=matrix[8];
    
    //We expect this to be normalized
    upVector3D[0]=matrix[1];
    upVector3D[1]=matrix[5];
    upVector3D[2]=matrix[9];
    
    //These were negated originally
    lookAt[0]=-matrix[2];
    lookAt[1]=-matrix[6];
    lookAt[2]=-matrix[10];
    
    //**** Try to solve a system of 3 equations ****
    //
    //[12] contains -(rightVector DOT eyePosition)
    //[13] contains -(upVector DOT eyePosition)
    //[14] contains -(lookVector DOT eye Position)
    //
    //The unknown is eyePosition. The others are already inside the matrix
    //rightVector.x * X + rightVector.y * Y + rightVector.z * Z = -matrix[12]
    //upVector.x * X + upVector.y * Y + upVector.z * Z = -matrix[13]
    //lookVector.x * X + lookVector.y * Y + lookVector.z * Z = -matrix[14]
    //
    if((rightVector[0]!=0.0)&&(upVector3D[0]!=0.0)&&(lookAt[0]!=0.0))		//Else we have problems!
    {
        ftemp=rightVector[0]/upVector3D[0];
        
        factors[0]=ftemp*upVector3D[0];
        factors[1]=ftemp*upVector3D[1];
        factors[2]=ftemp*upVector3D[2];
        factors[3]=ftemp*(-matrix[13]);
        
        newRightVector[0]=0.0;		//This becomes eliminated
        newRightVector[1]=rightVector[1]-factors[1];
        newRightVector[2]=rightVector[2]-factors[2];
        
        newMatrix_12_13_14[0]=(-matrix[12])-factors[3];
        
        //We now have
        //newRightVector.x * X + newRightVector.y * Y + newRightVector.z * Z = newMatrix_12_13_14[0]
        //where newRightVector.x = 0.0 so
        //newRightVector.y * Y + newRightVector.z * Z = newMatrix_12_13_14[0]
        
        //Now eliminate X from equation #2 using equation #3
        ftemp=upVector3D[0]/lookAt[0];
        
        factors[0]=ftemp*lookAt[0];
        factors[1]=ftemp*lookAt[1];
        factors[2]=ftemp*lookAt[2];
        factors[3]=ftemp*(-matrix[14]);
        
        newUpVector[0]=0.0;		//This becomes eliminated
        newUpVector[1]=upVector3D[1]-factors[1];
        newUpVector[2]=upVector3D[2]-factors[2];
        
        newMatrix_12_13_14[1]=(-matrix[13])-factors[3];
        
        //Now we have 2 equations with 2 unknowns
        //newRightVector.y * Y + newRightVector.z * Z = newMatrix_12_13_14[0]
        //newUpVector.y * Y + newUpVector.z * Z = newMatrix_12_13_14[1]
        
        if((newRightVector[1]!=0.0)&&(newUpVector[1]!=0.0))
        {
            ftemp=newRightVector[1]/newUpVector[1];
            
            factors[1]=ftemp*newUpVector[1];
            factors[2]=ftemp*newUpVector[2];
            factors[3]=ftemp*newMatrix_12_13_14[1];
            
            
            newRightVector2[1]=0.0;		//This becomes eliminated
            newRightVector2[2]=newUpVector[2]-factors[2];
            
            newMatrix_12_13_14_2[0]=newMatrix_12_13_14[0]-factors[3];
            
            //Now we have a single equation : the first one
            //newRightVector2.z * Z = newMatrix_12_13_14_2[0]
            //and it is easy to find Z
            eyePosition3D[2]=newMatrix_12_13_14_2[0]/newRightVector2[2];
            
            //Plug in Z into previous 2 variable equation to get Y
            eyePosition3D[1]=(newMatrix_12_13_14[0]-(newRightVector[2]*eyePosition3D[2]))/newRightVector[1];
            
            //Now we have Y and Z. Solve for X using the original equation
            //rightVector.x * X + rightVector.y * Y + rightVector.z * Z = -matrix[12]
            eyePosition3D[0]=(-matrix[12])-(rightVector[2]*eyePosition3D[2])-(rightVector[1]*eyePosition3D[1]);
            
            is_eyePosition3D_Solved=1;
        }
        else
            return -1;
    }
    else if((rightVector[1]!=0.0)&&(upVector3D[1]!=0.0)&&(lookAt[1]!=0.0))		//Else we have problems!
    {
        ftemp=rightVector[1]/upVector3D[1];
        
        factors[0]=ftemp*upVector3D[0];
        factors[1]=ftemp*upVector3D[1];
        factors[2]=ftemp*upVector3D[2];
        factors[3]=ftemp*(-matrix[13]);
        
        newRightVector[0]=rightVector[0]-factors[0];
        newRightVector[1]=0.0;		//This becomes eliminated
        newRightVector[2]=rightVector[2]-factors[2];
        
        newMatrix_12_13_14[0]=(-matrix[12])-factors[3];
        
        //We now have
        //newRightVector.x * X + newRightVector.y * Y + newRightVector.z * Z = newMatrix_12_13_14[0]
        //where newRightVector.y = 0.0 so
        //newRightVector.x * X + newRightVector.z * Z = newMatrix_12_13_14[0]
        
        //Now eliminate Y from equation #2 using equation #3
        ftemp=upVector3D[1]/lookAt[1];
        
        factors[0]=ftemp*lookAt[0];
        factors[1]=ftemp*lookAt[1];
        factors[2]=ftemp*lookAt[2];
        factors[3]=ftemp*(-matrix[14]);
        
        newUpVector[0]=upVector3D[0]-factors[0];
        newUpVector[1]=0.0;		//This becomes eliminated
        newUpVector[2]=upVector3D[2]-factors[2];
        
        newMatrix_12_13_14[1]=(-matrix[13])-factors[3];
        
        //Now we have 2 equations with 2 unknowns
        //newRightVector.x * X + newRightVector.z * Z = newMatrix_12_13_14[0]
        //newUpVector.x * X + newUpVector.z * Z = newMatrix_12_13_14[1]
        
        if((newRightVector[0]!=0.0)&&(newUpVector[0]!=0.0))
        {
            ftemp=newRightVector[0]/newUpVector[0];
            
            factors[0]=ftemp*newUpVector[0];
            factors[2]=ftemp*newUpVector[2];
            factors[3]=ftemp*newMatrix_12_13_14[1];
            
            
            newRightVector2[0]=0.0;		//This becomes eliminated
            newRightVector2[2]=newUpVector[2]-factors[2];
            
            newMatrix_12_13_14_2[0]=newMatrix_12_13_14[0]-factors[3];
            
            //Now we have a single equation : the first one
            //newRightVector2.z * Z = newMatrix_12_13_14_2[0]
            //and it is easy to find Z
            eyePosition3D[2]=newMatrix_12_13_14_2[0]/newRightVector2[2];
            
            //Plug in Z into previous 2 variable equation to get X
            eyePosition3D[0]=(newMatrix_12_13_14[0]-(newRightVector[2]*eyePosition3D[2]))/newRightVector[0];
            
            //Now we have X and Z. Solve for Y using the original equation
            //rightVector.x * X + rightVector.y * Y + rightVector.z * Z = -matrix[12]
            eyePosition3D[1]=(-matrix[12])-(rightVector[2]*eyePosition3D[2])-(rightVector[0]*eyePosition3D[0]);
            
            is_eyePosition3D_Solved=1;
        }
        else
            return -1;
    }
    else if((rightVector[2]!=0.0)&&(upVector3D[2]!=0.0)&&(lookAt[2]!=0.0))		//Else we have problems!
    {
        ftemp=rightVector[2]/upVector3D[2];
        
        factors[0]=ftemp*upVector3D[0];
        factors[1]=ftemp*upVector3D[1];
        factors[2]=ftemp*upVector3D[2];
        factors[3]=ftemp*(-matrix[13]);
        
        newRightVector[0]=rightVector[0]-factors[0];
        newRightVector[1]=rightVector[1]-factors[1];
        newRightVector[2]=0.0;		//This becomes eliminated
        
        newMatrix_12_13_14[0]=(-matrix[12])-factors[3];
        
        //We now have
        //newRightVector.x * X + newRightVector.y * Y + newRightVector.z * Z = newMatrix_12_13_14[0]
        //where newRightVector.z = 0.0 so
        //newRightVector.x * X + newRightVector.y * Y = newMatrix_12_13_14[0]
        
        //Now eliminate Z from equation #2 using equation #3
        ftemp=upVector3D[2]/lookAt[2];
        
        factors[0]=ftemp*lookAt[0];
        factors[1]=ftemp*lookAt[1];
        factors[2]=ftemp*lookAt[2];
        factors[3]=ftemp*(-matrix[14]);
        
        newUpVector[0]=upVector3D[0]-factors[0];
        newUpVector[1]=upVector3D[2]-factors[2];
        newUpVector[2]=0.0;		//This becomes eliminated
        
        newMatrix_12_13_14[1]=(-matrix[13])-factors[3];
        
        //Now we have 2 equations with 2 unknowns
        //newRightVector.x * X + newRightVector.y * Y = newMatrix_12_13_14[0]
        //newUpVector.x * X + newUpVector.y * Y = newMatrix_12_13_14[1]
        
        if((newRightVector[0]!=0.0)&&(newUpVector[0]!=0.0))
        {
            ftemp=newRightVector[0]/newUpVector[0];
            
            factors[0]=ftemp*newUpVector[0];
            factors[2]=ftemp*newUpVector[2];
            factors[3]=ftemp*newMatrix_12_13_14[1];
            
            
            newRightVector2[0]=0.0;		//This becomes eliminated
            newRightVector2[1]=newUpVector[1]-factors[1];
            
            newMatrix_12_13_14_2[0]=newMatrix_12_13_14[0]-factors[3];
            
            //Now we have a single equation : the first one
            //newRightVector2.y * Y = newMatrix_12_13_14_2[0]
            //and it is easy to find Y
            eyePosition3D[1]=newMatrix_12_13_14_2[0]/newRightVector2[1];
            
            //Plug in Y into previous 2 variable equation to get X
            eyePosition3D[0]=(newMatrix_12_13_14[0]-(newRightVector[1]*eyePosition3D[1]))/newRightVector[0];
            
            //Now we have X and Y. Solve for Z using the original equation
            //rightVector.x * X + rightVector.y * Y + rightVector.z * Z = -matrix[12]
            eyePosition3D[2]=(-matrix[12])-(rightVector[1]*eyePosition3D[1])-(rightVector[0]*eyePosition3D[0]);
            
            is_eyePosition3D_Solved=TRUE;
        }
        else
            return -1;
    }
    else
    {	//Some of the entries in the upper left 3x3 matrix are zero
        
        done[0]=done[1]=done[2]=FALSE;		//To know if all the components are filled
        
        //Try to find eyePosition3D
        if((matrix[0]==0.0)&&(matrix[4]==0.0)&&(matrix[8]!=0.0))
        {
            //rightVector.x * X + rightVector.y * Y + rightVector.z * Z = -matrix[12]
            eyePosition3D[2]=-matrix[12]/matrix[8];
            done[2]=TRUE;
        }
        else if((matrix[0]==0.0)&&(matrix[4]!=0.0)&&(matrix[8]==0.0))
        {
            //rightVector.x * X + rightVector.y * Y + rightVector.z * Z = -matrix[12]
            eyePosition3D[1]=-matrix[12]/matrix[4];
            done[1]=TRUE;
        }
        else if((matrix[0]!=0.0)&&(matrix[4]==0.0)&&(matrix[8]==0.0))
        {
            //rightVector.x * X + rightVector.y * Y + rightVector.z * Z = -matrix[12]
            eyePosition3D[0]=-matrix[12]/matrix[0];
            done[0]=TRUE;
        }
        
        if((matrix[1]==0.0)&&(matrix[5]==0.0)&&(matrix[9]!=0.0))
        {
            //upVector.x * X + upVector.y * Y + upVector.z * Z = -matrix[13]
            eyePosition3D[2]=-matrix[13]/matrix[9];
            done[2]=TRUE;
        }
        else if((matrix[1]==0.0)&&(matrix[5]!=0.0)&&(matrix[9]==0.0))
        {
            //upVector.x * X + upVector.y * Y + upVector.z * Z = -matrix[13]
            eyePosition3D[1]=-matrix[13]/matrix[5];
            done[1]=TRUE;
        }
        else if((matrix[1]!=0.0)&&(matrix[5]==0.0)&&(matrix[9]==0.0))
        {
            //upVector.x * X + upVector.y * Y + upVector.z * Z = -matrix[13]
            eyePosition3D[0]=-matrix[13]/matrix[1];
            done[0]=TRUE;
        }
        
        if((matrix[2]==0.0)&&(matrix[6]==0.0)&&(matrix[10]!=0.0))
        {
            //lookVector.x * X + lookVector.y * Y + lookVector.z * Z = -matrix[14]
            eyePosition3D[2]=-matrix[14]/matrix[10];
            done[2]=TRUE;
        }
        else if((matrix[2]==0.0)&&(matrix[6]!=0.0)&&(matrix[10]==0.0))
        {
            //lookVector.x * X + lookVector.y * Y + lookVector.z * Z = -matrix[14]
            eyePosition3D[1]=-matrix[14]/matrix[6];
            done[1]=TRUE;
        }
        else if((matrix[2]!=0.0)&&(matrix[6]==0.0)&&(matrix[10]==0.0))
        {
            //lookVector.x * X + lookVector.y * Y + lookVector.z * Z = -matrix[14]
            eyePosition3D[0]=-matrix[14]/matrix[2];
            done[0]=TRUE;
        }
        
        //Were all the components filled?
        if((done[0])&&(done[1])&&(done[2]))
            is_eyePosition3D_Solved=TRUE;
        else
            is_eyePosition3D_Solved=FALSE;
    }
    
    if(is_eyePosition3D_Solved)
    {
        //Compute center3D
        NormalizeVectorFLOAT_2(lookAt);
        
        center3D[0]=eyePosition3D[0]+lookAt[0];
        center3D[1]=eyePosition3D[1]+lookAt[1];
        center3D[2]=eyePosition3D[2]+lookAt[2];
        return 1;
    }
    else
    {
        return -1;
    }
}

//PURPOSE: These 2 functions are for transposing a 4x4 matrix (sreal and sreal2)
void glhTransposeMatrixf2(float *result, float *m)
{
    result[0]=m[0];
    result[1]=m[4];
    result[2]=m[8];
    result[3]=m[12];
    result[4]=m[1];
    result[5]=m[5];
    result[6]=m[9];
    result[7]=m[13];
    result[8]=m[2];
    result[9]=m[6];
    result[10]=m[10];
    result[11]=m[14];
    result[12]=m[3];
    result[13]=m[7];
    result[14]=m[11];
    result[15]=m[15];
}

