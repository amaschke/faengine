//
//  VariationChain.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/2/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "VariationChain.hpp"
#include "VariationGroup.hpp"
#include "VarParInstance.hpp"
#include "Utilities.hpp"
#include "NamedAffineMatrix.hpp"

using std::make_shared;

template std::string string_format<>( const char *format);
template std::string string_format<unsigned long>( const char *format, unsigned long arg);

VariationChain::VariationChain()
:  array()
{}

VariationChain::VariationChain(const VariationChain &v)
: array()
{}

// usage of shared_from_this() requires a two phase constructor
SharedVariationChain VariationChain::makeVariationChain()
{
    SharedVariationChain chain = std::shared_ptr<VariationChain>(new VariationChain());
    chain->array.emplace_back(make_shared<VariationGroup>(true, chain->shared_from_this()));
    return chain;
}

// usage of shared_from_this() requires a two phase constructor
SharedVariationChain VariationChain::makeVariationChain(bool pre, bool post)
{
    SharedVariationChain chain = std::shared_ptr<VariationChain>(new VariationChain());
    
    if (pre)
        chain->array.push_back(make_shared<VariationGroup>(false, chain->shared_from_this()));
    
    chain->array.push_back(make_shared<VariationGroup>(true, chain->shared_from_this()));
    
    if (post)
        chain->array.push_back(make_shared<VariationGroup>(false, chain->shared_from_this()));
    return chain;
}

// usage of shared_from_this() requires a two phase copy constructor
SharedVariationChain VariationChain::makeCopyOf(const VariationChain &v)
{
    SharedVariationChain chain = std::shared_ptr<VariationChain>(new VariationChain());
    
    for (const SharedVariationGroup & _group : v.array) {
        SharedVariationGroup copy = VariationGroup::makeCopyOf(_group);
        chain->array.push_back(copy);
        
        auto it = chain->array.back();
        VariationGroup &group = *it;
        group.setChain(chain->shared_from_this());
    }
    return chain;
}

SharedVariationGroup & VariationChain::operator[](size_t index)
{
    return array[index];
}

VariationGroupVector::iterator VariationChain::begin()
{
    return array.begin();
}

VariationGroupVector::iterator VariationChain::end()
{
    return array.end();
}

size_t VariationChain::indexOfGroup(SharedVariationGroup group, size_t defaultIndex) const
{
    auto it = std::find(array.begin(), array.end(), group);
    if (it != array.end())
        return uint(it - array.begin());
    return defaultIndex;
}
//size_t VariationChain::indexOfGroup(const SharedVariationGroup group, size_t defaultIndex) const
//{
//    auto it = std::find(array.begin(), array.end(), group);
//    if (it != array.end())
//        return uint(it - array.begin());
//    return defaultIndex;
//}

size_t VariationChain::indexOfGroup(SharedVariationGroup group) const
{
    auto it = std::find(array.begin(), array.end(), group);
    if (it != array.end())
        return uint(it - array.begin());
    return std::string::npos;
}

std::string VariationChain::groupNameForIndex(size_t index) const
{
    uint counter = 1;
    bool seenNormal = false;
    for (uint i = 0; i < array.size(); i++) {
        const SharedVariationGroup &group = array[i];
        if (group->amNormal) {
            seenNormal = true;
            counter = 0;
        }
        if (i == index) {
            if (!seenNormal)
                return string_format("Pre#%zu", (size_t)counter);
            else if (counter == 0)
                return string_format("Normal");
            else
                return string_format("Post#%zu", (size_t)counter);
        }
        counter++;
    }
    return "";
}

std::string VariationChain::groupNameForVariationGroup(const SharedVariationGroup &group) const
{
    size_t groupIndex = indexOfGroup(group);
    if (groupIndex == std::string::npos)
        return "Group not found";
    return groupNameForIndex(groupIndex);
}

SharedVarParInstance *VariationChain::currentVarparWithKey(const std::string &key, size_t groupIndex) const
{
    if (groupIndex >= array.size())
        return nullptr;
    
    const SharedVariationGroup &group = array[groupIndex];
    if (group->dictionary->find(key) == group->dictionary->end())
        return nullptr;
    return &group->dictionary->operator[](key);
}

SharedVarParInstance *VariationChain::currentVarparMatching(const SharedVarParInstance &varpar) const
{
    return currentVarparWithKey(varpar->instancedKey(), varpar->groupIndex());
}

void VariationChain::insertVarParInstance(const SharedVarParInstance &varpar)
{
    if (varpar->groupIndex() >= array.size()) {
        size_t addCount = varpar->groupIndex() - array.size() + 1;
        for (size_t i = 0; i < addCount; i++) {
            array.push_back(make_shared<VariationGroup>(false, shared_from_this()));
        }
    }
    SharedVariationGroup & group         = array[varpar->groupIndex()];
    SharedVarParInstance * currentVarpar = group->findVarpar(varpar->instancedKey());
    if (currentVarpar)
        (*currentVarpar)->floatValue =varpar->floatValue;
    else {
        varpar->setGroup(group);
        group->setObjectForKey(varpar->instancedKey(), varpar);
    }
}

NamedAffineMatrix VariationChain::currentMatrixWithKey(const std::string &key, size_t groupIndex) const
{
    const SharedVariationGroup &group  = array[groupIndex];
    return group->namedAffineMatrixForVariationKey(key);
}

std::vector<SharedVarParInstance> VariationChain::insertMatrix(const NamedAffineMatrix &matrix, const SharedVariationSet &variationSet)
{
    VariationGroupVector array;
    
    if (matrix.groupIndex == std::string::npos)
        return std::vector<SharedVarParInstance>();
    if (matrix.groupIndex >= array.size()) {
        size_t addCount = matrix.groupIndex - array.size() + 1;
        for (size_t i = 0; i < addCount; i++) {
            array.push_back(VariationGroup::groupWithAmNormal(false, shared_from_this()));
        }
    }
    SharedVariationGroup &group = array[matrix.groupIndex];
    group->setNamedAffineMatrix(matrix, matrix.name, variationSet);
    return group->affineMatrixVarparsForVariationKey(matrix.name);
}

SharedVariationGroup VariationChain::addNewGroup()
{
    SharedVariationGroup group = VariationGroup::groupWithAmNormal(false, shared_from_this());
    array.push_back(group);
    return group;
}

void VariationChain::addGroup(SharedVariationGroup & group)
{
    array.push_back(group);
}

const SharedVariationGroup & VariationChain::normalGroup() const
{
    for (const SharedVariationGroup & group : array) {
        if (group->amNormal)
            return group;
    }
    size_t count = array.size();
    if (count == 1 || count == 2)
        return array[0];
    return array[array.size()/2];
}

size_t VariationChain::normalGroupIndex() const
{
    for (size_t i = 0; i < array.size(); i++) {
        const SharedVariationGroup &group = array[i];
        if (group->amNormal)
            return i;
    }
    return std::string::npos;
}

void VariationChain::checkForNormalGroup() const
{
    if (array.size() > 0 && normalGroupIndex() == std::string::npos)
    {
        const SharedVariationGroup &group = array[0];
        group->amNormal = true;
    }
}

bool VariationChain::checkForNonBlankChain() const
{
    unsigned instanceCount = 0;
    for (const SharedVariationGroup & group : array) {
        instanceCount += group->dictionary->size();
    }
    return instanceCount > 0;
}

VariationGroupVector VariationChain::preGroups() const
{
    VariationGroupVector vector;
    size_t normalIndex = std::string::npos;
    for (size_t i = 0; i < array.size(); i++) {
        const SharedVariationGroup &group = array[i];
        if (group->amNormal) {
            normalIndex = i;
            break;
        }
    }
    if (normalIndex == 0 || normalIndex == std::string::npos)
        return vector;
    
    auto it = array.begin();
    std::advance(it, normalIndex + 1);
    vector.assign(array.begin(), it);
    return vector;
}

VariationGroupVector VariationChain::postGroups() const
{
    VariationGroupVector vector;
    size_t normalIndex = std::string::npos;
    for (size_t i = 0; i < array.size(); i++) {
        const SharedVariationGroup &group = array[i];
        if (group->amNormal) {
            normalIndex = i;
            break;
        }
    }
    if (normalIndex == array.size() - 1 || normalIndex == std::string::npos)
        return vector;
    
    auto it1 = array.begin();
    std::advance(it1, normalIndex + 1);
    auto it2 = array.end();
    
    vector.assign(it1, it2);
    return vector;
}

template std::string string_format<const char *, unsigned long, unsigned long, const char *>
                                                                (const char *, const char *, unsigned long, unsigned long, const char *);

std::string VariationChain::description()
{
    std::string groups;
    groups.append("[\n");
    for (SharedVariationGroup & group : array) {
        groups.append(group->description()).append("\n");
    }
    groups.append("]\n");

    VariationGroupVector _preGroups  = preGroups();
    VariationGroupVector _postGroups = postGroups();
    
    return string_format("%s Pre:%zu Post:%zu %s",
                         "VariationChain",
                         _preGroups.size(), _postGroups.size(), groups.c_str());
}

std::string VariationChain::deepDescription()
{
    std::string groups;
    groups.append("[\n");
    for (SharedVariationGroup & group : array) {
        groups.append(group->deepDescription()).append("\n");
    }
    groups.append("]\n");
    
    VariationGroupVector _preGroups  = preGroups();
    VariationGroupVector _postGroups = postGroups();
    
    return string_format("%s Pre:%zu Post:%zu %s",
                         "VariationChain",
                         _preGroups.size(), _postGroups.size(), groups.c_str());
}
