//
//  rand.c
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 4/30/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include <math.h>
#include <stdlib.h>
#include "rand.hpp"

#if defined(_WIN32)
#define RANDOM() rand()
#else
#define RANDOM random
#endif

float randValue()
{
    return (float)RANDOM()/(float)RAND_MAX;
}

float randInRange(float min, float max)
{
    float _rand = (float)RANDOM() / (float)RAND_MAX;
    return min + _rand *(max - min);
}

int randIntInRange(int min, int max)
{
    if (max - min + 1 == 0)
        return min;
    return min + RANDOM() % (max - min + 1);
}

float gaussDistribution(float mean, float sigma)
{
    float r1 = randValue();
    float r2 = randValue();
    
    float normal = sqrtf(-2.f * logf(r1)) * cosf(M_PI * 2.f * r2);
    return mean + sigma * normal;
}

double randDoubleValue()
{
    return (double)RANDOM()/(double)RAND_MAX;
}

// lambda is exponential decay scale factor
// max is range of interest from 0 to M - typical values are 3 / lambda
// larger max's will tend to produce more values near 0
// output is in range [0, 1] - lambda influences how fast it goes to 0
float exponentialDistribution(float max, float lambda)
{
    float x = randInRange(0.f, max);
    return x >= 0 ? expf(-lambda * x) : 0.f;
}

// lambda is exponential decay scale factor
// max is range of interest from 0 to M - typical values are 3 * lambda
// larger max's will tend to produce more values near 1
// output is in range [0, 1] - lambda influences how fast it goes to 1
float cumulExponentialDistribution(float max, float lambda)
{
    float x = randInRange(0.f, max);
    return x >= 0 ? 1.f - expf(-lambda * x) : 0.f;
}