//
//  XaosMatrix.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/5/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "XaosMatrix.hpp"
#include "XaosMatrixVector.hpp"

XaosMatrix::XaosMatrix(size_t xformCount)
    : vector(new XaosForXformsVector(xformCount))
{}

XaosMatrix::XaosMatrix(const XaosForXformsVector &matrix)
    : vector(new XaosForXformsVector(matrix))
{}

XaosMatrix::XaosMatrix(const XaosMatrix &other)
: vector(other.vector)
{}

XaosForXformsVector * XaosMatrix::matrix() const
{
    return vector;
}

size_t XaosMatrix::xformCount() const
{
    return matrix()->num;
}

void XaosMatrix::blendXaosFrom(const XaosMatrix *_from, const XaosMatrix *_to, XaosForXformsVector *target, double amount)
{
    if (! target)
        return;
    if (_from->xformCount() != _to->xformCount() || _from->xformCount() != target->num)
        return;
    
    size_t xformCount     = _from->xformCount();
    XaosForXformsVector *from = _from->matrix();
    XaosForXformsVector *to   = _to->matrix();
    for (size_t i = 0; i < xformCount; i++) {
        for (size_t j = 0; j < xformCount; j++) {
            float newWeight = (1.f - amount) * from->chaos(i, j) + amount * to->chaos(i, j);
            target->setChaos(i, j, newWeight);
        }
    }
}

bool XaosMatrix::operator==(const XaosMatrix &xaosMatrix)
{
    size_t xformCount = matrix()->num;
    
    XaosForXformsVector *other = xaosMatrix.matrix();
    
    if (xformCount != other->num)
        return false;
    
    for (size_t i = 0; i < xformCount; i++) {
        for (size_t j = 0; j < xformCount; j++) {
            if (matrix()->chaos(i, j) != other->chaos(i, j))
                return false;
        }
    }
    return true;
}

float XaosMatrix::chaosFrom(unsigned from, unsigned to) const
{
    return matrix()->chaos(from, to);
}

bool XaosMatrix::lockedFrom(unsigned from, unsigned to) const
{
    return matrix()->locked(from, to);
}

void XaosMatrix::setChaosWeight(float weight, unsigned from, unsigned to)
{
    matrix()->setChaos(from, to, weight);
}

void XaosMatrix::setLocked(bool locked, unsigned from, unsigned to)
{
    matrix()->setLocked(from, to, locked);
}


float XaosWeight::chaosWeight() const
{
    return xaosMatrix.chaosFrom(from, to);
}

void XaosWeight::setChaosWeight(float _weight, unsigned from, unsigned to)
{
    if (chaosWeight() == _weight)
        return;
    
    xaosMatrix.setChaosWeight(_weight, from, to);
}

bool XaosWeight::locked() const
{
    return xaosMatrix.lockedFrom(from, to);
}

void XaosWeight::setLocked(bool _locked, unsigned from, unsigned to)
{
    if (locked() == _locked)
        return;
    
    xaosMatrix.setLocked(_locked, from, to);
}

//int XaosWeight::from() const
//{
//    return from + 1;
//}
//
//int XaosWeight::to() const
//{
//    return to + 1;
//}

int XaosWeight::fromIndex() const
{
    return from;
}

int XaosWeight::toIndex() const
{
    return to;
}

