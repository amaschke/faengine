//
//  DeviceRuntime.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/15/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include <stdio.h>
#include <cstdlib>

#include "DeviceRuntime.hpp"
#include "DeviceContext.hpp"
#include "FlameExpanded.hpp"

#include "Flame.hpp"

DeviceRuntime::DeviceRuntime(uint _numBatches, uint _numDevices)
:
    clearMutex(),
    hostAccumBuffer(nullptr),
    numBatches(_numBatches),
    numDevices(_numDevices),
    g_pFlame(new Flame *[_numDevices]),
    BLOCKDIM       (new uint[_numDevices]),
    BLOCKSX        (new uint[_numDevices]),
    BLOCKSY        (new uint[_numDevices]),
    NUMPOINTS      (new uint[_numDevices]),
    renderedBatches(new uint[_numDevices])
{
    for (uint i= 0; i < numDevices; i++)
        g_pFlame[i] = NULL;
    
    for (uint i= 0; i < numDevices; i++)
        renderedBatches[i] = 0;
    
    xDim = 1;
    yDim = 1;
    
}

DeviceRuntime::~DeviceRuntime()
{
	if (hostAccumBuffer)
		ALIGNED_FREE(hostAccumBuffer);
    if (g_pFlame) {
        for (uint i = 0; i < numDevices; i++) {
            delete g_pFlame[i];
        }
        delete [] g_pFlame;
    }
    
    delete[] BLOCKDIM;
    delete[] BLOCKSX;
    delete[] BLOCKSY;
    delete[] NUMPOINTS;
}

bool DeviceRuntime::checkRenderedBatchCounts()
{
    uint batches = 0;
    for (int i = 0; i < numDevices; i++)
        batches += renderedBatches[i];
        
        bool ok = numBatches == batches;
        if (!ok) {
            fprintf(stderr, "Rendered Batch count different: rendered:%u expected:%u\n", batches, numBatches);
        }
    return ok;
}

uint DeviceRuntime::ITERATIONS()
{
    return NUM_ITERATIONS;
}

#define MULTI_DEVICE_BATCHES_MIN 10

size_t DeviceRuntime::maxDeviceCountFor(uint _numBatches, SharedDeviceContext & deviceContext)
{
    return _numBatches < MULTI_DEVICE_BATCHES_MIN ? 1 : deviceContext->selectedDeviceCount();
}

#pragma mark Quality to Batches Conversion

size_t DeviceRuntime::batchesPerTileForDesiredAdaptive(double desiredQuality,
                                                       double width,
                                                       double height,
                                                       size_t tiles,
                                                       size_t & blocksY,
                                                       uint perPointFuseIterations)
{
    const size_t BLOCKSY = 32;
    
    // NOTE: BLOCKDIM * BLOCKSX  is independent of warpsize
    // NOTE: NUMPOINTS * BLOCKSX is independent of warpsize
    // NOTE: BLOCKSY             is independent of warpsize
    size_t singlePointPoolSize   = NUM_POINTS_WARPSIZE_32 * BLOCKSX_WARPSIZE_32 * 32;
    size_t fuseIterations        = singlePointPoolSize * perPointFuseIterations;
    size_t iterationsPerBatch    = warpsPerBlock * 32 * BLOCKSX_WARPSIZE_32 * BLOCKSY * NUM_ITERATIONS;
    double requiredIterationsPerTile = width * height /tiles * desiredQuality;
    size_t batches               = ceil((requiredIterationsPerTile + fuseIterations) / iterationsPerBatch);
    size_t optBlockY             = ceil((requiredIterationsPerTile + fuseIterations)/(warpsPerBlock * BLOCKSX_WARPSIZE_32 * 32 * NUM_ITERATIONS));
    
    if (optBlockY > 32)  // Cap at 32
        optBlockY = 32;
        blocksY = optBlockY;
        if (optBlockY < 32)
            return 1;
    
    return batches;
}

size_t DeviceRuntime::batchesTotalForDesired(double desiredQuality,
                              size_t bladeCount,
                              double width,
                              double height,
                              size_t tiles,
                              size_t & blocksY,
                              uint perPointFuseIterations)
{
    size_t batchesPerTile = DeviceRuntime::batchesPerTileForDesiredAdaptive(desiredQuality, width, height, tiles, blocksY, perPointFuseIterations);
    if (batchesPerTile < bladeCount)
        batchesPerTile = bladeCount;
    return (int)tiles * batchesPerTile;
}

double DeviceRuntime::actualQualityWith(size_t batchesPerTile,
                                        double width,
                                        double height,
                                        size_t tiles,
                                        uint perPointFuseIterations)
{
    size_t singlePointPoolSize   = NUM_POINTS_WARPSIZE_32 * BLOCKSX_WARPSIZE_32 * 32;
    size_t fuseIterations        = singlePointPoolSize    * perPointFuseIterations;
    size_t iterationsPerBatch    = warpsPerBlock          * 32 * BLOCKSX_WARPSIZE_32 * 32 * NUM_ITERATIONS;
    size_t totalIterations       = tiles * batchesPerTile * iterationsPerBatch;
    size_t effectiveIterations   = totalIterations - fuseIterations;
    
    return effectiveIterations /(width * height);
}

double DeviceRuntime::actualQuality(double desiredQuality,
                                    size_t bladeCount,
                                    double width,
                                    double height,
                                    size_t tiles,
                                    uint perPointFuseIterations)
{
    size_t singlePointPoolSize   = NUM_POINTS_WARPSIZE_32 * BLOCKSX_WARPSIZE_32 * 32;
    size_t fuseIterations        = singlePointPoolSize    * perPointFuseIterations;
    size_t iterationsPerBatch    = warpsPerBlock          * 32 * BLOCKSX_WARPSIZE_32 * 32 * NUM_ITERATIONS;
    double requiredIterationsPerTile = width * height /tiles * desiredQuality;
    size_t batches               = ceil((requiredIterationsPerTile + fuseIterations) / iterationsPerBatch);
    double batchesPerTile        = batches/tiles;
    
    if (batchesPerTile < bladeCount)
        batchesPerTile = bladeCount;
        size_t totalIterations       = tiles * batchesPerTile * iterationsPerBatch;
        size_t effectiveIterations   = totalIterations - fuseIterations;
        
        return effectiveIterations /(width * height);
}

double DeviceRuntime::actualQuality(double desiredQuality,
                                    size_t bladeCount,
                                    double width,
                                    double height,
                                    size_t tiles,
                                    size_t blocksY,
                                    uint perPointFuseIterations)
{
    size_t singlePointPoolSize   = NUM_POINTS_WARPSIZE_32 * BLOCKSX_WARPSIZE_32 * 32;
    size_t fuseIterations        = singlePointPoolSize * perPointFuseIterations;
    size_t iterationsPerBatch    = warpsPerBlock * 32 * BLOCKSX_WARPSIZE_32 * blocksY * NUM_ITERATIONS;
    double requiredIterationsPerTile = width * height /tiles * desiredQuality;
    size_t batches               = ceil((requiredIterationsPerTile + fuseIterations) / iterationsPerBatch);
    double batchesPerTile            = batches/tiles;
    
    if (batchesPerTile < bladeCount)
        batchesPerTile = bladeCount;
    size_t totalIterations       = tiles * batchesPerTile * iterationsPerBatch;
    size_t effectiveIterations   = totalIterations - fuseIterations;
    
    return effectiveIterations /(width * height);
}

size_t DeviceRuntime::adaptiveBlockYDim(double desiredQuality,
                                        double width,
                                        double height,
                                        size_t tiles,
                                        uint perPointFuseIterations)
{
    // NOTE: BLOCKDIM * BLOCKSX is independent of warpsize
    // NOTE: NUMPOINTS * BLOCKSX is independent of warpsize
    // NOTE: BLOCKSY is independent of warpsize
    size_t singlePointPoolSize   = NUM_POINTS_WARPSIZE_32 * BLOCKSX_WARPSIZE_32 * 32;
    size_t fuseIterations        = singlePointPoolSize * perPointFuseIterations;
    double requiredIterationsPerTile = width * height /tiles * desiredQuality;
    size_t optBlockY             = ceil((requiredIterationsPerTile + fuseIterations)/(warpsPerBlock * BLOCKSX_WARPSIZE_32 * 32 * NUM_ITERATIONS));
    
    if (optBlockY > 32)  // Cap at 32
        optBlockY = 32;
        return optBlockY;
}

size_t DeviceRuntime::totalIterationsForBatches(size_t batches)
{
    size_t iterationsPerBatch    = warpsPerBlock * 32 * BLOCKSX_WARPSIZE_32 * 32 * NUM_ITERATIONS;
    return batches * iterationsPerBatch;
}

size_t DeviceRuntime::totalIterationsForBatches(size_t batches, size_t blocksY)
{
    size_t iterationsPerBatch    = warpsPerBlock * 32 * BLOCKSX_WARPSIZE_32 * blocksY * NUM_ITERATIONS;
    return batches * iterationsPerBatch;
}

void DeviceRuntime::copyFlame(struct Flame * flame, uint selectedDeviceNum)
{
    if (g_pFlame[selectedDeviceNum]) {
        delete g_pFlame[selectedDeviceNum];
        g_pFlame[selectedDeviceNum] = NULL;
    }
    flame->CloneAligned(&g_pFlame[selectedDeviceNum], 4096);
}

std::string DeviceRuntime::flameReport(uint selectedDeviceNum)
{
    return FlameExpanded::flameReport(g_pFlame[selectedDeviceNum]);
}

