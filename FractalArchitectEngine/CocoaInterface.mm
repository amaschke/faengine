//
//  CocoaInterface.m
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/10/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "CocoaInterface.h"
#import "VariationSet.hpp"

#include <dirent.h>
#include <vector>
#include <memory>
#include <openssl/md5.h>
#include <sstream>
#include <iomanip>
#include <string>

std::string pathForTestResource(const char *_bundlePath, const char *_filename)
{
    NSString *bundlePath = [NSString stringWithUTF8String:_bundlePath];
    NSString *filename   = [NSString stringWithUTF8String:_filename];
    NSString *extension  = [filename pathExtension];
    NSString *basename   = [filename substringToIndex:filename.length - extension.length - 1];
    NSString *folder     = @"Contents/Resources/";
    folder               = [folder stringByAppendingString:basename];
    NSBundle *bundle     = [NSBundle bundleWithPath:bundlePath];
    NSString *path       = [bundle pathForResource:basename ofType: extension];
    if (!path)
        return nil;
    return std::string([path UTF8String]);
}

static NSURL *URLForResource(const char *_basename, const char *_extension)
{
    NSString *extension      = [NSString stringWithUTF8String:_extension];
    NSBundle *mainBundle     = [NSBundle mainBundle];
    NSBundle *faEngineBundle = [NSBundle bundleWithURL:[[mainBundle privateFrameworksURL] URLByAppendingPathComponent:@"FA_Engine.framework"]];
    
    NSURL *url = [faEngineBundle URLForResource:[NSString stringWithUTF8String:_basename] withExtension: extension];
    if (url)
        return url;
    
    NSString *basename  = [NSString stringWithFormat:@"FA_Engine.framework/Resources/%s", _basename];
    return [mainBundle URLForResource:basename withExtension: extension];
}

std::string pathForResource(const char * _basename, const char * _extension)
{
    NSString *extension      = [NSString stringWithUTF8String:_extension];
    NSString *basename       = [NSString stringWithFormat:@"FA_Engine.framework/Resources/%s", _basename];
    NSBundle *mainBundle     = [NSBundle mainBundle];
    NSBundle *faEngineBundle = [NSBundle bundleWithURL:[[mainBundle privateFrameworksURL] URLByAppendingPathComponent:@"FA_Engine.framework"]];

    NSURL *url = [faEngineBundle URLForResource:[NSString stringWithUTF8String:_basename] withExtension: extension];
    if (url)
        return std::string([[url path] UTF8String]);
    
    NSString *path      = [[NSBundle mainBundle] pathForResource:basename ofType:extension];
    if (! path)
        return "";
    return std::string([path UTF8String]);
}

// check and see if its an embedded resource first, otherwise just use the original input path
std::string pathForPossibleResource(std::string _path)
{
    NSString *path           = [NSString stringWithUTF8String:_path.c_str()];
    NSString *extension      = [path pathExtension];
    NSString *basename       = [path stringByDeletingPathExtension];
    NSBundle *mainBundle     = [NSBundle mainBundle];
    
    NSURL *url = [mainBundle URLForResource:basename withExtension: extension];
    if (url)
        return std::string([[url path] UTF8String]);
    return _path;
}

std::string cachePath()
{
	return  "~/Library/Caches/com.centcom.FractalArchitectEngine";
}

std::string variationSetsDirectoryPath()
{
    const char * tildePath =
#ifdef __APPLE__
    "~/Library/Application Support/FractalArchitectEngine/VariationSets/";
#else
    "~/.fa4/Application Support/FractalArchitectEngine/VariationSets/";
#endif
    return std::string(expandTildeInPath(tildePath));
}

static NSURL *variationSetsDirectoryURL()
{
    NSString *varSetsPath = [@"~/Library/Application Support/FractalArchitectEngine/VariationSets" stringByExpandingTildeInPath];
    NSString *expandedVarSetsPath = [varSetsPath stringByExpandingTildeInPath];
    return [NSURL fileURLWithPath:expandedVarSetsPath isDirectory:YES];
}

static NSURL *cachedLibraryURL()
{
    return [variationSetsDirectoryURL() URLByAppendingPathComponent:@"library.xml"];
}

static NSURL *cachedMyLibraryURL()
{
    return [variationSetsDirectoryURL() URLByAppendingPathComponent:@"myLibrary.xml"];
}

bool cachedLibraryExists()
{
    
    NSError *error           = (NSError *)nil;
    NSFileManager *fm        = [NSFileManager defaultManager];
    NSURL *factoryLibraryURL = URLForResource(VariationSet::factoryLibraryBaseName(), "xml");

    NSDictionary *factoryAttributes = [fm attributesOfItemAtPath:[factoryLibraryURL path] error:&error];
    NSDictionary *cachedAttributes  = [fm attributesOfItemAtPath:[cachedLibraryURL() path]
error:&error];
    if (error) return NO;
    NSDate *factoryModDate = [factoryAttributes fileModificationDate];
    NSDate *cachedModDate  = [cachedAttributes fileModificationDate];
    BOOL outOfDate         = [cachedModDate compare:factoryModDate] == NSOrderedAscending;
    return ! outOfDate;
}

void cacheMyLibrary()
{
    NSError *error      = (NSError *)nil;
    NSURL *myLibraryURL = URLForResource("myLibrary", "xml");
    
    NSString *varSetsDir = [variationSetsDirectoryURL() path];
    NSFileManager *fm    = [NSFileManager defaultManager];
    [fm createDirectoryAtPath:varSetsDir
  withIntermediateDirectories:YES
                   attributes:(NSDictionary *)nil
                        error:&error];
    
    [fm copyItemAtURL:myLibraryURL toURL:cachedMyLibraryURL() error:&error];
}

void setupFAEngineApplicationSupport()
{
    // create VariationSets directory in the FractalArchitect's Application Support directory structure
    NSString *varSetsPath    = @"~/Library/Application Support/FractalArchitectEngine/VariationSets";
    
    NSError *error                = nil;
    NSFileManager *fm    = [NSFileManager defaultManager];
    NSString *expandedVarSetsPath = [varSetsPath stringByExpandingTildeInPath];
    if (! [fm fileExistsAtPath:expandedVarSetsPath])
        [fm createDirectoryAtPath:expandedVarSetsPath withIntermediateDirectories:YES attributes:nil error:&error];
    
}

const char * expandTildeInPath(const char *_path)
{
    NSString *path = [NSString stringWithUTF8String:_path];
    NSString *varSetsPath = [path stringByExpandingTildeInPath];
    
    return varSetsPath.UTF8String;
}

double osVersion()
{
    return NSAppKitVersionNumber;
}

// empty a directory - but leave the directory itself
void empty_dir(const char *path)
{
    static char abs_path[256];
    
    struct dirent *entry = NULL;
    DIR *dir             = NULL;
    dir                  = opendir(path);
    while((entry = readdir(dir)))
    {
        DIR *sub_dir       = NULL;
        FILE *file         = NULL;
        if(*(entry->d_name) != '.')
        {
            snprintf(abs_path, sizeof(abs_path), "%s/%s", path, entry->d_name);
            if((sub_dir = opendir(abs_path)))
            {
                closedir(sub_dir);
                remove_dir(abs_path);
            }
            else
            {
                if((file = fopen(abs_path, "r")))
                {
                    fclose(file);
                    remove(abs_path);
                }
            }
        }
    }
    closedir(dir);
}

// remove a directory
void remove_dir(const char *path)
{
    empty_dir(path);
    remove(path);
}


// get array of file names in this directory
std::vector<std::string> directoryFileNamesContents(const char *path)
{
    std::vector<std::string> names;
    
    struct dirent *entry = NULL;
    DIR *dir             = opendir(path);
    while((entry = readdir(dir)))
    {
        if(*(entry->d_name) != '.')
        {
            if (entry->d_type == DT_REG) {
                names.emplace_back(entry->d_name);
            }
        }
    }
    closedir(dir);
    return names;
}

std::vector<std::string> directorySubDirContents(const char *path)
{
    std::vector<std::string> names;
    
    struct dirent *entry = NULL;
    DIR *dir             = opendir(path);
    while((entry = readdir(dir)))
    {
        if(*(entry->d_name) != '.')
        {
            if (entry->d_type == DT_DIR) {
                names.emplace_back(entry->d_name);
            }
        }
    }
    closedir(dir);
    return names;
}

std::string md5DigestString(const std::string & str)
{
    unsigned char result[MD5_DIGEST_LENGTH];
    MD5((unsigned char*)str.c_str(), str.size(), result);
    
    std::ostringstream sout;
    sout<<std::hex<<std::setfill('0');
    for(long long c: result)
    {
        sout<<std::setw(2)<<(long long)c;
    }
    return sout.str();
}

