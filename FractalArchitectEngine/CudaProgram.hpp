//
//  CudaProgram.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 7/7/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef CudaProgram_hpp
#define CudaProgram_hpp

#include <cuda.h>
#include <vector>
#include <nvrtc.h>

#include "Exports.hpp"
#include "DeviceProgram.hpp"

class CudaContext;
class CudaDeviceKind;
class CudaProgram;
class VariationSet;

using uint            = unsigned int;
using SharedCudaContext = std::shared_ptr<CudaContext>;
using SharedCudaProgram = std::shared_ptr<CudaProgram>;

class FAENGINE_API CudaProgram : public DeviceProgram {
    friend class Flam4CudaRuntime;
public:
    CudaProgram(SharedCudaContext ctx, SharedDeviceKind _deviceKind, CUcontext _cucontext, bool forceRebuild, const SharedVariationSet &_variationSet);
    virtual ~CudaProgram();
    
    CudaDeviceKind *cudaDeviceKind();
    
    std::string ptxPathForUuid(const std::string &_uuid);
    std::vector<char> ptxContentsForUuid(const std::string & _uuid);
    void writePtxForUuid(const std::string &_uuid, std::vector<char> &ptxData);
    
    static bool outOfDateBinary(const std::string & binaryPath, const std::string &sourcePath);
    
    void logErrStatusForProgramName(const std::string &programName, nvrtcProgram program);
    
    CUresult status();
    void setStatus(CUresult status);
    void makeKernelsForDevice(nvrtcProgram program);
    void makeKernelsFromPTX(std::vector<char> &ptxData);
    
    CUmodule module();
    
private:
    
    // ========== Members =====================
    CUmodule _module;
    CUresult _status;
    CUcontext cucontext;
    
    CUfunction iteratePointsKernal;
    CUfunction setBufferKernal;
    CUfunction reductionKernal;
    CUfunction postProcessStep1Kernal;
    CUfunction postProcessStep2Kernal;
    CUfunction colorCurveRGB3ChannelsKernal;
    CUfunction colorCurveRGBChannelKernal;
    CUfunction FlexibleDensityEstimationKernal;
    CUfunction RGBA128FtoRGBA32UKernal;
    CUfunction RGBA128FtoBGRA32UKernal;
    CUfunction RGBA128FtoRGBA64UKernal;
    CUfunction RGBA128FtoRGBA128FKernal;
    CUfunction MergeKernal;
    CUfunction readChannelKernal;
    CUfunction writeChannelKernal;
    CUfunction writeChannelStripedKernal;
    CUfunction convolveRowKernal;
    CUfunction convolveColKernal;
};


#endif /* CudaProgram_hpp */
