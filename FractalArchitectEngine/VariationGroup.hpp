//
//  VariationGroup.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/2/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef VariationGroup_hpp
#define VariationGroup_hpp

#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

#include "Exports.hpp"
#include "pugixml.hpp"

class AffineMatrix;
class NamedAffineMatrix;
class VariationChain;
class VariationGroup;
class VariationParameter;
class VariationSet;
class VarParInstance;

using uint                 = unsigned int;
using SharedParameter      = std::shared_ptr<VariationParameter>;
using SharedPvg            = std::shared_ptr<VariationGroup>;
using SharedVariationChain = std::shared_ptr<VariationChain>;
using SharedVariationSet   = std::shared_ptr<VariationSet>;
using SharedVariationGroup = std::shared_ptr<VariationGroup>;
using SharedVarParInstance = std::shared_ptr<VarParInstance>;
using WeakVariationGroup   = std::weak_ptr<VariationGroup>;
using WeakVariationChain   = std::weak_ptr<VariationChain>;
using VarParSet            = std::unordered_map<std::string, SharedVarParInstance>;
using VariationGroupVector = std::vector<SharedVariationGroup>;
using AttrDict             = std::unordered_map<std::string, pugi::xml_attribute>;
using StringVector         = std::vector<std::string>;
using StringsDict          = std::unordered_map<std::string, StringVector>;


class FAENGINE_API VariationGroup  : public std::enable_shared_from_this<VariationGroup> {
    friend class Flam4ClRuntime;
    friend class Flam4CudaRuntime;
    friend class FlameParse;
    friend class VariationChain;
    friend class VarParInstance;

//    VariationGroup(const SharedVariationGroup &v);
public:
    VariationGroup();
    VariationGroup(const SharedVariationChain &_chain);
    VariationGroup(bool _amNormal, const SharedVariationChain &_chain);
    
    static SharedVariationGroup makeCopyOf(const SharedVariationGroup &_v);

    SharedVarParInstance & operator[](const std::string & key);
    
    void clear()  { dictionary->clear(); }
    size_t size() { return dictionary->size(); }
    StringVector allKeys();
    
    SharedVarParInstance * findVarpar(const std::string & key);
    VarParSet::iterator find(const std::string & key);
    VarParSet::iterator begin();
    VarParSet::iterator end();
    VarParSet getDictionary() { return *dictionary; }
        
    size_t      groupIndex() const;
    std::string groupName();
    SharedVariationChain getChain() const  { return chain.lock(); }
    void setChain(const SharedVariationChain &_chain);
    
    size_t getTargetGroupIndex() const                   { return targetGroupIndex; }
    void   setTargetGroupIndex(size_t _targetGroupIndex) { targetGroupIndex = _targetGroupIndex; }
    
    void setObjectForKey(const std::string & key, const SharedVarParInstance &varpar);
    
    std::string description();
    std::string deepDescription();

private:
    VariationGroup(const VariationGroup &v);
    
    void addVarParInstance(const std::string &_varparKey,
                           size_t _instanceNum,
                           const SharedPvg &_group,
                           size_t _xformIndex,
                           size_t targetGroupIndex,
                           float floatValue);
    
    static SharedVariationGroup identityMatrix2dGroupForVariationSet(const SharedVariationSet &variationSet,
                                                               const SharedVariationChain &_chain);
    static SharedVariationGroup groupWithAmNormal(bool _amNormal,
                                            const SharedVariationChain &_chain);
    static SharedVariationGroup matrix2dGroupForNamedMatrix(const NamedAffineMatrix &m,
                                                      const SharedVariationSet &variationSet,
                                                      const SharedVariationChain &chain);

    void addIdentityMatrix2DForVariationSet(const SharedVariationSet &variationSet);
    void addMatrix(const NamedAffineMatrix &m, const std::string &varKey, size_t instanceNum, const SharedVariationSet &variationSet);
    
    SharedParameter &findParameter(const std::string &varKey, const SharedVariationSet &variationSet);
    size_t maxInstanceNumForKey(const std::string & key);
    std::vector<size_t> *allInstanceNumsForKey(const std::string & key);
    void insertInstanceNumSlotAt(size_t instanceNum, const std::string &baseKey);
    void renumberInstances();
    
    bool amMatrix(const SharedVariationSet &variationSet);
    bool amPostMatrix(const SharedVariationSet &variationSet);
    bool amIdentityMatrix(const SharedVariationSet &variationSet);
    
    void parseGroupAttrs(AttrDict & attrs, SharedVariationSet & variationSet);
    
    SharedVarParInstance * varparForVariationKey(const std::string &varKey, const std::string &paramName, size_t instanceNum);
    SharedVarParInstance & varparRefForVariationKey(const std::string &varKey, const std::string &paramName, size_t instanceNum);
    
    float floatValueForVariationKey(const std::string &varKey, const std::string &paramName, size_t instanceNum);
    void setFloatValueForVariationKey(const std::string &varKey, const std::string &paramName, size_t instanceNum, float floatValue);
    
    AffineMatrix affineMatrixForVariationKey(std::string key);
    std::vector<SharedVarParInstance> affineMatrixVarparsForVariationKey(std::string key);
    void setNamedAffineMatrix(const NamedAffineMatrix &m, std::string key, const SharedVariationSet &variationSet);
    NamedAffineMatrix namedAffineMatrixForVariationKey(std::string key);
    
    void addFloatValueInstance(AttrDict & attrs, std::vector<std::string> & vars, const char *key, size_t i, SharedVariationSet & variationSet);
    void addFloatValueInstance(const char *key, SharedVariationSet & variationSet);
    
    // ==== Members ==========================
    
    bool                       amNormal;  // normal group
    WeakVariationChain         chain;
    std::unique_ptr<VarParSet> dictionary;
    size_t                     targetGroupIndex; // when chain is nil, this is the routing information for where to put this interest
};

#endif /* VariationGroup_hpp */
