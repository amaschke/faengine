//
//  CurveDefinition.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/6/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef CurveDefinition_hpp
#define CurveDefinition_hpp

#include <vector>
#include <memory>
#include "common.hpp"
#include "Exports.hpp"

#ifdef _WIN32
#include <Windows.h>
#endif

using namespace FA;


enum CurveType {
    eLinear,
    eExponential,
    eTanh,
    eSinh,
    eCos,
    eCos2,
    eCosw,
    eSinA,
    eCosA,
    eCatmull,
    eBspline,
    eNaturalCubic,
    eCatmull2D,
    eBspline2D,
    eCatmull3D,
    eBspline3D,
    eBezier,
};

enum PredefinedCurveType {
    eLinear_Ramp,
    eEaseIn_EaseOut_Ramp,  // tanh   a = 0.607
    eEaseIn_EaseOut_Ramp2, // cos half power   a = 0.607
    eFastIn_FastOut_Ramp,  // sinh   a = 0.607
};

enum CurveMode {
    eNormal,
    eFlipped,
    eMirrored,
    eFlippedMirrored,
};

using PointArray        = std::vector<Point>;
using Point3DArray      = std::vector<Point3D>;
using SharedData        = std::shared_ptr<std::vector<char>>;
using SharedPointVector = std::shared_ptr<std::vector<Point>>;
using UniqueData        = std::unique_ptr<std::vector<char>>;
using UniqueFloatVector = std::unique_ptr<std::vector<float>>;

template <class T, enum CurveType _curveType>
class FAENGINE_API CurveDefinition {
protected:
    CurveDefinition(float _a, float _b, bool _flipped, bool _mirrored, bool _integralA);
    
    CurveDefinition(const CurveDefinition &c);
    
    virtual std::shared_ptr<std::vector<float>> curveForCount(int count) = 0;
    virtual std::shared_ptr<std::vector<float>> curveForCountAlternate(int count) = 0;
    virtual std::shared_ptr<std::vector<float>> curveForCountAlternate(int count, float start, float end) = 0;
    virtual double curveAtOffset(double u) = 0;
    virtual T interpolatePointForU(float u) = 0;
    virtual bool flippable() = 0;
    virtual bool reversable() = 0;
    
    enum CurveMode setupCurveMode();
    
    std::vector<T> & getControlPoints() { return controlPoints; }
    
    std::shared_ptr<std::vector<T>> controlPointsData();
    void setControlPointsData(const std::shared_ptr<std::vector<T>> & controlPointsData);
    
    // =========== Members ============
    float                  a;
    float                  b;
    bool                   flipped;
    bool                   mirrored;
    bool                   integralA;
    enum CurveMode         curveMode;
    std::vector<T>         controlPoints;
};

#endif /* CurveDefinition_hpp */
