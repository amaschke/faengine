//
//  VariationGroup.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/2/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include <set>
#include <memory>
#include <unordered_set>
#include "VariationGroup.hpp"
#include "VariationChain.hpp"
#include "VarParInstance.hpp"
#include "NamedAffineMatrix.hpp"
#include "Variation.hpp"
#include "VariationSet.hpp"
#include "Utilities.hpp"
#include "VariationParameter.hpp"
#include "FlameParse.hpp"

#ifdef __linux
namespace std {
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
}
#endif

template std::string string_format<const char *>( const char *format, const char *);
template std::string string_format<const char *, unsigned int>(const char *, const char *, unsigned int);


VariationGroup::VariationGroup()
    : std::enable_shared_from_this<VariationGroup>(),
    amNormal(false),
    chain(),
    dictionary(std::make_unique<VarParSet>()),
    targetGroupIndex(std::string::npos)
{}

VariationGroup::VariationGroup(const SharedVariationChain &_chain)
    : VariationGroup()
{
    amNormal = true;
    chain = _chain;
}

VariationGroup::VariationGroup(const VariationGroup &_v)
: std::enable_shared_from_this<VariationGroup>(),
  amNormal(_v.amNormal),
  chain(_v.chain),
  dictionary(std::make_unique<VarParSet>()),
  targetGroupIndex(std::string::npos)
{}

//VariationGroup::VariationGroup(const SharedVariationGroup &v)
//    : VariationGroup(*v)
//{}

VariationGroup::VariationGroup(bool _amNormal, const SharedVariationChain &_chain)
    : std::enable_shared_from_this<VariationGroup>(),
    amNormal(_amNormal),
    chain(_chain),
    dictionary(std::make_unique<VarParSet>()),
    targetGroupIndex(std::string::npos)
{}

// usage of shared_from_this() requires a two phase copy constructor
SharedVariationGroup VariationGroup::makeCopyOf(const SharedVariationGroup &v)
{
    SharedVariationGroup vg = std::shared_ptr<VariationGroup>(new VariationGroup(*v));
    
    size_t groupIndex = v->groupIndex();
    auto keys         = ::allKeys(*v->dictionary);
    
    for (const std::string & key : keys) {
        const SharedVarParInstance &instance = v->dictionary->operator[](key);
        const VarParInstance &inst           = *instance;
        SharedVarParInstance copiedInstance  = std::make_shared<VarParInstance>(inst);
        
        copiedInstance->setGroup(vg->shared_from_this());
        vg->dictionary->operator[](key) = std::move(copiedInstance);
        vg->targetGroupIndex = vg->chain.expired() ?  vg->targetGroupIndex : groupIndex;
    }
    return vg;
}

void VariationGroup::setChain(const SharedVariationChain &_chain)
{
    chain = _chain;
    if (_chain)
        targetGroupIndex = groupIndex();
}

size_t VariationGroup::groupIndex() const
{
    if (chain.expired())
        return targetGroupIndex;
    
    // workaround for irritating compiler issue with const ness of this pointer
    SharedVariationGroup vg = const_cast<VariationGroup *>(this)->shared_from_this();
    return getChain()->indexOfGroup(vg, targetGroupIndex);
}

std::string VariationGroup::groupName()
{
    if (chain.expired())
        return "";
    return chain.lock()->groupNameForVariationGroup(shared_from_this());
}

SharedVariationGroup VariationGroup::groupWithAmNormal(bool _amNormal, const SharedVariationChain &_chain)
{
    return std::make_shared<VariationGroup>(_amNormal, _chain);
}


void VariationGroup::addVarParInstance(const std::string &_varparKey,
                                       size_t _instanceNum,
                                       const SharedPvg &_group,
                                       size_t _xformIndex,
                                       size_t targetGroupIndex,
                                       float floatValue)
{
    dictionary->emplace(VarParInstance::makeInstancedKey(_varparKey, _instanceNum),
                        std::make_shared<VarParInstance>(_varparKey, _instanceNum, shared_from_this(), _xformIndex,
                                                         targetGroupIndex, floatValue));
}

void VariationGroup::addIdentityMatrix2DForVariationSet(const SharedVariationSet &variationSet)
{
    addMatrix(NamedAffineMatrix(), "matrix2d", 1, variationSet);
}

SharedParameter &VariationGroup::findParameter(const std::string &varKey, const SharedVariationSet &variationSet)
{
    auto it = variationSet->variationParametersByKey.find(varKey);
    return it->second;
}

void VariationGroup::addMatrix(const NamedAffineMatrix &m, const std::string &varKey, size_t instanceNum, const SharedVariationSet &variationSet)
{
    Variation *variation   = variationSet->variationForKey(varKey);
    if (!variation)
        return;
    
    addVarParInstance(variation->getKey(), instanceNum, shared_from_this(), variation->getXformIndex(), m.groupIndex, 1.f);
    
    SharedParameter &vpa = findParameter(string_format("%s_%s", varKey.c_str(), "a"), variationSet);
    SharedParameter &vpb = findParameter(string_format("%s_%s", varKey.c_str(), "b"), variationSet);
    SharedParameter &vpc = findParameter(string_format("%s_%s", varKey.c_str(), "c"), variationSet);
    SharedParameter &vpd = findParameter(string_format("%s_%s", varKey.c_str(), "d"), variationSet);
    SharedParameter &vpe = findParameter(string_format("%s_%s", varKey.c_str(), "e"), variationSet);
    SharedParameter &vpf = findParameter(string_format("%s_%s", varKey.c_str(), "f"), variationSet);
    
    addVarParInstance(vpa->getKey(), instanceNum, shared_from_this(), vpa->getXformIndex(), m.groupIndex, m.a);
    addVarParInstance(vpb->getKey(), instanceNum, shared_from_this(), vpb->getXformIndex(), m.groupIndex, m.b);
    addVarParInstance(vpc->getKey(), instanceNum, shared_from_this(), vpc->getXformIndex(), m.groupIndex, m.c);
    addVarParInstance(vpd->getKey(), instanceNum, shared_from_this(), vpd->getXformIndex(), m.groupIndex, m.d);
    addVarParInstance(vpe->getKey(), instanceNum, shared_from_this(), vpe->getXformIndex(), m.groupIndex, m.e);
    addVarParInstance(vpf->getKey(), instanceNum, shared_from_this(), vpf->getXformIndex(), m.groupIndex, m.f);
}


SharedVariationGroup VariationGroup::identityMatrix2dGroupForVariationSet(const SharedVariationSet &variationSet,
                                                                    const SharedVariationChain &_chain)
{
    SharedVariationGroup group = std::make_shared<VariationGroup>(false, _chain);
    group->addIdentityMatrix2DForVariationSet(variationSet);
    return group;
}

SharedVariationGroup VariationGroup::matrix2dGroupForNamedMatrix(const NamedAffineMatrix &m,
                                           const SharedVariationSet &variationSet,
                                           const SharedVariationChain &_chain)
{
    SharedVariationGroup group = std::make_shared<VariationGroup>(false, _chain);
    group->setTargetGroupIndex(m.groupIndex);
    group->addMatrix(m, "matrix2d", 1, variationSet);
    return group;
}

SharedVarParInstance & VariationGroup::operator[](const std::string & key)
{
    return dictionary->operator[](key);
}

VarParSet::iterator VariationGroup::find(const std::string & key)
{
    return dictionary->find(key);
}

VarParSet::iterator VariationGroup::begin()
{
    return dictionary->begin();
}

VarParSet::iterator VariationGroup::end()
{
    return dictionary->end();
}

SharedVarParInstance * VariationGroup::findVarpar(const std::string & key)
{
    if (dictionary->find(key) == dictionary->end())
        return nullptr;
    return &dictionary->operator[](key);
}

void VariationGroup::setObjectForKey(const std::string & key, const SharedVarParInstance &varpar)
{
    varpar->setGroup(shared_from_this());
    dictionary->insert(std::make_pair<const std::string &, const SharedVarParInstance &>(key, varpar));
}

size_t VariationGroup::maxInstanceNumForKey(const std::string & key)
{
    std::string baseKey   = VarParInstance::uninstancedKey(key);
    size_t maxInstanceNum = 0;
    for (auto & pair : *dictionary) {
        const std::string key = pair.first;
        if (baseKey == VarParInstance::uninstancedKey(key)) {
            size_t instanceNum = VarParInstance::instanceNumOfKey(key);
            if (instanceNum != std::string::npos)
                maxInstanceNum = maxInstanceNum < instanceNum ? instanceNum : maxInstanceNum;
        }
    }
    return maxInstanceNum == 0 ? std::string::npos : maxInstanceNum;
}

std::vector<size_t> * VariationGroup::allInstanceNumsForKey(const std::string & key)
{
    std::set<size_t> set;
    std::vector<size_t> *vector = new std::vector<size_t>();
    std::string baseKey      = VarParInstance::uninstancedKey(key);
    
    for (auto & pair : *dictionary) {
        const std::string key = pair.first;
        if (baseKey == VarParInstance::uninstancedKey(key)) {
            set.insert(VarParInstance::instanceNumOfKey(key));
        }
    }
    vector->insert(vector->end(), set.begin(), set.end());    
    std::sort(vector->begin(), vector->end());
    return vector;
}

void VariationGroup::insertInstanceNumSlotAt(size_t instanceNum, const std::string &baseKey)
{
    std::unordered_map<std::string, SharedVarParInstance> temp;
    for (auto & pair : *dictionary) {
        const std::string key = pair.first;
        if (baseKey == VarParInstance::uninstancedKey(key)) {
            if (VarParInstance::instanceNumOfKey(key) >= instanceNum) {
                SharedVarParInstance & varpar = dictionary->operator[](key);
                size_t varparInstanceNum   = varpar->instanceNum;
                if (varparInstanceNum == std::string::npos)
                    continue;
                
                size_t adjustedInstanceNum = varparInstanceNum + 1;
                varpar->setInstanceNum(varparInstanceNum + 1);
                
                std::string newKey = VarParInstance::makeInstancedKey(baseKey, adjustedInstanceNum);
                temp.insert(std::make_pair<std::string &, SharedVarParInstance &>(newKey, varpar));
                dictionary->erase(key);
            }
        }
    }
    dictionary->insert(temp.begin(), temp.end());
}

// editing can leave gaps in the instance numbers inside a variation group - this will recompress the series
void VariationGroup::renumberInstances()
{
    std::vector<SharedVarParInstance> array;
    array.reserve(dictionary->size());
    for (auto it = dictionary->begin(); it != dictionary->end(); ++it ) {
        array.push_back(it->second);
    }
    std::sort(array.begin(), array.end(), VarParInstance::compare);
    
    std::vector<std::string> keys;
    keys.reserve(array.size());
    
    std::transform(array.begin(), array.end(), keys.begin(),
                   [](const SharedVarParInstance & instance) { return instance->instancedKey(); });
    
    dictionary->clear();
    size_t instanceNum      = std::string::npos;
    std::string lastBaseKey = "";
    
    for (size_t i = 0; i < keys.size(); i++) {
        const std::string currentKey = keys[i];
        const std::string baseKey = VarParInstance::uninstancedKey(currentKey);
        size_t instanceNum2 = VarParInstance::instanceNumOfKey(currentKey);
        
        if (baseKey == lastBaseKey)
            instanceNum++;
        else
            instanceNum = instanceNum2 == std::string::npos ? std::string::npos : 1;
        SharedVarParInstance &instance = array[i];
        instance->instanceNum = instanceNum;
        dictionary->operator[](instance->instancedKey()) = instance;
    }
}

template std::string string_format<const char *, void *, const char *, const char *, unsigned long, const char *>
                        (const char *, const char *, void *, const char *, const char *, unsigned long, const char *);

std::string VariationGroup::description()
{
    std::string keys;
    keys.append("[ ");
    for (std::string & key : ::allKeys(*dictionary)) {
        keys.append(key).append(" ");
    }
    keys.append("]");
    
    return string_format("%s Chain:%#lx Normal:%s %s Count:%zu %s",
                         "VariationGroup",
                         (void *)chain.lock().get(),
                         amNormal ? "YES" : "NO",
                         groupName().c_str(),
                         dictionary->size(),
                         keys.c_str());
}

std::string VariationGroup::deepDescription()
{
    std::string keys;
    keys.append("[ ");
    for (std::string & key : ::allKeys(*dictionary)) {
        keys.append(key).append(" ");
    }
    keys.append("]");
    
    keys.append("\n[\n");
    for (const std::string & key : ::allKeys(*dictionary)) {
        keys.append("\t").append(key).append(": {");
        
        const SharedVarParInstance &instance = dictionary->operator[](key);
        keys.append(instance->description().c_str()).append("}\n");
    }
    keys.append("]\n");
    
    
    return string_format("%s Chain:%#lx Normal:%s %s Count:%lu %s",
                         "VariationGroup",
                         (void *)chain.lock().get(),
                         amNormal ? "YES" : "NO",
                         groupName().c_str(),
                         (unsigned long)dictionary->size(),
                         keys.c_str());
}

#pragma mark Matrix2d Support

// we can do special renderer runtime handling for variationMatrixGroup
bool VariationGroup::amMatrix(const SharedVariationSet &variationSet)
{
    std::unordered_set<std::string> variations;
    variations.reserve(20);
    
    for (auto & pair : *dictionary) {
        const std::string key = pair.first;
        Variation *variation  = variationSet->variationForKey(key);
        if (variation) {
            variations.insert(VarParInstance::uninstancedKey(key));
        }
    }
    if (variations.size() == 1 &&
        (variations.find("matrix2d") != variations.end() ||
         variations.find("post_matrix2d") != variations.end() ||
         variations.find("matrix3d") != variations.end() ||
         variations.find("post_matrix3d") != variations.end())) {
            return true;
    }
    return false;
}

bool VariationGroup::amPostMatrix(const SharedVariationSet &variationSet)
{
    std::unordered_set<std::string> variations;
    variations.reserve(20);
    
    for (auto & pair : *dictionary) {
        const std::string key = pair.first;
        Variation *variation  = variationSet->variationForKey(key);
        if (variation) {
            variations.insert(VarParInstance::uninstancedKey(key));
        }
    }
    if (variations.size() == 1 &&
        (variations.find("post_matrix2d") != variations.end() ||
         variations.find("post_matrix3d") != variations.end())) {
            return true;
        }
    return false;
}

bool VariationGroup::amIdentityMatrix(const SharedVariationSet &variationSet)
{
    std::unordered_set<std::string> variations;
    for (auto & pair : *dictionary) {
        const std::string key = pair.first;
        Variation *variation  = variationSet->variationForKey(key);
        if (variation) {
            variations.insert(VarParInstance::uninstancedKey(key));
        }
    }
    if (variations.size() == 1 &&  // only variation in the group
        (variations.find("matrix2d") != variations.end() ||   // either matrix2d or post_matrix2d, their params are a, b, c, ..., f
         variations.find("post_matrix2d") != variations.end())) {
            for (auto & pair : *dictionary) {
                const std::string key = pair.first;
                Variation *variation  = variationSet->variationForKey(key);
                if (!variation) {  // its a parameter instance
                    std::string uninstancedKey = VarParInstance::uninstancedKey(key);
                    const SharedVarParInstance &instance = dictionary->operator[](key);
                    
                    if (uninstancedKey.length() == 1) {
                        std::string suffix = uninstancedKey.substr(uninstancedKey.length() - 1);
                        if (uninstancedKey == "a" ||
                            uninstancedKey == "b" ||
                            uninstancedKey == "c" ||
                            uninstancedKey == "d" ||
                            uninstancedKey == "e" ||
                            uninstancedKey == "f") {
                            if (instance->floatValue != 1.f)
                                return false;
                        }
                    }
                    else {
                        return false; // something is wrong with parameter name
                    }
                }
            }
            return true;
    }
    return false;
}


SharedVarParInstance *VariationGroup::varparForVariationKey(const std::string &varKey, const std::string &paramName, size_t instanceNum)
{
    std::string paramKey = string_format("%s_%s#%lu", varKey.c_str(), paramName.c_str(), (unsigned long)instanceNum);
    return findVarpar(paramKey);
}

SharedVarParInstance &VariationGroup::varparRefForVariationKey(const std::string &varKey, const std::string &paramName, size_t instanceNum)
{
    std::string paramKey = string_format("%s_%s#%lu", varKey.c_str(), paramName.c_str(), (unsigned long)instanceNum);
    return operator[](paramKey);
}

float VariationGroup::floatValueForVariationKey(const std::string &varKey, const std::string &paramName, size_t instanceNum)
{
    SharedVarParInstance * instance = varparForVariationKey(varKey, paramName, instanceNum);
    if (instance)
        return (*instance)->floatValue;
    return 0.f;
}

void VariationGroup::setFloatValueForVariationKey(const std::string &varKey, const std::string &paramName, size_t instanceNum, float floatValue)
{
    SharedVarParInstance * instance = varparForVariationKey(varKey, paramName, instanceNum);
    if (instance)
        (*instance)->floatValue = floatValue;
}

AffineMatrix VariationGroup::affineMatrixForVariationKey(std::string key)
{
    key                            = VarParInstance::variationMatrixPrefixForKey(key);
    SharedVarParInstance *instance = findVarpar(key);
    if (!instance)
        return AffineMatrix();
    
    size_t instanceNum = VarParInstance::instanceNumOfKey(key);
    std::string varKey = VarParInstance::uninstancedKey(key);
    
    return AffineMatrix(floatValueForVariationKey(varKey, "a", instanceNum),
                        floatValueForVariationKey(varKey, "b", instanceNum),
                        floatValueForVariationKey(varKey, "c", instanceNum),
                        floatValueForVariationKey(varKey, "d", instanceNum),
                        floatValueForVariationKey(varKey, "e", instanceNum),
                        floatValueForVariationKey(varKey, "f", instanceNum));
}

std::vector<SharedVarParInstance> VariationGroup::affineMatrixVarparsForVariationKey(std::string key)
{
    std::vector<SharedVarParInstance> array;
    array.reserve(7);
    
    key                            = VarParInstance::variationMatrixPrefixForKey(key);
    SharedVarParInstance *instance = findVarpar(key);
    if (!instance)
        return array;
    size_t instanceNum = VarParInstance::instanceNumOfKey(key);
    std::string varKey = VarParInstance::uninstancedKey(key);
    
    array.push_back(operator[](key));
    array.push_back(varparRefForVariationKey(varKey, "a", instanceNum));
    array.push_back(varparRefForVariationKey(varKey, "b", instanceNum));
    array.push_back(varparRefForVariationKey(varKey, "c", instanceNum));
    array.push_back(varparRefForVariationKey(varKey, "d", instanceNum));
    array.push_back(varparRefForVariationKey(varKey, "e", instanceNum));
    array.push_back(varparRefForVariationKey(varKey, "f", instanceNum));

    return array;
}

void VariationGroup::setNamedAffineMatrix(const NamedAffineMatrix &m, std::string key, const SharedVariationSet &variationSet)
{
    key                            = VarParInstance::variationMatrixPrefixForKey(key);
    SharedVarParInstance *instance = findVarpar(key);
    size_t instanceNum = VarParInstance::instanceNumOfKey(key);
    std::string varKey = VarParInstance::uninstancedKey(key);
    if (!instance) {
        addMatrix(m, varKey, instanceNum, variationSet);
    }
    else {
        setFloatValueForVariationKey(varKey, "a", instanceNum, m.a);
        setFloatValueForVariationKey(varKey, "b", instanceNum, m.b);
        setFloatValueForVariationKey(varKey, "c", instanceNum, m.c);
        setFloatValueForVariationKey(varKey, "d", instanceNum, m.d);
        setFloatValueForVariationKey(varKey, "e", instanceNum, m.e);
        setFloatValueForVariationKey(varKey, "f", instanceNum, m.f);
    }
}

NamedAffineMatrix VariationGroup::namedAffineMatrixForVariationKey(std::string key)
{
    key                            = VarParInstance::variationMatrixPrefixForKey(key);
    SharedVarParInstance *instance = findVarpar(key);
    if (!instance)
        return NamedAffineMatrix();
    
    size_t instanceNum = VarParInstance::instanceNumOfKey(key);
    std::string varKey = VarParInstance::uninstancedKey(key);
    
    NamedAffineMatrix m(floatValueForVariationKey(varKey, "a", instanceNum),
                       floatValueForVariationKey(varKey, "b", instanceNum),
                       floatValueForVariationKey(varKey, "c", instanceNum),
                       floatValueForVariationKey(varKey, "d", instanceNum),
                       floatValueForVariationKey(varKey, "e", instanceNum),
                       floatValueForVariationKey(varKey, "f", instanceNum));
    m.name = key + "_variationMatrix";
    m.groupIndex = (*instance)->groupIndex();
    return m;
}

void VariationGroup::addFloatValueInstance(AttrDict & attrs, std::vector<std::string> & vars, const char *key, size_t i, SharedVariationSet & variationSet)
{
    variationSet->addFloatValueInstance(std::strtof(vars[i].c_str(), nullptr),
                                        key,
                                        variationSet->xformIndexForKey(key),
                                        shared_from_this());
}

void VariationGroup::addFloatValueInstance(const char *key, SharedVariationSet & variationSet)
{
    variationSet->addFloatValueInstance(1.f, key, variationSet->xformIndexForKey(key), shared_from_this());
}

void VariationGroup::parseGroupAttrs(AttrDict & attrs, SharedVariationSet & variationSet)
{
    StringsDict keyInstances;
    keyInstances.reserve(100);
    
    // collect all instanced keys under their uninstanced baseKey
    for (std::string & key : ::allKeys(attrs)) {
        std::string baseKey      = VarParInstance::uninstancedAttributeKey(key);
        
        if (keyInstances.find(baseKey) == keyInstances.end()) {
            keyInstances[baseKey] = StringVector();
        }
        StringVector & instances = keyInstances[baseKey];
        instances.push_back(key);
    }

    for (auto & pair : variationSet->variationsByName) {
        const std::string & variationName = pair.first;
        if (keyInstances.find(variationName) == keyInstances.end())
            continue;
        StringVector & instances = keyInstances[variationName];
        
        SharedVariation &variation = variationSet->variationsByName[variationName];
        for (const std::string & instancedKey : instances) {
            size_t instanceNum = VarParInstance::instanceNumOfAttributeKey(instancedKey);
            bool amInstanced   = VarParInstance::amInstancedAttributeKey(instancedKey); // if uninstanced, the instanceNum will be 1
            
            variationSet->setFloatValue(FlameParse::attrAsFloat(attrs, instancedKey, 0.f),
                                        variation->key,
                                        instanceNum,
                                        variationSet->xformIndexForKey(variationName),
                                        shared_from_this());
        
            for (std::string & name :  keysSortedByValue(variation->parameters, VariationParameter::xformIndexPairCompare)) {
                SharedParameter & parameter   = variation->parameters[name];
                std::string instancedParamKey = VarParInstance::makeXmlAttributeKey(parameter->key, amInstanced ? instanceNum : std::string::npos);
                
                if (attrs.find(instancedParamKey) == attrs.end() && parameter->alias != nullptr) // if alias was used in the file
                    instancedParamKey = VarParInstance::makeInstancedKey(*parameter->alias, amInstanced ? instanceNum : std::string::npos);
                
                variationSet->setFloatValue(FlameParse::attrAsFloat(attrs, instancedParamKey, parameter->defaultValue),
                                            parameter->key,
                                            instanceNum,
                                            variationSet->xformIndexForKey(parameter->key),
                                            shared_from_this());
            }
        }
    }    
    // special handling for linear3D - an alias for linear
    if (keyInstances.find("linear3D") != keyInstances.end()) {
        StringVector & instances = keyInstances["linear3D"];
        for (const std::string instancedKey : instances) {
            size_t instanceNum = VarParInstance::instanceNumOfAttributeKey(instancedKey);
            variationSet->setFloatValue(FlameParse::attrAsFloat(attrs, instancedKey, 0.f),
                                        "linear",
                                        instanceNum,
                                        variationSet->xformIndexForKey("linear"),
                                        shared_from_this());
        }
    }
    if (keyInstances.find("flatten") != keyInstances.end()) {
        StringVector & instances = keyInstances["flatten"];
        for (const std::string instancedKey : instances) {
            size_t instanceNum = VarParInstance::instanceNumOfAttributeKey(instancedKey);
            variationSet->setFloatValue(FlameParse::attrAsFloat(attrs, instancedKey, 0.f),
                                        "post_flatten",
                                        instanceNum,
                                        variationSet->xformIndexForKey("post_flatten"),
                                        shared_from_this());
        }
    }
    
    // older deprecated way of setting variations
    if (attrs.find("var") != attrs.end()) {
        std::vector<std::string> vars = FlameParse::splitOnSpaces(attrs["var"].value());
        addFloatValueInstance(attrs, vars, "linear",       0, variationSet);
        addFloatValueInstance(attrs, vars, "sinusoidal",   1, variationSet);
        addFloatValueInstance(attrs, vars, "spherical",    2, variationSet);
        addFloatValueInstance(attrs, vars, "swirl",        3, variationSet);
        addFloatValueInstance(attrs, vars, "horseshoe",    4, variationSet);
        addFloatValueInstance(attrs, vars, "polar",        5, variationSet);
        addFloatValueInstance(attrs, vars, "handkerchief", 6, variationSet);
        addFloatValueInstance(attrs, vars, "heart",        7, variationSet);
        addFloatValueInstance(attrs, vars, "disc",         8, variationSet);
        addFloatValueInstance(attrs, vars, "spiral",       9, variationSet);
        addFloatValueInstance(attrs, vars, "hyperbolic",   10, variationSet);
        addFloatValueInstance(attrs, vars, "diamond",      11, variationSet);
        addFloatValueInstance(attrs, vars, "ex",           12, variationSet);
        addFloatValueInstance(attrs, vars, "julia",        13, variationSet);
        addFloatValueInstance(attrs, vars, "bent",         14, variationSet);
    }
    
    // another older deprecated way of setting variations
    if (attrs.find("var1") != attrs.end()) {
        int idx = FlameParse::attrAsInt(attrs, "var1");
        switch (idx) {
            case 0:
                addFloatValueInstance("linear", variationSet);
                break;
            case 1:
                addFloatValueInstance("sinusoidal", variationSet);
                break;
            case 2:
                addFloatValueInstance("spherical", variationSet);
                break;
            case 3:
                addFloatValueInstance("swirl", variationSet);
                break;
            case 4:
                addFloatValueInstance("horseshoe", variationSet);
                break;
            case 5:
                addFloatValueInstance("polar", variationSet);
                break;
            case 6:
                addFloatValueInstance("handkerchief", variationSet);
                break;
            case 7:
                addFloatValueInstance("heart", variationSet);
                break;
            case 8:
                addFloatValueInstance("disc", variationSet);
                break;
            case 9:
                addFloatValueInstance("spiral", variationSet);
                break;
            case 10:
                addFloatValueInstance("hyperbolic", variationSet);
                break;
            case 11:
                addFloatValueInstance("diamond", variationSet);
                break;
            case 12:
                addFloatValueInstance("ex", variationSet);
                break;
            case 13:
                addFloatValueInstance("julia", variationSet);
                break;
            case 14:
                addFloatValueInstance("bent", variationSet);
                break;
            default:
                break;
        }
    }
}

StringVector VariationGroup::allKeys()
{
    return ::allKeys(*dictionary);
}


