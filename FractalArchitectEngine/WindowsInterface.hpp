#pragma once

//
//  WindowsInterface.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 6/9/16.
//  Copyright � 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef WindowsInterface_h
#define WindowsInterface_h

#include <string>
#include <vector>

#include "Exports.hpp"

using uchar = unsigned char;
using Blob = std::vector<uchar>;

FAENGINE_API void setupFAEngineApplicationSupport();
FAENGINE_API const char *expandTildeInPath(const char *_path);

FAENGINE_API const std::string loadEmbeddedResource(const char * basename, const char * _extension);
FAENGINE_API const Blob loadEmbeddedProfile(const char * name);

FAENGINE_API std::string getExecutablePath();
FAENGINE_API std::string getThisDLLPath();

FAENGINE_API std::string pathForResource(const char * _basename, const char * _extension);
FAENGINE_API std::string pathForTestResource(const char *_bundlePath, const char *filename);
FAENGINE_API std::string testDataPath();

FAENGINE_API double osVersion();

FAENGINE_API void empty_dir(const char *path);
FAENGINE_API void remove_dir(const char *path);

FAENGINE_API std::vector<std::string> directoryFileNamesContents(const char *path);
FAENGINE_API std::vector<std::string> directorySubDirContents(const char *path);

FAENGINE_API std::string md5DigestString(const std::string & str);

FAENGINE_API std::string cachePath();
FAENGINE_API std::string variationSetsDirectoryPath();
FAENGINE_API std::string applicationSupportPath();


#endif /* WindowsInterface_h */
