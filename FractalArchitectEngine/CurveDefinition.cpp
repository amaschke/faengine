//
//  CurveDefinition.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/6/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "CurveDefinition.hpp"

template <class T, enum CurveType _curveType>
CurveDefinition<T, _curveType>::CurveDefinition(float _a, float _b, bool _flipped, bool _mirrored, bool _integralA)
: controlPoints(), a(_a), b(_b), flipped(_flipped), mirrored(_mirrored), integralA(_integralA)
{
    setupCurveMode();
}

template <class T, enum CurveType _curveType>
CurveDefinition<T, _curveType>::CurveDefinition(const CurveDefinition &c)
: controlPoints(c.controlPoints), a(c.a), b(c.b), flipped(c.flipped), mirrored(c.mirrored), integralA(c.integralA)
{
    setupCurveMode();
}

template <class T, enum CurveType _curveType>
CurveMode CurveDefinition<T, _curveType>::setupCurveMode()
{
    if (flipped & mirrored)
        return curveMode = eFlippedMirrored;
    else if (flipped)
        return curveMode = eFlipped;
    else if (mirrored)
        return curveMode = eMirrored;
    return curveMode = eNormal;
}

template <class T, enum CurveType _curveType>
std::shared_ptr<std::vector<T>> CurveDefinition<T, _curveType>::controlPointsData()
{
    size_t length                = controlPoints.size();
    std::shared_ptr<std::vector<T>> controlPointsData = std::make_shared<std::vector<T>>(length);
    T *_controlPoints            = (T *)controlPointsData->data();
    
    for (size_t i = 0; i < controlPoints.size(); i++)
        _controlPoints[i] = controlPoints[i];
    return controlPointsData;
}

template <class T, enum CurveType _curveType>
void CurveDefinition<T, _curveType>::setControlPointsData(const std::shared_ptr<std::vector<T>> & controlPointsData)
{
    
    size_t cpCount    = controlPointsData->size();
    T *_controlPoints = (T *)controlPointsData->data();
    
    controlPoints.clear();
    
    for (size_t i = 0; i < cpCount; i++) {
        T point = _controlPoints[i];
        controlPoints.push_back(point);
    }
}

template class CurveDefinition<Point, eNaturalCubic>;

