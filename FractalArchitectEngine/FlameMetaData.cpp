//
//  FlameMetaData.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/6/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "FlameMetaData.hpp"
#include "common.hpp"

FlameMetaData::FlameMetaData(const std::string &_uuid, const std::string &_variationSetUuid)
  :
    name(), url(), nick(), notes(), editXML(), uuid(_uuid), variationSetUuid(_variationSetUuid),
    rgbCurve(), redCurve(), greenCurve(), blueCurve(), applyToBackground(false), qualityAdjust(1.f),
    backgroundImage(), renderMode(renderModeNormal), compositeColor(), imagePath()

{
    if (_uuid.length() == 0)
        uuid = stringWithUUID();
}

FlameMetaData::FlameMetaData(FlameMetaData &o)
    :
    name(o.name),
    url(o.url),
    nick(o.nick),
    notes(o.notes),
    editXML(o.editXML),
    uuid(o.uuid),
    variationSetUuid(o.variationSetUuid),
    rgbCurve(o.rgbCurve),
    redCurve(o.redCurve),
    greenCurve(o.greenCurve),
    blueCurve(o.blueCurve),
    applyToBackground(o.applyToBackground),
    qualityAdjust(o.qualityAdjust),
    backgroundImage(std::move(o.backgroundImage)),
    renderMode(o.renderMode),
    compositeColor(o.compositeColor),
    imagePath(o.imagePath)
{
}

void FlameMetaData::setVariationSetUuid(const std::string &_variationSetUuid)
{
    variationSetUuid = _variationSetUuid;
}

void FlameMetaData::setUuid(const std::string &_uuid)
{
    if (_uuid.length() == 0)
        uuid = stringWithUUID();
    else
        uuid = _uuid;
}
