//
//  CocoaInterface.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/10/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef CocoaInterface_h
#define CocoaInterface_h

#include <string>
#include <vector>

extern "C" bool cachedLibraryExists();
extern "C" void cacheFactoryLibrary();
extern "C" void cacheMyLibrary();
extern "C" void setupFAEngineApplicationSupport();

std::string cachePath();
std::string variationSetsDirectoryPath();

extern "C" const char *expandTildeInPath(const char *_path);

std::string pathForTestResource(const char *_bundlePath, const char *filename);
std::string pathForResource(const char * _basename, const char * _extension);
std::string pathForPossibleResource(std::string _path);   // check and see if its an embedded resource first, otherwise just use the original input path

extern "C" double osVersion();

extern "C" void empty_dir(const char *path);
extern "C" void remove_dir(const char *path);

std::vector<std::string> directoryFileNamesContents(const char *path);
std::vector<std::string> directorySubDirContents(const char *path);

std::string md5DigestString(const std::string & str);

#endif /* CocoaInterface_h */
