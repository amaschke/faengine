//
//  RenderInfo.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/24/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef RenderInfo_hpp
#define RenderInfo_hpp

#include <memory>
#include <vector>
#include <string>
#include "Exports.hpp"
#include "Enums.hpp"

#ifdef __linux__
typedef u_int32_t uint32;
#endif

class ElectricSheepStuff;
class FlameExpanded;
class VariationSet;

using uchar               = unsigned char;
using uint                = unsigned int;
using SharedVariationSet  = std::shared_ptr<VariationSet>;
using SharedElc           = std::shared_ptr<ElectricSheepStuff>;
using SharedFE            = std::shared_ptr<FlameExpanded>;
using BladesVector        = std::vector<SharedFE>;
using SharedBlades        = std::shared_ptr<BladesVector>;

struct FAENGINE_API RenderInfo : public std::enable_shared_from_this<RenderInfo> {
    
     RenderInfo();
    
    // ============== Members =======================
    SharedBlades            blades;
    SharedElc               elcData;
    int                     width;
    int                     height;
    uint                    supersample;
    bool                    useAlpha;
    bool                    flipped;
    uint                    bitDepth;
    std::string             pixelFormat;
    std::vector<uchar>      profileData;
    RenderingIntent         renderingIntent;
    float                   epsilon;
    float                   quality;
    bool                    verbose;
    std::string             imageURL;
    bool                    saveRenderState;
    std::string             uuid;
    uint                    fuseIterations;
    bool                    overrideFuseIterations;
    uint                    frameNumber;
};
#endif /* RenderInfo_hpp */
