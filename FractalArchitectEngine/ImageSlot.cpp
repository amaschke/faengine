//
//  ImageSlot.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 6/5/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "ImageSlot.hpp"

ImageSlot::ImageSlot(SharedImage & _image,
                     int _index,
                     struct rgba _color,
                     std::string & _uuid,
                     float _fusedFraction,
                     float _retainedFraction,
                     size_t _videoFrame,
                     bool _haveVideoFrame)
:
    image(_image),
    index(_index),
    color(_color),
    uuid(_uuid),
    fusedFraction(_fusedFraction),
    retainedFraction(_retainedFraction),
    videoFrame(_videoFrame),
    haveVideoFrame(_haveVideoFrame)
{}
