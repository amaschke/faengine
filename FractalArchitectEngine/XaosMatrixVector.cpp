/*
 *  XaosMatrix.cpp
 *  FractalArchitect
 *
 *  Created by Steven Brodhead on 2/23/11.
 *  Copyright 2011 Centcom Inc. All rights reserved.
 *
 */

//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "XaosMatrixVector.hpp"
#include <assert.h>
#include "XaosMatrix.hpp"


Xaos::Xaos()
{
    weight = 1.f;
    locked = false;
}

Xaos & Xaos::operator=(const Xaos &other)
{
    weight = other.weight;
    locked = other.locked;
    return *this;
}

XaosVector::XaosVector()
{
    num      = 0;
    toWeight = nullptr;
}

XaosVector::~XaosVector()
{
    if (toWeight){
        delete toWeight;
        toWeight = nullptr;
    }
}


XaosForXformsVector::XaosForXformsVector()
{
    num  = 0;
    from = nullptr;
}

// create a unity xaos matrix for dimension num
XaosForXformsVector::XaosForXformsVector(size_t _num)
{
    assert(_num < 100);
    this->num  = _num;
    if (num == 0)
        from = nullptr;
    else
        from       = new struct XaosVector[num];
    for (size_t i = 0; i < num; i++) {
        from[i].num = num;
        from[i].toWeight = new Xaos[num];
        for (size_t j = 0; j < num; j++) {
            from[i].toWeight[j].weight = 1.f;
            from[i].toWeight[j].locked = false;
        }
    }
}

// append new unity matrix rows - or reduce the dimension of the xaos matrix
XaosForXformsVector::XaosForXformsVector(size_t num, XaosForXformsVector *old)
{
    assert(num < 100);
    assert(num != old->num);
    this->num  = num;
    from       = new struct XaosVector[num];
    
    if (num > old->num) {
        for (size_t i = 0; i < old->num; i++) {
            from[i].num = num;
            from[i].toWeight = new Xaos[num];
            for (size_t j = 0; j < old->num; j++)
                from[i].toWeight[j] = old->from[i].toWeight[j];
            for (size_t j = old->num; j < num; j++) {
                from[i].toWeight[j].weight = 1.f;
                from[i].toWeight[j].locked = false;
            }
        }
        for (size_t i = old->num; i < num; i++) {
            from[i].num = num;
            from[i].toWeight = new Xaos[num];
            for (size_t j = 0; j < num; j++) {
                from[i].toWeight[j].weight = 1.f;
                from[i].toWeight[j].locked = false;
            }
        }
    }
    else {
        for (size_t i = 0; i < num; i++) {
            from[i].num = num;
            from[i].toWeight = new Xaos[num];
            for (size_t j = 0; j < num; j++) {
                from[i].toWeight[j] = old->from[i].toWeight[j];
            }
        }
    }
}

// create xaos matrix from brick
XaosForXformsVector::XaosForXformsVector(size_t num, size_t num2, void *brick)
{
    assert(num < 100);
    this->num = num;
    if (num == 0)
        from = nullptr;
    else
        from = new struct XaosVector[num];
    
    for (size_t i = 0; i < num; i++) {
        from[i].num = num;
        from[i].toWeight = new Xaos[num];
        for (size_t j = 0; j < num; j++)
            from[i].toWeight[j] = ((Xaos *)brick)[i * num + j];
    }
}

// delete a transform
XaosForXformsVector::XaosForXformsVector(size_t num, XaosForXformsVector *old, size_t deleteOldIndex)
{
    assert(num < 100);
    assert(num < old->num);
    
    this->num  = num;
    if (num == 0)
        from = nullptr;
    else
        from = new struct XaosVector[num];
    
    for (size_t i = 0; i < deleteOldIndex; i++) {
        from[i].num = num;
        from[i].toWeight = new Xaos[num];
        for (size_t j = 0; j < deleteOldIndex; j++)
            from[i].toWeight[j] = old->from[i].toWeight[j];
        
        for (size_t j = deleteOldIndex + 1; j < old->num; j++)
            from[i].toWeight[j-1] = old->from[i].toWeight[j];
    }
    for (size_t i = deleteOldIndex + 1; i < old->num; i++) {
        from[i-1].num = num;
        from[i-1].toWeight = new Xaos[num];
        for (size_t j = 0; j < deleteOldIndex; j++)
            from[i-1].toWeight[j] = old->from[i].toWeight[j];
        for (size_t j = deleteOldIndex + 1; j < old->num; j++)
            from[i-1].toWeight[j-1] = old->from[i].toWeight[j];
    }
}

// copy the xaos matix
XaosForXformsVector::XaosForXformsVector(const XaosForXformsVector &other)
{
    if (other.num == 0) {
        this->num = other.num;
        from = nullptr;
        return;
    }
    assert(other.num < 100);
    assert(other.from->num == other.num);
    this->num  = other.num;
    from       = new struct XaosVector[other.num];
    const XaosForXformsVector *old = &other;
    
    for (size_t i = 0; i < old->num; i++) {
        from[i].num = other.num;
        from[i].toWeight = new Xaos[other.num];
        for (size_t j = 0; j < old->num; j++)
            from[i].toWeight[j] = old->from[i].toWeight[j];
    }
}

// for use when splitting a transform
// split the xaos matrix at splitIndex - duplicate that xaos matrix's row
XaosForXformsVector::XaosForXformsVector(const XaosForXformsVector &old, size_t splitIndex)
{
    num  = old.num + 1;
    
    assert(num < 100);
    
    from = new struct XaosVector[num];
    
    for (size_t i = 0; i < num - 1; i++) {
        from[i].num = num;
        from[i].toWeight = new Xaos[num];
        for (size_t j = 0; j < num - 1; j++)
            from[i].toWeight[j] = old.from[i].toWeight[j];
        
        from[i].toWeight[num - 1] = old.from[i].toWeight[splitIndex];
    }
    // duplicate the vector from the original's splitIndex
    from[num - 1].num = num;
    from[num - 1].toWeight = new Xaos[num];
    for (size_t j = 0; j < num - 1; j++)
        from[num - 1].toWeight[j] = old.from[splitIndex].toWeight[j];
    
    from[num - 1].toWeight[num - 1] = old.from[splitIndex].toWeight[splitIndex];
}

// permute the xaos matrix with the permuted index
XaosForXformsVector::XaosForXformsVector(const XaosForXformsVector &other, unsigned *permutedIndex)
{
    if (other.num == 0) {
        this->num = other.num;
        from = nullptr;
        return;
    }
    assert(other.num < 100);
    assert(other.from->num == other.num);
    this->num  = other.num;
    from       = new struct XaosVector[other.num];
    const XaosForXformsVector *old = &other;
    
    for (size_t i = 0; i < old->num; i++) {
        from[i].num = other.num;
        from[i].toWeight = new Xaos[other.num];
        for (size_t j = 0; j < old->num; j++)
            from[i].toWeight[j] = old->from[permutedIndex[i]].toWeight[permutedIndex[j]];
    }
}

// create xaos matrix from brick
XaosForXformsVector::XaosForXformsVector(size_t num, void *brick)
{
    assert(num < 100);
    this->num = num;
    if (num == 0)
        from = nullptr;
    else
        from = new struct XaosVector[num];
    
    for (size_t i = 0; i < num; i++) {
        from[i].num = num;
        from[i].toWeight = new Xaos[num];
        for (size_t j = 0; j < num; j++)
            from[i].toWeight[j] = ((Xaos *)brick)[i * num + j];
    }
}


XaosForXformsVector::~XaosForXformsVector()
{
    if (from) {
        delete [] from;
        from = nullptr;
    }
}

void XaosForXformsVector::deleteChildren()
{
    if (from) {
        delete [] from;
        from = nullptr;
    }
}

float XaosForXformsVector::chaos(size_t _from, size_t to)
{
    XaosVector &fromVector = from[_from];
    
    assert(num < 100);
    assert(_from < num && to < fromVector.num);
    
    if (_from >= num || to >= fromVector.num) {
        return 1.f;
    }
    
    return fromVector.toWeight[to].weight;
}

bool XaosForXformsVector::locked(size_t _from, size_t to)
{
    XaosVector &fromVector = from[_from];
    
    assert(num < 100);
    assert(_from < num && to < fromVector.num);
    
    if (_from >= num || to >= fromVector.num) {
        return false;
    }
    
    return fromVector.toWeight[to].locked;
}

void XaosForXformsVector::setChaos(size_t _from, size_t to, float newChaos, bool locked)
{
    XaosVector &fromVector = from[_from];
    
    assert(num < 100);
    assert(_from < num && to < fromVector.num);
    
    fromVector.toWeight[to].weight = newChaos;
    fromVector.toWeight[to].locked = locked;
}

void XaosForXformsVector::setLocked(size_t _from, size_t to, bool locked)
{
    XaosVector &fromVector = from[_from];
    
    assert(num < 100);
    assert(_from < num && to < fromVector.num);
    
    fromVector.toWeight[to].locked = locked;
}

Xaos *XaosForXformsVector::brick()
{
    assert(num < 100);
    
    Xaos *brick = new Xaos[num * num];
    for (size_t i = 0; i < num; i++) {
        for (size_t j = 0; j < num; j++) {
            brick[i * num + j] = from[i].toWeight[j];
        }
    }
    return brick;
}

float *XaosForXformsVector::weightsBrick()
{
    assert(num < 100);
    
    float *brick = new float[num * num];
    for (size_t i = 0; i < num; i++) {
        for (size_t j = 0; j < num; j++) {
            brick[i * num + j] = from[i].toWeight[j].weight;
        }
    }
    return brick;
}

Xaos *XaosForXformsVector::brick2(size_t count)
{
    assert(num == count && num == from->num);
    Xaos *brick = new Xaos[count * count];
    for (size_t i = 0; i < count; i++) {
        for (size_t j = 0; j < count; j++) {
            brick[i * count + j] = from[i].toWeight[j];
        }
    }
    return brick;
}

bool XaosForXformsVector::usesXaos()
{
    for (size_t i = 0; i < num; i++) {
        for (size_t j = 0; j < num; j++) {
            if (from[i].toWeight[j].weight != 1.f)
                return true;
        }
    }
    return false;
}

// append new rows
bool XaosForXformsVector::equivalent(XaosForXformsVector *other)
{
    if (num != other->num)
        return false;
    
    for (size_t i = 0; i < num; i++) {
        for (size_t j = 0; j < num; j++) {
            if (from[i].toWeight[j].weight != other->from[i].toWeight[j].weight)
                return false;
        }
    }
    return true;
}


