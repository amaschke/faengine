//
//  RenderQueue.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 6/5/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "RenderListener.hpp"
#include "RenderQueue.hpp"
#include "RenderParams.hpp"
#include "ClContext.hpp"
#include "DeviceRuntime.hpp"
#include "RenderTile.hpp"
#include "ImageSlot.hpp"
#include "FlameExpanded.hpp"

#ifdef _WIN32
#include <Windows.h>
#endif

#ifdef __linux
namespace std {
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
}
#endif

void RenderQueue::f(RenderQueue *_this)
{
    for (;;) {
        std::unique_lock<std::mutex> lock(_this->mutex);
        _this->condition.wait(lock, [&]{ return _this->stop || !_this->queue.empty(); });
        if(_this->stop || _this->queue.empty())
            return;
        
        SharedParams request = _this->queue.front();
        _this->queue.pop();
        
        if (request->signalQueueStop)
            _this->stop = true;
        
        lock.unlock();
        
        // process request
        if (! _this->stop)
            _this->handleRequest(request);
    }
}

void RenderQueue::enqueueStop(SharedDeviceContext deviceContext)
{
    SharedParams params = std::make_shared<RenderParams>(deviceContext);
    
    params->signalQueueStop = true;
    
    enqueue(params);
    
    if (worker.joinable())
        worker.join();
}

RenderQueue::RenderQueue()
: stop(false), queue(), mutex(), condition()//, worker(std::thread(f, this))
{
	worker = std::thread(f, this);
}

// the destructor joins all threads
RenderQueue::~RenderQueue()
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stop = true;
    }
    condition.notify_all();
    if (worker.joinable())
        worker.join();
}


void RenderQueue::enqueue(SharedParams request)
{
    std::unique_lock<std::mutex> lock(mutex);
    queue.emplace(request);
    condition.notify_one();
} // unlock mutex happens here

void RenderQueue::calcRenderTime(SharedParams & request,
                                 std::string filename,
                                 duration<double> _duration,
                                 duration<double> deDuration,
                                 bool hasFinalXfom,
                                 float fusedFraction,
                                 float retainedFraction)
{
    SharedFlameExpanded & firstFe = request->blades->operator[](0);

    uint perPointFuseIterations =
    request->deviceContext->maxFuseIterationsFromSelectedDevices(hasFinalXfom);
    
    size_t blocksY;
    size_t numBatches =
    DeviceRuntime::batchesPerTileForDesiredAdaptive(request->quality,
                                                    request->size.width,
                                                    request->size.height,
                                                    1,
                                                    blocksY,
                                                    perPointFuseIterations);
    request->listener->renderStats(filename,
                                  _duration,
                                   deDuration,
                                  request->size.width,
                                  request->size.height,
                                  request->quality,
                                  numBatches,
                                  "",
                                  fusedFraction,
                                  retainedFraction,
                                   firstFe->getFlame()->params.oversample);
}

void RenderQueue::handleRequest(SharedParams request)
{
    if (request->blades->size() == 0)
        return;
    
    time_point<high_resolution_clock> batchStartTime;
    time_point<high_resolution_clock> now = high_resolution_clock::now();
    duration<double> _duration(0.);
    duration<double> deDuration(0.);
    
    if (request->batchStart) {
        if (request->listener)
            request->listener->beforeRender();
        batchStartTime = high_resolution_clock::now();
    }
    SharedFlameExpanded &fe = request->blades->operator[](0);
    float fusedFraction    = 1.f;
    float retainedFraction = 1.f;
	SharedImage image = RenderTile::doRender(*request, fusedFraction, retainedFraction, _duration, deDuration);
    
    if (! image) {
        return;
    }
    
    if (std::dynamic_pointer_cast<ThumbnailItemRenderParams>(request)) {
        auto tirp = std::dynamic_pointer_cast<ThumbnailItemRenderParams>(request);
        
        SharedImageSlot imageSlot = std::make_shared<ImageSlot>(image,
                                                                (uint)tirp->index,
                                                                tirp->background,
                                                                tirp->uuid,
                                                                fusedFraction,
                                                                retainedFraction,
                                                                tirp->videoFrame,
                                                                tirp->haveVideoFrame);
        if (request->batchEnd) {
            if (request->listener)
                request->listener->renderDone();
            batchStartTime = now;
        }
        if (request->listener)
            request->listener->submitImage(imageSlot);
    }
    else if (std::dynamic_pointer_cast<FileOutputRenderParams>(request)) {
        auto forp = std::dynamic_pointer_cast<FileOutputRenderParams>(request);
        
        if (request->viewListener)
            request->viewListener->setImage(image, forp->outURL, forp->hasAlpha, forp->size, _duration);
        
        calcRenderTime(request, fileNameFromPath(forp->outURL), _duration, deDuration, fe->getFlame()->params.numFinal > 0, fusedFraction, retainedFraction);
    }
    else if (std::dynamic_pointer_cast<FileBatchRenderParams>(request)) {
        auto fbrp = std::dynamic_pointer_cast<FileBatchRenderParams>(request);
        
        std::string fileURL = fbrp->directory + "/" + fbrp->filename;
        
        if (request->viewListener)
            request->viewListener->setImage(image, fileURL, request->hasAlpha, request->size, _duration);
        
        if (fbrp->batchEnd && request->listener)
            request->listener->imageBatchTime(duration_cast<duration<double>>(high_resolution_clock::now() - batchStartTime));
    }
    else {
        // (UniqueImage _image, float _fusedFraction, float _retainedFraction, double _renderTime, enum InitialColorSpace colorSpace)
        UniqueRender result = std::make_unique<RenderResult>(std::move(image), fusedFraction, retainedFraction, _duration, request->eColorSpace);
        
        request->viewListener->setRenderResult(std::move(result));
        
        calcRenderTime(request, fileNameFromPath(request->url), _duration, deDuration, fe->getFlame()->params.numFinal > 0, fusedFraction, retainedFraction);
    }
    
    if (request->batchEnd && request->listener)
        request->listener->renderDone();
}
