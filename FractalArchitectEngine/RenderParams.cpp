//
//  RenderParams.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/24/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "RenderParams.hpp"
#include "DeviceContext.hpp"
#include "Enums.hpp"

class CudaContext;

using SharedCudaContext    = std::shared_ptr<CudaContext>;

#define DEFAULT_EPSILON  1.e-7f

RenderParams::RenderParams(SharedDeviceContext & _deviceContext)
:   listener(nullptr),
    viewListener(nullptr),
    url(""),
    batchStart(false),
    batchEnd(false),
    flipped(false),
    frameNum(0),
    earlyClipMode(false),
    pixelFormat("RGBA"),
    backgroundImageRep(nullptr),
    blades(),
    usePriorFraction(noPrior),
    overrideFuseIterations(false),
    gpuPlatform(useOpenCL),
    favorSplitMerge(true),
    epsilon(DEFAULT_EPSILON),
    deviceContext(_deviceContext),
    selectedDeviceNum(0)
{
    if (std::dynamic_pointer_cast<SharedCudaContext>(deviceContext)) {
        gpuPlatform = useCUDA;
    }
}

RenderParams::~RenderParams()
{
    delete backgroundImageRep;
}

ThumbnailItemRenderParams::ThumbnailItemRenderParams(SharedDeviceContext & _deviceContext)
: RenderParams(_deviceContext), uuid(), videoFrame(0), haveVideoFrame(false)
{
    background.r = 0.f;
    background.g = 0.f;
    background.b = 0.f;
    background.a = 1.f;
}

FileOutputRenderParams::FileOutputRenderParams(SharedDeviceContext & _deviceContext)
:
    RenderParams(_deviceContext),
    outURL(),
    renderingIntent(RenderingIntentSaturation),
    saveRenderState(false),
    maxTileMemory(0LL),
    uuid(),
    continuousMode(false),
    fusedFraction(1.f),
    retainedFraction(1.f)
{}

FileBatchRenderParams::FileBatchRenderParams(SharedDeviceContext & _deviceContext)
: RenderParams(_deviceContext), directory(), filename()
{}

std::string RenderParams::colorSpaceNameFor(enum InitialColorSpace colorSpace)
{
    switch (colorSpace) {
        case sRGB:
            return "sRGB";
            break;
        case AdobeRGB:
            return "AdobeRGB";
            break;
        case WideGamutRGB:
            return "WideGamutRGB";
            break;
        case ProPhotoRGB:
            return "ProPhoto";
            break;
        default:
            return "GenericRGB";
            break;
    }
}

enum InitialColorSpace RenderParams::colorSpaceFor(std::string colorSpaceName)
{
    if (colorSpaceName == "sRGB")
        return sRGB;
    if (colorSpaceName == "AdobeRGB")
        return AdobeRGB;
    if (colorSpaceName == "WideGamutRGB")
        return WideGamutRGB;
    if (colorSpaceName == "ProPhoto")
        return ProPhotoRGB;
    else
        return GenericRGB;
}
