//
//  RenderParams.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/24/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef RenderParams_hpp
#define RenderParams_hpp

#include "Exports.hpp"
#include "RenderListener.hpp"
#include "Enums.hpp"
#include "common.hpp"
#include "ElectricSheepStuff.hpp"
#include "RenderEnums.hpp"
#include "Image.hpp"

#include <string>
#include <memory>
#include <vector>

class DeviceContext;
class FlameExpanded;
class Listener;
class ViewListener;

using SharedFlameExpanded = std::shared_ptr<FlameExpanded>;
using BladesVector        = std::vector<SharedFlameExpanded>;
using SharedBlades        = std::shared_ptr<BladesVector>;
using SharedDeviceContext = std::shared_ptr<DeviceContext>;

struct FAENGINE_API RenderParams {
    RenderParams(SharedDeviceContext & _deviceContext);
    virtual ~RenderParams();
    
    static enum InitialColorSpace colorSpaceFor(std::string colorSpaceName);
    static std::string            colorSpaceNameFor(enum InitialColorSpace colorSpace);
    
    // ==================  Members =======================
    
    bool                       signalQueueStop = false; // when true, tells the queue to stop
    RenderListener            *listener; // placeholder for listeners that need to be notified of render completion
    ViewListener              *viewListener;
    FA::Size                   size;
    int                        quality;
    uint                       bitDepth;
    bool                       hasAlpha;
    enum InitialColorSpace     eColorSpace;
    SharedBlades               blades;
    std::string                url;
    bool                       batchStart;
    bool                       batchEnd;
    size_t                     frameNum;
    bool                       flipped;
    bool                       earlyClipMode;
    std::string                pixelFormat;
    FA::Image                 *backgroundImageRep;
    enum PriorUse              usePriorFraction;
    bool                       overrideFuseIterations;
    uint                       cpuFuseIterations;
    uint                       gpuFuseIterations;
    enum GpuPlatformUsed       gpuPlatform;
    bool                       favorSplitMerge; // for multiple GPU renders, do we want to do alternate single GPU renders by frame number
                                                // or do we want to want to split the render batches and merge into the final result
    float                      epsilon;
    SharedDeviceContext        deviceContext;
    uint                       selectedDeviceNum;
};

// params for rendering a single Thumbnail with thumbnail routing info
struct FAENGINE_API ThumbnailItemRenderParams : public RenderParams
{
    ThumbnailItemRenderParams(SharedDeviceContext & _deviceContext);
    
    size_t      index;
    std::string uuid;
    struct rgba background;
    bool        haveVideoFrame;
    size_t      videoFrame;
};

// params for rendering an image to a file
struct FAENGINE_API FileOutputRenderParams : public RenderParams
{
    FileOutputRenderParams(SharedDeviceContext & _deviceContext);
    
    std::string        outURL;
    RenderingIntent    renderingIntent;
    bool               saveRenderState;
    size_t             maxTileMemory;
    std::string        uuid;
    bool               continuousMode;
    float              fusedFraction;
    float              retainedFraction;
};

// params for rendering a single image in a batch of images
struct FAENGINE_API FileBatchRenderParams : public RenderParams
{
    FileBatchRenderParams(SharedDeviceContext & _deviceContext);
    
    std::string directory; // output directory for the entire batch
    std::string filename;  // specific filename for this specific image in the batch
};

#endif /* RenderParams_hpp */
