//
//  RenderTile.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/25/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "RenderTile.hpp"
#include "DeviceRuntime.hpp"
#include "Flame.hpp"
#include "Enums.hpp"
#include "ColorNode.hpp"
#include "ColorGradient.hpp"
#include "FlameTile.hpp"
#include "FlameTileArray.hpp"
#include "ImageRepTileArray.hpp"
#include "Utilities.hpp"
#include "RenderEnums.hpp"
#include "RenderState.hpp"
#include "RenderInfo.hpp"
#include "FlameExpanded.hpp"
#include "NaturalCubicCurve.hpp"
#include "VariationSet.hpp"
#include "DeviceKind.hpp"
#include "RenderGeometry.hpp"
#include "ClContext.hpp"
#include "RenderParams.hpp"
#include "DeviceProgram.hpp"
#include "Flam4RenderDispatcher.hpp"
#include "rand.hpp"

#include <fstream>
#include <iostream>
#include <atomic>

#ifdef _WIN32
#include <Windows.h>
#include "WindowsInterface.hpp"
#else
#include "CocoaInterface.h"
#endif


#if defined(OPENCL_ONLY)
#include "Flam4ClRuntime.hpp"

#elif defined(CUDA_ONLY)
#include "Flam4CudaRuntime.hpp"

#else
#include "Flam4ClRuntime.hpp"

#ifndef NO_CUDA
#include "Flam4CudaRuntime.hpp"
#endif
#endif

#ifdef __linux
namespace std {
    template<typename T, typename ...Args>
    std::unique_ptr<T> make_unique( Args&& ...args )
    {
        return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
    }
}
#endif

using std::string;

using uchar = unsigned char;

static char buf[1024];

std::vector<uchar> readBlobFromFile(std::string filename)
{
    // open the file:
    std::basic_ifstream<char> file(filename, std::ios::binary);
    
    // read the data:
    return std::vector<uchar>((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
}

#ifdef __APPLE__
const std::vector<uchar> RenderTile::getProfileFor(const string & choice)
{
    if (choice == "sRGB") {
        return readBlobFromFile("/System/Library/ColorSync/Profiles/sRGB Profile.icc");
    }
    else if (choice == "AdobeRGB") {
        return readBlobFromFile("/System/Library/ColorSync/Profiles/AdobeRGB1998.icc");
    }
    else if (choice == "WideGamutRGB") {
        return readBlobFromFile(pathForResource("WideGamutRGB", "icc"));
    }
    else if (choice == "ProPhoto") {
        return readBlobFromFile(pathForResource("ProPhoto", "icm"));
    }
    else {
        return readBlobFromFile("/System/Library/ColorSync/Profiles/Generic RGB Profile.icc");
    }
}

#elif defined(_WIN32)
const std::vector<uchar> RenderTile::getProfileFor(const string & choice)
{
	if (choice == "sRGB") {
		return loadEmbeddedProfile("sRGB.icc");
	}
	else if (choice == "AdobeRGB") {
		return loadEmbeddedProfile("AdobeRGB1998.icc");
	}
	else if (choice == "WideGamutRGB") {
		return loadEmbeddedProfile("WideGamutRGB.icc");
	}
	else {
		return loadEmbeddedProfile("ProPhoto.icc");
	}
}

#else
const std::vector<uchar> RenderTile::getProfileFor(const string & choice)
{
    if (choice == "sRGB") {
        return readBlobFromFile("/usr/share/color/colord/sRGB.icc");
    }
    else if (choice == "AdobeRGB") {
        return readBlobFromFile("/usr/share/color/colord/AdobeRGB1998.icc");
    }
    else if (choice == "WideGamutRGB") {
        return readBlobFromFile(pathForResource("WideGamutRGB", "icc"));
    }
    else {
        return readBlobFromFile(pathForResource("/usr/share/color/colord/ProPhotoRGB", "icc"));
    }
}
#endif

void RenderTile::normalizeXformWeights(Flame *flame)
{
    // normalize the weights and put them into intervals on [0,1]
    float sum = 0.0f;
    for (int n = 0; n < flame->params.numTrans; n++)
    {
        sum+=flame->trans[n].weight;
    }
    float sum2 = 0.0f;
    for (int n = 0; n < flame->params.numTrans; n++)
    {
        sum2 += flame->trans[n].weight/sum;
        flame->trans[n].weight = sum2;
    }
}

std::vector<ColorNode> RenderTile::loadColorNodesFromFlame(struct Flame *flame)
{
    std::vector<ColorNode> nodes;

    for (int i = 0; i < flame->numColors; i++) {
        Color color(flame->colorIndex[i]);
        float location  = flame->colorLocations[i];
        
        nodes.emplace_back(color, location);
    }
    return nodes;
}

void RenderTile::saveExpandedGradientToFlame(struct Flame *flame, enum PaletteMode paletteMode)
{
    int paletteSize = 256;
    if (flame->numColors == paletteSize)
        return;
    
    ColorGradient gradient(loadColorNodesFromFlame(flame), paletteMode);

	if (flame->pageAligned) {
		ALIGNED_FREE(flame->colorIndex);
		ALIGNED_FREE(flame->colorLocations);

		ALIGNED_MALLOC(struct rgba, flame->colorIndex, flame->alignment, sizeof(struct rgba)*paletteSize);
		ALIGNED_MALLOC(float, flame->colorLocations, flame->alignment, sizeof(float)*paletteSize);
	}
	else {
		delete[] flame->colorIndex;
		delete[] flame->colorLocations;
		flame->colorIndex = new rgba[paletteSize];
		flame->colorLocations = new float[paletteSize];
	}

    flame->numColors = paletteSize;
    
    for (int i = 0; i < flame->numColors; i++) {
        Color newColor = gradient.interpolatedColorAtLocation((float)i/(flame->numColors - 1));
        
        flame->colorIndex[i] = newColor;
        flame->colorLocations[i] = (float)i/(flame->numColors - 1);
    }
}

void RenderTile::logRenderState(string & prefix, FlameTileArray *tiles, ImageRepTileArray *imageTiles, int tileIndex, int progressForTile)
{
    size_t numBatches      = 0;
    size_t batchesRendered = 0;
    
    string s;
    
    for (uint i = 0; i < tiles->size(); i++) {
        size_t _numBatches      = tiles->numBatchesAtIndex(i);
        size_t _batchesRendered = imageTiles->batchesRenderedAtIndex(i);
        numBatches               += _numBatches;
        
        if (i == tileIndex && progressForTile > 0) { // rendering this tile now
            batchesRendered += progressForTile;
        }
        else {
            batchesRendered += _batchesRendered;
        }
        snprintf(buf, sizeof(buf), "Tile %u:  batchesRendered:%zu numBatches:%zu\n", i, _batchesRendered, _numBatches);
        s.append(buf);
    }
    snprintf(buf, sizeof(buf), "%s\nTotal: batchesRendered:%zu numBatches:%zu\n%s",
             prefix.c_str(), batchesRendered, numBatches, s.c_str());
    systemLog(buf);
}

size_t RenderTile::cumulativeProgress(ImageRepTileArray *imageTiles, size_t tileIndex, size_t progressForTile)
{
    int batches = 0;
    for (int i = 0; i < imageTiles->size(); i++) {
        if (i != tileIndex || progressForTile == 0)
            batches += imageTiles->batchesRenderedAtIndex(i);
    }
    return batches + progressForTile;
}

unsigned RenderTile::maxProgress(FlameTileArray *tiles)
{
    int batches = 0;
    for (int i = 0; i < tiles->size(); i++) {
		SharedFlameTile & tile = tiles->tileAtIndex(i);
        batches += tile->numBatches;
    }
    return batches;
}

bool RenderTile::checkProgress(SharedRenderState renderState,
                               std::string & variationSetUuid,
                               time_point<high_resolution_clock> now,
                               bool lastBlade,
                               bool lastSubSample,
                               size_t  bladeIndex,
                               size_t bladeBatches,
                               uint  subSample)
{
    return true;
}

ProceedType RenderTile::checkForStop(SharedRenderState renderState,
                                     std::string & variationSetUuid,
                                     bool lastBlade,
                                     bool lastSubSample)
{
    return proceedOK;
}

size_t RenderTile::renderedBatchesSoFar(SharedRenderState renderState,
                                      size_t subSampleBegin,
                                      size_t bladeIndexBegin)
{
    size_t renderedBatches = 0;
    
    for (size_t bladeIndex = 0; bladeIndex < renderState->renderInfo->blades->size(); bladeIndex++) {
        SharedFlameExpanded & fe = renderState->tiles->tileAtIndex(renderState->tileIndex)->blades->operator[](bladeIndex);
        Flame *flame             = fe->getFlame();
        for (int subSample = 0; subSample < flame->params.numBatches; subSample++)
        {
            if (bladeIndex == bladeIndexBegin && subSample == subSampleBegin)
                return renderedBatches + subSample;
        }
        renderedBatches += flame->params.numBatches;
    }
    return  renderedBatches;
}

void RenderTile::adjustBackgroundForCurves(SharedFlameExpanded &fe)
{
    Flame *flame = fe->getFlame();
    struct rgba background = flame->params.background;
    if (fe->rgbCurve) {
        NaturalCubicCurve rgbCurve(NaturalCubicCurve(fe->rgbCurve));
        float preluma  = 0.297361f * background.r + 0.627355f * background.g + 0.075285f * background.b;
        float postluma = rgbCurve.evaluateForU(preluma);
        if (preluma != 0.f) {
            background.r   = postluma/preluma * background.r;
            background.g   = postluma/preluma * background.g;
            background.b   = postluma/preluma * background.b;
        }
        else {
            background.r   = postluma;
            background.g   = postluma;
            background.b   = postluma;
        }
    }
    if (fe->redCurve) {
        NaturalCubicCurve redCurve(fe-> redCurve);
        background.r   = redCurve.evaluateForU(background.r);
    }
    if (fe->greenCurve) {
        NaturalCubicCurve greenCurve(fe-> greenCurve);
        background.g   = greenCurve.evaluateForU(background.g);
    }
    if (fe->blueCurve) {
        NaturalCubicCurve blueCurve(fe->blueCurve);
        background.b   = blueCurve.evaluateForU(background.b);
    }
    
    background.r = background.r > 1.f ? 1.f : background.r;
    background.r = background.r < 0.f ? 0.f : background.r;
    background.g = background.g > 1.f ? 1.f : background.g;
    background.g = background.g < 0.f ? 0.f : background.g;
    background.b = background.b > 1.f ? 1.f : background.b;
    background.b = background.b < 0.f ? 0.f : background.b;
    
    flame->params.background = background;
}

#if defined(OPENCL_ONLY)
static SharedClRuntime makeRuntime(RenderState *renderState, enum GpuPlatformUsed gpuPlatformInUse,
                                       SharedDeviceContext deviceContext, uint numBatches)
{
    Flam4ClRuntime *runtime = new Flam4ClRuntime(std::static_pointer_cast<ClContext>(deviceContext) , numBatches);
      return std::shared_ptr<Flam4ClRuntime>(runtime);
}

#elif defined(METAL_ONLY)
static Flam4CommonRuntime *makeRuntime(RenderState *renderState, enum GpuPlatformUsed gpuPlatformInUse,
                                       SharedDeviceContext deviceContext, uint numBatches)
{
    return [[Flam4MetalRuntime alloc] initWithMetalContext:(MetalContext *)deviceContext
                                                numBatches:numBatches];
}

#elif defined(CUDA_ONLY)
static SharedCudaRuntime makeRuntime(RenderState *renderState, enum GpuPlatformUsed gpuPlatformInUse,
                                     SharedDeviceContext deviceContext, uint numBatches)
{
    Flam4CudaRuntime *runtime = new Flam4CudaRuntime(std::static_pointer_cast<CudaContext>(deviceContext) , numBatches);
    return std::shared_ptr<Flam4CudaRuntime>(runtime);
}

#else
static SharedRuntime makeRuntime(RenderState *renderState, enum GpuPlatformUsed gpuPlatformInUse,
                                       SharedDeviceContext deviceContext, uint numBatches)
{
    switch (gpuPlatformInUse) {
        default:
        case useOpenCL:
        {
            Flam4ClRuntime *runtime = new Flam4ClRuntime(std::static_pointer_cast<ClContext>(deviceContext) , numBatches);
            return std::shared_ptr<Flam4ClRuntime>(runtime);
        }
//        case useMetal:
//            return [[Flam4MetalRuntime alloc] initWithMetalContext:(MetalContext *)deviceContext
//                                                        numBatches:numBatches];
//            break;
#ifndef NO_CUDA
        case useCUDA:
        {
            Flam4CudaRuntime *runtime = new Flam4CudaRuntime(std::static_pointer_cast<CudaContext>(deviceContext) , numBatches);
            return std::shared_ptr<Flam4CudaRuntime>(runtime);
        }
#endif
    }
}
#endif

bool RenderTile::renderBladeBatches(SharedRenderState renderState,
                                    std::string & variationSetUuid,
                                    SharedElc elc,
                                    size_t subSampleBegin,
                                    size_t bladeIndexBegin,
                                    size_t & renderedBatches,
                                    time_point<high_resolution_clock> now,
                                    bool alternateGPURendering)
{
    int clearCounter = CLEAR_FREQUENCY; // if we are resuming,  rendertarget buffer will have been zeroed
    size_t bladeBatches = 0;
    
    uint selectedDeviceNum  = renderState->selectedDeviceNum;
    SharedRuntime & runtime = renderState->runtime;
    
    for (size_t bladeIndex = bladeIndexBegin; bladeIndex < renderState->renderInfo->blades->size(); bladeIndex++) {
        SharedFlameExpanded & fe = renderState->tiles->tileAtIndex(renderState->tileIndex)->blades->operator[](bladeIndex);

        const SharedVariationSet & variationSet = VariationSet::variationSetForUuid(fe->variationSetUuid);
        
        if (fe->applyToBackground)
            adjustBackgroundForCurves(fe);
        
        runtime->copyFlame(fe->getFlame(), selectedDeviceNum);
        
        // we dont want to modify the originals as they might be saved to RenderState later on
        Flame *flame = runtime->g_pFlame[selectedDeviceNum];
        
        flame->prepareSwitchMatrix(elc->xaosMatrix->weightsBrick());
        normalizeXformWeights(flame);
        saveExpandedGradientToFlame(flame, fe->elc->paletteMode);
        
        runtime->makeVarUsageListsForSelectedDeviceNum(selectedDeviceNum, *flame, variationSet);
        
        for (size_t subSample = subSampleBegin; subSample < flame->params.numBatches; subSample++)
        {
            if (--clearCounter == 0) { // see if its time to add rendertarget to accum buffer and clear it
                clearCounter = CLEAR_FREQUENCY;
                if (renderState->runtime->hostAccumBuffer) {
                    renderState->runtime->addToAccumBufferForSelectedDeviceNum(renderState->selectedDeviceNum);
                    renderState->runtime->clearRenderTargetForSelectedDeviceNum(renderState->selectedDeviceNum,
                                                                                renderState->maxWorkGroupSize,
                                                                                variationSet);
                }
            }
            
            // Metal GPU hangs take about 5 seconds to recover
            time_point<high_resolution_clock> now = high_resolution_clock::now();
            
            runtime->renderBatchForSelectedDeviceNum(selectedDeviceNum,
                                                     elc,
                                                     renderState->renderInfo->epsilon,
                                                     renderState->maxWorkGroupSize,
                                                     variationSet,
                                                     (uint)subSample);
            renderedBatches += 1;
            
            duration<double> renderBatchDuration = duration_cast<duration<double>>(high_resolution_clock::now() - now);
            //            NSLog(@"Batch duration: %g sec", renderBatchDuration/1000.);
            if (renderBatchDuration.count() > 4.)
                break; // stop renderBatch loop
            
        }
        subSampleBegin = 0;
        bladeBatches += flame->params.numBatches;
    }
    return true;
}

#pragma mark Single Device Render

//This function renders a single frame.
SharedImage RenderTile::RenderTileCore(SharedRenderState renderState,
                                       SharedFE firstFe,
                                       SharedElc elc,
                                       size_t subSampleBegin,
                                       size_t bladeIndexBegin,
                                       uchar *backgroundImage,
                                       bool alternateGPURendering,
                                       duration<double> & _duration,
                                       duration<double> & deDuration)
{
    time_point<high_resolution_clock> now   = high_resolution_clock::now();
    uint selectedDeviceNum                  = renderState->selectedDeviceNum;
    const SharedVariationSet & variationSet = VariationSet::variationSetForUuid(firstFe-> variationSetUuid);
    const SharedDeviceKind & deviceKind     = renderState->deviceContext->deviceKindForSelectedDevice(selectedDeviceNum);
    
    duration<double> buildTime(0.);
    DeviceProgram * program   = deviceKind->programForVariationSet(variationSet, buildTime);
    if (! program)
        return nullptr;
    SharedRuntime & runtime  = renderState->runtime;
    
    runtime->copyFlame(firstFe->getFlame(), selectedDeviceNum);

    // we dont want to modify the originals as they might be saved to RenderState later on
    Flame *flame = runtime->g_pFlame[selectedDeviceNum];
    flame->prepareSwitchMatrix(elc->xaosMatrix->weightsBrick());
    normalizeXformWeights(flame);

    saveExpandedGradientToFlame(flame, firstFe->getElc()->paletteMode);
    
    if (! renderState->renderInfo->overrideFuseIterations)
        runtime->fuseIterations(renderState->deviceContext->maxFuseIterationsFromSelectedDevices(flame->params.numFinal > 0),
                                selectedDeviceNum);
    else
        runtime->fuseIterations(renderState->renderInfo->fuseIterations, selectedDeviceNum);
    
        runtime->startCuda(renderState, selectedDeviceNum);
        
        runtime->runFuseForSelectedDeviceNum(selectedDeviceNum, renderState->renderInfo->epsilon, renderState->maxWorkGroupSize, variationSet);
        
        runtime->startFrameForSelectedDeviceNum(selectedDeviceNum, renderState->maxWorkGroupSize, variationSet, buildTime);
    size_t renderedBatches = subSampleBegin;
    bool finished =
    renderBladeBatches(renderState,
                       firstFe->variationSetUuid,
                       elc,
                       subSampleBegin,
                       bladeIndexBegin,
                       renderedBatches,
                       now,
                       alternateGPURendering);
    if (! finished)
        return nullptr;
    
    renderState->runtime->renderedBatches[selectedDeviceNum] = (uint)renderedBatches;
    
    // wait for all batches to finish
    renderState->runtime->synchronizeQueueForSelectedDeviceNum(selectedDeviceNum);
    
    //    if (renderState->renderInfo->verbose)
    //        printf("\n");
    
    uint bitDepth    = renderState->renderInfo->bitDepth;
    int hdrMultiple  = bitDepth/8;
    
    uint supersample = renderState->renderInfo->supersample;
    uint width       = renderState->renderInfo->width / supersample;
    uint height      = renderState->renderInfo->height / supersample;
    
    void *img = NULL;
    if (bitDepth == 8)
        img = new cl_uchar4[hdrMultiple*width*height];
    else if (bitDepth == 16)
        img = new cl_ushort4[hdrMultiple*width*height];
    else
        img = new cl_float4[hdrMultiple*width*height];
    
    // Finish frame saves the merge of hostAccumBuffer + renderTarget back to hostAccumBuffer, if it exists
    // so we dont need to do that here
    
    // see if we do density estimation and post processing on a different context
    deDuration = duration<double>(0.);
    Point fuseInfo =
    renderState->runtime->finishFrame(renderState,
                                      img,
                                      (float *)backgroundImage,
                                      renderedBatches,
                                      variationSet,
                                      elc,
                                      firstFe,
                                      deDuration);
    renderState->fusedFraction    = fuseInfo.x;
    renderState->retainedFraction = fuseInfo.y;
    
    // ownership of img transferred to image unique ptr
    SharedImage image = std::make_shared<FA::Image>(width, height, "RGBA", renderState->renderInfo->bitDepth, renderState->renderInfo->useAlpha,
		(uchar *)img);
    image->flipped = true;
    image->profile = std::move(std::make_unique<Blob>(renderState->renderInfo->profileData));
    
    
    _duration                    = duration_cast<duration<double>>(high_resolution_clock::now() - now - buildTime);
    renderState->cummRenderTime += _duration;
    
    if (renderState->renderInfo->verbose) {
        size_t totalIterations      = DeviceRuntime::totalIterationsForBatches(renderedBatches);
        double mips                 = (double)totalIterations/_duration.count()/1000000.;
        snprintf(buf, sizeof(buf), "RenderTime: %f sec. Mips:%s [%ux%u] Quality:%s",
                 _duration.count(), prettyFloat(mips).c_str(),
                 renderState->renderInfo->width,
                 renderState->renderInfo->height,
                 prettyFloat(renderState->renderInfo->quality).c_str());
    }
    return image;
}

#pragma mark Multiple Device Render - Threaded

void RenderTile::RenderTileCoreThreadPart1(SharedRenderState renderState,
                                           SharedFE firstFe,
                                           uint selectedDeviceNum,
                                           uint subSampleBegin,
                                           duration<double> & buildTime)
{
    buildTime = duration<double>(0.);
    const SharedVariationSet & variationSet = VariationSet::variationSetForUuid(firstFe-> variationSetUuid);
    size_t maxWorkGroupSize =
        renderState->deviceContext->maxWorkGroupSizeForKernelsForSelectedDevice(selectedDeviceNum, variationSet);
    Flame *flame            = firstFe->getFlame();

    if (! renderState->renderInfo->overrideFuseIterations) {
        renderState->runtime->fuseIterations(renderState->deviceContext->maxFuseIterationsFromSelectedDevices(flame->params.numFinal > 0),
                                             selectedDeviceNum);
    }
    else {
        renderState->runtime->fuseIterations(renderState->renderInfo->fuseIterations,
                                             selectedDeviceNum);
    }
    
    renderState->runtime->startCuda(renderState, selectedDeviceNum);
    
    renderState->runtime->runFuseForSelectedDeviceNum(selectedDeviceNum, renderState->renderInfo->epsilon, (uint)maxWorkGroupSize, variationSet);
    
    renderState->runtime->startFrameForSelectedDeviceNum(selectedDeviceNum, (uint)maxWorkGroupSize, variationSet, buildTime);
}

uint RenderTile::RenderTileCoreThreadPart2(SharedRenderState renderState,
                                           SharedFE firstFe,
                                           SharedElc elc,
                                           SharedDispatcher dispatcher,
                                           uint selectedDeviceNum)
{
    // wait for completion of initialization
    renderState->runtime->synchronizeQueueForSelectedDeviceNum(selectedDeviceNum);
    
    uint renderedBatches  = 0;
    SharedRuntime runtime = renderState->runtime;
    int clearCounter      = CLEAR_FREQUENCY; // if we are resuming,  rendertarget buffer will have been zeroed
    const SharedVariationSet & variationSet = VariationSet::variationSetForUuid(firstFe-> variationSetUuid);
    size_t maxWorkGroupSize =
        renderState->deviceContext->maxWorkGroupSizeForKernelsForSelectedDevice(selectedDeviceNum, variationSet);
    
    Allocation allocation;
    while ((allocation =dispatcher->allocateBatch()).bladeIndex != std::string::npos) {
        size_t bladeIndex = allocation.bladeIndex;
        size_t subSample  = allocation.subSample;
        
        SharedFlameExpanded & fe                = renderState->tiles->tileAtIndex(renderState->tileIndex)->blades->operator[](bladeIndex);
        const SharedVariationSet & variationSet = VariationSet::variationSetForUuid(fe->variationSetUuid);
        
        if (fe->applyToBackground)
            adjustBackgroundForCurves(fe);
        
        runtime->copyFlame(fe->getFlame(), selectedDeviceNum);
        
        // we dont want to modify the originals as they might be saved to RenderState later on
        Flame *flame = runtime->g_pFlame[selectedDeviceNum];
        
        flame->prepareSwitchMatrix(elc->xaosMatrix->weightsBrick());
        normalizeXformWeights(flame);
        saveExpandedGradientToFlame(flame, fe->elc->paletteMode);
        
        runtime->makeVarUsageListsForSelectedDeviceNum(selectedDeviceNum, *flame, variationSet);
        
        if (--clearCounter == 0) { // see if its time to add rendertarget to accum buffer and clear it
            // add some randomization so that the hostAccumBuffer does not lead to a bottleneck as much
            clearCounter = randIntInRange(CLEAR_FREQUENCY - 50, CLEAR_FREQUENCY + 50); // between 150 and 250
            if (runtime->hostAccumBuffer) {
                // clear to shared host accumbuffer
                if (runtime->hostAccumBuffer) {
                    {
                        std::unique_lock<std::mutex> lock(runtime->clearMutex);
                        runtime->addToAccumBufferForSelectedDeviceNum(selectedDeviceNum);
                    }
                    runtime->clearRenderTargetForSelectedDeviceNum(selectedDeviceNum,
                                                                   (uint)maxWorkGroupSize,
                                                                   variationSet);
                }
            }
        }
        // Metal GPU hangs take about 5 seconds to recover
        time_point<high_resolution_clock> now = high_resolution_clock::now();
        
        runtime->renderBatchForSelectedDeviceNum(selectedDeviceNum,
                                                 elc,
                                                 renderState->renderInfo->epsilon,
                                                 (uint)maxWorkGroupSize,
                                                 variationSet,
                                                 (uint)subSample);
        
        dispatcher->batchCompleted();
        renderedBatches++;
        
        bool lastBlade     = bladeIndex + 1 == renderState->renderInfo->blades->size();
        bool lastSubSample = dispatcher->lastSubSampleForSubSample(subSample, bladeIndex);
        ProceedType proceed = checkForStop(renderState,
                                           firstFe->variationSetUuid,
                                           lastBlade,
                                           lastSubSample);
        duration<double> renderBatchDuration = duration_cast<duration<double>>(high_resolution_clock::now() - now);
        if (renderBatchDuration.count() > 4.)
            proceed = proceedCancel;
        
        if (proceed != proceedOK) {
            dispatcher->setProceed(proceed); // signals other threads that we are cancelled or suspended
            return renderedBatches;
        }
    }
    // wait for completion of all batches - merge phase requires all devices complete batches
    renderState->runtime->synchronizeQueueForSelectedDeviceNum(selectedDeviceNum);
    return renderedBatches;
}

SharedImage RenderTile::RenderTileCoreThreadPart3(SharedRenderState renderState,
                                                  SharedFE firstFe,
                                                  SharedDispatcher dispatcher,
                                                  SharedElc elc,
                                                  uchar *backgroundImage,
                                                  uint selectedPostProcessDevNum,
                                                  duration<double> & _duration,
                                                  duration<double> & deDuration,
                                                  duration<double> buildTime)
{
    const SharedVariationSet & variationSet = VariationSet::variationSetForUuid(firstFe->variationSetUuid);
    int hdrMultiple         = renderState->renderInfo->bitDepth/8;
    SharedRuntime & runtime = renderState->runtime;
    uint renderedBatches    = runtime->numBatches;
    uint supersample        = renderState->renderInfo->supersample;
    uint width              = renderState->renderInfo->width / supersample;
    uint height             = renderState->renderInfo->height / supersample;
    
    void *img = NULL;
    if (renderState->renderInfo->bitDepth == 8)
        img = new cl_uchar4[hdrMultiple*renderState->renderInfo->width*renderState->renderInfo->height];
    else if (renderState->renderInfo->bitDepth == 16)
        img = new cl_ushort4[hdrMultiple*renderState->renderInfo->width*renderState->renderInfo->height];
    else
        img = new cl_float4[hdrMultiple*renderState->renderInfo->width*renderState->renderInfo->height];
    
    //merge onto the renderState's device - the primary device
    size_t maxWorkGroupSize0 =
        renderState->deviceContext->maxWorkGroupSizeForKernelsForSelectedDevice(selectedPostProcessDevNum, variationSet);
    
    // gathering of all the device histograms and summing them together onto primary device's histogram
    for (int selectedDeviceNum = 0; selectedDeviceNum < runtime->numDevices; selectedDeviceNum++)
    {
        if (selectedDeviceNum == selectedPostProcessDevNum)
            continue;
        else {
            std::unique_lock<std::mutex> lock(runtime->clearMutex);

            runtime->grabFrameFromSelectedDeviceNum(selectedDeviceNum);
            runtime->mergeFramesToSelectedDeviceNum(selectedPostProcessDevNum,
                                                    selectedDeviceNum,
                                                    (uint)maxWorkGroupSize0,
                                                    variationSet,
                                                    renderState);
        }
    }
    // Finish frame saves the merge of hostAccumBuffer + renderTarget back to hostAccumBuffer, if it exists
    // so we dont need to do that here
    
    // see if we do density estimation and post processing on a different context
    deDuration = duration<double>(0.);
    Point fuseInfo =
        runtime->finishFrame(renderState,
                             img,
                             (float *)backgroundImage,
                             renderedBatches,
                             variationSet,
                             elc,
                             firstFe,
                             deDuration);
    renderState->fusedFraction    = fuseInfo.x;
    renderState->retainedFraction = fuseInfo.y;
    
    // ownership of img transferred to image unique ptr
    SharedImage image = std::make_shared<FA::Image>(width, height,
                                                    "RGBA",
                                                    renderState->renderInfo->bitDepth,
                                                    renderState->renderInfo->useAlpha,
                                                    (uchar *)img);
    image->flipped = true;
    image->profile = std::move(std::make_unique<Blob>(renderState->renderInfo->profileData));
    
    _duration                    = duration_cast<duration<double>>(high_resolution_clock::now() - dispatcher->now - buildTime);
    renderState->cummRenderTime += _duration;
    
    if (renderState->renderInfo->verbose) {
        size_t totalIterations      = DeviceRuntime::totalIterationsForBatches(renderedBatches);
        double mips                 = (double)totalIterations/_duration.count()/1000000.;
        snprintf(buf, sizeof(buf), "RenderTime: %f sec. Mips:%s [%ux%u] Quality:%s",
                 _duration.count(), prettyFloat(mips).c_str(),
                 renderState->renderInfo->width,
                 renderState->renderInfo->height,
                 prettyFloat(renderState->renderInfo->quality).c_str());
    }
    return image;
}

SharedImage RenderTile::renderTileCoreThreaded(SharedRenderState renderState,
                                               SharedFE firstFe,
                                               SharedElc elc,
                                               size_t subSampleBegin,
                                               size_t bladeIndexBegin,
                                               uchar *backgroundImage,
                                               bool continuousMode,
                                               duration<double> & _duration,
                                               duration<double> & deDuration)
{
	time_point<high_resolution_clock> now   = high_resolution_clock::now();
	uint selectedDeviceNum                  = renderState->selectedDeviceNum;
    const SharedVariationSet & variationSet = VariationSet::variationSetForUuid(firstFe-> variationSetUuid);
    const SharedDeviceKind & deviceKind     = renderState->deviceContext->deviceKindForSelectedDevice(selectedDeviceNum);
    
    duration<double> buildTime(0.);
    DeviceProgram * program   = deviceKind->programForVariationSet(variationSet, buildTime);
    if (! program)
        return nullptr;
    
    std::atomic<uint> renderedBatches(0);
    SharedRuntime & runtime     = renderState->runtime;
    SharedDispatcher dispatcher = std::make_shared<Flam4RenderDispatcher>(renderState, bladeIndexBegin, subSampleBegin, now);
    
    SharedFlameTile & tile          = renderState->tiles->tileAtIndex(renderState->tileIndex);
    renderState->renderInfo->width  = tile->width;
    renderState->renderInfo->height = tile->height;
    
    std::vector<std::thread> workers;
	auto lambda = [&renderedBatches](SharedRenderState renderState,
		SharedFE firstFe,
		SharedElc elc,
		SharedDispatcher dispatcher,
		uint selectedDeviceNum,
		size_t subSampleBegin,
		duration<double> & buildTime)
	{
		RenderTileCoreThreadPart1(renderState,
			firstFe,
			selectedDeviceNum,
			(uint)subSampleBegin,
			buildTime);
		uint deviceRenderedBatches =
			RenderTileCoreThreadPart2(renderState,
				firstFe,
				elc,
				dispatcher,
				selectedDeviceNum);
		renderedBatches += deviceRenderedBatches;
		renderState->runtime->renderedBatches[selectedDeviceNum] = deviceRenderedBatches;
	};

    for (uint selectedDeviceNum = 0; selectedDeviceNum < runtime->numDevices; selectedDeviceNum++)
    {
        runtime->copyFlame(firstFe->getFlame(), selectedDeviceNum);
        
        // we dont want to modify the originals as they might be saved to RenderState later on
        Flame *flame = runtime->g_pFlame[selectedDeviceNum];
        flame->prepareSwitchMatrix(elc->xaosMatrix->weightsBrick());
        normalizeXformWeights(flame);

        saveExpandedGradientToFlame(flame, firstFe->getElc()->paletteMode);
        
        auto t = std::thread(lambda, renderState, firstFe, elc, dispatcher, selectedDeviceNum, subSampleBegin, std::ref(buildTime));
        workers.push_back(std::move(t));
    }
    // wait for all devices to finish
    std::for_each(workers.begin(), workers.end(), [](std::thread &t) {
        if (t.joinable())
            t.join();
    });
    workers.clear();
    
    if (dispatcher->getProceed() == proceedOK) { // if we pause a render we dont do all batches
        if (renderedBatches != tile->numBatches) {
            snprintf(buf, sizeof(buf), "Missing Batches:  Rendered %u batches out of expected count of %zu", (uint)renderedBatches, tile->numBatches);
        }
    }
    
    // complete processing on the primary device
    return RenderTileCoreThreadPart3(renderState,
                                     firstFe,
                                     dispatcher,
                                     elc,
                                     backgroundImage,
                                     renderState->selectedDeviceNum,
                                     _duration,
                                     deDuration,
                                     buildTime);
}

#pragma mark Render Tile Set

//This function renders a single frame and saves it to outFile.
//hwnd should be NULL if you're rendering a frame larger than 4096x4096
//since openGL cannot handle textures that large.
SharedImage RenderTile::RenderPreviewTileSetCore(SharedRenderState renderState,
                                                 FA::Image * backgroundImage,
                                                 bool continuousMode,
                                                 enum GpuPlatformUsed gpuPlatformInUse,
                                                 SharedDeviceContext deviceContext,
                                                 bool favorSplitMerge,
                                                 duration<double> & _duration,
                                                 duration<double> & deDuration)
{
    renderState->renderInfo->flipped    = renderState->tiles->flipped;
    renderState->renderInfo->blades     = renderState->tiles->blades;
    renderState->renderInfo->pixelFormat = "RGBA";
    
    SharedRepTileArray imageArray = std::shared_ptr<ImageRepTileArray>(renderState->tiles->makeImageRepTileArray(false));
    
    // iterate through the tiles do each in turn
    duration<double> cummRenderTime(0.);
    float priorFraction = renderState->fusedFraction * renderState->retainedFraction;
    
    renderState->priorFraction  = priorFraction;
    renderState->cummRenderTime = cummRenderTime;
    renderState->imageArray     = imageArray;
//    renderState->priorState =nil;
    
    for (uint i = 0; i < renderState->tiles->size(); i++) {
		SharedFlameTile & tile               = renderState->tiles->tileAtIndex(i);
        duration<double> originalRenderTime  = cummRenderTime;
        renderState->renderInfo->pixelFormat = "RGBA";
        renderState->renderInfo->width       = tile->width;
        renderState->renderInfo->height      = tile->height;
        
        SharedRuntime runtime = makeRuntime(renderState.get(), gpuPlatformInUse, deviceContext, (uint)tile->numBatches);
        renderState->runtime    = runtime;
        renderState->tileIndex  = i;

        
        SharedFE & firstFe = renderState->tiles->tileAtIndex(renderState->tileIndex)->blades->operator[](0);
        SharedElc & elc    = renderState->renderInfo->elcData;
        
        uchar * backgroundPixels = backgroundImage ? backgroundImage->bitmap.get() : nullptr;
        
		SharedImage rep;
        if (runtime->numDevices == 1 || (!favorSplitMerge)) {
            rep =
            RenderTileCore(renderState,
                           firstFe,
                           elc,
                           0,
                           0,
                           backgroundPixels,
                           runtime->numDevices > 1,
                           _duration,
                           deDuration);
        }
        else { // multiple render device case that splits batches between the devices then merges them later
            rep = renderTileCoreThreaded(renderState,
                                         firstFe,
                                         elc,
                                         0,
                                         0,
                                         backgroundPixels,
                                         continuousMode,
                                         _duration,
                                         deDuration);
        }
        
        if (! rep) {
            for (int selectedDeviceNum = 0; selectedDeviceNum < runtime->numDevices; selectedDeviceNum++)
                runtime->stopCudaForSelectedDeviceNum(selectedDeviceNum);
            return SharedImage();
        }
        
        imageArray->setTileAt(tile->row,
                              tile->column,
                              rep,
                              cummRenderTime - originalRenderTime,
                              (uint)tile->numBatches);
        
        for (int selectedDeviceNum = 0; selectedDeviceNum < runtime->numDevices; selectedDeviceNum++)
            runtime->stopCudaForSelectedDeviceNum(selectedDeviceNum);
    }
    return imageArray->combinedImageRep(std::make_unique<Blob>(renderState->renderInfo->profileData));
}

// determine how to split the render area into tiles
SharedFlmTileArray RenderTile::splitAreaIntoTiles(SharedDeviceContext deviceContext,
                                                  SharedFE fe,
                                                  float area,
                                                  float width,
                                                  float height,
                                                  Flame *flame,
                                                  bool flipped,
                                                  int desiredQuality,
                                                  int warpSize,
                                                  size_t maxTileMemory,
                                                  uint supersample)
{
    float maxArea   = area;
    if (maxTileMemory != 0LL) {
        float areaLimit = ceilf((float)(maxTileMemory - 1048576LL)/36.f);
        maxArea = maxArea > areaLimit ? areaLimit : maxArea;
    }
    width  = supersample * width;
    height = supersample * height;
    
    RenderGeometry::changeAspectRatio(flame, width, height);
    
    float minHeight = 100.f;
    float minWidth  = 100.f;
    float overlap   = supersample * FlameTile::overlap();
    size_t amount;

    bool success = deviceContext->checkOclGPUMemAvailable(amount,
                                                          maxArea,
                                                          1.f,
                                                          flame->params.numTrans,
                                                          supersample);
    if (!success) {
        printf("Unable to allocate enough memory for any tile size");
        return nullptr;
    }
    
    int rows = 1;
    int cols = 1;
    if (width >= height) { // horizontal tiling
        if (maxArea >= width * height || maxArea/height >= minWidth + 2*overlap) { // 1 row is enough
            cols = ceilf(area/maxArea);
            rows = 1;
        }
        else if (height >= minHeight + 2*overlap) {  // too narrow case - split height by multiples of minHeight
            int minTiles = ceilf(area/(maxArea));
            
            rows = ceilf((minWidth + 2*overlap)*height/maxArea); // calc number of rows needed to keep size reasonable
            cols = ceilf(minTiles/rows);
            float effHeight = height + 2 * overlap * (rows - 1);
            float effWidth  = width  + 2 * overlap * (cols - 1);
            minTiles = ceilf(effWidth * effHeight/maxArea);
            cols = ceilf(minTiles/rows);
        }
        else {
            cols = ceilf(width/(minWidth + 2*overlap));
            rows = 1;
        }
    }
    else  {		// vertical tiling  (width < height)
        if (maxArea >= width * height || maxArea/width >= minHeight + 2*overlap) {
            cols = 1;
            rows = ceilf(area/maxArea);
        }
        else if (width >= minWidth + 2*overlap) {  // too short case - split width by multiples of minWidth
            int minTiles = ceilf(area/maxArea);
            
            cols = ceilf((minHeight + 2*overlap)*width/maxArea); // calc number of cols needed to keep size reasonable
            rows = ceilf(minTiles/cols);
            float effHeight = height + 2 * overlap * (rows - 1);
            float effWidth  = width  + 2 * overlap * (cols - 1);
            minTiles = ceilf(effWidth * effHeight/maxArea);
            rows = ceilf(minTiles/cols);
        }
        else {
            cols = 1;
            rows = ceilf(height/(minHeight + 2.f*overlap));
        }
    }
    SharedBlades blades = std::make_shared<BladesVector>();
    blades->push_back(fe);
    
    const SharedVariationSet & variationSet = VariationSet::variationSetForUuid(fe->variationSetUuid);
    
    return std::make_shared<FlameTileArray>(rows,
                                            cols,
                                            blades,
                                            width,
                                            height,
                                            flipped,
                                            false,
                                            1,
                                            variationSet);
}

SharedImage RenderTile::renderAsPreview(SharedDeviceContext deviceContext,
                                        int selectedDeviceNum,
                                        SharedBlades blades,
                                        Rect rect,
                                        int quality,
                                        bool hasAlpha,
                                        int bitsPerPixel,
                                        const string &colorspaceName,
                                        bool flipped,
                                        float epsilon,
                                        uint maxWorkGroupSize,
                                        bool verbose,
                                        duration<double> & _duration,
                                        duration<double> & deDuration)
{
    if (blades->size() == 0)
        return SharedImage();
        
	enum GpuPlatformUsed gpuPlatform = useOpenCL;
#ifndef NO_CUDA
    if (std::dynamic_pointer_cast<SharedCudaContext>(deviceContext)) {
        gpuPlatform = useCUDA;
    }
#endif
    SharedFE & fe = blades->operator[](0);
    
    const SharedVariationSet & variationSet = VariationSet::variationSetForUuid(fe->variationSetUuid);
    Flame *flame = fe->getFlame();
    
    SharedRenderInfo renderInfo = std::make_shared<RenderInfo>();
    renderInfo->uuid            = "";
    renderInfo->useAlpha        = hasAlpha;
    renderInfo->bitDepth        = bitsPerPixel;
    renderInfo->profileData     = RenderTile::getProfileFor(colorspaceName);
    renderInfo->renderingIntent = RenderingIntentSaturation;
    renderInfo->epsilon         = epsilon;
    renderInfo->quality         = quality;
    renderInfo->pixelFormat     = "RGBA";
    renderInfo->imageURL        = "";  // only needed for saving render state
    renderInfo->saveRenderState = false;
    renderInfo->elcData         = fe->elc;
    renderInfo->blades          = blades;
    renderInfo->flipped         = flipped;
    renderInfo->supersample     = flame->params.oversample;
    renderInfo->verbose         = true;
    
    SharedFlmTileArray tiles = RenderTile::splitAreaIntoTiles(deviceContext,
                                                              fe,
                                                              rect.size.width * rect.size.height,
                                                              rect.size.width, rect.size.height,
                                                              flame,
                                                              flipped,
                                                              quality,
                                                              deviceContext->warpSizeForSelectedDevice(selectedDeviceNum),
                                                              0LL,
                                                              flame->params.oversample);
    
    uint supersampleAdjust = tiles->flame()->params.oversample * tiles->flame()->params.oversample;
    for (SharedFlameTile &tile : tiles->getTiles()) {
        size_t blocksY = 32;
        float tileQuality = ceilf((float)quality / supersampleAdjust);
        
        uint perPointFuseIterations = deviceContext->maxFuseIterationsFromSelectedDevices(flame->params.numFinal > 0);
        
        size_t batches = DeviceRuntime::batchesPerTileForDesiredAdaptive(tileQuality,
                                                                         tile->width,
                                                                         tile->height,
                                                                         1,
                                                                         blocksY,
                                                                         perPointFuseIterations);
        tile->allocateBatchesToBlades(batches, deviceContext);
    }
    float fusedFraction = 1.f;
    float retainedFraction = 1.f;
    duration<double> renderTime;
    
    SharedRenderState renderState = std::make_shared<RenderState>(renderTime, fusedFraction, retainedFraction);
    
    renderState->deviceContext     = deviceContext;
    renderState->selectedDeviceNum = selectedDeviceNum;
    renderState->renderInfo        = renderInfo;
    renderState->tiles             = tiles;
    renderState->maxWorkGroupSize  = (uint)deviceContext->maxWorkGroupSizeForKernelsForSelectedDevice(selectedDeviceNum, variationSet);
    //    renderState.ProgressListener:(NSObject<ProgressListenerProtocol> *)nil];
    renderState->usePriorFuseFraction = usePrior;
    renderState->proceed              = proceedOK;
    
    return RenderTile::RenderPreviewTileSetCore(renderState, nullptr, false, gpuPlatform, deviceContext, true, _duration, deDuration);
}

SharedImage RenderTile::doRender(RenderParams &request, float &fusedFraction, float &retainedFraction, duration<double> & _duration, duration<double> & deDuration)
{
    if (request.blades->size() == 0)
        return SharedImage();
    
    SharedFE & fe = request.blades->operator[](0);
    
    const SharedVariationSet & variationSet = VariationSet::variationSetForUuid(fe->variationSetUuid);
    Flame *flame = fe->getFlame();
    //    rect.size.width  *= flame->params.oversample;
    //    rect.size.height *= flame->params.oversample;
    
    std::string colorSpace;
    switch (request.eColorSpace) {
        case sRGB:
            colorSpace = "sRGB";
            break;
        case AdobeRGB:
            colorSpace = "AdobeRGB";
            break;
        case WideGamutRGB:
            colorSpace = "WideGamutRGB";
            break;
        case ProPhotoRGB:
            colorSpace = "ProPhoto";
            break;
        default:
            colorSpace = "GenericRGB";
            break;
    }
    size_t maxTileMemory             = 0LL;
    bool continuousMode              = false;
    enum PriorUse usePriorFraction   = noPrior;

    SharedRenderInfo renderInfo = std::make_shared<RenderInfo>();
    renderInfo->uuid            = "";
    renderInfo->useAlpha        = request.hasAlpha;
    renderInfo->bitDepth        = request.bitDepth;
    renderInfo->profileData     = RenderTile::getProfileFor(colorSpace);
    renderInfo->renderingIntent = RenderingIntentSaturation;
    renderInfo->epsilon         = request.epsilon;
    renderInfo->quality         = request.quality;
    renderInfo->pixelFormat     = "RGBA";
    renderInfo->imageURL        = "";  // only needed for saving render state
    renderInfo->saveRenderState = false;
    renderInfo->elcData         = fe->elc;
    renderInfo->blades          = request.blades;
    renderInfo->flipped         = request.flipped;
    renderInfo->supersample     = flame->params.oversample;
    renderInfo->verbose         = true;
    
    if (dynamic_cast<FileOutputRenderParams *>(&request) != nullptr) {
        FileOutputRenderParams & forp = dynamic_cast<FileOutputRenderParams &>(request);
        renderInfo->renderingIntent   = forp.renderingIntent;
        renderInfo->saveRenderState   = forp.saveRenderState;
        maxTileMemory                 = forp.maxTileMemory;
        renderInfo->uuid              = forp.uuid;
        continuousMode                = forp.continuousMode;
        usePriorFraction              = forp.usePriorFraction;
        fusedFraction                 = forp.fusedFraction;
        retainedFraction              = forp.retainedFraction;
    }
    if (dynamic_cast<FileBatchRenderParams *>(&request) != nullptr) {
        FileBatchRenderParams & fbrp = dynamic_cast<FileBatchRenderParams &>(request);
        usePriorFraction             = fbrp.usePriorFraction;
    }
    
    SharedFlmTileArray tiles = RenderTile::splitAreaIntoTiles(request.deviceContext,
                                                              fe,
                                                              request.size.width * request.size.height,
                                                              request.size.width, request.size.height,
                                                              flame,
                                                              request.flipped,
                                                              request.quality,
                                                              request.deviceContext->warpSizeForSelectedDevice(request.selectedDeviceNum),
                                                              maxTileMemory,
                                                              flame->params.oversample);
    
    uint supersampleAdjust = tiles->flame()->params.oversample * tiles->flame()->params.oversample;
    for (SharedFlameTile &tile : tiles->getTiles()) {
        size_t blocksY = 32;
        float tileQuality = ceilf((float)request.quality / supersampleAdjust);
        
        uint perPointFuseIterations = request.deviceContext->maxFuseIterationsFromSelectedDevices(flame->params.numFinal > 0);
        
        size_t batches = DeviceRuntime::batchesPerTileForDesiredAdaptive(tileQuality,
                                                                         tile->width,
                                                                         tile->height,
                                                                         1,
                                                                         blocksY,
                                                                         perPointFuseIterations);
        tile->allocateBatchesToBlades(batches, request.deviceContext);
    }
    duration<double> renderTime;
    
    SharedRenderState renderState = std::make_shared<RenderState>(renderTime, fusedFraction, retainedFraction);
    
    renderState->deviceContext     = request.deviceContext;
    renderState->selectedDeviceNum = request.selectedDeviceNum;
    renderState->renderInfo        = renderInfo;
    renderState->tiles             = tiles;
    renderState->maxWorkGroupSize  = (uint)request.deviceContext->maxWorkGroupSizeForKernelsForSelectedDevice(request.selectedDeviceNum, variationSet);
    //    renderState.ProgressListener:(NSObject<ProgressListenerProtocol> *)nil];
    renderState->usePriorFuseFraction = usePriorFraction;
    renderState->proceed              = proceedOK;
    
    return RenderTile::RenderPreviewTileSetCore(renderState, nullptr, continuousMode, request.gpuPlatform, request.deviceContext, true, _duration, deDuration);
}

#ifdef __ORIGINAL__

void allocateBatchesToBlades(DeviceContext *deviceContext,
                             NSArray *blades,
                             size_t batchesDesired,
                             double width,
                             double height)
{
    size_t bladeCount    = [blades count];
    size_t actualBatches = batchesDesired;
    FlameExpanded *firstFe = [blades objectAtIndex:0];
    
    if (batchesDesired <= bladeCount) {
        for (FlameExpanded *fe in blades) {
            [fe flame]->params.numBatches = 1.f;
        }
        actualBatches = bladeCount;
    }
    else  { // batchesDesired > bladeCount
        size_t batchesPerBlade = floorf((float)batchesDesired/bladeCount);
        float residual             = batchesDesired - bladeCount * batchesPerBlade;
        size_t total           = 0;
        for (FlameExpanded *fe in blades) {
            [fe flame]->params.numBatches = batchesPerBlade;
            total += batchesPerBlade;
        }
        if (residual > 0.f) {
            for (FlameExpanded *fe in blades) {
                [fe flame]->params.numBatches += 1.f;
                if (++total >= batchesDesired)
                    break;
            }
        }
        actualBatches = total;
    }
    uint perPointFuseIterations = [deviceContext maxFuseIterationsFromSelectedDevices:[firstFe flame]->params.numFinal > 0];
    float actualQuality = [Flam4CommonRuntime actualQualityWith:actualBatches
                                                          width:width
                                                         height:height
                                                          tiles:1
                                         perPointFuseIterations:perPointFuseIterations];
    for (FlameExpanded *fe in blades) {
        [fe flame]->params.desiredQuality = actualQuality;
    }
}


#ifndef NO_NSALERT
NSBitmapImageRep *renderFromPriorState(DeviceContext *deviceContext,
                                       RenderStateDocument *renderStateDoc,
                                       bool postProcessAgain,
                                       float *fusedFraction,
                                       float *retainedFraction,
                                       enum GpuPlatformUsed gpuPlatformInUse,
                                       bool useAlpha)
{
    ImageRepTileArray *imageArray = [renderStateDoc imageArray];
    
    FlameTileArray *tiles         = [renderStateDoc tiles];
    ElcEncoded *elcEncoded        = [NSKeyedUnarchiver unarchiveObjectWithData:[renderStateDoc elcArchived]];
    
    NSData *profileData    = (NSData *)[[renderStateDoc state] objectForKey:@"profileData"];
    float epsilon          = [[[renderStateDoc state] objectForKey:@"epsilon"] floatValue];
    float quality          = [[[renderStateDoc state] objectForKey:@"quality"] floatValue];
    int subSample          = [[[renderStateDoc state] objectForKey:@"subSample"] intValue];
    int bladeIndexBegin    = [[[renderStateDoc state] objectForKey:@"bladeIndex"] intValue];
    int bitDepth           = [[[renderStateDoc state] objectForKey:@"bitDepth"] intValue];
    int pixelFormat        = [[[renderStateDoc state] objectForKey:@"pixelFormat"] intValue];
    int tileIndex          = [[[renderStateDoc state] objectForKey:@"tileIndex"] intValue];
    int renderingIntent    = [[[renderStateDoc state] objectForKey:@"renderingIntent"] intValue];
    NSString *uuid         = [[renderStateDoc state]  objectForKey:@"uuid"];
    enum PriorUse usePriorFraction  = (enum PriorUse)[[[renderStateDoc state] objectForKey:@"usePriorFraction"]intValue];
    float priorFraction    = [[[renderStateDoc state] objectForKey:@"priorFraction"] floatValue];
    
    FlameExpanded *firstFe     = [[tiles blades] objectAtIndex:0];
    VariationSet *variationSet = [VariationSet variationSetForUuid:[firstFe variationSetUuid]];
    
    RenderInfo *renderInfo = [[RenderInfo alloc] init];
    [renderInfo setUuid:uuid];
    [renderInfo setUseAlpha:useAlpha];
    [renderInfo setBitDepth:bitDepth];
    [renderInfo setProfileData:profileData];
    [renderInfo setRenderingIntent:renderingIntent];
    [renderInfo setEpsilon:epsilon];
    [renderInfo setQuality:quality];
    [renderInfo setPixelFormat:pixelFormat];
    [renderInfo setElcData:[elcEncoded elcData]];
    [renderInfo setImageURL:[renderStateDoc imageURL]];
    [renderInfo setSaveRenderState:[renderStateDoc saveRenderState]];
    [renderInfo setBlades:[tiles blades]];
    [renderInfo setElcData:[firstFe elcData]];
    [renderInfo setFlipped:[tiles flipped]];
    
    RenderState *renderState = [[RenderState alloc] init];
    renderState->DeviceContext:deviceContext];
    renderState->SelectedDeviceNum:0];
    renderState->RenderInfo:renderInfo];
    renderState->Tiles:tiles];
    renderState->MaxWorkGroupSize:(uint)[deviceContext maxWorkGroupSizeForKernelsForSelectedDevice:renderState->selectedDeviceNum variationSet:variationSet]];
    renderState->ProgressListener:renderStateDoc];
    renderState->FusedFraction:fusedFraction];
    renderState->RetainedFraction:retainedFraction];
    renderState->UsePriorFuseFraction:usePriorFraction];
    renderState->ImageArray:imageArray];
    renderState->TileIndex:tileIndex];
    renderState->PriorFraction:priorFraction];
    renderState->PriorState:renderStateDoc];
    renderState->Proceed:proceedOK];
    
    duration<double> cummTime(0.);
    for (int i = 0; i < [tiles count]; i++) {
        cummTime += [imageArray timeAtIndex:i];
    }
    ElectricSheepStuff *elc = (ElectricSheepStuff *)[[[renderState renderInfo] elcData] bytes];
    
    // render time for paused last batch in tile has not been recorded to imageArray
    duration<double> priorRenderTimeForTile = [[[renderStateDoc state] objectForKey:@"renderTimeForTile"] longValue];
    cummTime += priorRenderTimeForTile;
    
    // see if we have already finished the tile the resume render starts from
    if (priorRenderTimeForTile == 0 && tileIndex < [tiles count] && subSample == [tiles numBatchesAtIndex:tileIndex]) {
        tileIndex++;
        subSample = 0;
    }
    
    // resume from a pause
    if (priorRenderTimeForTile > 0 && [renderStateDoc resumeable]) {
        // iterate through the tiles do each in turn
        unsigned maxBatches = maxProgress(tiles);
        for (int i = tileIndex; i < [tiles count]; i++) {
            [renderStateDoc setTileIndex:i];
            
            FlameTile *tile                  = [tiles tileAtIndex:i];
            time_t originalRenderTime = cummTime;
            Flam4CommonRuntime *runtime = makeRuntime(renderState, gpuPlatformInUse, deviceContext, [tile numBatches]);
            
            [renderInfo setWidth:[tile width]];
            [renderInfo setHeight:[tile height]];
            [renderInfo setSupersample:[tile flame]->params.oversample];
            
            
            allocateBatchesToBlades(deviceContext, [tile blades], [tile numBatches], [tile width], [tile height]);
            
            renderState->TileIndex:i];
            renderState->Runtime:runtime];
            renderState->CummRenderTime:&cummTime];
            
            size_t batchesSoFar = cumulativeProgress([renderState imageArray], i, subSample);
            if ([[renderState progressListener] isMemberOfClass:[PngSettingsController class]] &&
                [renderState progressListener]) {
                if ([[renderState progressListener] currentProgress] > batchesSoFar)
                    NSLog(@"Retrograde progress");
                uint fuseIterations = [renderState->runtime->fuseIterations][renderState->selectedDeviceNum];
                [[renderState progressListener] performSelectorOnMainThread:@selector(progressUpdate:)
                                                                 withObject:[RenderProgress renderProgresssWithProgress:batchesSoFar
                                                                                                            elapsedTime:((double)*[renderState cummRenderTime]/1000.)
                                                                                                              tileIndex:i
                                                                                                             maxBatches:maxBatches
                                                                                                         fuseIterations:fuseIterations
                                                                                                            frameNumber:[[renderState renderInfo] frameNumber]]
                                                              waitUntilDone:NO];
            }
            
            
            //            logRenderState(@"resume before tile render", tiles, imageArray, i, priorRenderTimeForTile);
            NSBitmapImageRep *rep = nil;
            if ([runtime numDevices] == 1) {
                rep = RenderTileCore(renderState,
                                     firstFe,
                                     elc,
                                     subSample,
                                     bladeIndexBegin,
                                     nil,
                                     gpuPlatformInUse,
                                     deviceContext,
                                     NO);
            }
            else { // multiple render device case
                rep = renderTileCoreThreaded(renderState,
                                             firstFe,
                                             elc,
                                             subSample,
                                             bladeIndexBegin,
                                             nil,
                                             [renderStateDoc continuousMode]);
            }
            
            subSample       = 0; // only read from prior render state once
            bladeIndexBegin = 0;
            if (rep == nil) {
                for (int selectedDeviceNum = 0; selectedDeviceNum < [runtime numDevices]; selectedDeviceNum++)
                    [runtime oclStopCudaForSelectedDeviceNum:selectedDeviceNum];
                [runtime release];
                [renderInfo release];
                [renderState release];
                return nil;
            }
            [imageArray setTileAtRow:[tile row]
                                 col:[tile column]
                          withObject:rep
                            withTime:cummTime - originalRenderTime + priorRenderTimeForTile
                 withBatchesRendered:[tile numBatches]];
            priorRenderTimeForTile = 0; // only add once
            
            if ([renderStateDoc saveRenderState]) {
                // all other device's histograms + host accum buf have already been summed into the main render target to do density estimation
                // save render target to host accum buffer is the goal, so it will be saved from there
                if ([runtime hostAccumBuffer]) {
                    [renderState->runtime->addToAccumBufferForSelectedDeviceNum:renderState->selectedDeviceNum];
                }
                [RenderStateDocument saveRenderStateWithContext:renderState
                                                 cummRenderTime:cummTime
                                              renderTimeForTile:0 // recorded already in imageArray
                                                      subSample:[tile numBatches]
                                                     bladeIndex:bladeIndexBegin
                                                initialImageRep:nil
                                                           show:NO
                                                         paused:NO];
                //                [RenderStateDocument saveRenderTargetState:renderState]; -- already saved by renderTileCore's finishFrame
            }
            
            for (int selectedDeviceNum = 0; selectedDeviceNum < [renderState->runtime->numDevices]; selectedDeviceNum++)
                [runtime oclStopCudaForSelectedDeviceNum:selectedDeviceNum];
            [runtime release];
        }
    }
    
    // now go back and add more rendering time if necessary - if user bumped up quality
    if ([renderStateDoc rechargeable]) {
        unsigned maxBatches = maxProgress(tiles);
        for (int i = 0; i < [tiles count]; i++) {
            [renderStateDoc setTileIndex:i];
            
            FlameTile *tile    = [tiles tileAtIndex:i];
            int numBatchesDone = [imageArray batchesRenderedAtIndex:i];
            
            [renderInfo setWidth:[tile width]];
            [renderInfo setHeight:[tile height]];
            [renderInfo setSupersample:[tile flame]->params.oversample];
            
            allocateBatchesToBlades(deviceContext, [tile blades], [tile numBatches], [tile width], [tile height]);
            
            //            logRenderState(@"recharge before tile render", tiles, imageArray, i, 0);
            
            if ([tile numBatches] > numBatchesDone) {
                time_t originalRenderTime = cummTime;
                Flam4CommonRuntime *runtime = makeRuntime(renderState, gpuPlatformInUse, deviceContext, [tile numBatches]);
                renderState->TileIndex:i];
                renderState->Runtime:runtime];
                renderState->CummRenderTime:&cummTime];
                
                size_t batchesSoFar = cumulativeProgress([renderState imageArray], i, numBatchesDone);
                if ([[renderState progressListener] isMemberOfClass:[PngSettingsController class]] &&
                    [renderState progressListener]) {
                    if ([[renderState progressListener] currentProgress] > batchesSoFar)
                        NSLog(@"Retrograde progress");
                    uint fuseIterations = [renderState->runtime->fuseIterations][renderState->selectedDeviceNum];
                    [[renderState progressListener] performSelectorOnMainThread:@selector(progressUpdate:)
                                                                     withObject:[RenderProgress renderProgresssWithProgress:batchesSoFar
                                                                                                                elapsedTime:((double)*[renderState cummRenderTime]/1000.)
                                                                                                                  tileIndex:i
                                                                                                                 maxBatches:maxBatches
                                                                                                             fuseIterations:fuseIterations
                                                                                                                frameNumber:[[renderState renderInfo] frameNumber]]
                                                                  waitUntilDone:NO];
                }
                
                NSBitmapImageRep *rep = nil;
                if ([runtime numDevices] == 1) {
                    rep = RenderTileCore(renderState,
                                         firstFe,
                                         elc,
                                         numBatchesDone,
                                         0,
                                         nil,
                                         gpuPlatformInUse,
                                         deviceContext,
                                         NO);
                }
                else { // multiple render device case
                    rep = renderTileCoreThreaded(renderState,
                                                 firstFe,
                                                 elc,
                                                 numBatchesDone,
                                                 0,
                                                 nil,
                                                 [renderStateDoc continuousMode]);
                }
                if (rep == nil) {
                    for (int selectedDeviceNum = 0; selectedDeviceNum < [runtime numDevices]; selectedDeviceNum++)
                        [runtime oclStopCudaForSelectedDeviceNum:selectedDeviceNum];
                    [runtime release];
                    [renderInfo release];
                    [renderState release];
                    return nil;
                }
                [imageArray setTileAtRow:[tile row]
                                     col:[tile column]
                              withObject:rep
                                withTime:[imageArray timeAtIndex:i] + cummTime - originalRenderTime
                     withBatchesRendered:[tile numBatches]];
                //                logRenderState(@"recharge after tile render", tiles, imageArray, i, 0);
                
                if ([renderStateDoc saveRenderState]) {
                    // all other device's histograms + host accum buf have already been summed into the main render target to do density estimation
                    // save render target to host accum buffer is the goal, so it will be saved from there
                    if ([runtime hostAccumBuffer]) {
                        [renderState->runtime->addToAccumBufferForSelectedDeviceNum:renderState->selectedDeviceNum];
                    }
                    [RenderStateDocument saveRenderStateWithContext:renderState
                                                     cummRenderTime:cummTime
                                                  renderTimeForTile:0 // recorded already in imageArray
                                                          subSample:[tile numBatches]
                                                         bladeIndex:0
                                                    initialImageRep:nil
                                                               show:NO
                                                             paused:NO];
                    //                    [RenderStateDocument saveRenderTargetState:renderState]; -- already saved by renderTileCore's finishFrame
                }
                for (int selectedDeviceNum = 0; selectedDeviceNum < [runtime numDevices]; selectedDeviceNum++)
                    [runtime oclStopCudaForSelectedDeviceNum:selectedDeviceNum];
                [runtime release];
            }
        }
    }
    // we want to just do the postProcessing pass
    if (postProcessAgain) {
        unsigned maxBatches = maxProgress(tiles);
        for (int i = 0; i < [tiles count]; i++) {
            [renderStateDoc setTileIndex:i];
            
            time_t time;
            FlameTile *tile         = [tiles tileAtIndex:i];
            Flam4CommonRuntime *runtime = makeRuntime(renderState, gpuPlatformInUse, deviceContext, [tile numBatches]);
            
            [renderInfo setWidth:[tile width]];
            [renderInfo setHeight:[tile height]];
            [renderInfo setSupersample:[tile flame]->params.oversample];
            
            allocateBatchesToBlades(deviceContext, [tile blades], [tile numBatches], [tile width], [tile height]);
            
            renderState->TileIndex:i];
            renderState->Runtime:runtime];
            renderState->CummRenderTime:&time];
            
            size_t batchesSoFar = cumulativeProgress([renderState imageArray], i, [tile numBatches]);
            if ([[renderState progressListener] isMemberOfClass:[PngSettingsController class]] &&
                [renderState progressListener]) {
                if ([[renderState progressListener] currentProgress] > batchesSoFar)
                    NSLog(@"Retrograde progress");
                uint fuseIterations = [renderState->runtime->fuseIterations][renderState->selectedDeviceNum];
                [[renderState progressListener] performSelectorOnMainThread:@selector(progressUpdate:)
                                                                 withObject:[RenderProgress renderProgresssWithProgress:batchesSoFar
                                                                                                            elapsedTime:((double)*[renderState cummRenderTime]/1000.)
                                                                                                              tileIndex:i
                                                                                                             maxBatches:maxBatches
                                                                                                         fuseIterations:fuseIterations
                                                                                                            frameNumber:[[renderState renderInfo] frameNumber]]
                                                              waitUntilDone:NO];
            }
            NSBitmapImageRep *rep = nil;
            if ([runtime numDevices] == 1) {
                rep = RenderTileCore(renderState,
                                     firstFe,
                                     elc,
                                     [tile numBatches],
                                     0,
                                     nil,
                                     gpuPlatformInUse,
                                     deviceContext,
                                     NO);
            }
            else { // multiple render device case
                rep = renderTileCoreThreaded(renderState,
                                             firstFe,
                                             elc,
                                             [tile numBatches],
                                             0,
                                             nil,
                                             [renderStateDoc continuousMode]);
            }
            if (rep == nil) {
                for (int selectedDeviceNum = 0; selectedDeviceNum < [runtime numDevices]; selectedDeviceNum++)
                    [runtime oclStopCudaForSelectedDeviceNum:selectedDeviceNum];
                [runtime release];
                [renderInfo release];
                [renderState release];
                return nil;
            }
            [imageArray setTileAtRow:[tile row]
                                 col:[tile column]
                          withObject:rep
                            withTime:[imageArray timeAtIndex:i] 
                 withBatchesRendered:[tile numBatches]];
            
            for (int selectedDeviceNum = 0; selectedDeviceNum < [runtime numDevices]; selectedDeviceNum++)
                [runtime oclStopCudaForSelectedDeviceNum:selectedDeviceNum];
            [runtime release];
        }
    }
    NSBitmapImageRep *rep = [imageArray combinedImageRep:profileData];
    
    [renderStateDoc release];
    [renderInfo release];
    [renderState release];
    return rep;
}

#endif



#endif
