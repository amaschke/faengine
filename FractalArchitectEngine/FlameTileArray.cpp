//
//  FlameTileArray.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/15/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "FlameTileArray.hpp"
#include "Flame.hpp"
#include "FlameTile.hpp"
#include "VariationSet.hpp"
#include "FlameExpanded.hpp"
#include "common.hpp"
#include "ElectricSheepStuff.hpp"
#include "ImageRepTileArray.hpp"

#ifdef __linux
namespace std {
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
}
#endif


FlameTileArray::FlameTileArray(uint _rows, uint _cols, bool _flipped, uint _supersample)
:
    blades(),
    tiles(),
    rows(_rows),
    cols(_cols),
    flipped(_flipped),
    backgroundImage(),
    supersample(_supersample),
    width(0.f),
    height(0.f)
{
    tiles.reserve(_rows * _cols);
    for (int i = 0; i < _rows * _cols; i++)
        tiles.emplace_back();
}

// calculate the flames for each tile
FlameTileArray::FlameTileArray(uint _rows,
                               uint _cols,
                               SharedBlades & _blades,
                               float _width,
                               float _height,
                               bool _flipped,
                               bool transparent,
                               uint _supersample,
                               const SharedVariationSet & variationSet)
: FlameTileArray(_rows, _cols, _flipped, _supersample)
{
    width = _width;
    height = _height;
    blades = _blades;

    Flame *flame = this->flame();
    
    if (rows == 1 && cols == 1) {
        tiles.clear();
        tiles.push_back(std::make_unique<FlameTile>(blades, 0, 0, width, height, transparent, false));
        return;
    }
    float fwidth  = roundf(flame->params.size[0] * flame->params.scale * powf(2.f, flame->params.zoom)); // normalized file coordinates
    float fheight = roundf(flame->params.size[1] * flame->params.scale * powf(2.f, flame->params.zoom));
    float overlap = supersample * FlameTile::overlap();
    
    // split the total window into tiles
    float cumHeight  = 0.f;
    float fcumHeight = 0.f;
    if (_flipped) {
        for (int i = _rows - 1; i >= 0; i--) {  // do rows in reverse order
            float tileHeight  = tileHeightAtRow(i);
            float ftileHeight = tileHeight/height *fheight;
            handleColumns(_cols,
                          fwidth,
                          fheight,
                          fcumHeight,
                          overlap,
                          i,
                          transparent,
                          variationSet);
            
            cumHeight  += tileHeight  - 2*overlap;
            fcumHeight += ftileHeight - 2*overlap/height*fheight;
        }
    }
    else {
        for (int i = 0; i < _rows; i++) {  // do rows in forward order
            float tileHeight  = tileHeightAtRow(i);
            float ftileHeight = tileHeight/height *fheight;
            handleColumns(_cols,
                          fwidth,
                          fheight,
                          fcumHeight,
                          overlap,
                          i,
                          transparent,
                          variationSet);
            
            cumHeight  += tileHeight  - 2*overlap;
            fcumHeight += ftileHeight - 2*overlap/height*fheight;
        }
    }
}

FlameTileArray::~FlameTileArray()
{
    delete backgroundImage;
}

void FlameTileArray::tileFlame(struct Flame *flame,
                               float width,
                               float height,
                               float tileWidth,
                               float tileHeight,
                               float cumWidth,
                               float cumHeight,
                               const SharedVariationSet & variationSet)
{
    // calculate the new size and centers for each tile
    // first get the centers of each tile in view coordinates
    // then unrotate & untranslate to get the new centers in normalized fractal coordinates
    float x0 = (cumWidth  + tileWidth/2.0f  - width/2.0f)/(flame->params.scale * powf(2.f, flame->params.zoom));
    float y0 = (cumHeight + tileHeight/2.0f - height/2.0f)/(flame->params.scale * powf(2.f, flame->params.zoom));
    
    float x1, y1;
    if (variationSet->is3DCapable) {
        x1 = x0 * cosf(flame->params.rotation) - y0 * sinf(flame->params.rotation);
        y1 = x0 * sinf(flame->params.rotation) + y0 * cosf(flame->params.rotation);
    }
    else {
        x1 = x0 * cosf(-flame->params.rotation) - y0 * sinf(-flame->params.rotation);
        y1 = x0 * sinf(-flame->params.rotation) + y0 * cosf(-flame->params.rotation);
    }
    flame->params.size[0] = tileWidth/flame->params.scale/powf(2.f, flame->params.zoom);
    flame->params.size[1] = tileHeight/flame->params.scale/powf(2.f, flame->params.zoom);
    
    flame->params.center[0] = x1 + flame->params.center[0];
    flame->params.center[1] = y1 + flame->params.center[1];
}

float FlameTileArray::tileWidthAtCol(int col)
{
    float overlap = supersample * FlameTile::overlap();
    float  base = (col == cols - 1) ? (width - (cols - 1) * floorf(width/cols)) : floorf(width/cols);
    return base + ((col > 0) ? overlap : 0.f) + ((col < cols - 1) ? overlap : 0.f);
}

float FlameTileArray::tileHeightAtRow(int row)
{
    float overlap = supersample * FlameTile::overlap();
    float  base = (row == rows - 1) ? (height - (rows - 1) * floorf(height/rows)) : floorf(height/rows);
    return base + ((row > 0) ? overlap : 0.f) + ((row < rows - 1) ? overlap : 0.f);
}

float FlameTileArray::tileXAtCol(int col)
{
    float overlap = supersample * FlameTile::overlap();
    int baseX     = col * (int)floorf(width/cols);
    return baseX - ((col > 0) ? overlap : 0.f);
}

float FlameTileArray::tileYAtRow(int row)
{
    float overlap = supersample * FlameTile::overlap();
    int baseY     = row * (int)floorf(height/rows);
    return baseY - ((row > 0) ? overlap : 0.f);
}

FA::Image * FlameTileArray::tileImageforTileAtRow(int row, int col, FA::Image &image)
{
    return image.clippedImage(tileXAtCol(col), tileYAtRow(row), tileWidthAtCol(col), tileHeightAtRow(row));
}

void FlameTileArray::handleColumns(int _cols,
                                   float fwidth,
                                   float fheight,
                                   float fcumHeight,
                                   float overlap,
                                   int i,
                                   bool transparent,
                                   const SharedVariationSet & variationSet)
{
    float tileHeight  = tileHeightAtRow(i);
    float ftileHeight = tileHeight/height *fheight;
    float cumWidth  = 0.f;
    float fcumWidth = 0.f;
    
    for (int j = 0; j < _cols; j++) {
        float tileWidth   = tileWidthAtCol(j);
        float ftileWidth  = tileWidth/width *fwidth;
        
        tiles[i * _cols + j] = std::make_unique<FlameTile>(blades, i, j, tileWidth, tileHeight, transparent, rows*cols > 1);
		SharedFlameTile & tile = tiles[i * _cols + j];
        
        if (backgroundImage)
            tile->backgroundImage =  tileImageforTileAtRow(i, j, *backgroundImage);
        
        for (SharedFlameExpanded & fe : *tile->blades) {
            Flame *flame     = fe->getFlame();
            
            // calculate the new size and centers for each tile
            // first get the centers of each tile in view coordinates
            // then unrotate & untranslate to get the new centers in normalized fractal coordinates
            float x0 = (fcumWidth  + ftileWidth/2.0f  - fwidth/2.0f)/(flame->params.scale * powf(2.f, flame->params.zoom));
            float y0 = (fcumHeight + ftileHeight/2.0f - fheight/2.0f)/(flame->params.scale * powf(2.f, flame->params.zoom));
            
            float x1, y1;
            if (variationSet->is3DCapable) {
                x1 = x0 * cosf(flame->params.rotation) - y0 * sinf(flame->params.rotation);
                y1 = x0 * sinf(flame->params.rotation) + y0 * cosf(flame->params.rotation);
            }
            else {
                x1 = x0 * cosf(-flame->params.rotation) - y0 * sinf(-flame->params.rotation);
                y1 = x0 * sinf(-flame->params.rotation) + y0 * cosf(-flame->params.rotation);
            }
            
            flame->params.size[0] = ftileWidth/flame->params.scale/powf(2.f, flame->params.zoom);
            flame->params.size[1] = ftileHeight/flame->params.scale/powf(2.f, flame->params.zoom);
            
            flame->params.center[0] = x1 + flame->params.center[0];
            flame->params.center[1] = y1 + flame->params.center[1];
        }
        
        cumWidth  += tileWidth  - 2*overlap;
        fcumWidth += ftileWidth - 2*overlap/width*fwidth;
    }
}

struct Flame * FlameTileArray::flame()
{
    if (blades->size() < 1)
        return nullptr;
    return blades->operator[](0)->getFlame();
}

struct Flame * FlameTileArray::nextFlame()
{
    if (blades->size() < 2)
        return nullptr;
    return blades->operator[](1)->getFlame();
}

struct ElectricSheepStuff * FlameTileArray::getElc()
{
    if (blades->size() == 0)
        return (struct ElectricSheepStuff *)nullptr;
    SharedFlameExpanded & fe = blades->operator[](0);
    return fe->getElc();
}

size_t FlameTileArray::numBatches()
{
    size_t batches = 0;
    for (SharedFlameTile & tile : tiles)
        batches += tile->numBatches;
    return batches;
}

size_t FlameTileArray::numBatchesAtIndex(size_t index)  {
    return tileAtIndex(index)->numBatches;
}

ImageRepTileArray * FlameTileArray::makeImageRepTileArray(bool _flipped)
{
    return new ImageRepTileArray(rows, cols, _flipped);
}


#ifdef __ORIGINAL__

- (NSString *)description
{
    return [NSString stringWithFormat:@"%s grid:[%u X %u] area:[%s X %s]", [super description], rows, cols,
            [NSString prettyFloat:width], [NSString prettyFloat:height]];
}

#endif
