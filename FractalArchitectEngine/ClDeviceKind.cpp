//
//  ClDeviceKind.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/17/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "ClDeviceKind.hpp"
#include "ClContext.hpp"
#include "ClProgram.hpp"
#include "VariationSet.hpp"
#include "Utilities.hpp"
#include "jsoncpp/json/json.h"

#ifdef _WIN32
#include "WindowsInterface.hpp"
#else
#include "CocoaInterface.h"
#endif

#include <iostream>
#include <string.h>

ClDeviceKind::ClDeviceKind(std::string & _kind, std::string & _vendor, const SharedDeviceContext & _deviceContext, cl_context _context)
:
    DeviceKind(_kind, _vendor, (SharedDeviceContext &)_deviceContext),
    deviceOpenClVersion(),
    platformInstances(),
    context(_context)
{}

ClDeviceKind::~ClDeviceKind()
{
    if (workGroupDims)
        free(workGroupDims);
    workGroupDims = nullptr;
    
    clReleaseContext(context);
    context = nullptr;
}

bool ClDeviceKind::createContext()
{
    return true;
}

#pragma mark Variation Set Support

ClProgram * ClDeviceKind::programForVariationSetUuid(const std::string & variationSetUuid, duration<double> & buildTime)
{
	SharedDeviceProgram & program = programs[variationSetUuid];

    if (! program)
        return makeProgramFromSource(VariationSet::variationSetForUuid(variationSetUuid), false, buildTime);
    else
        return dynamic_cast<ClProgram *>(program.get());
}

ClProgram * ClDeviceKind::programForVariationSet(const SharedVariationSet & variationSet, duration<double> & buildTime)
{
    SharedDeviceProgram & _program = programs[variationSet->uuid];
    ClProgram *program             = static_cast<ClProgram *>(_program.get());
    if (! program) {
        program = makeProgramFromSource(variationSet, false, buildTime);
        return program;
    }
    else {
        buildTime = duration<double>(0.);
        return program;
    }
}

void ClDeviceKind::removeProgramForVariationSetUuid(std::string &variationSetUuid)
{
    programs.erase(variationSetUuid);
}

ClProgram * ClDeviceKind::makeProgramFromSource(const SharedVariationSet & variationSet, bool forceRebuild, duration<double> & buildTime)
{
    SharedClContext clContext = std::dynamic_pointer_cast<ClContext>(deviceContext.lock());

    std::shared_ptr<ClProgram> clProgram = std::make_shared<ClProgram>(clContext, shared_from_this(), forceRebuild, variationSet);
    programs[variationSet->uuid]         = clProgram;
    buildTime                            = clProgram->buildTime;
    return clProgram.get();
}

#pragma mark Serialization

Json::Value ClDeviceKind::makeJson()
{
    Json::Value value(Json::objectValue);
    
    value["name"]      = kind;
    value["OSVersion"] = osVersion();
    value["uuid"]      = stringWithUUID();
    return value;
}

Json::Value ClDeviceKind::makeJsonForDeviceKindWithUuid(const std::string & uuid)
{
    Json::Value value(Json::objectValue);
    
    value["name"]      = kind;
    value["OSVersion"] = osVersion();
    value["uuid"]      = uuid;
    return value;
}

// find Json for binary that may or may not be valid for the current OS version, returns nil if no Json entry exists for that name
Json::Value & ClDeviceKind::existingJsonForDeviceName(const std::string & name, Json::Value & kindsJson)
{
    return kindsJson[name];
}

// find Json for binary that may or may not be valid for the current OS version, returns nil if no Json found
Json::Value & ClDeviceKind::existingJsonForDeviceName(const std::string & name)
{
    Json::Value kindsJson = DeviceProgram::readDevicesJson();
    return ClDeviceKind::existingJsonForDeviceName(name, kindsJson);
}

Json::Value & ClDeviceKind::existingJson()
{
    return ClDeviceKind::existingJsonForDeviceName(kind);
}

bool ClDeviceKind::JsonIsValid(Json::Value & Json)
{
    double version = Json["OSVersion"].asDouble();
    return version == osVersion();
}

enum DeviceKindJsonStatus ClDeviceKind::checkJsonStatus(Json::Value & kindsJson)
{
    if (! kindsJson)
        return noJsonFile;
    
    Json::Value & Json = ClDeviceKind::existingJsonForDeviceName(kind, kindsJson);
    if (Json.isNull())
        return noJsonEntry;
    if (! ClDeviceKind::JsonIsValid(Json))
        return binaryInvalid;
    return jsonOK;
}

// process the device kind and return its uuid
const std::string ClDeviceKind::processDeviceKind()
{
    Json::Value json;;
    Json::Value kindsJson = DeviceProgram::readDevicesJson();
    
    switch (checkJsonStatus(kindsJson)) {
        case noJsonFile:
        {
            kindsJson = Json::Value();
            json      = makeJson();
            kindsJson[kind] = json;
            ClProgram::writeDevicesJson(kindsJson);
            rebuild = true;
        }
            break;
        case noJsonEntry:
        {
            json = makeJson();
            kindsJson[kind] = json;
            ClProgram::writeDevicesJson(kindsJson);
            rebuild = true;
        }
            break;
        case binaryInvalid:
        {
            json            = ClDeviceKind::existingJsonForDeviceName(kind, kindsJson);
            kindsJson[kind] = makeJsonForDeviceKindWithUuid(json["uuid"].asString());
            ClProgram::writeDevicesJson(kindsJson);
            rebuild = true;
        }
            break;
        case jsonOK:
        default:
            rebuild = false;
            break;
    }
    json = ClDeviceKind::existingJsonForDeviceName(kind, kindsJson);
    return json["uuid"].asString();
}

#pragma mark Properties

static const char *kernelTest =
"__kernel void testKernel(__global const float *a, "
"__global const float *b, "
"__global float *result) "
"{"
"    int gid = get_global_id(0); "
"    result[gid] = a[gid] + b[gid]; "
"}";

uint ClDeviceKind::determineWarpSizeForDevice(void * _device)
{
    cl_device_id device = (cl_device_id)_device;
    size_t preferredWorkGroupSize = 1;
    size_t length = strlen(kernelTest);
    cl_int errNum = CL_SUCCESS;
    
    // Create program from source
    cl_program myprogram = clCreateProgramWithSource(context,
                                                     1,
                                                     &kernelTest,
                                                     &length,
                                                     &errNum);
    if (errNum != CL_SUCCESS)
        return (uint)preferredWorkGroupSize;
    
    // Build program from either source or binaries
    errNum = clBuildProgram(myprogram,
                            1,
                            &device,
                            NULL,
                            NULL,
                            NULL);
    
    if (errNum != CL_SUCCESS) {
        clReleaseProgram(myprogram);
        return (uint)preferredWorkGroupSize;
    }
    cl_kernel testKernel = clCreateKernel(myprogram, "testKernel", &errNum);
    
    errNum = clGetKernelWorkGroupInfo(testKernel, device,
                                      CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,
                                      sizeof(preferredWorkGroupSize),
                                      &preferredWorkGroupSize,
                                      NULL);
    
    clReleaseKernel(testKernel);
    clReleaseProgram(myprogram);
    return (uint)preferredWorkGroupSize;
}

cl_device_id ClDeviceKind::deviceIDForInstanceIndex(size_t index)
{
    if (clDeviceInstances.size() == 0)
        return NULL;
    return (cl_device_id)clDeviceInstances[index];
}

void ClDeviceKind::deviceKindProperties()
{
    cl_int errNum;
    cl_device_id deviceID = (cl_device_id)clDeviceInstances[0];
    
    maxWorkgroupDims = 3;
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_TYPE, sizeof(cl_device_type), &deviceType, NULL);
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(cl_uint), &maxWorkgroupDims, NULL);
    
    workGroupDims = (size_t *)malloc(sizeof(size_t)*maxWorkgroupDims);
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(size_t)*maxWorkgroupDims, workGroupDims, NULL);
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, sizeof(cl_ulong), &maxConstantBufSize, NULL);
    
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t),   &maxWorkGroupSize, NULL);
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_MAX_MEM_ALLOC_SIZE,  sizeof(cl_ulong), &maxMemAllocSize, NULL);
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_GLOBAL_MEM_SIZE,     sizeof(cl_ulong), &globalMemSize, NULL);
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_HOST_UNIFIED_MEMORY, sizeof(cl_bool),  &unifiedMemory, NULL);
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_IMAGE_SUPPORT,       sizeof(cl_bool),  &deviceImageSupport, NULL);
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_LOCAL_MEM_SIZE,      sizeof(cl_ulong), &localMemSize, NULL);
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_LOCAL_MEM_TYPE,      sizeof(cl_device_local_mem_type), &localMemType, NULL);
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_MAX_COMPUTE_UNITS,   sizeof(cl_uint),  &maxComputeUnits, NULL);
    
    deviceName          = getDeviceName();
    deviceOpenClVersion = getDeviceOpenClVersion();
}

void ClDeviceKind::setupSubDevicesFor(void * _cpuDeviceID)
{
    cl_device_id cpuDeviceID = (cl_device_id)_cpuDeviceID;

    if (maxComputeUnits == 1)
        return;
    
    cl_uint maxSubDevices = 0;
    cl_int errNum = clGetDeviceInfo(cpuDeviceID, CL_DEVICE_PARTITION_MAX_SUB_DEVICES,   sizeof(cl_uint),  &maxSubDevices, NULL);
    if (maxSubDevices == 0) // cant partition
        return;
    
    static cl_device_partition_property pprops[4];
    size_t retSize = 0;
    errNum = clGetDeviceInfo(cpuDeviceID, CL_DEVICE_PARTITION_PROPERTIES,   4*sizeof(cl_device_partition_property),
                             &pprops, &retSize);
    
    if (pprops[0] == 0) // cant partition
        return;
    
    clContext()->subDeviceCount  = maxComputeUnits - 1;
    clContext()->subDeviceIDs    = (void* *)malloc(sizeof(cl_device_id) * (maxComputeUnits - 1));
    clContext()->subdeviceQueues = (cl_command_queue *)malloc(sizeof(cl_command_queue) * (maxComputeUnits - 1));
    
    for (uint count = 1; count <= maxComputeUnits; count++) {
        static cl_device_partition_property props[4];
        props[0] = CL_DEVICE_PARTITION_BY_COUNTS;
        props[1] = count;
        props[2] = CL_DEVICE_PARTITION_BY_COUNTS_LIST_END;
        props[3] = 0;                             // End of the property list
        
        cl_device_id subdevice_id[1];
        cl_uint num_entries = 1;
        cl_uint numDevices = 0;
        
        // Create the sub-devices:
        errNum = clCreateSubDevices(cpuDeviceID, props, num_entries, subdevice_id, &numDevices);
        if (errNum != CL_SUCCESS)
            return;
        clContext()->subDeviceIDs[count - 1]    = subdevice_id[0];
        clContext()->subdeviceQueues[count - 1] = clCreateCommandQueue(context, subdevice_id[0], 0, &errNum);
    }
}

std::string ClDeviceKind::getDeviceVendor()
{
    cl_int errNum;
    cl_device_id deviceID = (cl_device_id)clDeviceInstances[0];
    size_t paramValueSize;
    
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_VENDOR, 0, NULL, &paramValueSize);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return std::string();
    }
    
    char *_vendor = (char *)alloca(sizeof(char) * paramValueSize);
    
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_VENDOR, paramValueSize, _vendor, NULL);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return std::string();
    }
    return _vendor;
}

std::string ClDeviceKind::getDeviceName()
{
    cl_int errNum;
    cl_device_id deviceID = (cl_device_id)clDeviceInstances[0];
    size_t paramValueSize;
    
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_NAME, 0, NULL, &paramValueSize);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return std::string();
    }
    
    char *_deviceName = (char *)alloca(sizeof(char) * paramValueSize);
    
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_NAME, paramValueSize, _deviceName, NULL);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return std::string();
    }
    return _deviceName;
}

std::string ClDeviceKind::getDeviceOpenClVersion()
{
    cl_int errNum;
    cl_device_id deviceID = (cl_device_id)clDeviceInstances[0];
    size_t paramValueSize;
    
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_VERSION, 0, NULL, &paramValueSize);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return std::string();
    }
    
    char *_deviceVersion = (char *)alloca(sizeof(char) * paramValueSize);
    
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_VERSION, paramValueSize, _deviceVersion, NULL);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return std::string();
    }
    return _deviceVersion;
}

std::string ClDeviceKind::getDeviceExtensions()
{
    cl_int errNum;
    cl_device_id deviceID = (cl_device_id)clDeviceInstances[0];
    size_t paramValueSize;
    
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_EXTENSIONS, 0, NULL, &paramValueSize);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return std::string();
    }
    
    char *_deviceExtensions = (char *)alloca(sizeof(char) * paramValueSize);
    
    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_EXTENSIONS, paramValueSize, _deviceExtensions, NULL);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return std::string();
    }
    return _deviceExtensions;
}



#ifdef __ORIGINAL__



- (NSData *)deviceIDs
{
    cl_device_id *deviceIDs = (cl_device_id *)malloc(sizeof(cl_device_id)*[instances count]);
    for (int i = 0; i < [instances count]; i++) {
        deviceIDs[i] = (cl_device_id)[[instances objectAtIndex:i] pointerValue];
    }
    return [NSData dataWithBytesNoCopy:deviceIDs length:sizeof(cl_device_id)*[instances count]];
}

- (NSString *)summary
{
    NSString *_deviceType = [self isCPU] ? @"CPU:" : @"GPU:";
    if ([kind rangeOfString:vendor].location != NSNotFound)
        vendor = @"";
        return [NSString stringWithFormat:@"%s %s%s%s %s",
                _deviceType, vendor, vendor ? @" " : @"",
                kind, @""];
}



#endif
