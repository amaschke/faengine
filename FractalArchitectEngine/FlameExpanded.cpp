//
//  FlameExpanded.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/6/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include <memory>
#include <iostream>

#include "FlameExpanded.hpp"
#include "Flame.hpp"
#include "ElectricSheepStuff.hpp"
#include "VariationChain.hpp"
#include "XaosMatrix.hpp"
#include "XaosMatrixVector.hpp"
#include "VariationSet.hpp"
#include "common.hpp"
#include "VariationGroup.hpp"
#include "VarParInstance.hpp"
#include "Utilities.hpp"
#include "Variation.hpp"
#include "VariationParameter.hpp"
#include "ColorMap.hpp"
#include "SpatialFilter.hpp"

#ifdef __linux
namespace std {
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
}
#endif

FlameExpanded::FlameExpanded()
: std::enable_shared_from_this<FlameExpanded>(),
    flame(),
    elc  (),
    meta     (),
    uuid(),
    variationSetUuid(),
    rgbCurve(),
    redCurve(),
    greenCurve(),
    blueCurve(),
    applyToBackground(false),
    qualityAdjust(1.f),
    backgroundImage(),
    renderMode(renderModeNormal),
    compositeColor(),
    imagePath()
{
}

FlameExpanded::FlameExpanded(SharedFlame && _flame,
                             SharedElc && _elc,
                             SharedFlameMetaData && _meta,
                             const std::string &_uuid,
                             const std::string &_variationSetUuid)
: std::enable_shared_from_this<FlameExpanded>(),
flame(std::move(_flame)),
elc  (std::move(_elc)),
meta     (std::move(_meta)),
uuid(_uuid),
variationSetUuid(_variationSetUuid), rgbCurve(),
redCurve(),
greenCurve(),
blueCurve(),
applyToBackground(false),
qualityAdjust(1.f),
backgroundImage(),
renderMode(renderModeNormal),
compositeColor(),
imagePath()
{}

FlameExpanded::FlameExpanded(const FlameExpanded &f)
: std::enable_shared_from_this<FlameExpanded>(f),
    flame(std::make_shared<Flame>(*f.flame)),
    elc  (std::make_shared<ElectricSheepStuff>(*f.elc)),
    meta     (std::make_shared<FlameMetaData>(*f.meta)),
    uuid(f.uuid),
    variationSetUuid(f.variationSetUuid),
    rgbCurve(f.rgbCurve),
    redCurve(f.redCurve),
    greenCurve(f.greenCurve),
    blueCurve(f.blueCurve),
    applyToBackground(f.applyToBackground),
    qualityAdjust(f.qualityAdjust),
    backgroundImage(f.backgroundImage),
    renderMode(f.renderMode),
    compositeColor(f.compositeColor),
    imagePath(f.imagePath)
{}

// create document wrapper for this single flame instance
SharedElc FlameExpanded::createDefaultElectricSheepStuff(int transformCount)
{
    SharedElc elc = std::make_shared<ElectricSheepStuff>();
    elc->xaosMatrix = transformCount == 0 ? std::make_unique<XaosForXformsVector>() : std::make_unique<XaosForXformsVector>(transformCount);
    return elc;
}

size_t FlameExpanded::countOfXforms() const
{
    return getFlame()->params.numTrans;
}

void FlameExpanded::splitTransformAtIndex(uint splitIndex)
{
    UniqueXaosMatrix     _xaosMatrix    = std::make_unique<XaosForXformsVector>(*xaosMatrix());
    getElc()->xaosMatrix                   = std::move(_xaosMatrix);
    getFlame()->splitTransformAt(splitIndex);
}

void FlameExpanded::increaseXformCountTo(unsigned xformCount)
{
    if (xformCount <= countOfXforms())
        return;
    size_t extra = xformCount - countOfXforms();
    
    struct Flame *flame = getFlame();
    for (unsigned j = 0; j < extra; j++) {
        float maxWeight = flame->trans[0].weight;
        unsigned index  = 0;
        for (unsigned i = 1; i < flame->params.numTrans; i ++)
        {
            if (flame->trans[i].weight > maxWeight) {
                maxWeight = flame->trans[i].weight;
                index     = i;
            }
        }
        splitTransformAtIndex(index);
    }
}

bool FlameExpanded::nonBlankChains() const
{
    for (int i = 0; i < flame->params.numTrans; i++) {
        // create a compatible result chain
        const SharedVariationChain &chain = flame->xformVarChains[i];
        if (! chain->checkForNonBlankChain())
            return false;
    }
    return true;
}

#pragma mark Properties

void FlameExpanded::setupMeta()
{
    meta  =  std::make_shared<FlameMetaData>(uuid, variationSetUuid);
    
    meta->rgbCurve   = rgbCurve;
    meta->redCurve   = redCurve;
    meta->greenCurve = greenCurve;
    meta->blueCurve  = blueCurve;
    meta->applyToBackground = applyToBackground;
    meta->qualityAdjust = qualityAdjust;
//    meta->renderMode = renderMode;
//    meta->compositeColor = [compositeColor retain];
//    meta->imagePath = [imagePath retain];
//    meta->backgroundImage = [backgroundImage retain];
}

FlameMetaData * FlameExpanded::getMeta()
{
    if (! meta)
        setupMeta();
    return meta.get();
}

void FlameExpanded::setMeta(FlameMetaData *_meta)
{
    meta  =  std::make_shared<FlameMetaData>(*_meta);
}

void FlameExpanded::setMeta(SharedFlameMetaData &&_meta)
{
    meta  = std::move(_meta);
}

void FlameExpanded::setUuid(const std::string &_uuid)
{
    if (uuid == _uuid)
        return;
    
    uuid = _uuid;
    if (! meta)
        setupMeta();
    meta->setUuid(_uuid);
}

void FlameExpanded::setVariationSetUuid(const std::string &_variationSetUuid)
{
    if (variationSetUuid == _variationSetUuid)
        return;
    
    if (! meta)
        setupMeta();
    variationSetUuid = _variationSetUuid;
    
    meta->setVariationSetUuid(variationSetUuid);
}

void FlameExpanded::setRgbCurve(SharedPointVector _rgbCurve)
{
    if (rgbCurve.get() == _rgbCurve.get())
        return;
    
    if (! meta)
        setupMeta();
    rgbCurve = _rgbCurve;
    
    meta->rgbCurve =_rgbCurve;
}

void FlameExpanded::setRedCurve(SharedPointVector _redCurve)
{
    if (redCurve.get() == _redCurve.get())
        return;
    
    if (! meta)
        setupMeta();
    redCurve = _redCurve;
    
    meta->redCurve =_redCurve;
}

void FlameExpanded::setGreenCurve(SharedPointVector _greenCurve)
{
    if (greenCurve.get() == _greenCurve.get())
        return;
    
    if (! meta)
        setupMeta();
    greenCurve = _greenCurve;
    
    meta->greenCurve =_greenCurve;
}

void FlameExpanded::setBlueCurve(SharedPointVector _blueCurve)
{
    if (blueCurve.get() == _blueCurve.get())
        return;
    
    if (! meta)
        setupMeta();
    blueCurve = _blueCurve;
    
    meta->blueCurve =_blueCurve;
}

void FlameExpanded::setApplyToBackground(bool _applyToBackground)
{
    if (! meta)
        setupMeta();
    
    applyToBackground = _applyToBackground;
    meta->applyToBackground = _applyToBackground;
}

void FlameExpanded::setQualityAdjust(float _qualityAdjust)
{
    if (! meta)
        setupMeta();
    qualityAdjust = _qualityAdjust;
    meta->qualityAdjust = _qualityAdjust;
}

bool FlameExpanded::operator==(const FlameExpanded &f)
{
    return uuid == f.uuid;
}

#pragma Color String Utilities

std::string rgbaToString(struct rgba & color) {
    static char s[50];
    snprintf(s, sizeof(s), "R:%s G:%s B:%s A:%s",
                   prettyFloat(color.r).c_str(),
                   prettyFloat(color.g).c_str(),
                   prettyFloat(color.b).c_str(),
                   prettyFloat(color.a).c_str());
    return std::string(s);
}

std::string hexadecimalValueOfAnNSColor(struct rgba & color)
{
    static char s[10];
    int red   = color.r*255.f;
    int green = color.g*255.f;
    int blue  = color.b*255.f;
    
    snprintf(s, sizeof(s), "#%02X%02X%02X", red, green, blue);
    
    return std::string(s);
}

#pragma mark Text Report

std::string FlameExpanded::headerTextReport()
{
    float scaleFactor   = flame->params.scale * powf(2.f, flame->params.zoom);
    float docViewWidth  = flame->params.size[0] * scaleFactor;
    float docViewHeight = flame->params.size[1] * scaleFactor;
    
    static char str[600];
    snprintf(str, sizeof(str), "center: %s x %s  size: %s x %s  pixelSize: %s x %s  rotate: %s  scale: %s  zoom: %s\n",
        prettyFloat(flame->params.center[0]).c_str(),
        prettyFloat(flame->params.center[1]).c_str(),
        prettyFloat(flame->params.size[0]).c_str(),
        prettyFloat(flame->params.size[1]).c_str(),
        prettyFloat(docViewWidth).c_str(),
        prettyFloat(docViewHeight).c_str(),
        prettyFloat(flame->params.rotation / (2.0*M_PI/360.0)).c_str(),
        prettyFloat(flame->params.scale).c_str(),
        prettyFloat(flame->params.zoom).c_str());
    std::string line1(str);
    
    snprintf(str, sizeof(str), "brightness: %s  gamma: %s  vibrancy: %s  gammaThreshold: %s superSampleWidth: %s alphaGamma: %s estimatorRadius: %u\n",
        prettyFloat(flame->params.brightness).c_str(),
        prettyFloat(flame->params.gamma).c_str(),
        prettyFloat(flame->params.vibrancy).c_str(),
        prettyFloat(flame->params.gammaThreshold).c_str(),
        prettyFloat(flame->params.supersampleWidth).c_str(),
        prettyFloat(flame->params.alphaGamma).c_str(),
        flame->params.estimatorRadius);
    std::string line2(str);

	static char buf1[50], buf2[50], buf3[50];

#ifdef _WIN32
	snprintf(buf1, sizeof(buf1), "%s (%s rads)", prettyFloat(180.f*flame->params.cam_yaw / M_PI).c_str(), prettyFloat(flame->params.cam_yaw).c_str());
	snprintf(buf2, sizeof(buf2), "%s (%s rads)", prettyFloat(180.f*flame->params.cam_pitch / M_PI).c_str(), prettyFloat(flame->params.cam_pitch).c_str());
	snprintf(buf3, sizeof(buf3), "%s (%s rads)", prettyFloat(180.f*flame->params.rotation / M_PI).c_str(), prettyFloat(flame->params.rotation).c_str());
#else
	snprintf(buf1, sizeof(buf1), "%s° (%s rads)", prettyFloat(180.f*flame->params.cam_yaw / M_PI).c_str(), prettyFloat(flame->params.cam_yaw).c_str());
	snprintf(buf2, sizeof(buf2), "%s° (%s rads)", prettyFloat(180.f*flame->params.cam_pitch / M_PI).c_str(), prettyFloat(flame->params.cam_pitch).c_str());
	snprintf(buf3, sizeof(buf3), "%s° (%s rads)", prettyFloat(180.f*flame->params.rotation / M_PI).c_str(), prettyFloat(flame->params.rotation).c_str());
#endif

    
    snprintf(str, sizeof(str), "Camera: yaw: %s  pitch: %s  roll: %s\n", buf1, buf2, buf3);
    std::string line3(str);

    snprintf(str, sizeof(str), "  perspective: %s  dof: %s zpos: %s clipToNDC: %s\ncam_x: %s  cam_y: %s  cam_z: %s  fov: %s  near: %s  orthowidth: %s\n",
        prettyFloat(flame->params.cam_perspective).c_str(),
        prettyFloat(flame->params.cam_dof).c_str(),
        prettyFloat(flame->params.cam_zpos).c_str(),
        flame->params.clipToNDC ? "YES" : "NO",
        prettyFloat(flame->params.cam_x).c_str(),
        prettyFloat(flame->params.cam_y).c_str(),
        prettyFloat(flame->params.cam_z).c_str(),
        prettyFloat(flame->params.cam_fov).c_str(),
        prettyFloat(flame->params.cam_near).c_str(),
        prettyFloat(flame->params.cam_orthowide).c_str());
    std::string line3b(str);

    std::string defaultFuseIters = elc->defaultFuseIters ? "YES" : "NO";
    snprintf(str, sizeof(str), "uuid:  %s defaultFuseIters: %s  qualityAdjust:%s cpuFuse: %s  gpuFuse: %s\n",
        uuid.c_str(),
        defaultFuseIters.c_str(),
        prettyFloat(qualityAdjust).c_str(),
        prettyFloat(elc->cpuFuseIterations).c_str(),
        prettyFloat(elc->gpuFuseIterations).c_str());
    std::string line4(str);
    
    std::string highlightClipping = flame->params.highlightPower >= 0.f? "On" : "Off";
    snprintf(str, sizeof(str), "Spatial Filter Weight: %s  Filter Shape: %s  Supersample: %s Highlight Clipping: %s \n",
            prettyFloat(elc->filter).c_str(),
            SpatialFilter::filterNameForFilterShapeEnum(elc->filterShape).c_str(),
            prettyFloat(flame->params.oversample).c_str(),
            highlightClipping.c_str());
    std::string line5(str);
    
    std::string currentBackground = rgbaToString(flame->params.background);
    snprintf(str, sizeof(str), "symmetryKind: %s  background: %s frames: %u\n",
            prettyFloat(flame->params.symmetryKind).c_str(),
            hexadecimalValueOfAnNSColor(flame->params.background).c_str(),
             flame->params.frame);
    std::string line6(str);
    
    SharedVariationSet variationSet = VariationSet::variationSetForUuid(variationSetUuid);
    snprintf(str, sizeof(str), "Variation Set Name: %s  UUID: %s\n", variationSet->name.c_str(), variationSet->uuid.c_str());
    std::string line8(str);
    
    return line1 + line2 + line3 + line3b + line4 + line5 + line6 + line8;
}

std::string FlameExpanded::curvesTextReportForCurve(SharedPointVector &curve, size_t cpCount, const std::string & which)
{
    std::string description;
    description.reserve(16*cpCount);
    description.append("\n").append(which).append(" Control Points:\n");
    
    const size_t nodesPerLine = 4;
    size_t nodeCount          = 0;

    for (size_t i = 0; i < cpCount; i++) {
        Point point = curve->operator[](i);
        
        static char str[20];
        snprintf(str, sizeof(str), "%zu", i);
        description.append(str).append(": (").append(prettyFloat(point.x)).append(", ").append(prettyFloat(point.y)).append(")");
        
        if (++nodeCount == nodesPerLine) {
            nodeCount = 0;
            description.append("\n");
        }
        else {
            description.append("  ");
        }
    }
    return description;
}

std::string FlameExpanded::curvesTextReport()
{
    std::string line1;
    if (rgbCurve) {
        line1 = curvesTextReportForCurve(rgbCurve, rgbCurve->size(), "Rgb");
        line1.append("\n");
    }
    std::string line2;
    if (redCurve) {
        line2 = curvesTextReportForCurve(redCurve, redCurve->size(), "Red");
        line2.append("\n");
    }
    std::string line3;
    if (greenCurve) {
        line3 = curvesTextReportForCurve(greenCurve, greenCurve->size(), "Green");
        line3.append("\n");
}
    std::string line4;
    if (blueCurve) {
        line4 = curvesTextReportForCurve(blueCurve, blueCurve->size(), "Blue");
        line4.append("\n");
}
    std::string applyCurves = applyToBackground ? "Apply curves to background" : "Don't apply curves to background";
    
    return applyCurves + line1 + line2 + line3 + line4 + "\n";
}

// xform #1  weight:1.77591932  colorIndex:0.81825006  color_sped:0.95410913  rotates:YES
//     linear:-0.06358564
//     julia:-0.61095572
//
// Pre: 1.04646778 0    0      Post: 1.      0        0
// 1.04646778 0    0            0.      1.       0

std::string FlameExpanded::xformTextReport(struct xForm *xform, SharedVariationChain chain, size_t which, bool isFinalXform, const SharedVariationSet variationSet)
{
    static char str[400];
    static char buf[40];
    
    snprintf(buf, sizeof(buf), "\nXform #%zu", which);
    snprintf(str, sizeof(str), "%s  weight: %s  colorIndex: %s  color_speed: %s  opacity: %s%% var_color: %s  rotates: %s\n",
            isFinalXform ? "Final xform" :  buf,
            prettyFloat(xform->weight).c_str(),
            prettyFloat(xform->color).c_str(),
            prettyFloat(0.5f - 0.5f * xform->symmetry).c_str(),
            prettyFloat(100.f * xform->opacity).c_str(),
            prettyFloat(xform->var_color).c_str(),
            isFinalXform ? "NO" : (xform->rotates != 0 ? "YES" : "NO"));
    std::string line1(str);
    
    std::string lineN;
    lineN.reserve(200);
    
    size_t groupIndex = 0;
    for (SharedVariationGroup & variationGroup : *chain) {
        lineN.append("Variation Group ").append(chain->groupNameForIndex(groupIndex++)).append(" ========\n");
        
        if (variationGroup->size() == 0) {
            lineN.append("    === Empty ===\n");
            continue;
        }
       
        for (std::string & key : keysSortedByValue(variationGroup->getDictionary(), VarParInstance::xformIndexPairCompare)) {
            if (VarParInstance::instanceNumOfKey(key) == std::string::npos)
                continue;
            
            Variation *pvariation = variationSet-> variationForKey(key);
            if (pvariation) {
                Variation & variation   = *pvariation;
                SharedVarParInstance varpar = variationGroup->operator[](key);
                if (varpar->floatValue == 0.f)
                    continue;
                
                size_t instanceNum = VarParInstance::instanceNumOfKey(key);
                std::string varS;
                
                snprintf(str, sizeof(str), "    %s #%zu: %s  ",
                        variation.getName().c_str(), instanceNum,  prettyFloat(variationGroup->operator[](key)->floatValue).c_str());
                varS.append(str);
                
                for (std::string & name :  keysSortedByValue(variation.getParameters(), VariationParameter::xformIndexPairCompare)) {
                    SharedParameter & param = variation.getParameters()[name];
                    std::string instancedParamKey = VarParInstance::makeInstancedKey(param->getKey(), instanceNum);
                    
                    snprintf(str, sizeof(str), "%s: %s  ",
                            param->getName().c_str(),
                            prettyFloat(variationGroup->operator[](instancedParamKey)->floatValue).c_str());
                    varS.append(str);
                }
                lineN.append(varS).append("\n");
            }
            
        }
    }
    lineN.append("\n");
    
    snprintf(str, sizeof(str), "    Pre:  %10.10f  %10.10f  %10.10f\n",
            xform->a,
            xform->b,
            xform->c);
    std::string lineM1(str);

    snprintf(str, sizeof(str), "          %10.10f  %10.10f  %10.10f\n",
            xform->d,
            xform->e,
            xform->f);
    std::string lineM2(str);

    snprintf(str, sizeof(str), "    Post: %10.10f  %10.10f  %10.10f\n",
            xform->pa,
            xform->pb,
            xform->pc);
    std::string lineM3(str);

    snprintf(str, sizeof(str), "          %10.10f  %10.10f  %10.10f\n",
            xform->pd,
            xform->pe,
            xform->pf);
    std::string lineM4(str);
    
    return line1 + lineN + lineM1 + lineM2 + lineM3 + lineM4;
}

std::string FlameExpanded::chaosTextReportForMatrix(XaosForXformsVector *matrix)
{
    std::string lineN;
    lineN.reserve(200);
    static char buf[40];
    
    for (int i = 0; i < matrix->num; i++) {
        snprintf(buf, sizeof(buf), "From #%u: To ", i + 1);
        lineN.append(buf);
        for (int j = 0; j < matrix->num; j++) {
            snprintf(buf, sizeof(buf), "#%u: %s %s  ", j + 1, prettyFloat(matrix->chaos(i, j)).c_str(), matrix->locked(i, j) ? "L" : " ");
            lineN.append(buf);
        }
        lineN.append("\n");
    }
    return "\nXaos Matrix:\n" + lineN;
}

std::string FlameExpanded::xformReport(struct xForm *xform, SharedVariationChain chain, size_t which, bool isFinalXform)
{
    static char str[400];
    static char buf[40];
    
    snprintf(buf, sizeof(buf), "\nXform #%zu", which);
    snprintf(str, sizeof(str), "%s  weight: %s  colorIndex: %s  color_speed: %s  opacity: %s%% var_color: %s  rotates: %s\n",
             isFinalXform ? "Final xform" :  buf,
             prettyFloat(xform->weight).c_str(),
             prettyFloat(xform->color).c_str(),
             prettyFloat(0.5f - 0.5f * xform->symmetry).c_str(),
             prettyFloat(100.f * xform->opacity).c_str(),
             prettyFloat(xform->var_color).c_str(),
             isFinalXform ? "NO" : (xform->rotates != 0 ? "YES" : "NO"));
    std::string line1(str);
    
    std::string lineN;
    lineN.reserve(200);
    
    size_t groupIndex = 0;
    for (SharedVariationGroup & variationGroup : *chain) {
        lineN.append("Variation Group ").append(chain->groupNameForIndex(groupIndex++)).append(" ========\n");
        
        if (variationGroup->size() == 0) {
            lineN.append("    === Empty ===\n");
            continue;
        }
        
        for (std::string & key : keysSortedByValue(variationGroup->getDictionary(), VarParInstance::xformIndexPairCompare)) {
            if (VarParInstance::instanceNumOfKey(key) == std::string::npos)
                continue;
            
            SharedVarParInstance varpar = variationGroup->operator[](key);
            if (varpar->floatValue == 0.f)
                continue;
            
            std::string varS;
            
            snprintf(str, sizeof(str), "    %s: %s  ",
                     key.c_str(),  prettyFloat(variationGroup->operator[](key)->floatValue).c_str());
            varS.append(str);
            lineN.append(varS).append("\n");
        }
    }
    lineN.append("\n");
    
    snprintf(str, sizeof(str), "    Pre:  %10.10f  %10.10f  %10.10f\n",
             xform->a,
             xform->b,
             xform->c);
    std::string lineM1(str);
    
    snprintf(str, sizeof(str), "          %10.10f  %10.10f  %10.10f\n",
             xform->d,
             xform->e,
             xform->f);
    std::string lineM2(str);
    
    snprintf(str, sizeof(str), "    Post: %10.10f  %10.10f  %10.10f\n",
             xform->pa,
             xform->pb,
             xform->pc);
    std::string lineM3(str);
    
    snprintf(str, sizeof(str), "          %10.10f  %10.10f  %10.10f\n",
             xform->pd,
             xform->pe,
             xform->pf);
    std::string lineM4(str);
    
    return line1 + lineN + lineM1 + lineM2 + lineM3 + lineM4;
}

std::string FlameExpanded::flameReport(Flame * flame)
{
    std::string s;

    for (int i = 0; i < flame->params.numTrans; i++)
        s.append(xformReport(&flame->trans[i],
                                 flame->xformVarChains[i],
                                 i + 1,
                                 false));
    for (int i = 0; i < flame->params.numFinal; i++)
        s.append(xformReport(&flame->finals[i],
                                 flame->finalVarChains[i],
                                 i + 1,
                                 true));
    return s;
}

std::string FlameExpanded::textReport()
{
    SharedVariationSet variationSet = VariationSet::variationSetForUuid(variationSetUuid);
    std::string s;
    
    s.append(FlameExpanded::headerTextReport());
    
    for (int i = 0; i < flame->params.numTrans; i++)
        s.append(xformTextReport(&flame->trans[i],
                                 flame->xformVarChains[i],
                                 i + 1,
                                 false,
                                 variationSet));
    for (int i = 0; i < flame->params.numFinal; i++)
        s.append(xformTextReport(&flame->finals[i],
                                 flame->finalVarChains[i],
                                 i + 1,
                                 true,
                                 variationSet));
    
    s.append(FlameExpanded::chaosTextReportForMatrix(elc->xaosMatrix.get())).append("\n");
    s.append(FlameExpanded::curvesTextReport()).append("\n");
    
    ColorMap *colorMap = ColorMap::makeColorMapFromFlame(flame.get(), elc->paletteMode);
    s.append(colorMap->info().c_str());
    return s;
}

#pragma mark Original

#ifdef __ORIGINAL__

- (enum RenderMode)renderMode
{
    return renderMode;
}

- (void)setRenderMode:(enum RenderMode)_renderMode
{
    if (! meta)
        [self setupMeta];
    renderMode = _renderMode;
    meta->renderMode = _renderMode;
}

- (NSImage *)backgroundImage
{
    return backgroundImage;
}

- (void)setBackgroundImage:(NSImage *)_backgroundImage
{
    if (backgroundImage == _backgroundImage)
        return;
    
    if (! meta)
        [self setupMeta];
    [backgroundImage release];
    backgroundImage = [_backgroundImage retain];
    
    meta->setBackgroundImage(_backgroundImage);
}

- (NSColor *)compositeColor
{
    return compositeColor;
}

- (void)setCompositeColor:(NSColor *)_compositeColor
{
    if (compositeColor == _compositeColor)
        return;
    
    if (! meta)
        [self setupMeta];
    
    [compositeColor release];
    compositeColor = [_compositeColor retain];
    
    meta->setCompositeColor(_compositeColor);
}

- (uint)estimatorRadius
{
    return [self flame]->params.estimatorRadius;
}

- (void)setEstimatorRadius:(uint)_estimatorRadius
{
    [self flame]->params.estimatorRadius = _estimatorRadius;
}

- (RenderModeSettings *)renderModeSettings
{
    RenderModeSettings *rms = [RenderModeSettings settings];
    [rms setBackgroundImage:[self backgroundImage]];
    [rms setRenderMode:[self renderMode]];
    [rms setCompositeColor:[self compositeColor]];
    [rms setImagePath:[self imagePath]];
    [rms setEstimatorRadius:[self estimatorRadius]];
    [rms setGamma:[self flame]->params.gamma];
    [rms setAlphaGamma:[self flame]->params.alphaGamma];
    return rms;
}

- (void)setRenderModeSettings:(RenderModeSettings *)settings
preserveEstimatorRadius:(BOOL)preserveEstimatorRadius
preserveCompositeColor:(BOOL)preserveCompositeColor
preserveGamma:(BOOL)preserveGamma
preserveAlphaGamma:(BOOL)preserveAlphaGamma
{
    if (! meta)
        [self setupMeta];
    
    [self setRenderMode:[settings renderMode]];
    [self setBackgroundImage:[settings backgroundImage]];
    if (!preserveCompositeColor)
        [self setCompositeColor:[settings compositeColor]];
    if (!preserveEstimatorRadius)
        [self setEstimatorRadius:[settings estimatorRadius]];
    if (!preserveGamma)
        [self flame]->params.gamma = [settings gamma];
        if (!preserveAlphaGamma)
            [self flame]->params.alphaGamma = [settings alphaGamma];
            [self setImagePath:[settings imagePath]];
}

- (NSString *)imagePath
{
    return imagePath;
}

- (void)setImagePath:(NSString *)_imagePath
{
    if (imagePath == _imagePath)
        return;
    
    if (! meta)
        [self setupMeta];
    [imagePath release];
    imagePath = [_imagePath retain];
    meta->setImagePath(_imagePath);
}

#endif


