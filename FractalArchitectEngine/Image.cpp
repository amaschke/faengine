//
//  Image.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 6/14/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//  Created by Steven Brodhead on 5/16/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "Image.hpp"
#include <string.h>

#ifdef __linux
namespace std {
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
}
#endif


FA::Image::Image()
:
    width(0),
    height(0),
    bitDepth(8),
    map("RGBA"),
    bitmap(nullptr),
    profile(),
    flipped(false), 
	transparent(false)
{}

FA::Image::Image(uint _width,
                 uint _height,
                 const std::string & _map,
                 uint _bitDepth,
				 bool _transparent,
                 uchar * _bitmap)
: width(_width), height(_height), bitDepth(_bitDepth), map(_map), bitmap(_bitmap), profile(), flipped(false), transparent(_transparent)
{}

FA::Image::Image(const Image & o)
:
    width(o.width),
    height(o.height),
    bitDepth(o.bitDepth),
    map(o.map),
    flipped(o.flipped),
    bitmap(new uchar[bytesPerRowFor(o.map, o.width, o.bitDepth) * o.height]),
    profile(std::make_unique<Blob>(*o.profile)),
	transparent(o.transparent)
{
    memcpy(bitmap.get(), o.bitmap.get(), bytesPerRowFor(o.map, o.width, o.bitDepth) * o.height);
}

// convert from RGBA to RGB Image
SharedImage FA::Image::RGBAtoRGB()
{
    SharedImage rep  = std::make_shared<FA::Image>(width, height, "RGB", bitDepth, transparent, new uchar[bytesPerRowFor("RGB", width, bitDepth) * height]);
    rep->profile     = std::move(std::make_unique<Blob>(*profile));
    rep->flipped     = flipped;
    rep->transparent = transparent;
    
    // convert from incoming rgba array to rgb array - flip upside down
    if (bitDepth == 32) {
        float* rgba = (float *)bitmap.get();
        float* rgb  = (float *)rep->bitmap.get();
        
        for (unsigned int y = 0; y < height; ++y)
        {
            for (unsigned int x = 0; x < width; ++x)
            {
                rgb[3*(y*width + x)    ] = rgba[4*(y*width + x)    ];
                rgb[3*(y*width + x) + 1] = rgba[4*(y*width + x) + 1];
                rgb[3*(y*width + x) + 2] = rgba[4*(y*width + x) + 2];
            }
        }
    }
    else if (bitDepth == 16) {
        unsigned short* rgba = (unsigned short *)bitmap.get();
        unsigned short* rgb  = (unsigned short *)rep->bitmap.get();
        
        for (unsigned int y = 0; y < height; ++y)
        {
            for (unsigned int x = 0; x < width; ++x)
            {
                rgb[3*(y*width + x)    ] = rgba[4*(y*width + x)    ];
                rgb[3*(y*width + x) + 1] = rgba[4*(y*width + x) + 1];
                rgb[3*(y*width + x) + 2] = rgba[4*(y*width + x) + 2];
            }
        }
    }
    else {
        unsigned char* rgba = (unsigned char *)bitmap.get();
        unsigned char* rgb  = (unsigned char *)rep->bitmap.get();
        
        for (unsigned int y = 0; y < height; ++y)
        {
            for (unsigned int x = 0; x < width; ++x)
            {
                rgb[3*(y*width + x)    ] = rgba[4*(y*width + x)    ];
                rgb[3*(y*width + x) + 1] = rgba[4*(y*width + x) + 1];  
                rgb[3*(y*width + x) + 2] = rgba[4*(y*width + x) + 2];  
            }
        }
    }
    return rep;
}

uint FA::Image::samplesPerPixel()
{
    if (map.length() == 0)
        return 4;
    
    return (map == "RGBA" || map == "rgba") ? 4 : 3;
}

uint FA::Image::bytesPerRow()
{
    if (map.length() == 0)
        return 0;
    uint bytesPerSample = bitDepth / 8;
    if (map == "RGBA" || map == "rgba")
        return 4 * bytesPerSample * width;
    else
        return 3 * bytesPerSample * width;
}

uint FA::Image::bytesPerRowFor(const std::string &map, uint width, uint bitDepth)
{
    if (map.length() == 0)
        return 0;
    uint bytesPerSample = bitDepth / 8;
    if (map == "RGBA" || map == "rgba")
        return 4 * bytesPerSample * width;
    else
        return 3 * bytesPerSample * width;
}

uint FA::Image::bitsPerPixel()
{
    if (map.length() == 0)
        return 0;
    if (map == "RGBA" || map == "rgba")
        return 4 * bitDepth;
    else
        return 3 * bitDepth;
}

FA::Image * FA::Image::clippedImage(uint x, uint y, uint clipWidth, uint clipHeight)
{
    if (x >= width || y >= height)
        return new FA::Image(0, 0, map, bitDepth, transparent, nullptr);
    
    if (x + clipWidth > width)
        clipWidth -= width - x - clipWidth;
    if (y + clipHeight > height)
        clipHeight -= width - y - clipHeight;
    
    FA::Image * image = new FA::Image(clipWidth, clipHeight, map, bitDepth, transparent,
                   new uchar[bytesPerRowFor(map, clipWidth, bitDepth) * clipHeight]);
    
    // combine them row by row
    uchar *pixels     = image->bitmap.get();
    uchar *tilePixels = bitmap.get() + bytesPerRow() * y + x * bytesPerPixel();
    
    for (uint i = y; i < y + clipHeight; i++) {  // for each row of pixels in reverse order
        memcpy((void *)pixels, tilePixels, image->bytesPerRow());
        
        pixels     += image->bytesPerRow();
        tilePixels  = bitmap.get() + bytesPerRow() * i + x * bytesPerPixel();
    }
    return image;
}

