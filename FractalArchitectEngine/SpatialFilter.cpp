//
//  SpatialFilter.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/13/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "SpatialFilter.hpp"
#include "Flame.hpp"
#include "ElectricSheepStuff.hpp"
#include <unordered_map>
#include <cstdlib>
#include <math.h>

#ifdef __linux
namespace std {
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
}
#endif

SpatialFilter::SpatialFilter(const char * _name, FilterFunction _func, float _support)
: name(_name), func(_func), support(_support)
{}

SpatialFilter::SpatialFilter(std::string & _name, FilterFunction _func, float _support)
: SpatialFilter(_name.c_str(), _func, _support)
{}

#define HERMITE_FILTER_SUPPORT (1.0f)
static float hermite_filter(float t) {
    /* f(t) = 2|t|^3 - 3|t|^2 + 1, -1 <= t <= 1 */
    if(t < 0.0)
        t = -t;
    if(t < 1.0)
        return((2.0 * t - 3.0) * t * t + 1.0);
    return(0.0);
}

#define BOX_FILTER_SUPPORT (0.5f)
static float box_filter(float t)    /* pulse/Fourier window */
{
    // make_clist() calls the filter function with t inverted (pos = left, neg = right)
    if ((t >= -0.5f) && (t < 0.5f))
        return 1.0f;
    else
        return 0.0f;
}

#define TENT_FILTER_SUPPORT (1.0f)
static float tent_filter(float t)   /* box (*) box, bilinear/triangle */
{
    if (t < 0.0f)
        t = -t;
    
    if (t < 1.0f)
        return 1.0f - t;
    else
        return 0.0f;
}

#define BELL_SUPPORT (1.5f)
static float bell_filter(float t)    /* box (*) box (*) box */
{
    if (t < 0.0f)
        t = -t;
    
    if (t < .5f)
        return (.75f - (t * t));
    
    if (t < 1.5f)
    {
        t = (t - 1.5f);
        return (.5f * (t * t));
    }
    
    return (0.0f);
}

#define B_SPLINE_SUPPORT (2.0f)
static float B_spline_filter(float t)  /* box (*) box (*) box (*) box */
{
    float tt;
    
    if (t < 0.0f)
        t = -t;
    
    if (t < 1.0f)
    {
        tt = t * t;
        return ((.5f * tt * t) - tt + (2.0f / 3.0f));
    }
    else if (t < 2.0f)
    {
        t = 2.0f - t;
        return ((1.0f / 6.0f) * (t * t * t));
    }
    
    return (0.0f);
}

// Dodgson, N., "Quadratic Interpolation for Image Resampling"
#define QUADRATIC_SUPPORT 1.5f
//static float quadratic(float t, const float R)
//{
//    if (t < 0.0f)
//        t = -t;
//    if (t < QUADRATIC_SUPPORT)
//    {
//        float tt = t * t;
//        if (t <= .5f)
//            return (-2.0f * R) * tt + .5f * (R + 1.0f);
//        else
//            return (R * tt) + (-2.0f * R - .5f) * t + (3.0f / 4.0f) * (R + 1.0f);
//    }
//    else
//        return 0.0f;
//}

float flam3_quadratic_filter(float x) {
    if (x < -1.5f)
        return(0.0f);
    
    if (x < -0.5f)
        return(0.5f * (x + 1.5f) * (x + 1.5f));
    if (x < 0.5f)
        return(0.75f - x*x);
    if (x < 1.5f)
        return(0.5f * (x - 1.5f) * (x - 1.5f));
    return(0.0);
}


//static float quadratic_interp_filter(float t)
//{
//    return quadratic(t, 1.0f);
//}
//
//static float quadratic_approx_filter(float t)
//{
//    return quadratic(t, .5f);
//}
//
//static float quadratic_mix_filter(float t)
//{
//    return quadratic(t, .8f);
//}


// Mitchell, D. and A. Netravali, "Reconstruction Filters in Computer Graphics."
// Computer Graphics, Vol. 22, No. 4, pp. 221-228.
// (B, C)
// (1/3, 1/3)  - Defaults recommended by Mitchell and Netravali
// (1, 0)	   - Equivalent to the Cubic B-Spline
// (0, 0.5)		- Equivalent to the Catmull-Rom Spline
// (0, C)		- The family of Cardinal Cubic Splines
// (B, 0)		- Duff's tensioned B-Splines.
static float mitchell(float t, const float B, const float C)
{
    float tt;
    
    tt = t * t;
    
    if(t < 0.0f)
        t = -t;
    
    if(t < 1.0f)
    {
        t = (((12.0f - 9.0f * B - 6.0f * C) * (t * tt))
             + ((-18.0f + 12.0f * B + 6.0f * C) * tt)
             + (6.0f - 2.0f * B));
        
        return (t / 6.0f);
    }
    else if (t < 2.0f)
    {
        t = (((-1.0f * B - 6.0f * C) * (t * tt))
             + ((6.0f * B + 30.0f * C) * tt)
             + ((-12.0f * B - 48.0f * C) * t)
             + (8.0f * B + 24.0f * C));
        
        return (t / 6.0f);
    }
    
    return (0.0f);
}

#define MITCHELL_SUPPORT (2.0f)
static float mitchell_filter(float t)
{
    return mitchell(t, 1.0f / 3.0f, 1.0f / 3.0f);
}

#define CATMULL_ROM_SUPPORT (2.0f)
static float catmull_rom_filter(float t)
{
    return mitchell(t, 0.0f, .5f);
}

static double sinc(double x)
{
    x = (x * M_PI);
    
    if ((x < 0.01f) && (x > -0.01f))
        return 1.0f + x*x*(-1.0f/6.0f + x*x*1.0f/120.0f);
    
    return sin(x) / x;
}

#define HANNING_FILTER_SUPPORT (1.0f)
float hanning_filter(float x) {
    return(0.5f + 0.5f * cos(M_PI * x));
}

#define HAMMING_FILTER_SUPPORT (1.0f)
float hamming_filter(float x) {
    return(0.54f + 0.46f * cos(M_PI * x));
}

static float clean(double t)
{
    const float EPSILON = .0000125f;
    if (fabs(t) < EPSILON)
        return 0.0f;
    return (float)t;
}

//static double blackman_window(double x)
//{
//	return .42f + .50f * cos(M_PI*x) + .08f * cos(2.0f*M_PI*x);
//}

static double blackman_exact_window(double x)
{
    return 0.42659071f + 0.49656062f * cos(M_PI*x) + 0.07684867f * cos(2.0f*M_PI*x);
}

#define BLACKMAN_SUPPORT (3.0f)
static float blackman_filter(float t)
{
    if (t < 0.0f)
        t = -t;
    
    if (t < 3.0f)
        //return clean(sinc(t) * blackman_window(t / 3.0f));
        return clean(sinc(t) * blackman_exact_window(t / 3.0f));
    else
        return (0.0f);
}

#define GAUSSIAN_SUPPORT (1.25f)
static float gaussian_filter(float t) // with blackman window
{
    if (t < 0)
        t = -t;
    if (t < GAUSSIAN_SUPPORT)
        return clean(exp(-2.0f * t * t) * sqrt(2.0f / M_PI) * blackman_exact_window(t / GAUSSIAN_SUPPORT));
    else
        return 0.0f;
}

// Windowed sinc -- see "Jimm Blinn's Corner: Dirty Pixels" pg. 26.
#define LANCZOS2_SUPPORT (2.0f)
static float lanczos2_filter(float t)
{
    if (t < 0.0f)
        t = -t;
    
    if (t < 2.0f)
        return clean(sinc(t) * sinc(t / 2.0f));
    else
        return (0.0f);
}

#define LANCZOS3_SUPPORT (3.0f)
static float lanczos3_filter(float t)
{
    if (t < 0.0f)
        t = -t;
    
    if (t < 3.0f)
        return clean(sinc(t) * sinc(t / 3.0f));
    else
        return (0.0f);
}

#define LANCZOS4_SUPPORT (4.0f)
//static float lanczos4_filter(float t)
//{
//    if (t < 0.0f)
//        t = -t;
//    
//    if (t < 4.0f)
//        return clean(sinc(t) * sinc(t / 4.0f));
//    else
//        return (0.0f);
//}

#define LANCZOS6_SUPPORT (6.0f)
//static float lanczos6_filter(float t)
//{
//    if (t < 0.0f)
//        t = -t;
//    
//    if (t < 6.0f)
//        return clean(sinc(t) * sinc(t / 6.0f));
//    else
//        return (0.0f);
//}

#define LANCZOS12_SUPPORT (12.0f)
//static float lanczos12_filter(float t)
//{
//    if (t < 0.0f)
//        t = -t;
//    
//    if (t < 12.0f)
//        return clean(sinc(t) * sinc(t / 12.0f));
//    else
//        return (0.0f);
//}

static double bessel0(double x)
{
    const double EPSILON_RATIO = 1E-16;
    double xh, sum, pow, ds;
    int k;
    
    xh = 0.5 * x;
    sum = 1.0;
    pow = 1.0;
    k = 0;
    ds = 1.0;
    while (ds > sum * EPSILON_RATIO) // FIXME: Shouldn't this stop after X iterations for max. safety?
    {
        ++k;
        pow = pow * (xh / k);
        ds = pow * pow;
        sum = sum + ds;
    }
    
    return sum;
}

//static const float KAISER_ALPHA = 4.0;
static double kaiser(double alpha, double half_width, double x)
{
    const double ratio = (x / half_width);
    return bessel0(alpha * sqrt(1 - ratio * ratio)) / bessel0(alpha);
}

#define KAISER_SUPPORT 3.f
static float kaiser_filter(float t)
{
    if (t < 0.0f)
        t = -t;
    
    if (t < KAISER_SUPPORT)
    {
        // db atten
        const float att = 40.0f;
        const float alpha = (float)(exp(log((double)0.58417 * (att - 20.96)) * 0.4) + 0.07886 * (att - 20.96));
        //const float alpha = KAISER_ALPHA;
        return (float)clean(sinc(t) * kaiser(alpha, KAISER_SUPPORT, t));
    }
    
    return 0.0f;
}

// filters[] is a list of all the available filter functions.
UniqueFilterMap SpatialFilter::filters;

void SpatialFilter::initialize()
{
    if (SpatialFilter::filters.size() > 0)
        return;
    SpatialFilter::filters.emplace(shapeHermite,  std::make_unique<SpatialFilter>("hermite",          hermite_filter,         HERMITE_FILTER_SUPPORT));
    SpatialFilter::filters.emplace(shapeBox,      std::make_unique<SpatialFilter>("box",              box_filter,             BOX_FILTER_SUPPORT));
    SpatialFilter::filters.emplace(shapeTriangle, std::make_unique<SpatialFilter>("triangle",         tent_filter,            TENT_FILTER_SUPPORT));
    SpatialFilter::filters.emplace(shapeBell,     std::make_unique<SpatialFilter>("bell",             bell_filter,            BELL_SUPPORT));
    SpatialFilter::filters.emplace(shapeBspline,  std::make_unique<SpatialFilter>("bspline",          B_spline_filter,        B_SPLINE_SUPPORT));
    SpatialFilter::filters.emplace(shapeMitchell, std::make_unique<SpatialFilter>("mitchell",         mitchell_filter,        MITCHELL_SUPPORT));
    SpatialFilter::filters.emplace(shapeLanczos2, std::make_unique<SpatialFilter>("lanczos2",         lanczos2_filter,        LANCZOS2_SUPPORT));
    SpatialFilter::filters.emplace(shapeLanczos3, std::make_unique<SpatialFilter>("lanczos3",         lanczos3_filter,        LANCZOS3_SUPPORT));
    SpatialFilter::filters.emplace(shapeBlackman, std::make_unique<SpatialFilter>("blackman",         blackman_filter,        BLACKMAN_SUPPORT));
    SpatialFilter::filters.emplace(shapeKaiser,   std::make_unique<SpatialFilter>("kaiser",           kaiser_filter,          KAISER_SUPPORT));
    SpatialFilter::filters.emplace(shapeGaussian, std::make_unique<SpatialFilter>("gaussian",         gaussian_filter,        GAUSSIAN_SUPPORT));
    SpatialFilter::filters.emplace(shapeCatmull,  std::make_unique<SpatialFilter>("catmullrom",       catmull_rom_filter,     CATMULL_ROM_SUPPORT));
    SpatialFilter::filters.emplace(shapeQuadratic,std::make_unique<SpatialFilter>("quadratic_interp", flam3_quadratic_filter, QUADRATIC_SUPPORT));
    SpatialFilter::filters.emplace(shapeHanning,  std::make_unique<SpatialFilter>("hanning",          hanning_filter,         HANNING_FILTER_SUPPORT));
    SpatialFilter::filters.emplace(shapeHamming,  std::make_unique<SpatialFilter>("hamming",          hamming_filter,         HAMMING_FILTER_SUPPORT));
}

std::string SpatialFilter::filterNameForFilterShapeEnum(enum FilterShape filterShape)
{
    initialize();
    const UniqueFilter & spatialFilter = SpatialFilter::filters[filterShape];
    return spatialFilter->name;
}

uint SpatialFilter::filterWidthFor(struct ElectricSheepStuff & elc,  uint maxFilterWidth)
{
    initialize();
    enum FilterShape filterShape = elc.filterShape;
    int supersample              = elc.supersample;
    float filterStrength         = elc.filter;
    
    if (filterStrength == 0.f) // no filter needed
        return 0;
    
    UniqueFilter & spatialFilter = filters[filterShape];
    float support                = spatialFilter->support;
    
    float fw = 2.f * support * supersample * filterStrength;
    
    uint fwidth = ((int) fw) + 1;
    
    // make sure it is an odd filter width
    if ((fwidth & 1) == 0)
        fwidth++;
    
    // clamp to OpenCL kernel compile filterwidth limit
    if (fwidth > maxFilterWidth) {
//        NSLog(@"filterWidth of %u clamped to %u", fwidth, maxFilterWidth);
        fwidth = maxFilterWidth;
    }
    return fwidth;
}

float SpatialFilter::maxFilterStrengthFor(enum FilterShape filterShape, uint supersample, uint maxFilterWidth)
{
    initialize();
    UniqueFilter & spatialFilter = filters[filterShape];
    float support                = spatialFilter->support;
    return (maxFilterWidth - 1) /(2.f * support * supersample);
}

float SpatialFilter::maxFilterStrengthFor(struct ElectricSheepStuff & elc, uint maxFilterWidth)
{
    initialize();
    enum FilterShape filterShape = elc.filterShape;
    int supersample              = elc.supersample;
    
    UniqueFilter & spatialFilter = filters[filterShape];
    float support                = spatialFilter->support;
    return (maxFilterWidth - 1) /(2.f * support * supersample);
}

float * SpatialFilter::createFilter(struct ElectricSheepStuff &elc, uint *pFilterWidth,  uint maxFilterWidth)
{
    initialize();
    enum FilterShape filterShape = elc.filterShape;
    int supersample              = elc.supersample;
    float filterStrength         = elc.filter;
    
    if (filterStrength == 0.f) // no filter needed
        return NULL;
    
    UniqueFilter & spatialFilter = filters[filterShape];
    float support                = spatialFilter->support;
    
    float fw = 2.f * support * supersample * filterStrength;
    float adjust;
    
    uint fwidth = ((int) fw) + 1;
    if (fwidth == 1)
        return NULL;
    
    // make sure it is an odd filter width
    if ((fwidth & 1) == 0)
        fwidth++;
    
    // clamp to OpenCL kernel compile filterwidth limit
    if (fwidth > maxFilterWidth) {
//        NSLog(@"filterWidth of %u clamped to %u", fwidth, maxFilterWidth);
        fwidth = maxFilterWidth;
    }
    
    /* Calculate the coordinate scaling factor for the kernel values */
    if (fw > 0.f)
        adjust = support * fwidth / fw;
        else
            adjust = 1.0;
            
            float *filter = (float *)calloc(fwidth, sizeof(float));
            for (uint i = 0; i < fwidth; i++)
                filter[i] = (*spatialFilter->func)(((2.f * i + 1.f) / (float)fwidth - 1.f)*adjust);
                
                float total = 0.f;
                for (uint i = 0; i < fwidth; i++)
                    total += filter[i];
                    if (total != 0.f) {
                        for (uint i = 0; i < fwidth; i++)
                            filter[i] /= total;
                            }
    *pFilterWidth = fwidth;
    return filter;
}
