//
//  ColorGradient.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/13/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//     
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//     
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.


#include "ColorGradient.hpp"
#include "ColorNode.hpp"
#include "NaturalCubicCurve.hpp"

#ifdef __linux
namespace std {
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
}
#endif

ColorGradient::ColorGradient(ColorNodeVector _colorNodes, enum PaletteMode _paletteMode)
: colorNodes(std::make_unique<ColorNodeVector>(_colorNodes)), paletteMode(_paletteMode)
{
    splitOut();
}

ColorGradient::ColorGradient(const ColorGradient & c)
: colorNodes(std::make_unique<ColorNodeVector>(*c.colorNodes)), paletteMode(c.paletteMode)
{
    splitOut();
}

void ColorGradient::splitOut()
{
    for (unsigned i = 0; i < colorNodes->size(); i++) {
        colorStops.push_back(colorNodes->operator[](i).color);
        locations.push_back(colorNodes->operator[](i).location);
    }
    
}

bool ColorGradient::operator==(const ColorGradient & c)
{
    if (colorNodes->size() != c.colorNodes->size())
        return false;
    for (size_t i = 0; i < colorNodes->size(); i++) {
        if (colorNodes->operator[](i) != c.colorNodes->operator[](i))
            return false;
    }
    return true;
}

std::string ColorGradient::paletteModeDescription()
{
    if (paletteMode == modeStep)
        return "stepMode";
    else if (paletteMode == modeLinear)
        return "linearMode";
    return "smoothMode";
}

std::string ColorGradient::description()
{
    std::string s;
    s.append("ColorGradient").append(" ").append(paletteModeDescription()).append("\n").append(info());
    return s;
}

std::string ColorGradient::info()
{
    static char buf[40];
    size_t count = colorNodes->size();
    std::string s; s.reserve(16*count);
    
   
    snprintf(buf, sizeof(buf), "\n%zu Color Nodes:\n", count);
    s.append(buf);
    
    const size_t nodesPerLine = 4;
    size_t nodeCount          = 0;
    for (ColorNode & node : *colorNodes) {
        s.append(node.lineDescriptionForColorNodeCount(count));
        
        if (++nodeCount == nodesPerLine) {
            nodeCount = 0;
            s.append("\n");
        }
        else {
            s.append("  ");
        }
    }
    return s;
}

NaturalCubicCurve ColorGradient::hueCurve()
{
    std::vector<Point> huePoints;
    for (ColorNode & node : *colorNodes) {
        hsva hsv = rgba::RGBtoHSV(rgba(node.color));
        Point hue(node.location, hsv.h);
        huePoints.push_back(hue);
    }
    return NaturalCubicCurve(&huePoints);
}

NaturalCubicCurve ColorGradient::saturationCurve()
{
    std::vector<Point> saturationPoints;
    for (ColorNode & node : *colorNodes) {
        hsva hsv = rgba::RGBtoHSV(rgba(node.color));
        Point saturation(node.location, hsv.s);
        saturationPoints.push_back(saturation);
    }
    return NaturalCubicCurve(&saturationPoints);
}

NaturalCubicCurve ColorGradient::brightnessCurve()
{
    std::vector<Point> brightnessPoints;
    for (ColorNode & node : *colorNodes) {
        hsva hsv = rgba::RGBtoHSV(rgba(node.color));
        Point brightness(node.location, hsv.v);
        brightnessPoints.push_back(brightness);
    }
    return NaturalCubicCurve(&brightnessPoints);
}

ColorGradient * ColorGradient::makeSmoothGradient()
{
    int count = 256;
    
    std::vector<ColorNode> newColorNodes;
    NaturalCubicCurve hues         = hueCurve();
    NaturalCubicCurve saturations  = saturationCurve();
    NaturalCubicCurve brightnesses = brightnessCurve();
    
    for (int i = 0; i < count; i++) {
        float location   = i++ /((float)count - 1.f);
        float hue        = hues.evaluateForU(location);
        float saturation = saturations.evaluateForU(location);
        float brightness = brightnesses.evaluateForU(location);
        
        hsva hsv(hue, saturation, brightness, 1.f);
        rgba _color = hsva::HSVtoRGB(hsv);
        Color color(_color);
        newColorNodes.emplace_back(color, location);
    }
    return new ColorGradient(newColorNodes, modeSmooth);
}

// find which range of X holds the key, returns the left index of the containing range
int ColorGradient::binary_range_search(const float* X, float key, int imin, int imax)
{
    int omax = imax;
    
    // continue searching while [imin,imax] is not empty
    while (imax >= imin)
    {
        // calculate the midpoint for roughly equal partition
        int imid = (imin + imax)/2;
        if (X[imid] == key)
            // key found at index imid
            return imid;
        // determine which subarray to search
        else if (X[imid] < key) {
            // check for enclosing range  Xi < key < Xi+1 (if == binary search will find that one)
            if (imid + 1 <= omax && X[imid+1] > key)
                return imid;
            // change min index to search upper subarray
            imin = imid + 1;
        }
        else {  //   Xi > key
            // check for enclosing range  Xi-1 < key < Xi  (if == binary search will find that one)
            if (imid - 1 >= 0 && X[imid-1] < key)
                return imid - 1;
            // change max index to search lower subarray
            imax = imid - 1;
        }
    }
    // key was not found
    return -1;
}

Color ColorGradient::interpolatedColorAtLocation(float location)
{
    if (locations.size() == 0)
        return Color();
    if (locations.size() == 1)
        return colorStops[0];
    int index = binary_range_search(locations.data(), location, 0, (unsigned)locations.size() - 1);
    
    if (index == -1) {
        if (location < locations[0])
            return colorStops[0];
        else
            return colorStops[colorStops.size() - 1];
    }
    if (index == locations.size() - 1)
        return colorStops[colorStops.size() - 1];
    
    float left         = locations[index];
    float right        = locations[index + 1];
    rgba & leftColor  = colorStops[index];
    rgba & rightColor = colorStops[index + 1];
    
    float percent = (location - left)/(right - left);
    
    float r = (1.f - percent)*leftColor.r + percent*rightColor.r;
    float g = (1.f - percent)*leftColor.g + percent*rightColor.g;
    float b = (1.f - percent)*leftColor.b + percent*rightColor.b;
    float a = (1.f - percent)*leftColor.a + percent*rightColor.a;
    
    return Color (r, g, b, a);
}

