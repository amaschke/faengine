cmake_minimum_required(VERSION 3.5.2)
project(faengine)

message (STATUS "FOO: " ${PROJECT_SOURCE_DIR})
message (STATUS "BAR: " ${PROJECT_BINARY_DIR})

include_directories(. /opt/intel/opencl-sdk/include /usr/include/GraphicsMagick)

find_library(OPENCL_LIBRARY OpenCL PATHS /opt/intel/opencl-sdk/lib64)
find_library(GRAPHICSMAGICK GraphicsMagick++)
find_library(CRYPTO_LIBRARY crypto)
find_library(BOOST_REGEX_LIB boost_regex)

if (EXISTS /usr/local/cuda)
   find_library(CUDA_LIBRARY cuda               PATHS /usr/local/cuda-7.5/targets/x86_64-linux/lib)
   find_library(CUDART_LIBRARY cudart           PATHS /usr/local/cuda-7.5/targets/x86_64-linux/lib)
   find_library(NVRTC_LIBRARY nvrtc             PATHS /usr/local/cuda-7.5/targets/x86_64-linux/lib)
   find_library(NVRTC_BUILTINS_LIBRARY nvrtc-builtins PATHS  /usr/local/cuda-7.5/targets/x86_64-linux/lib)
endif(EXISTS /usr/local/cuda)

set (CUDA_SOURCES 
	CudaContext.cpp
	CudaDeviceKind.cpp
	CudaProgram.cpp)

set(LIBRARY_SOURCES 
	jsoncpp/jsoncpp.cpp
	AffineMatrix.cpp
	CameraViewport.cpp
	ClContext.cpp
	ClDeviceKind.cpp
	ClPlatform.cpp
	ClProgram.cpp
	ColorGradient.cpp
	ColorMap.cpp
	ColorNode.cpp
	CpuMemStats.cpp
	CurveDefinition.cpp
	DeviceContext.cpp
	DeviceKind.cpp
	DeviceProgram.cpp
	DeviceRuntime.cpp
	DeviceUsage.cpp
	ElectricSheepStuff.cpp
	Flam4RenderDispatcher.cpp
	Flam4ClRuntime.cpp
	Flame.cpp
	FlameExpanded.cpp
	FlameMetaData.cpp
	FlameParse.cpp
	FlameTileArray.cpp
	FlameTile.cpp
	Image.cpp
	ImageRepTileArray.cpp
	ImageSlot.cpp
	LinuxInterface.cpp
	NamedAffineMatrix.cpp
	NaturalCubicCurve.cpp
	pugixml.cpp
	rand.cpp
	RenderGeometry.cpp
	RenderHarness.cpp
	RenderInfo.cpp
	RenderListener.cpp
	RenderModeSettings.cpp
	RenderParams.cpp
	RenderQueue.cpp
	RenderState.cpp
	RenderTile.cpp
	SpatialFilter.cpp
	Utilities.cpp
	VariationChain.cpp
	Variation.cpp
	VariationGroup.cpp
	VariationParameter.cpp
	VariationSet.cpp
	VarParInstance.cpp
	XaosMatrix.cpp
	XaosMatrixVector.cpp
	GlhFunctions.c
)

# ==== FAEngine ================================
add_library(faengine SHARED ${LIBRARY_SOURCES})
target_compile_options(faengine PRIVATE -std=c++11 -g)
if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    # g++ does not support regex on recent Linux versions -- for now use Boost instead
    target_compile_definitions(faengine PRIVATE -DBOOST_REGEX)
endif(${CMAKE_SYSTEM_NAME} MATCHES "Linux")


add_executable(FAClRender ../FAClRender/main.cpp) 
target_compile_options(FAClRender PRIVATE -std=c++11 -g)
add_dependencies(FAClRender faengine)

# ==== the command line executable ====
target_link_libraries(FAClRender 
    ${OPENCL_LIBRARY} 
    faengine 
    ${GRAPHICSMAGICK} 
    ${BOOST_REGEX_LIB} 
    ${CRYPTO_LIBRARY} 
    ${CUDART_LIBRARY} 
    ${NVRTC_LIBRARY} 
    ${NVRTC_BUILTINS_LIBRARY})

# ==== Unit Testing ====
add_custom_command(
  OUTPUT ../FAEngineUnitTests/runner.cpp
  COMMAND ../FAEngineUnitTests/generate.sh 
  DEPENDS ../FAEngineUnitTests/MyTest.h 
)

add_executable(unittest 
    ../FAEngineUnitTests/VariationSetTest.cpp
    ../FAEngineUnitTests/ColorGradientTest.cpp
    ../FAEngineUnitTests/RenderTest.cpp
    ../FAEngineUnitTests/runner.cpp)
target_compile_options(unittest PRIVATE -std=c++11)
add_dependencies(unittest faengine)
target_include_directories(unittest PRIVATE ~/Downloads/cxxtest-master)

target_link_libraries(unittest 
    ${OPENCL_LIBRARY} 
    faengine 
    ${GRAPHICSMAGICK} 
    ${BOOST_REGEX_LIB} 
    ${CRYPTO_LIBRARY} 
    ${CUDART_LIBRARY} 
    ${NVRTC_LIBRARY} 
    ${NVRTC_BUILTINS_LIBRARY})

# Want to be able to build for OpenCL testing on PCs that dont have Nvidia GPUs
# (On Linux, CUDA Toolkit cannot be safely installed on PC that does not have an Nvidia GPU)
if (EXISTS /usr/local/cuda)
    list(APPEND LIBRARY_SOURCES ${CUDA_SOURCES})
else (EXISTS /usr/local/cuda)
    target_compile_definitions(FAClRender PRIVATE -DNO_CUDA)
    target_compile_definitions(unittest PRIVATE -DNO_CUDA)
    target_compile_definitions(faengine PRIVATE -DNO_CUDA)
endif(EXISTS /usr/local/cuda)

install (
FILES
    library.xml
    myLibrary.xml
    Flam4_3dKernal_Template.cl
    Flam4_3dKernal_Template.cu
    flam3-palettes.xml
    ProPhoto.icm
    AdobeRGB1998.icc
    WideGamutRGB.icc
 DESTINATION
    ~/.fa4/Resources)

install (FILES sRGB_Profile.icc DESTINATION ~/.fa4/Resources RENAME sRGB.icc)

install (DIRECTORY ../TestData FILE_PERMISSIONS OWNER_WRITE OWNER_READ WORLD_READ DESTINATION /home/sbrodhead/Test)

