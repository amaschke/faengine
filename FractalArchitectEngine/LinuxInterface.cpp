//
//  LinuxInterface.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 8/24/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//

#include "LinuxInterface.hpp"
#include "VariationSet.hpp"
#include "Utilities.hpp"
#include <stdlib.h>
#include <limits.h>
#include <dirent.h>
#include <errno.h>
#include <openssl/md5.h>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <wordexp.h>

// Expand relative path to absolute path
const char *expandTildeInPath(const char *_path)
{
    wordexp_t p;

    wordexp(_path, &p, 0);
    const char *result =  p.we_wordv[0];
    return result;
}

std::string variationSetsDirectoryPath()
{
    return std::string(expandTildeInPath("~/.fa4/Application\\ Support/FractalArchitectEngine/VariationSets"));
}

static std::string cachedMyLibraryPath()
{
    return variationSetsDirectoryPath() + "/myLibrary.xml";
}

static std::string cachedLibraryPath()
{
    return variationSetsDirectoryPath() + "/library.xml";
}

double osVersion()
{
    return 1.; // dont know what to report for Windows here ???
}

std::string pathForTestResource(const char *_bundlePath, const char *_filename)
{
    std::string path(expandTildeInPath("~/.fa4/Resources"));
    return path + '/' + _filename;
}

std::string pathForResource(const char * _basename, const char * _extension)
{
    std::string path(expandTildeInPath("~/.fa4/Resources"));
    std::string fname(_basename);
    return path + '/' + fname + '.' + _extension;
}

// empty a directory - but leave the directory itself
void empty_dir(const char *path)
{
    static char abs_path[256];

    struct dirent *entry = NULL;
    DIR *dir             = NULL;
    dir                  = opendir(path);
    while((entry = readdir(dir)))
    {
        DIR *sub_dir       = NULL;
        FILE *file         = NULL;
        if(*(entry->d_name) != '.')
        {
            snprintf(abs_path, sizeof(abs_path), "%s/%s", path, entry->d_name);
            if((sub_dir = opendir(abs_path)))
            {
                closedir(sub_dir);
                remove_dir(abs_path);
            }
            else
            {
                if((file = fopen(abs_path, "r")))
                {
                    fclose(file);
                    remove(abs_path);
                }
            }
        }
    }
    closedir(dir);
}

// remove a directory
void remove_dir(const char *path)
{
    empty_dir(path);
    remove(path);
}


// get array of file names in this directory
std::vector<std::string> directoryFileNamesContents(const char *path)
{
    std::vector<std::string> names;

    struct dirent *entry = NULL;
    DIR *dir             = opendir(path);
    while((entry = readdir(dir)))
    {
        if(*(entry->d_name) != '.')
        {
            if (entry->d_type == DT_REG) {
                names.emplace_back(entry->d_name);
            }
        }
    }
    closedir(dir);
    return names;
}

std::vector<std::string> directorySubDirContents(const char *path)
{
    std::vector<std::string> names;

    struct dirent *entry = NULL;
    DIR *dir             = opendir(path);
    while((entry = readdir(dir)))
    {
        if(*(entry->d_name) != '.')
        {
            if (entry->d_type == DT_DIR) {
                names.emplace_back(entry->d_name);
            }
        }
    }
    closedir(dir);
    return names;
}

std::string md5DigestString(const std::string & str)
{
    unsigned char result[MD5_DIGEST_LENGTH];
    MD5((unsigned char*)str.c_str(), str.size(), result);

    std::ostringstream sout;
    sout<<std::hex<<std::setfill('0');
    for(long long c: result)
    {
        sout<<std::setw(2)<<(long long)c;
    }
    return sout.str();
}

std::string cachePath()
{
	return  expandTildeInPath("~/.fa4/Caches/com.centcom.FractalArchitectEngine");
}

//bool fileExists(char* filename)
//{
//    struct stat fileInfo;
//    return stat(filename, &fileInfo) == 0;
//}

bool cachedLibraryExists()
{
    struct stat fileInfo;
    std::string factoryLibraryPath = pathForResource(VariationSet::factoryLibraryBaseName(), "xml");

    if (stat(factoryLibraryPath.c_str(), &fileInfo) != 0)
        return false;

    time_t factoryModDate = fileInfo.st_mtime;

    if (stat(cachedLibraryPath().c_str(), &fileInfo) != 0)
        return false;

    time_t cachedModDate = fileInfo.st_mtime;
    return cachedModDate > factoryModDate;
}

void cacheMyLibrary()
{
    std::string myLibraryPath = pathForResource("myLibrary", "xml");
    std::ifstream ifstr(myLibraryPath);
    if (ifstr.fail()) {
        return;
    }
    std::string libraryText((std::istreambuf_iterator<char>(ifstr)),
                         std::istreambuf_iterator<char>());

	std::string varSetsDir = VariationSet::variationSetsDirectoryURL();

	if ((mkpath(varSetsDir.c_str(), 0777) == -1) && (errno != EEXIST)) {
		std::string msg = "Failed to create directory: " + varSetsDir + "  " + strerror(errno);
		throw std::runtime_error(msg);
	}
	if ((unlink( VariationSet::cachedMyLibraryURL().c_str()) == -1) && (errno != ENOENT)) {
		std::string msg = "Failed to remove file: " +  VariationSet::cachedMyLibraryURL() + "  " + strerror(errno);
		throw std::runtime_error(msg);
	}

	// copy file from path in bundle to the cached location
	std::ofstream  dst( VariationSet::cachedMyLibraryURL().c_str(), std::ios::binary);
	dst << libraryText;
}


