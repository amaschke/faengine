//
//  ClContext.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/17/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef ClContext_hpp
#define ClContext_hpp

//
//  ClContext.h
//  FractalArchitect
//
//  Created by Steven Brodhead on 11/8/11.
//  Copyright (c) 2011-2012 Centcom Inc. All rights reserved.
//

#ifdef __APPLE__
#include <OpenCL/cl.h>
#elif defined(_WIN32)
#include <CL/OpenCL.h>

#else
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl.h>
#endif
#include "Exports.hpp"
#include "DeviceContext.hpp"

#include <string>
#include <vector>
#include <memory>

class ClContext;
class ClDeviceKind;
class ClPlatform;
class DeviceUsage;
class VariationSet;

using SharedClDeviceKind = std::shared_ptr<ClDeviceKind>;
using SharedClContext    = std::shared_ptr<ClContext>;
using StringsVector      = std::vector<std::string>;
using UniquePlatform     = std::unique_ptr<ClPlatform>;
using PlatformsVector    = std::vector<UniquePlatform>;

class FAENGINE_API ClContext : public DeviceContext {
    friend class ClDeviceKind;
    friend class Flam4ClRuntime;
public:
    static SharedClContext makeClContext(cl_device_type filter,
                                         bool forceRebuild,
                                         SharedVariationSet & _defaultVariationSet,
                                         bool _singleDeviceAllowed,
                                         bool _canUseGPU,
                                         bool _ignoreQuarantine);
    
    virtual ~ClContext();
        
    static void printDeviceList(cl_device_type devType);
        
    static std::string getDeviceNameForDeviceID(cl_device_id _id);
    static std::string getDeviceVendorForDeviceID(cl_device_id _id);
    
    std::string selectedDevicesDescription() override;
    void * deviceIDForDevice(uint i);
    
    bool oclDeviceAllocCheckForDeviceNum(size_t deviceNum,
                                         float & area,
                                         size_t & amountAlloced,
                                         uint warpSize,
                                         uint xformCount,
                                         uint supersample) override;
    bool oclDeviceCheckForEnoughMemoryArea(size_t area, uint xformCount, uint supersample) override;
    
    bool checkOclGPUMemAvailable(size_t & memoryAvailable,
                                 float & area,
                                 float niceFactor,
                                 uint warpSize,
                                 uint xformCount,
                                 uint supersample,
                                 uint deviceNum) override;
    
    void refreshMemoryStats() override;

    bool hasDiscreteGPU() override;
    static bool hasGPU(StringsVector &vendor, bool checkLocalMemoryOnDevice, BoolsVector & localMemoryArray, bool ignoreQuarantine);

    static bool checkAvailabilityForDeviceID(cl_device_id _id);
    static bool checkAvailabilityOfOneDeviceID(cl_device_id * ids, unsigned count);
    static bool checkQuarantineForDeviceID(cl_device_id _id);
    static bool checkNonQuarantineOfOneDeviceID(cl_device_id * ids, unsigned count);

	cl_platform_id    *getPlatformIDs() { return platformIDs; }

private:
    ClContext(cl_device_type filter,
              bool forceRebuild,
              SharedVariationSet & _defaultVariationSet,
              bool _singleDeviceAllowed,
              bool _canUseGPU,
              bool ignoreQuarantine);
    void phase2Constructor();
    
    static void logPlatformInfo (cl_platform_id id, cl_platform_info name, std::string & str);
    void logDeviceName(int deviceNum, cl_device_id _id);
    void logDeviceVendor(int deviceNum, cl_device_id _id);
    static void logDeviceTypeInfo (int deviceNum, cl_device_id id);
    static bool isCpu(cl_device_id id);
    
    void parseOpenClVersion();
    bool createDeviceKinds() override;
    
    void refreshProgramForVariationSet(SharedVariationSet &variationSet, bool rebuild);
    bool makeTestProgramForVariationSet(SharedVariationSet &variationSet);
    
    bool selectedDevicesHasGPU();
    cl_device_id firstCPUDeviceID();
    
    size_t deviceNumForDeviceID(void * deviceID);
    size_t deviceUsageIndexForDeviceID(void * deviceID);
    
    cl_command_queue queueForSelectedDevice(uint selectedDeviceNum);
    
    std::string selectedDevicesPreferenceKey()  { return "SelectedDevices"; }
    
    // ====== Members ============
    cl_command_queue  *queues;
    cl_command_queue  *subdeviceQueues;
    PlatformsVector    platforms;
    cl_platform_id    *platformIDs;
    cl_context        *contexts;
    
    
    static ClContext * context;
};

#endif /* ClContext_hpp */
