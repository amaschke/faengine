//
//  GlhFunctions.h
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/22/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//

#ifndef GlhFunctions_h
#define GlhFunctions_h

extern void MultiplyMatrixByVector4by4OpenGL_FLOAT(float *resultvector, const float *matrix, const float *pvector);
extern void MultiplyMatrices4by4OpenGL_FLOAT(float *result, float *matrix1, float *matrix2);
extern void glhTransposeMatrixf2(float *result, float *m);

extern void glhLookAtf2( float *matrix, float *eyePosition3D,
                        float *center3D, float *upVector3D );
extern void glhLookAtLHf2( float *matrix, float *eyePosition3D,
                          float *center3D, float *upVector3D );
extern void glhTranslatef2(float *matrix, float x, float y, float z);
extern void glhPerspectiveInfiniteFarPlanef2(float *matrix, float fovyInDegrees, float aspectRatio, float znear);
extern void glhOrthofInfiniteFarPlane(float *matrix, float left, float right, float bottom, float top);
extern void glhOrthof2(float *matrix, float left, float right, float bottom, float top, float znear, float zfar);
extern int glhProjectf(float objx, float objy, float objz,
                       float *modelview, float *projection,
                       int *viewport,
                       float *windowCoordinate);
extern int glhUnProjectf(float winx, float winy, float winz,
                         float *modelview, float *projection,
                         int *viewport,
                         float *objectCoordinate);
extern void glhRotateAboutZf2(float *matrix, float angleInRadians);

extern void glhQuaternionIdentityf(float *quat);
extern int glhQuaternionIsIdentityf(float *quat);
extern int glhQuaternionIsPuref(float *quat);
extern void glhQuaternionConjugatef(float *quat);
void glhQuaternionInvertf(float *outquat, float *quat);
extern void glhQuaternionMagnitudef(float *magnitude, float *quat);
extern void glhQuaternionMagnitudeSquaredf(float *magnitude, float *quat);
extern void glhQuaternionDotProductf(float *result, float *quat1, float *quat2);
extern void glhQuaternionExpf(float *outquat, float *quat);
extern void glhQuaternionLnf(float *outquat, float *quat);
extern void glhQuaternionNormalizef(float *quat);
extern void glhQuaternionMultiplyf(float *outquat, float *quat1, float *quat2);
extern void glhQuaternionQuatToRotationMatrixf(float *matrix, float *quat);
extern void glhQuaternionQuatToRotationMatrixTf(float *matrix, float *quat);
extern void glhQuaternionRotationMatrixToQuatf(float *quat, float *matrix);
extern void glhQuaternionQuatToAxisAndAnglef(float *axis, float *angleInRadians, float *quat);
extern void glhQuaternionAxisAndAngleToQuatf(float *quat, float *axis, float *angleInRadians);
extern void glhQuaternionEulerAnglesToQuatf(float *quat, float roll, float pitch, float yaw);
extern void glhQuaternionSLERPf(float *outquat, float *quat1, float *quat2, float t);
extern void glhQuaternionSQUADSplinef(float *outquat, float *quat1, float *quat2, float *quat3, float *quat4, float t, int amFirst, int amLast);

extern int glhExtractLookAtFromMatrixf2(float *matrix, float *eyePosition3D, float *center3D, float *upVector3D);

extern void NormalizeVectorFLOAT_2(float *pvector);
extern float DistanceSquareBetweenTwoPoints_FLOAT(float *ppoint1, float *ppoint2);


#endif /* GlhFunctions_h */
