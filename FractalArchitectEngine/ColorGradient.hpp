//
//  ColorGradient.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/13/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef ColorGradient_hpp
#define ColorGradient_hpp

#include "Exports.hpp"
#include "Enums.hpp"
#include "common.hpp"

#include <vector>
#include <memory>
#include <string>

class NaturalCubicCurve;
class ColorGradient;
class ColorNode;


using ColorNodeVector       = std::vector<ColorNode>;
using SharedColorNodeVector = std::shared_ptr<ColorNodeVector>;


class FAENGINE_API ColorGradient {
    friend class ColorMap;
public:
    ColorGradient(ColorNodeVector _colorNodes, enum PaletteMode _paletteMode);
    ColorGradient(const ColorGradient & c);
    
    bool operator==(const ColorGradient & c);
    bool operator!=(const ColorGradient & c)    { return ! operator==(c); }
    
    std::string paletteModeDescription();
    std::string description();
    std::string info();
    
    NaturalCubicCurve hueCurve();
    NaturalCubicCurve saturationCurve();
    NaturalCubicCurve brightnessCurve();
    
    ColorGradient * makeSmoothGradient();
    
    Color interpolatedColorAtLocation(float location);
    static int binary_range_search(const float* X, float key, int imin, int imax);
    
private:
    void splitOut();
    
    // ========== Members ==========
	SharedColorNodeVector colorNodes;
    enum PaletteMode paletteMode;
    
    std::vector<rgba> colorStops;
    std::vector<float> locations;    
};

#endif /* ColorGradient_hpp */
