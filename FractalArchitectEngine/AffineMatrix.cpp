//
//  AffineMatrix.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 4/29/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include <math.h>

#include "AffineMatrix.hpp"
#include "Flame.hpp"
#include "rand.hpp"

AffineMatrix::AffineMatrix()
: a(1.f), b(0.f), c(0.f),
  d(0.f), e(1.f), f(0.f)
{}

AffineMatrix::AffineMatrix(float _a, float _b, float _c, float _d, float _e, float _f)
: a(_a), b(_b), c(_c), d(_d), e(_e), f(_f)
{}

AffineMatrix::AffineMatrix(const struct xForm &xf)
: a(xf.a), b(xf.b), c(xf.c), d(xf.d), e(xf.e), f(xf.f)
{}

AffineMatrix::AffineMatrix(const struct xForm &xf, bool postXform)
{
    if (postXform) {
        a = xf.pa;
        b = xf.pb;
        c = xf.pc;
        d = xf.pd;
        e = xf.pe;
        f = xf.pf;
    }
    else {
        a = xf.a;
        b = xf.b;
        c = xf.c;
        d = xf.d;
        e = xf.e;
        f = xf.f;
    }
}

bool AffineMatrix::operator==(const AffineMatrix &matrix) const
{
    if (a == matrix.a &&
        b == matrix.b &&
        c == matrix.c &&
        d == matrix.d &&
        e == matrix.e &&
        f == matrix.f)
        return true;
    return false;
}

bool AffineMatrix::operator!=(const AffineMatrix &matrix) const
{
    return !(*this == matrix);
}

void AffineMatrix::rotateBy(float theta)
{
    float A =  a * cosf(theta) + -d * sinf(theta);
    float B =  b * cosf(theta) + -e * sinf(theta);
    float C =  c * cosf(theta) + -f * sinf(theta);
    float D =  a * sinf(theta) +  d * cosf(theta);
    float E =  b * sinf(theta) +  e * cosf(theta);
    float F =  c * sinf(theta) +  f * cosf(theta);
    a = A;
    b = B;
    c = C;
    d = D;
    e = E;
    f = F;
}

void AffineMatrix::rotateXform(struct xForm &xf, float theta)
{
    float A =  xf.a * cosf(theta) + -xf.d * sinf(theta);
    float B =  xf.b * cosf(theta) + -xf.e * sinf(theta);
    float C =  xf.c * cosf(theta) + -xf.f * sinf(theta);
    float D =  xf.a * sinf(theta) +  xf.d * cosf(theta);
    float E =  xf.b * sinf(theta) +  xf.e * cosf(theta);
    float F =  xf.c * sinf(theta) +  xf.f * cosf(theta);
    xf.a = A;
    xf.b = B;
    xf.c = C;
    xf.d = D;
    xf.e = E;
    xf.f = F;
}

void AffineMatrix::rotatePostXform(struct xForm &xf, float theta)
{
    float A =  xf.pa * cosf(theta) + -xf.pd * sinf(theta);
    float B =  xf.pb * cosf(theta) + -xf.pe * sinf(theta);
    float C =  xf.pc * cosf(theta) + -xf.pf * sinf(theta);
    float D =  xf.pa * sinf(theta) +  xf.pd * cosf(theta);
    float E =  xf.pb * sinf(theta) +  xf.pe * cosf(theta);
    float F =  xf.pc * sinf(theta) +  xf.pf * cosf(theta);
    xf.pa = A;
    xf.pb = B;
    xf.pc = C;
    xf.pd = D;
    xf.pe = E;
    xf.pf = F;
}

void AffineMatrix::scaleBy(float x)
{
    a *= x;
    b *= x;
    c *= x;
    d *= x;
    e *= x;
    f *= x;
}

void AffineMatrix::scaleXform(struct xForm &xf, float x)
{
    xf.a *= x;
    xf.b *= x;
    xf.c *= x;
    xf.d *= x;
    xf.e *= x;
    xf.f *= x;
}

void AffineMatrix::scalePostXform(struct xForm &xf, float x)
{
    xf.pa *= x;
    xf.pb *= x;
    xf.pc *= x;
    xf.pd *= x;
    xf.pe *= x;
    xf.pf *= x;
}

void AffineMatrix::translateBy(const Point &t)
{
    c += t.x;
    f += t.y;
}

void AffineMatrix::translateXform(struct xForm &xf, const Point &t)
{
    xf.c += t.x;
    xf.f += t.y;
}

void AffineMatrix::translatePostXform(struct xForm &xf, const Point &t)
{
    xf.pc += t.x;
    xf.pf += t.y;
}

void AffineMatrix::transformBy(const AffineMatrix &m)
{
    float A = a * m.a + d * m.b;
    float B = b * m.a + e * m.b;
    float C = c * m.a + f * m.b + m.c;
    float D = a * m.d + d * m.e;
    float E = b * m.d + e * m.e;
    float F = c * m.d + f * m.e + m.f;
    a = A;
    b = B;
    c = C;
    d = D;
    e = E;
    f = F;
}

void AffineMatrix::transformXform(struct xForm &xf, const AffineMatrix &m)
{
    float A = xf.a * m.a + xf.d * m.b;
    float B = xf.b * m.a + xf.e * m.b;
    float C = xf.c * m.a + xf.f * m.b + m.c;
    float D = xf.a * m.d + xf.d * m.e;
    float E = xf.b * m.d + xf.e * m.e;
    float F = xf.c * m.d + xf.f * m.e + m.f;
    xf.a = A;
    xf.b = B;
    xf.c = C;
    xf.d = D;
    xf.e = E;
    xf.f = F;
}

void AffineMatrix::transformPostXform(struct xForm &xf, const AffineMatrix &m)
{
    float A = xf.pa * m.a + xf.pd * m.b;
    float B = xf.pb * m.a + xf.pe * m.b;
    float C = xf.pc * m.a + xf.pf * m.b + m.c;
    float D = xf.pa * m.d + xf.pd * m.e;
    float E = xf.pb * m.d + xf.pe * m.e;
    float F = xf.pc * m.d + xf.pf * m.e + m.f;
    xf.pa = A;
    xf.pb = B;
    xf.pc = C;
    xf.pd = D;
    xf.pe = E;
    xf.pf = F;
}

Point AffineMatrix::transformPoint(const Point &p)
{
    return Point(a * p.x + b * p.y + c, d * p.x + e * p.y + f);
}


void AffineMatrix::invert()
{
    float Z = a * e - b * d;
    float z = 1/Z;
    
    float A =  e / z;
    float D = -b / z;
    float G = (b * f - c * e) / z;
    float B = -d / z;
    float E =  a / z;
    float H = (c * d - a * f) / z;
    
    a = A;
    b = D;
    c = G;
    d = B;
    e = E;
    f = H;
}

void AffineMatrix::horizReflect()
{
    d = -d;
    e = -e;
    f = -f;
}

void AffineMatrix::vertReflect()
{
    a = -a;
    b = -b;
    c = -c;
}

void AffineMatrix::horizVertReflect()
{
    a = -a;
    b = -b;
    c = -c;
    d = -d;
    e = -e;
    f = -f;
}

void AffineMatrix::invertXform(struct xForm &xf)
{
    float Z = xf.a * xf.e - xf.b * xf.d;
    float z = 1.f / Z;
    
    float A =  xf.e / z;
    float D = -xf.b / z;
    float G = (xf.b * xf.f - xf.c * xf.e) / z;
    float B = -xf.d / z;
    float E =  xf.a / z;
    float H = (xf.c * xf.d - xf.a * xf.f) / z;
    
    xf.a = A;
    xf.b = D;
    xf.c = G;
    xf.d = B;
    xf.e = E;
    xf.f = H;
}

void AffineMatrix::invertPostXform(struct xForm &xf)
{
    float Z = xf.pa * xf.pe - xf.pb * xf.pd;
    float z = 1.f / Z;
    
    float A =  xf.pe / z;
    float D = -xf.pb / z;
    float G = (xf.pb * xf.pf - xf.pc * xf.pe) / z;
    float B = -xf.pd / z;
    float E =  xf.pa / z;
    float H = (xf.pc * xf.pd - xf.pa * xf.pf) / z;
    
    xf.pa = A;
    xf.pb = D;
    xf.pc = G;
    xf.pd = B;
    xf.pe = E;
    xf.pf = H;
}


void AffineMatrix::setIdentity()
{
    a = 1.f;
    b = 0.f;
    c = 0.f;
    d = 0.f;
    e = 1.f;
    f = 0.f;
}

void AffineMatrix::setXformToIdentity(struct xForm &xf)
{
    xf.a = 1.f;
    xf.b = 0.f;
    xf.c = 0.f;
    xf.d = 0.f;
    xf.e = 1.f;
    xf.f = 0.f;
}

void AffineMatrix::setPostXformToIdentity(struct xForm &xf)
{
    xf.pa = 1.f;
    xf.pb = 0.f;
    xf.pc = 0.f;
    xf.pd = 0.f;
    xf.pe = 1.f;
    xf.pf = 0.f;
}

AffineMatrix * AffineMatrix::matrixWithRotation(float theta)
{
    return new AffineMatrix(cosf(theta),
                           -sinf(theta),
                            0.f,
                            sinf(theta),
                            cosf(theta),
                            0.f);
}

AffineMatrix * AffineMatrix::matrixWithScale(float x)
{
    return new AffineMatrix(x, 0.f, 0.f, 0.f, x, 0.f);
}

AffineMatrix * AffineMatrix::matrixWithTranslate(const Point &t)
{
    return new AffineMatrix(1.f, 0.f, t.x, 0.f, 1.f, t.y);
}

AffineMatrix * AffineMatrix::matrixWithXform(const struct xForm &xf)
{
    return new AffineMatrix(xf, false);
}

AffineMatrix * AffineMatrix::matrixWithPostXform(const struct xForm &xf)
{
    return new AffineMatrix(xf, true);
}

AffineMatrix * AffineMatrix::matrixWithInverseOfXform(const struct xForm &xf)
{
    AffineMatrix *m = new AffineMatrix(xf.a, xf.b, xf.c, xf.d, xf.e, xf.f);
    m->invert();
    return m;
}

AffineMatrix * AffineMatrix::matrixWithInverseOfPostXform(const struct xForm &xf)
{
    AffineMatrix *m = new AffineMatrix(xf.pa, xf.pb, xf.pc, xf.pd, xf.pe, xf.pf);
    m->invert();
    return m;
}

AffineMatrix * AffineMatrix::matrixWithMatrix(const AffineMatrix &m)
{
    return new AffineMatrix(m);
}

AffineMatrix * AffineMatrix::matrixWithCoefs(float _a, float _b, float _c, float _d, float _e, float _f)
{
    return new AffineMatrix(_a, _b, _c, _d, _e, _f);
}

void AffineMatrix::fillWith(float _a, float _b, float _c, float _d, float _e, float _f)
{
    a = _a;
    b = _b;
    c = _c;
    d = _d;
    e = _e;
    f = _f;
}

void AffineMatrix::fillXform(struct xForm &xf)
{
    xf.a = a;
    xf.b = b;
    xf.c = c;
    xf.d = d;
    xf.e = e;
    xf.f = f;
}

void AffineMatrix::fillPostXform(struct xForm &xf)
{
    xf.pa = a;
    xf.pb = b;
    xf.pc = c;
    xf.pd = d;
    xf.pe = e;
    xf.pf = f;
}

bool AffineMatrix::isIdentityXform(const struct xForm &xf)
{
    return xf.a == 1.f && xf.b == 0.f && xf.c == 0.f && xf.d == 0.f && xf.e == 1.f && xf.f == 0.f;
}

bool AffineMatrix::isIdentityPostXform(const struct xForm &xf)
{
    return xf.pa == 1.f && xf.pb == 0.f && xf.pc == 0.f && xf.pd == 0.f && xf.pe == 1.f && xf.pf == 0.f;
}

AffineMatrix * AffineMatrix::randomPreMatrix()
{
    // pick some random affine matrix values
    float affineSelect   = randValue();
    AffineMatrix *matrix = new AffineMatrix();
    
    // 20% change of identity matrix
    if (affineSelect > 0.2f && affineSelect <= 0.4f) {
        matrix->rotateBy(randInRange(0.f, 2 * M_PI));
    }
    if (affineSelect > 0.4f && affineSelect <= 0.6f) {
        matrix->translateBy(Point(randInRange(0.f, 1.5f), randInRange(0.f, 1.5f)));
    }
    if (affineSelect > 0.6f && affineSelect <= 0.8f) {
        matrix->scaleBy(randInRange(0.25f, 1.5f));
    }
    else if (affineSelect > 0.8f) {
        matrix->rotateBy(randInRange(0.f, 2 * M_PI));
        matrix->translateBy(Point(randInRange(0.f, 1.5f), randInRange(0.f, 1.5f)));
        matrix->scaleBy(randInRange(0.f, 1.2f));
    }
    return matrix;
}

AffineMatrix * AffineMatrix::randomPostMatrix()
{
    // post affine matrix values
    float affineSelect   = randValue();
    AffineMatrix *matrix = new AffineMatrix();
    
    // 60% change of identity matrix
    if (affineSelect > 0.6f && affineSelect <= 0.7f) {
        matrix->rotateBy(randInRange(0.f, 2 * M_PI));
    }
    if (affineSelect > 0.7f && affineSelect <= 0.8f) {
        matrix->translateBy(Point(randInRange(0.f, 1.5f), randInRange(0.f, 1.5f)));
    }
    if (affineSelect > 0.8f && affineSelect <= 0.9f) {
        matrix->scaleBy(randInRange(0.25f, 1.5f));
    }
    else if (affineSelect > 0.9f) {
        matrix->rotateBy(randInRange(0.f, 2 * M_PI));
        matrix->translateBy(Point(randInRange(0.f, 1.5f), randInRange(0.f, 1.5f)));
        matrix->scaleBy(randInRange(0.f, 1.2f));
    }
    return matrix;
}

AffineMatrix * AffineMatrix::interpolateFrom(const AffineMatrix &fromMatrix,
                                             const AffineMatrix &toMatrix,
                                             float amount,
                                             bool polarInterpolation)
{
    float fa = fromMatrix.a;
    float fb = fromMatrix.b;
    float fc = fromMatrix.c;
    float fd = fromMatrix.d;
    float fe = fromMatrix.e;
    float ff = fromMatrix.f;
    float ta = toMatrix.a;
    float tb = toMatrix.b;
    float tc = toMatrix.c;
    float td = toMatrix.d;
    float te = toMatrix.e;
    float tf = toMatrix.f;
    
    if (polarInterpolation) {
        // convert to polar coordinates, then interpolate the radiuses & angles
        float fangle      = atan2f(fd, fa);
        float fr          = sqrtf(fd*fd + fa*fa);
        float tangle      = atan2f(td, ta);
        float tr          = sqrtf(td*td + ta*ta);
        float deltaAngle  = tangle - fangle;
        
        if (fabsf(deltaAngle) > M_PI) {
            if (tangle < fangle)
                tangle = tangle + 2 * M_PI;
                else
                    fangle = fangle + 2 * M_PI;
                    }
        float rangle  = fangle + amount * (tangle - fangle);
        float rradius = fr + amount * (tr - fr);
        
        float a = rradius * cosf(rangle);
        float d = rradius * sinf(rangle);
        
        fangle      = atan2f(fe, fb);
        fr          = sqrtf(fe*fe + fb*fb);
        tangle      = atan2f(te, tb);
        tr          = sqrtf(te*te + tb*tb);
        deltaAngle  = tangle - fangle;
        
        if (fabsf(deltaAngle) > M_PI) {
            if (tangle < fangle)
                tangle = tangle + 2 * M_PI;
                else
                    fangle = fangle + 2 * M_PI;
                    }
        rangle  = fangle + amount * (tangle - fangle);
        rradius = fr + amount * (tr - fr);
        
        float b = rradius * cosf(rangle);
        float e = rradius * sinf(rangle);
        
        return  new AffineMatrix(a, b, fc + amount *(tc -fc), d, e, ff + amount *(tf -ff));
    }
    else {
        return  new AffineMatrix(fa + amount *(ta -fa),
                                 fb + amount *(tb -fb),
                                 fc + amount *(tc -fc),
                                 fd + amount *(td -fd),
                                 fe + amount *(te -fe),
                                 ff + amount *(tf -ff));
    }
}

