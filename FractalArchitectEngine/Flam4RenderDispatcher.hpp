//
//  Flam4RenderDispatcher.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 7/31/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//

#ifndef Flam4RenderDispatcher_hpp
#define Flam4RenderDispatcher_hpp

#include "RenderEnums.hpp"

#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>

class RenderState;
class Flam4RenderDispatcher;

using SharedRenderState = std::shared_ptr<RenderState>;
using SharedDispatcher  = std::shared_ptr<Flam4RenderDispatcher>;

using namespace std::chrono;

struct Allocation {
    size_t bladeIndex;
    size_t subSample;
};


class Flam4RenderDispatcher {
	friend class RenderTile;
    SharedRenderState renderState;
    volatile size_t   bladeIndex;
    volatile size_t   subSample;
    size_t            renderedBatches;
    enum ProceedType  proceed;
    
    std::mutex        mutex;
	time_point<high_resolution_clock> now;

public:
    Flam4RenderDispatcher(SharedRenderState _renderState, size_t _bladeIndex, size_t _subSample, time_point<high_resolution_clock> _now);
    
    size_t getBladeIndex();
    void setBladeIndex(size_t _bladeIndex);
    
    size_t getSubSample();
    void setSubSample(size_t _subSample);
    
    size_t getRenderedBatches();
    void setRenderedBatches(size_t _renderedBatches);
    
    enum ProceedType getProceed();
    void setProceed(enum ProceedType _proceed);
    
    size_t checkSetup();
    
    struct Allocation allocateBatch();
    
    void batchCompleted();
    
    bool lastSubSampleForSubSample(size_t _subSample, size_t _bladeIndex);
    size_t batchesToGo();
};

#endif /* Flam4RenderDispatcher_hpp */
