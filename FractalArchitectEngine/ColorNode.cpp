//
//  ColorNode.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/8/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "ColorNode.hpp"
#include <math.h>
#include "common.hpp"

ColorNode::ColorNode(const Color &_color, float _location)
    : color(_color), location(_location)
{}

bool ColorNode::operator==(const ColorNode &c)
{
    if (color.red != c.color.red)
        return false;
    if (color.green != c.color.green)
        return false;
    if (color.blue != c.color.blue)
        return false;
    if (color.alpha != c.color.alpha)
        return false;
    if (location != c.location)
        return false;
    return true;
}

std::string ColorNode::description()
{
    static char buf[40];
    snprintf(buf, sizeof(buf), "%s  %s %f",  "ColorNode", color.hexadecimalValueOfAnNSColor().c_str(), location);
    return  buf;
}

std::string ColorNode::lineDescriptionForColorNodeCount(size_t colorNodeCount)
{
    static char buf[40];
    float maxIndex = (float)colorNodeCount -1.f;
    snprintf(buf, sizeof(buf), "#%3zu: %s  Loc: %6.6f",
            (size_t)roundf(location * maxIndex) + 1,
            color.hexadecimalValueOfAnNSColor().c_str(), location);
    return  buf;
}

std::string Color::hexadecimalValueOfAnNSColor()
{
    using uint = unsigned int;
    static char s[20];
    snprintf(s, sizeof(s), "#%02X%02X%02X", (uint)(red*255.f), (uint)(green*255.f), (uint)(blue*255.f));
    
    return std::string(s);
}

Color::Color(const rgba &c)
:  red(c.r), green(c.g), blue(c.b), alpha(c.a)
{}

bool Color::operator==(const Color & c)
{
    if (red != c.red)
        return  false;
    if (green != c.green)
        return  false;
    if (blue != c.blue)
        return  false;
    if (alpha != c.alpha)
        return  false;
    return true;
}

float ColorNode::red()
{
    return 255.0f * color.red;
}

float ColorNode::green()
{
    return 255.0f * color.green;
}

float ColorNode::blue()
{
    return 255.0f * color.blue;
}

hsva rgba::RGBtoHSV(const rgba & color)
{
    float r = color.r;
    float g = color.g;
    float b = color.b;
    float mx = fmax(fmax(r,g),b);
    float mn = fmin(fmin(r,g),b);
    float h,s,v;
    if (mx == mn)
        h = 0.0f;
    else if (mx == r)
        h = .16666666667f*(g-b)/(mx-mn);
    else if (mx == g)
        h = .16666666667f*(b-r)/(mx-mn)+.33333333f;
    else
        h = .16666666667f*(r-g)/(mx-mn)+.66666667f;
    h = h-floor(h);
    if (mx == 0.0f)
        s = 0.0f;
    else
        s = (mx-mn)/(mx);
    v = mx;
    if (v > 1.0f) // clamp to 1.f if to high value
        v = 1.0f;
    return hsva(h, s, v, color.a);
}

hsva rgba::RGBtoHSVHueAdjusted(const rgba & color)
{
    float r = color.r;
    float g = color.g;
    float b = color.b;
    float mx = fmax(fmax(r,g),b);
    float mn = fmin(fmin(r,g),b);
    float h,s,v;
    if (mx == mn)
        h = 0.0f;
    else if (mx == r)
        h = .16666666667f*(g-b)/(mx-mn);
    else if (mx == g)
        h = .16666666667f*(b-r)/(mx-mn)+.33333333f;
    else
        h = .16666666667f*(r-g)/(mx-mn)+.66666667f;
    h = h-floor(h);
    if (mx == 0.0f)
        s = 0.0f;
    else
        s = (mx-mn)/(mx);
    v = mx;
    if (v > 1.0f)
    {
        if (h < .33333333f)
        {
            h += (.16666667f-h)*(1.0f-powf(.75f,v-1.0f));
        }
        else if (h < 0.5f)
        {
            h += (h-0.5f)*(1.0f-powf(.75f,v-1.0f));
        }
        else if (h > 0.8333333f)
        {
            h += (h-0.8333333f)*(1.0f-powf(.75f,v-1.0f));
        }
        //float l = .2126f*r+.7152f*g+.0722f*b;
        //float l = (40.0f*r+20.0f*g+b)/61.0f;
        float l = 0.4f+0.4f*cosf(2.0f*M_PI*(h-0.16666666667f));
        float s1 = s*powf(1.0f/v,0.6f*(1.0f-l));
        s= s < s1 ? s : s1;
    }
    hsva result(h, s, v, color.a);
    return result;
}

rgba hsva::HSVtoRGB(const hsva & color)
{
    float h = color.h;
    float s = color.s;
    float v = color.v;
    float r,g,b;
    int hi = ((int)floor(h*6.0f))%6;
    float f = h*6.0f-floor(h*6.0f);
    float p = v*(1.0f-s);
    float q = v*(1.0f-f*s);
    float t = v*(1.0f-(1.0f-f)*s);
    switch (hi)
    {
        case 0:
        default:
        {
            r = v;
            g = t;
            b = p;
        }break;
        case 1:
        {
            r = q;
            g = v;
            b = p;
        }break;
        case 2:
        {
            r = p;
            g = v;
            b = t;
        }break;
        case 3:
        {
            r = p;
            g = q;
            b = v;
        }break;
        case 4:
        {
            r = t;
            g = p;
            b = v;
        }break;
        case 5:
        {
            r = v;
            g = p;
            b = q;
        }break;
    }
    rgba result(r, g, b, color.a);
    return result;
}
