//
//  Utilities.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/2/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifdef __APPLE__
#include <uuid/uuid.h>
#include <unistd.h>
#include <syslog.h>
#include <libgen.h>
#include <sys/errno.h>

#elif defined(_WIN32)
#include <stdlib.h>
#include <cctype>
#include <direct.h>
#include <Windows.h>
#include <errno.h>
#include <rpc.h>
#endif

#include <sys/stat.h>
#include "Utilities.hpp"
#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>

#ifdef __APPLE__
#include <mach/mach_time.h>
#else
#include <time.h>
#endif
#ifdef __linux
#include <uuid/uuid.h>
#include <libgen.h>
#endif

#include <string>
#include <string.h>

#pragma mark Utilities

// create a new GUID
std::string stringWithUUID()
{
#ifdef _WIN32
	uuid_t uuid;
	UuidCreate(&uuid);
	unsigned char *buf;
	UuidToString(&uuid, &buf);

	std::string s = std::string((const char *)buf);
	RpcStringFree(&buf);
	return s;
#else
	static char s[10];

	uuid_t buf;
    	uuid_generate(buf);

	std::string uuid;

	for (int i = 0; i < sizeof(buf); i++) {
		snprintf(s, sizeof(s), "%02X", buf[i]);
		uuid.append(s);
		if (i == 3)
			uuid.append("-");
		else if (i == 5)
			uuid.append("-");
		else if (i == 7)
			uuid.append("-");
		else if (i == 9)
			uuid.append("-");
	}
	return uuid;
#endif
}

// character case insenstive compare
static bool icompare_pred(unsigned char a, unsigned char b)
{
    return std::tolower(a) < std::tolower(b);
}

// character case insenstive equals
static bool iequal_pred(unsigned char a, unsigned char b)
{
    return std::tolower(a) == std::tolower(b);
}

// case insensitive string compare
bool icompare(std::string const& a, std::string const& b)
{
    if (a.length()==b.length()) {
        return std::equal(b.begin(), b.end(),
                          a.begin(), icompare_pred);
    }
    return a.length() < b.length();
}

// case insensitive string equals
bool iequal(std::string const& a, std::string const& b)
{
    if (a.length()==b.length()) {
        return std::equal(b.begin(), b.end(),
                          a.begin(), iequal_pred);
    }
    return false;
}

// log this message to the system log
void systemLog(const std::string &msg)
{
    systemLog(msg.c_str());
}

// log this message to the system log
void systemLog(const char *msg)
{
    printf("%s\n", msg);
#ifdef __APPLE__
    openlog("slog", LOG_PID|LOG_CONS, LOG_USER);
    syslog(LOG_INFO, "%s\n", msg);
    closelog();
#endif
}

// get the filename portion from the path
std::string fileNameFromPath(const std::string &path)
{
    size_t index = path.rfind("/");
    if (index != std::string::npos) {
        return path.substr(index + 1);
    }
    return std::string(path);
}

// get the filename portion from the path
const char * directoryNameFromPath(const std::string &path)
{
    struct stat attrs;
    // if path refers to an existing  directory already
    if (stat(path.c_str(), &attrs) != -1 && attrs.st_mode == S_IFDIR)
        return path.c_str();

    size_t index = path.rfind("/");
    if (index != std::string::npos) {
        return path.substr(0, index - 1).c_str();
    }
    return std::string(path).c_str();
}

// does this file exist at the path?
bool fileExists(const std::string& spath)
{
    return fileExists(spath.c_str());
}

// does this file exist at the path?
bool fileExists(const char *path)
{
    struct stat   buffer;
    int status = stat (path, &buffer);
    return status == 0;
}

// trim the specified character from both ends of the string
std::string string_trim(const std::string &s, char _char)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && *it == _char)
        it++;
//
    std::string::const_reverse_iterator rit = s.rbegin();
    while (rit.base() != it && *rit == _char)
        rit++;

    return std::string(it, rit.base());
}

template std::string  string_format<float>(const char *, float);

// make a pretty string from float values for reporting purposes
std::string prettyFloat(float value)
{
	static char buf[20];
	snprintf(buf, sizeof(buf), "A%0.4lf", value);
	std::string s(buf);
    //std::string s = string_format("A%0.4lf", value);
    s = string_trim(s, '0');
    s = string_trim(s, '.');
    return s.substr(1);
}

// make a pretty string from float values for reporting purposes - keeps the float .f format
std::string prettyFloatF(float value)
{
	static char buf[20];
	snprintf(buf, sizeof(buf), "A%0.4lf", value);
	std::string s(buf);
	//std::string s = string_format("A%0.4lf", value);
    s = string_trim(s, '0');
    if (s.find(".") == std::string::npos)
        s.append(".");
    return s.substr(1).append("f");
}

// recursive mkdir
#ifdef _WIN32

char * dirname(char *path)
{
	static char path_buffer[_MAX_PATH];

	strncpy(path_buffer, path, _MAX_PATH- 1);
	path_buffer[_MAX_PATH - 1] = '\0';

	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	errno_t err;

	// remove trailing '/' or '\'
	if (path_buffer[strlen(path_buffer) - 1] == '/' || path_buffer[strlen(path_buffer) - 1] == '\\')
		path_buffer[strlen(path_buffer) - 1] = '\0';

	err = _splitpath_s(path_buffer, drive, _MAX_DRIVE, dir, _MAX_DIR, fname,
		_MAX_FNAME, ext, _MAX_EXT);
	if (err != 0)
		return ".";

	err = _makepath_s(path_buffer, _MAX_PATH, drive, dir, "", "");

	return path_buffer;
}


int mkpath(const char *s, int mode)
{
	char *q, *r = NULL, *path = NULL, *up = NULL;
	int rv;

	rv = -1;
#ifdef _WIN32
	if (strcmp(s, ".") == 0 || strcmp(s, "\\") == 0 || strcmp(s, "/") == 0 || (strlen(s) == 3 && (s[2] == '\\' || s[2] == '/')))
#else
	if (strcmp(s, ".") == 0 || strcmp(s, "/") == 0)
#endif
		return (0);

	if ((path = strdup(s)) == NULL)
		exit(1);

	if ((q = strdup(s)) == NULL)
		exit(1);

	if ((r = dirname(q)) == NULL)
		goto out;

	if ((up = strdup(r)) == NULL)
		exit(1);

	if ((mkpath(up, mode) == -1) && (errno != EEXIST))
		goto out;

	if ((_mkdir(path) == -1) && (errno != EEXIST))
		rv = -1;
	else
		rv = 0;

out:
	if (up != NULL)
		free(up);
	free(q);
	free(path);
	return (rv);
}
#else
int mkpath(const char *s, mode_t mode)
{
    char *q, *r = NULL, *path = NULL, *up = NULL;
    int rv;

    rv = -1;
    if (strcmp(s, ".") == 0 || strcmp(s, "/") == 0)
        return (0);

    if ((path = strdup(s)) == NULL)
        exit(1);

    if ((q = strdup(s)) == NULL)
        exit(1);

    if ((r = dirname(q)) == NULL)
        goto out;

    if ((up = strdup(r)) == NULL)
        exit(1);

    if ((mkpath(up, mode) == -1) && (errno != EEXIST))
        goto out;

    if ((mkdir(path, mode) == -1) && (errno != EEXIST))
        rv = -1;
    else
        rv = 0;

out:
    if (up != NULL)
        free(up);
    free(q);
    free(path);
    return (rv);
}
#endif

// convert integer i to string
const char * itoa(int i)
{
    static char buf[20];
    snprintf( buf, sizeof(buf), "%i", i); // Extra space for '\0'
    return buf;
}

#ifdef __APPLE__
time_t GetTickCount(void)
{
    static mach_timebase_info_data_t    sTimebaseInfo;

    // If this is the first time we've run, get the timebase.
    // We can use denom == 0 to indicate that sTimebaseInfo is
    // uninitialised because it makes no sense to have a zero
    // denominator is a fraction.

    if ( sTimebaseInfo.denom == 0 ) {
        (void) mach_timebase_info(&sTimebaseInfo);
    }

    // Convert to nanoseconds.

    // Do the maths.  We hope that the multiplication doesn't
    // overflow; the price you pay for working in fixed point.

    return (time_t)(mach_absolute_time()/1000000) * sTimebaseInfo.numer / sTimebaseInfo.denom;
}
#elif !defined(_WIN32)
time_t GetTickCount(void)
{
	timespec time1;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);

	return 1000000 * (unsigned long long)time1.tv_sec + time1.tv_nsec;
}
#endif

