//
//  common.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 4/29/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef common_h
#define common_h

#include <string>

struct alignas(16) FAPoint
{
    float x;
    float y;
    float z;
    float pal;
} ;

namespace FA {
    struct Point {
        float x;
        float y;
        
        explicit Point() = default;
        explicit Point(float _x, float _y) : x(_x), y(_y) {}
    };

    struct Point3D {
        float x;
        float y;
        float z;

        explicit Point3D() = default;
        explicit Point3D(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}
    };

    struct Size {
        float width;
        float height;
        
        explicit Size() = default;
        explicit Size(float _width, float _height) : width(_width), height(_height) {}
    };

    struct Rect {
        Point origin;
        Size size;
        
        explicit Rect() = default;
        explicit Rect(Point _origin, Size _size) : origin(_origin), size(_size) {}
        explicit Rect(float x, float y, float width, float height): origin(Point(x, y)), size(Size(width, height)) {}
        
    };

    inline float midX(Rect & r) { return r.origin.x + 0.5f * r.size.width; }
    inline float midY(Rect & r) { return r.origin.y + 0.5f * r.size.height; }
}

struct rgba;
struct Color {
    float red;
    float green;
    float blue;
    float alpha;
    
    Color() : red(0.f), green(0.f), blue(0.f), alpha(1.f) {}
    Color(float _red, float _green, float _blue, float _alpha=1.f) : red(_red), green(_green), blue(_blue), alpha(_alpha) {}
    Color(const Color &c) :  red(c.red), green(c.green), blue(c.blue), alpha(c.alpha) {}
    Color(const rgba &c);
    
    bool operator==(const Color & c);
    bool operator!=(const Color & c)    { return ! operator==(c); }

    std::string hexadecimalValueOfAnNSColor();
};

struct hsva;
struct alignas(16) rgba
{
    float r;
    float g;
    float b;
    float a;
    
    rgba() : r(0.f), g(0.f), b(0.f), a(1.f) {}
    rgba(float _r, float _g, float _b, float _a = 1.f) : r(_r), g(_g), b(_b), a(_a) {}
    rgba(const rgba &c)   :  r(c.r), g(c.g), b(c.b), a(c.a) {}
    rgba(const Color & c) : r(c.red), g(c.green), b(c.blue), a(c.alpha){}
    
    
    static hsva RGBtoHSV(const rgba & color);
    static hsva RGBtoHSVHueAdjusted(const rgba & color);
    
};

struct alignas(16) hsva
{
    float h;
    float s;
    float v;
    float a;
    
    explicit hsva() : h(0.f), s(0.f), v(0.f), a(1.f) {}
    explicit hsva(float _h, float _s, float _v, float _a = 1.f) : h(_h), s(_s), v(_v), a(_a) {}
    
    static rgba HSVtoRGB(const hsva & color);
    
};


#endif /* common_h */
