//
//  ClProgram.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/17/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "ClProgram.hpp"
#include "ClDeviceKind.hpp"
#include "DefinesOCL.h"
#include "VariationSet.hpp"
#include "Utilities.hpp"
#include "ClPlatform.hpp"
#include "ClContext.hpp"

#ifdef _WIN32
#include <Windows.h>
#include "WindowsInterface.hpp"
#endif

#include <string>

using std::string;

static char buf[1024];

static bool checkErr(cl_int err, const char *name)
{
    if (err != CL_SUCCESS) {
        size_t size = snprintf( nullptr, 0, "ERROR: %s (%u)", name, err) + 1; // Extra space for '\0'
        
        std::unique_ptr<char[]> buf(new char[size]);
        
        snprintf(buf.get(), size, "ERROR: %s (%u)", name, err);
        systemLog(buf.get());
        return true;
    }
    return false;
}

ClDeviceKind * ClProgram::clDeviceKind()
{
    return static_cast<ClDeviceKind *>(deviceKind.lock().get());
}

bool outOfDateBinary(const string & binaryPath, const string &sourcePath)
{
    if (! fileExists(binaryPath))
        return true;
    
    struct stat sourceAttributes;
    struct stat binaryAttributes;
    
    stat(binaryPath.c_str(), &binaryAttributes);
    stat(sourcePath.c_str(), &sourceAttributes);
    
    time_t sourceModDate = sourceAttributes.st_mtime;
    time_t binaryModDate = binaryAttributes.st_mtime;
    
    return binaryModDate < sourceModDate;
}

ClProgram::ClProgram(SharedClContext ctx, SharedDeviceKind _deviceKind, bool forceRebuild, const SharedVariationSet &_variationSet)
: DeviceProgram(ctx, _deviceKind, _variationSet)
{
    iteratePointsKernal = nullptr;
    setBufferKernal = nullptr;
    reductionKernal = nullptr;
    postProcessStep1Kernal = nullptr;
    postProcessStep2Kernal = nullptr;
    colorCurveRGB3ChannelsKernal = nullptr;
    colorCurveRGBChannelKernal = nullptr;
    FlexibleDensityEstimationKernal = nullptr;
    RGBA128FtoRGBA32UKernal = nullptr;
    RGBA128FtoBGRA32UKernal = nullptr;
    RGBA128FtoRGBA64UKernal = nullptr;
    RGBA128FtoRGBA128FKernal = nullptr;
    MergeKernal = nullptr;
    readChannelKernal = nullptr;
    writeChannelKernal = nullptr;
    writeChannelStripedKernal = nullptr;
    convolveRowKernal = nullptr;
    convolveColKernal = nullptr;
    
    program = nullptr;
    
    cl_int errNum = 0;
    string includeDirective;
    
    time_point<high_resolution_clock> now = high_resolution_clock::now();
    
    std::vector<char> binaryContents = binaryContentsForUuid(uuid);

    cl_device_id *deviceIDs =  (cl_device_id *)clDeviceKind()->deviceIDs().data();

    string sourcePath = _deviceKind->isCPU() ? variationSet->CPUKernelURL() : variationSet->GPUKernelURL(useOpenCL);

	forceRebuild = ctx->forceRebuild;

    SharedClDeviceKind clDeviceKind = std::dynamic_pointer_cast<ClDeviceKind>(_deviceKind);

#ifdef _WIN32
	// templates are embedded in DLL
	if (VariationSet::outOfDateDerived(sourcePath, getThisDLLPath()))
		forceRebuild = true;
#endif
    bool outOfDate    = outOfDateBinary(pathForBinaryForUuid(uuid), sourcePath);
    uint warpSize     = clDeviceKind->determineWarpSizeForDevice(deviceIDs[0]);
    
    if (binaryContents.size() == 0)
        forceRebuild = true;
    if (outOfDate)
        forceRebuild = true;
    
    // build from prebuilt binaries
    if (!forceRebuild) {
        program = loadProgramFromBinaryContents(binaryContents, deviceIDs);
        if (program == nullptr) { // loading program can fail after OpenCL driver updates
            forceRebuild = true;
            program = createProgramFromSourceWithWarpSize(warpSize, includeDirective);
        }
    }
    else
        program = createProgramFromSourceWithWarpSize(warpSize, includeDirective);
    if (program == NULL) {
        throw  std::runtime_error("Create program failed");
    }
    // Build program from either source or binaries
    errNum = clBuildProgram(program,
                            (cl_uint)_deviceKind->numClDevices(),
                            deviceIDs,
                            includeDirective.c_str(),
                            nullptr,
                            nullptr);

    systemLog("Compiling OpenCL program finished");

    if (errNum != CL_SUCCESS)
    {
        logErrStatusForProgramName(sourcePath, deviceIDs);

        throw  std::runtime_error("Build programe failed");
    }
    // if built from source
    else if (forceRebuild) {
        saveBinariesForDeviceIDs(deviceIDs);
    }
    makeKernelsForDeviceIDs(deviceIDs);
    
    buildTime = duration_cast<duration<double>>(high_resolution_clock::now() - now);
}

ClProgram::~ClProgram()
{
    clReleaseKernel(iteratePointsKernal);
    clReleaseKernel(setBufferKernal);
    clReleaseKernel(reductionKernal);
    clReleaseKernel(postProcessStep1Kernal);
    clReleaseKernel(postProcessStep2Kernal);
    clReleaseKernel(colorCurveRGB3ChannelsKernal);
    clReleaseKernel(colorCurveRGBChannelKernal);
    clReleaseKernel(FlexibleDensityEstimationKernal);
    clReleaseKernel(RGBA128FtoRGBA32UKernal);
    clReleaseKernel(RGBA128FtoBGRA32UKernal);
    clReleaseKernel(RGBA128FtoRGBA64UKernal);
    clReleaseKernel(RGBA128FtoRGBA128FKernal);
    clReleaseKernel(MergeKernal);
    clReleaseKernel(readChannelKernal);
    clReleaseKernel(writeChannelKernal);
    clReleaseKernel(writeChannelStripedKernal);
    clReleaseKernel(convolveRowKernal);
    clReleaseKernel(convolveColKernal);
    
    clReleaseProgram(program);
}

size_t ClProgram::getMaxWorkGroupSizeForKernel(cl_kernel kernel,  cl_device_id device)
{
    size_t maxWorkGroupSize = 0UL;
    clGetKernelWorkGroupInfo(kernel, device,
                             CL_KERNEL_WORK_GROUP_SIZE,
                             sizeof(maxWorkGroupSize),
                             &maxWorkGroupSize,
                             NULL);
    return maxWorkGroupSize;
}

cl_program ClProgram::loadProgramFromBinaryContents(std::vector<char> & binaryContents, cl_device_id *deviceIDs)
{
    ClDeviceKind *pDeviceKind = static_cast<ClDeviceKind *>(deviceKind.lock().get());
    
    cl_uint numDevices             = (cl_uint)pDeviceKind->numClDevices();
    size_t *binarySizes            = (size_t *)malloc(sizeof(size_t) *numDevices);
    const unsigned char **binaries = (const unsigned char **)malloc(sizeof(const unsigned char *)*numDevices);
    cl_int *binaryStatuses         = (cl_int *)malloc(sizeof(cl_int) *numDevices);
    
    const unsigned char *binary = (const unsigned char *)binaryContents.data();
    size_t length               = binaryContents.size();
    for (int i = 0; i < numDevices; i++) {
        binaries[i]    = binary;
        binarySizes[i] = length;
        binaryStatuses[i] = CL_SUCCESS;
    }
    
    // Create program from binaries
    cl_int errNum = 0;
    
    cl_program _program = clCreateProgramWithBinary(pDeviceKind->context,
                                                    numDevices,
                                                    deviceIDs,
                                                    binarySizes,
                                                    binaries,
                                                    binaryStatuses,
                                                    &errNum);
    
    if (checkErr(errNum, "clCreateProgramWithBinary")) {
        free(binarySizes);
        free(binaries);
        free(binaryStatuses);
        return NULL;
    }
    for (int i = 0; i < numDevices; i++) {
        if (binaryStatuses[i] != CL_SUCCESS) {
            free(binarySizes);
            free(binaries);
            free(binaryStatuses);
            return NULL;
        }
    }
    
    free(binarySizes);
    free(binaries);
    free(binaryStatuses);
    return _program;
}

cl_program ClProgram::createProgramFromSourceWithWarpSize(uint warpSize, std::string & includeDirective)
{
    cl_int errNum          = 0;
    string sourceStr = sourceContents(useOpenCL);
    const char *src = sourceStr.c_str();
    size_t length   = sourceStr.length();

    if (warpSize > 1) {
        snprintf(buf, sizeof(buf), "-D KERNEL_RUNTIME -D WARP_SIZE=%u -D NUM_POINTS=%u -D DENSITY_KERNAL_RADIUS=%u",
                  warpSize, NUM_POINTS_WARPSIZE_32/32*warpSize, clDeviceKind()->maxDensityEstimationRadius());
    }
    else {  // Apple only for CPU
        //  NOTE: this sets NUM_POINTS to 2 not 64 - but with value 64 we get render artifacts ????
        snprintf(buf, sizeof(buf), "-D KERNEL_RUNTIME -D WARP_SIZE=32 -D NUM_POINTS=%u -D DENSITY_KERNAL_RADIUS=%u",
                 2*warpSize, clDeviceKind()->maxDensityEstimationRadius());
    }
    includeDirective = buf;
    
    if (! variationSet->is3DCapable)
        includeDirective.append(" -D FOR_2D");
#ifdef __linux
	// for Intel platform, compile for Debug and add the path to the CL source file
	const std::string &sourceURL = variationSet->kernelURL(deviceKind.lock()->isGPU(), useOpenCL);
	cl_platform_id platformID    = clDeviceKind()->platformInstances[0];
	std::string platformVendor   = ClPlatform::platformVendor(platformID);
	std::string platformName = ClPlatform::platformName(platformID);
	size_t pos = platformVendor.find("Intel");

	if (pos != std::string::npos)
		includeDirective.append(" -g -s \"").append(sourceURL).append("\"");
#endif
    
#ifdef _WIN32
	// for Intel platform, compile for Debug and add the path to the CL source file
	const std::string &sourceURL = variationSet->kernelURL(deviceKind.lock()->isGPU(), useOpenCL);
	cl_platform_id platformID    = clDeviceKind()->platformInstances[0];
	std::string platformVendor   = ClPlatform::platformVendor(platformID);
	std::string platformName = ClPlatform::platformName(platformID);
	size_t pos = platformVendor.find("Intel");

	if (pos != std::string::npos)
		includeDirective.append(" -g -s \"").append(sourceURL).append("\"");

	// for AMD platform, compile for Debug and enable printf
	pos = platformName.find("AMD");
    if (pos != std::string::npos) {
		//includeDirective.append(" -g -O0");          // NOTE: for CodeXL -- it turns off optimization
//        includeDirective.append(" -D AMD_PRINTF");   // NOTE: Uncomment this to enable AMD printf extension in kernels
	}

#endif
    // Create program from source
    cl_program _program = clCreateProgramWithSource(clDeviceKind()->context,
                                                    1,
                                                    &src,
                                                    &length,
                                                    &errNum);
    if (checkErr(errNum, "clCreateProgramWithSource"))
        return nullptr;
    return _program;
}

void ClProgram::logErrStatusForProgramName(std::string &programName, cl_device_id * deviceIDs)
{
    // Determine the reason for the error
    for (int i = 0; i <  clDeviceKind()-> numDevices(); i++) {
        cl_build_status status = CL_BUILD_NONE;
        clGetProgramBuildInfo(program,
                              deviceIDs[i],
                              CL_PROGRAM_BUILD_STATUS,
                              sizeof(status),
                              &status,
                              NULL);
        if (status == CL_BUILD_SUCCESS)
            continue;
        
        size_t buildLogSize = 0;
        clGetProgramBuildInfo(program,
                              deviceIDs[i],
                              CL_PROGRAM_BUILD_LOG,
                              0,
                              NULL,
                              &buildLogSize);
        char *buildLog = (char *)malloc(buildLogSize);
        
        clGetProgramBuildInfo(program,
                              deviceIDs[i],
                              CL_PROGRAM_BUILD_LOG,
                              buildLogSize,
                              buildLog,
                              NULL);
        snprintf(buf, sizeof(buf), "Error building OpenCL program:%s for device:%s",
                 programName.c_str(), clDeviceKind()->kind.c_str());
        systemLog(buf);
        systemLog(buildLog);
        
        free(buildLog);
    }
    clReleaseProgram(program);
    program = NULL;
}


void ClProgram::saveBinariesForDeviceIDs(cl_device_id * deviceIDs)
{
    cl_int errNum = 0;
    cl_uint numDevices = (cl_uint)clDeviceKind()-> numDevices();
    
    // this always returns the number of devices in the entire context - NOT the subset for which the program is compiled !!!
    errNum = clGetProgramInfo(program, CL_PROGRAM_NUM_DEVICES, sizeof(cl_uint), &numDevices, NULL);
    
    cl_device_id *devices = (cl_device_id *)malloc(sizeof(cl_device_id) *numDevices);
    errNum = clGetProgramInfo(program, CL_PROGRAM_DEVICES, sizeof(cl_device_id) *numDevices, devices, NULL);
    
    size_t *binarySizes = (size_t *)malloc(sizeof(size_t) *numDevices);
    errNum = clGetProgramInfo(program, CL_PROGRAM_BINARY_SIZES, sizeof(size_t) *numDevices, binarySizes, NULL);
    
    const char **binaries = (const char **)malloc(sizeof(unsigned char *)*numDevices);
    for (int i = 0; i < numDevices; i++)
        binaries[i] = (const char *)malloc(binarySizes[i]);
    errNum = clGetProgramInfo(program, CL_PROGRAM_BINARIES, sizeof(unsigned char *)*numDevices, binaries, NULL);
    
    // find the binaries for the device we just built for
    for (int i = 0; i < numDevices; i++) {
        for (int j = 0; j < numDevices; j++) {
            if (deviceIDs[j] == devices[i]) {
                writeBinaryForUuid(uuid, binaries[i], binarySizes[i]);
                break;
            }
        }
    }
    
    for (int i = 0; i < numDevices; i++)
        free((void *)binaries[i]);
    free(binaries);
    free(binarySizes);
    free(devices);
}

void ClProgram::saveBinaryForDeviceID(cl_device_id deviceID)
{
    size_t binarySize;
    clGetProgramInfo(program, CL_PROGRAM_BINARY_SIZES, sizeof(size_t), &binarySize, NULL);
    
    const char *binary = (const char *)malloc(binarySize);
    clGetProgramInfo(program, CL_PROGRAM_BINARIES, sizeof(unsigned char **), &binary, NULL);
    
    writeBinaryForUuid(uuid, binary, binarySize);
}

void ClProgram::makeKernelsForDeviceIDs(cl_device_id * deviceIDs)
{
    cl_int errNum          = 0;
    iteratePointsKernal      = clCreateKernel(program, "iteratePointsKernal", &errNum);
    setBufferKernal          = clCreateKernel(program, "setBufferKernal", &errNum);
    reductionKernal          = clCreateKernel(program, "reductionKernal", &errNum);
    postProcessStep1Kernal   = clCreateKernel(program, "postProcessStep1Kernal", &errNum);
    postProcessStep2Kernal   = clCreateKernel(program, "postProcessStep2Kernal", &errNum);
    colorCurveRGBChannelKernal = clCreateKernel(program, "colorCurveRGBChannelKernal", &errNum);
    colorCurveRGB3ChannelsKernal = clCreateKernel(program, "colorCurveRGB3ChannelsKernal", &errNum);
    FlexibleDensityEstimationKernal  = clCreateKernel(program, "FlexibleDensityEstimationKernal", &errNum);
    
    RGBA128FtoRGBA32UKernal  = clCreateKernel(program, "RGBA128FtoRGBA32UKernal", &errNum);
    RGBA128FtoBGRA32UKernal  = clCreateKernel(program, "RGBA128FtoBGRA32UKernal", &errNum);
    RGBA128FtoRGBA64UKernal  = clCreateKernel(program, "RGBA128FtoRGBA64UKernal", &errNum);
    RGBA128FtoRGBA128FKernal = clCreateKernel(program, "RGBA128FtoRGBA128FKernal", &errNum);
    MergeKernal              = clCreateKernel(program, "MergeKernal", &errNum);
    readChannelKernal        = clCreateKernel(program, "readChannelKernel", &errNum);
    writeChannelKernal       = clCreateKernel(program, "writeChannelKernel", &errNum);
    writeChannelStripedKernal= clCreateKernel(program, "writeChannelStripedKernel", &errNum);
    convolveRowKernal        = clCreateKernel(program, "convolutionRowsKernel", &errNum);
    convolveColKernal        = clCreateKernel(program, "convolutionColumnsKernel", &errNum);
    for (int i = 0; i < clDeviceKind()-> numDevices(); i++) {
        size_t DK_mwgs = clDeviceKind()->maxWorkGroupSize;
        size_t mwgs = 0UL;
        mwgs = getMaxWorkGroupSizeForKernel(iteratePointsKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(setBufferKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(reductionKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(postProcessStep1Kernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(postProcessStep2Kernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(colorCurveRGB3ChannelsKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(colorCurveRGBChannelKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(FlexibleDensityEstimationKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(RGBA128FtoRGBA32UKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(RGBA128FtoBGRA32UKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(RGBA128FtoRGBA64UKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(RGBA128FtoRGBA128FKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(MergeKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(readChannelKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(writeChannelKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(convolveRowKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        mwgs = getMaxWorkGroupSizeForKernel(convolveColKernal, deviceIDs[i]);
        DK_mwgs = mwgs < DK_mwgs ? mwgs : DK_mwgs;
        maxWorkGroupSize = DK_mwgs;
    }
}


#ifdef __ORIGINAL__


- (id)initFromBitcodeWithContext:(ClContext *)ctx deviceKind:(ClDeviceKind *)_deviceKind
{
    self = [super initFromBitcodeWithContext:ctx deviceKind:_deviceKind];
    if (self) {
        program = NULL;
        iteratePointsKernal = NULL;
        setBufferKernal = NULL;
        reductionKernal = NULL;
        postProcessStep1Kernal = NULL;
        postProcessStep2Kernal = NULL;
        colorCurveRGB3ChannelsKernal = NULL;
        colorCurveRGBChannelKernal = NULL;
        FlexibleDensityEstimationKernal = NULL;
        RGBA128FtoRGBA32UKernal = NULL;
        RGBA128FtoBGRA32UKernal = NULL;
        RGBA128FtoRGBA64UKernal = NULL;
        RGBA128FtoRGBA128FKernal = NULL;
        MergeKernal = NULL;
        readChannelKernal = NULL;
        writeChannelKernal = NULL;
        writeChannelStripedKernal = NULL;
        convolveRowKernal = NULL;
        convolveColKernal = NULL;
        
        if (! context)
            return nil;
        
        cl_int errNum = 0;
        cl_device_id *deviceIDs    =  (cl_device_id *)[[deviceKind deviceIDs] bytes];
        
        NSString *bitcodeBasename;
        uint warpSize = [deviceKind determineWarpSizeForDevice:deviceIDs[0]];
        if ([deviceKind isCPU]) {
            [self setBasename:@"Flam4_KernalCPU"];
            if (sizeof(int) == 4)
                bitcodeBasename = [deviceKind deviceImageSupport] ? @"Flam4_KernalCPU.image.cpu32" : @"Flam4_KernalCPU.cpu32";
                else
                    bitcodeBasename = [deviceKind deviceImageSupport] ? @"Flam4_KernalCPU.image.cpu64" : @"Flam4_KernalCPU.cpu64";
                    }
        else {
            [self setBasename:@"Flam4_Kernal"];
            switch (warpSize) {
                case 16:
                    bitcodeBasename = [deviceKind deviceImageSupport] ? @"Flam4_Kernal.image.w16.gpu32" : @"Flam4_Kernal.w16.gpu32";
                    break;
                case 32:
                default:
                    bitcodeBasename = [deviceKind deviceImageSupport] ? @"Flam4_Kernal.image.w32.gpu32" : @"Flam4_Kernal.w32.gpu32";
                    break;
                case 64:
                    bitcodeBasename = [deviceKind deviceImageSupport] ? @"Flam4_Kernal.image.w64.gpu32" : @"Flam4_Kernal.w64.gpu32";
                    break;
            }
        }
        NSString *bitcodePath = [[NSBundle mainBundle] pathForResource:bitcodeBasename ofType:@"bc"];
        [self setUuid:[deviceKind processDeviceKind]];
        NSData   *binaryContents   = [self binaryContentsForUuid:uuid];
        
        
        bool outOfDate    = NO;
        NSError *error    = nil;
        NSFileManager *fm = [NSFileManager defaultManager];
        NSDictionary *binaryAttributes = [fm attributesOfItemAtPath:[self pathForBinaryForUuid:uuid] error:&error];
        if (! error) {
            NSDictionary *sourceAttributes = [fm attributesOfItemAtPath:bitcodePath error:&error];
            NSDate *binaryModDate  = [binaryAttributes fileModificationDate];
            NSDate *bitcodeModDate = [sourceAttributes fileModificationDate];
            outOfDate              = [binaryModDate compare:bitcodeModDate] == NSOrderedAscending;
        }
        
        if (!binaryContents)
            [deviceKind setRebuild:YES];
        else if (outOfDate)
            [deviceKind setRebuild:YES];
        
        // build from prebuilt binaries
        if (![deviceKind rebuild])
            program = [self loadProgramFromBinaryContents:binaryContents deviceIDs:deviceIDs];
            else
                program = [self loadProgramFromBitcode:bitcodePath deviceIDs:deviceIDs];
                
                if (program == NULL) {
                    [context release];
                    [basename release];
                    return nil;
                }
        
        NSString *includeDirective = @"";
        // Build program from either source or binaries
        errNum = clBuildProgram(program,
                                (cl_uint)[deviceKind numDevices],
                                deviceIDs,
                                [includeDirective UTF8String],
                                NULL,
                                NULL);
        if (errNum != CL_SUCCESS)
        {
            [self logErrStatusForProgramName:bitcodePath deviceIDs:deviceIDs];
            [context release];
            [basename release];
            return nil;
        }
        // if built from source
        else if ([deviceKind rebuild]) {
            [self saveBinariesForDeviceIDs:deviceIDs];
        }
        [self makeKernelsForDeviceIDs:deviceIDs];
    }
    return self;
}

#endif
