//
//  FlameTile.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/15/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "FlameTile.hpp"
#include "Flame.hpp"
#include "FlameExpanded.hpp"
#include "DeviceContext.hpp"
#include "DeviceRuntime.hpp"

FlameTile::FlameTile(SharedBlades & _blades,
                     uint _row,
                     uint _column,
                     float _width,
                     float _height,
                     bool transparent,
                     bool moreThanOneTile)
:
    row(_row),
    column(_column),
    width(_width),
    height(_height),
    backgroundImage(nullptr),
    numBatches(0),
    blades()
{
    // make deep copy of the blades - each tile needs its own flame blade instances
    SharedBlades newBlades = std::make_shared<FlamesVector>();
    
    newBlades->reserve(_blades->size());
    
    if (moreThanOneTile) {
        for (SharedFlameExpanded &fe : *_blades) {
            SharedFlameExpanded feCopy = std::make_shared<FlameExpanded>(*fe);
            if (feCopy->getFlame() && transparent)
                feCopy->getFlame()->params.background.a = 0.f;
            newBlades->push_back(feCopy);
        }
    }
    else {
        for (SharedFlameExpanded &fe : *_blades) {
            if (fe->getFlame() && transparent)
                fe->getFlame()->params.background.a = 0.f;
            newBlades->push_back(fe);
        }
    }
    blades = std::move(newBlades);
}

FlameTile::~FlameTile()
{
    delete backgroundImage;
}

float FlameTile::overlap()
{
    return 8.f;
}

std::string FlameTile::description()
{
    static char buf[100];
    snprintf(buf, sizeof(buf), "%s at:[%u X %u] area:[%s X %s]", "FlameTile", row, column,
            prettyFloat(width).c_str(), prettyFloat(height).c_str());
    return buf;
}

void FlameTile::allocateBatches(size_t batchesDesired, SharedBlades & blades, float width, float height,
                     const SharedDeviceContext & deviceContext)
{
    SharedFlameExpanded & firstFe = blades->operator[](0);
    
    size_t bladeCount    = blades->size();
    size_t actualBatches = batchesDesired;
    
    if (batchesDesired <= bladeCount) {
        for (SharedFlameExpanded &  fe : *blades) {
            fe->getFlame()->params.numBatches = 1.f;
        }
        actualBatches = bladeCount;
    }
    else  { // batchesDesired > bladeCount
        size_t batchesPerBlade = floorf((float)batchesDesired/bladeCount);
        float residual             = batchesDesired - bladeCount * batchesPerBlade;
        size_t total           = 0;
        for (SharedFlameExpanded &  fe : *blades) {
            fe->getFlame()->params.numBatches = batchesPerBlade;
            total += batchesPerBlade;
        }
        if (residual > 0.f) {
            for (SharedFlameExpanded &  fe : *blades) {
                fe->getFlame()->params.numBatches += 1.f;
                if (++total >= batchesDesired)
                    break;
            }
        }
        actualBatches = total;
    }
    uint perPointFuseIterations = deviceContext->maxFuseIterationsFromSelectedDevices(firstFe->getFlame()->params.numFinal > 0);
    float actualQuality = DeviceRuntime::actualQualityWith(actualBatches, width, height, 1, perPointFuseIterations);
    for (SharedFlameExpanded &  fe : *blades) {
        fe->getFlame()->params.desiredQuality = floorf(actualQuality);
        fe->getFlame()->params.quality        = floorf(actualQuality);
    }
}

void FlameTile::allocateBatchesToBlades(size_t batchesDesired, const SharedDeviceContext & deviceContext)
{
    numBatches = batchesDesired;
    FlameTile::allocateBatches(batchesDesired, blades, width, height, deviceContext);
}

std::string FlameTile::bladeBatchStatistics(SharedBlades & blades)
{
    uint sum = 0;
    uint min = 0;
    uint max = 0;
    size_t count = blades->size();
    for (SharedFlameExpanded &  fe : *blades) {
        uint _numBatches = fe->getFlame()->params.numBatches;
        sum += _numBatches;
        max  = max < _numBatches ? _numBatches : max;
        min  = min > _numBatches ? _numBatches : min;
    }
    static char buf[80];
    snprintf(buf, sizeof(buf), "BladeBatches:bladeCount:%zu min:%u max:%u mean:%f totalBatches:%u", (size_t)count, min, max, (float)sum/count, sum);
    return buf;
}

std::string FlameTile::bladeBatchStatistics()
{
    return FlameTile::bladeBatchStatistics(blades);
}
