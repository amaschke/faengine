//
//  XaosMatrix.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/5/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef XaosMatrix_hpp
#define XaosMatrix_hpp

#include "Exports.hpp"
#include "XaosMatrixVector.hpp"

class XaosForXformsVector;

class FAENGINE_API XaosMatrix {
    
    XaosForXformsVector *vector;
    
public:
    XaosMatrix(size_t xformCount);
    XaosMatrix(const XaosMatrix &other);
    XaosMatrix(const XaosForXformsVector &matrix);
    
    bool operator==(const XaosMatrix &xaosMatrix);
    
    XaosForXformsVector *matrix() const;
    size_t xformCount() const;
    
    void blendXaosFrom(const XaosMatrix *_from, const XaosMatrix *_to, XaosForXformsVector *target, double amount);
    
    float chaosFrom(unsigned from, unsigned to) const;
    bool lockedFrom(unsigned from, unsigned to) const;
    void setChaosWeight(float weight, unsigned from, unsigned to);
    void setLocked(bool locked, unsigned from, unsigned to);
};

class FAENGINE_API XaosWeight {
    int   from;
    int   to;
    XaosMatrix &xaosMatrix;
public:
    
    int fromIndex() const;
    int toIndex() const;
    float chaosWeight() const;
    bool locked() const;
    
    XaosWeight(XaosMatrix &_xaosMatrix, int _from, int _to) : xaosMatrix(_xaosMatrix), from(_from), to(_to) {}
    
    void setChaosWeight(float _weight, unsigned from, unsigned to);
    void setLocked(bool _locked, unsigned from, unsigned to);
};

#endif /* XaosMatrix_hpp */
